@extends('layouts.front')

@section('content')

  <section class="login-signup">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-9">
          <h2 class="retailer-title">Retailer Register</h2>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-reg" role="tabpanel" aria-labelledby="nav-reg-tab">
              <div class="login-area signup-area">
                <div class="header-area pb-4">
                  <h4 class="title">NEW CUSTOMER</h4>
                </div>
                <div class="login-form signup-form">
                  @include('includes.admin.form-login')
                  <form class="mregisterform" action="{{route('user-register-submit')}}" method="POST">
                        {{ csrf_field() }}
                      <div class="row">
                          <div class="col-md-6">
                            <div class="form-input">
                              <input type="text" class="User Name" name="name" placeholder="{{ $langg->lang182 }}" required="">
                              <i class="icofont-user-alt-5"></i>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-input">
                              <input type="email" class="User Name" name="email" placeholder="{{ $langg->lang183 }}" required="">
                              <i class="icofont-email"></i>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-input">
                              <input type="text" class="User Name" name="shop_name" placeholder="Business Name" required="">
                              <i class="icofont-cart"></i>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-input">
                              <input type="text" class="User Name" name="web_address" placeholder="Web Address" required="">
                              <i class="icofont-opencart"></i>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-input">
                                <input type="password" class="Password" name="password" placeholder="{{ $langg->lang186 }}"
                                  required="">
                                <i class="icofont-ui-password"></i>
                              </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-input">
                              <input type="password" class="Password" name="password_confirmation"
                                placeholder="{{ $langg->lang187 }}" required="">
                              <i class="icofont-ui-password"></i>
                            </div>
                          </div>
                      </div>  

                      <div class="header-area pt-5 pb-4">
                        <h4 class="title">Additional Information</h4>
                      </div>

                      <div class="row">
                          <div class="col-md-6">
                            <div class="form-input">
                              <span>Estimated monthly accessory spending?</span>
                              <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <option value="1">$200 - $1,000</option>
                                  <option value="2">$1,001 - $2,500</option>
                                  <option value="3">$2,501 - $5,000</option>
                                  <option value="4">$5,001 - $7,500</option>
                                  <option value="5">$7,501 - $10,000</option>
                                  <option value="6">$10,001 - $15,000</option>
                                  <option value="7">$15,000 - $20,000</option>
                                  <option value="8">$20,001 - $30,000</option>
                                  <option value="9">$30,001 - $50,000</option>
                                  <option value="10">$50,001 - $75,000</option>
                                  <option value="11">$75,000+</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-input">
                                  <span>How did you hear about us?</span>
                                  <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButtondata-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <option value="1">Email</option>
                                      <option value="2">Event / Festival</option>
                                      <option value="3">Family / Friend</option>
                                      <option value="4">Sales Rep</option>
                                      <option value="5">Internet Search</option>
                                      <option value="6">Magazine Ad</option>
                                      <option value="7">Facebook Ad</option>  
                                      <option value="8">Social Media</option>
                                      <option value="9">Other</option>
                                  </select>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-input">
                                <span>Sales Rep :</span>
                                <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <option value="21995415">Anton Silotang</option>
                                    <option value="4059596">Bryan Martinez</option>
                                    <option value="7">Drew Batta</option>
                                    <option value="16685330">Eddie Wittner</option>
                                    <option value="7049632">Henry Robles</option>
                                    <option value="22452354">Jeremey Roberts</option>
                                    <option value="-5">Rohit Batta</option>
                                    <option value="22139811">Sandy Mawikere</option>
                                    <option value="9">Sean Batta</option>
                                    <option value="22503872">Will Ompi</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-input">
                              <span>Business Type :</span>
                              <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <option value="5">Carrier Stores</option>
                                  <option value="19">Flea Market / Swap Meet / Trade Shows</option>
                                  <option value="6">International</option>
                                  <option value="4">Online Seller (ex: Amazon, eBay, ecommerce website)</option>
                                  <option value="17">Other</option>
                                  <option value="1">Repair Shops &amp; Accessories</option>
                                  <option value="2">Retailer (ex: Mall Cart, Kiosk, Storefront)</option>
                                  <option value="3">Wholesale and Distribution</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-input">
                              <span>Carriers</span>
                              <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <option value="1">AT&amp;T</option>
                                  <option value="7">Boost Mobile</option>
                                  <option value="6">Cricket</option>
                                  <option value="5">Metro by T-Mobile</option>
                                  <option value="11">Others</option>
                                  <option value="8">Sprint</option>
                                  <option value="4">StraightTalk</option>
                                  <option value="3">T-Mobile</option>
                                  <option value="13">TotalWireless</option>
                                  <option value="12">TracFone</option>
                                  <option value="9">US Cellular</option>
                                  <option value="2">Verizon</option>
                                  <option value="10">Virgin Mobile</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 pad">
                            <div class="form-input">
                                <input type="text" class="User Name" name="number_of_locations" placeholder="Number of locations?" required="">
                                <i class="icofont-ui-cart"></i>
                            </div>
                          </div>
                      </div>

                      <div class="header-area pt-5 pb-4">
                        <h4 class="title">Billing Information</h4>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-input">
                              <input type="text" class="User Name" name="address" placeholder="Shipping Address" required="">
                              <i class="icofont-location-pin"></i>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-input">
                              <input type="text" class="User Name" name="shop_address" placeholder="Business Address" required="">
                              <i class="icofont-opencart"></i>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-input">
                            <input type="text" class="User Name" name="phone" placeholder="Contact number" required="">
                            <i class="icofont-phone"></i>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-input">
                            <input type="text" class="User Name" name="shop_number" placeholder="Business Number" required="">
                            <i class="icofont-shopping-cart"></i>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-input">
                            <input type="text" class="User Name" name="city" placeholder="City" required="">
                            <i class="icofont-ui-cart"></i>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-input">
                            <input type="text" class="User Name" name="zip" placeholder="ZIP" required="">
                            <i class="icofont-ui-cart"></i>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-input">
                            <span>Country :</span>
                            <select class="btn dropdown-toggle User Name" name="country" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <option value="">{{ $langg->lang157 }}</option>
                                @foreach (DB::table('countries')->get() as $data)
                                    <option value="{{ $data->country_name }}" >
                                        {{ $data->country_name }}
                                    </option>   
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="file-upload-area form-input">
                            <div class="upload-file">
                                <span class="User Name">Personal ID :</span>
                                <input type="file" name="personal_id" class="upload">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="file-upload-area form-input">
                            <div class="upload-file">
                                <span class="User Name">Tax ID :</span>
                                <input type="file" name="tax_id" class="upload">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            @if($gs->is_capcha == 1)
                              <ul class="captcha-area">
                                <li>
                                  <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i
                                      class="fas fa-sync-alt pointer refresh_code "></i></p>
                                </li>
                              </ul>
                        </div>
                        <div class="col-md-6">
                            <div class="form-input">
                                <input type="text" class="Password" name="codes" placeholder="{{ $langg->lang51 }}" required="">
                                <i class="icofont-refresh"></i>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-8 offset-md-2 text-center">
                            <input class="mprocessdata" type="hidden" value="{{ $langg->lang188 }}">
                            <button type="submit" class="submit-btn">{{ $langg->lang189 }}</button>
                        </div>
                      </div>
                      <input type="hidden" name="vendor"  value="1">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection