@extends('layouts.front')
@section('content')      

<section class="user-dashbord">
    <div class="container">
      <div class="row">
        @include('includes.user-dashboard-sidebar')
        <div class="col-lg-8">
            <div class="user-profile-details">
                <div class="account-info">
                    <div class="header-area">
                        <h4 class="title">
                          Subscriptions
                        </h4>
                    </div>
                    <div class="edit-info-area">
                        <div class="body">
                            <div class="edit-info-area-form">
                                <div class="gocover"
                                    style="background: url({{ asset('assets/images/'.$gs->loader) }}) no-repeat scroll center center rgba(45, 45, 45, 0.5);">
                                </div>
                                @if (count($orders) > 0)                                
                                <form id="userform" action="{{route('user-profile-subscribtion')}}" method="post">
                                    {{ csrf_field() }}
                                  @foreach($orders as $key => $order)
                                    @php                                         
                                         $cart = unserialize(bzdecompress(utf8_decode($order->cart)));
                                         $i = 0;
                                     @endphp                                     
                                        @foreach($cart->items as $key => $product)
                                         @php
                                            $order_durations = explode(',', $order->order_duration);
                                             // $prod_key = $$i;
                                            // $orderDuration = App\Models\OrderDuration::with('duration')->where(['user_id'=> $user->id, 'product_id' =>$product['item']['id'] ])->first();
                                            // $orderDuration = App\Models\OrderDuration::with('duration')->whereIn('id',$order_durations)->get();
                                            $orderDuration = App\Models\OrderDuration::with('duration')->where(function($q) use ($order_durations, $i){
                                                    // foreach($order_durations as  $value){
                                                        $q->where('id', $order_durations[$i]);
                                                    // }
                                                })->first();
                                         @endphp                                       
                                        <div class="row">
                                            <div class="col-lg-6">
                                                @if(!empty($product['keys']))
                                                @php
                                                $keys = explode(',', $product['keys']);
                                                $values = explode('|-', $product['values']);
                                                @endphp
                                                @foreach($keys as $k => $key)
                                                <strong>{{strtoupper(str_replace('_', ' ', $key))}}:</strong> 
                                                @if($k == 1 || $k == 2 || $k == 3)
                                                @php
                                                $data = json_decode($values[$k]);
                                                @endphp
                                                @foreach($data as $i => $d)
                                                {{$d->title}}@if(($i+1) != sizeof($data)),@endif
                                                @endforeach
                                                @else
                                                {{$values[$k]}}
                                                @endif
                                                <br>
                                                @endforeach
                                                <hr>
                                                @endif
                                            </div> 
                                            <div class="col-lg-6">                                               
                                                <label for="">Subscription:</label>
                                              
                                                <select class="mb-3 rounded-0 form-control" name="duration_id[]">
                                                <option value="" selected="" hidden="">Select</option>

                                                 @foreach ($allDurations as $key => $duration)  
                                                    <option value="{{$duration->id }}" {{ $orderDuration->duration_id == $duration->id ? 'selected' : ''}}>
                                                        {{ $duration->title }}
                                                    </option>       
                                                 @endforeach
                                            </select>
                                            {{-- <a class="btn btn-sm btn-danger text-white" href="{{ route('user-subscribtion-cancel',['duration_id' => $orderDuration->id, 'order_id' => $orderDuration->order_id]) }}">Cancel</a> --}}
                                            <input type="hidden" name="product_id" value="{{$product['item']['id']}}">
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            <input type="hidden" name="order_number" value="{{ $order->order_number }}">
                                            <input type="hidden" name="customer_email" value="{{ $order->customer_email }}">
                                            <input type="hidden" name="order_duration_id[]" value="{{  $orderDuration->id }}">
                                            </div>
                                        </div>
                                        @php
                                        $i++;                                            
                                        @endphp
                                    @endforeach
                                    @endforeach
                                    <div class="form-links">
                                        <button class="submit-btn" type="submit">{{ $langg->lang271 }}</button>
                                    </div>
                                </form>
                                @else
                                <div class="row">
                                <div class="col-md-12">
                                <div class="text-center">
                                    <p>No Subscription</p>
                                    
                                </div>
                                </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>

@endsection