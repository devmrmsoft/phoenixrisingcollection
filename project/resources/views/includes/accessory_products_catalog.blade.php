@php
use App\Models\Subcategory;
$filterBrands = DB::table('categories')->where('status',1)->get();

@endphp
        <div class="col-lg-3 col-md-6">
          <div class="left-area">
            @if ((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) && !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) && !empty(json_decode($childcat->attributes, true))))
            <div class="tags-area">
              <div class="header-area">
                <h4 class="title">
                    Filters
                </h4>
              </div>
              <     div class="body-area">
                <form id="attrForm" action="{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">
                  <ul class="filter">
                    <div class="single-filter">
                      @if (!empty($cat) && !empty(json_decode($cat->attributes, true)))
                        @foreach ($cat->attributes as $key => $attr)
                          <div class="my-2 sub-title">
                            <span><i class="fas fa-arrow-alt-circle-right"></i> {{$attr->name}}</span>
                          </div>
                          @if (!empty($attr->attribute_options))
                            @foreach ($attr->attribute_options as $key => $option)
                              <div class="form-check ml-0 pl-0">
                                <input name="{{$attr->input_name}}[]" class="form-check-input attribute-input" type="checkbox" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}">
                                <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                              </div>
                            @endforeach
                          @endif
                        @endforeach
                      @endif


                      <div class="my-2 sub-title">
                        <span><i class="fas fa-arrow-alt-circle-right"></i> Brands</span>
                      </div>
                      @isset($cat->id)
                        @php 
                          $getCatModels   = Subcategory::where('category_id',$cat->id)->get();
                        @endphp

                        @foreach($getCatModels as $filterBrand)
                        <div class="form-check ml-0 pl-0">
                          <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-{{ $filterBrand->id }}" value="{{ $filterBrand->id }}" onchange='brandFilterChange(this);'>
                          <label class="form-check-label" for="filter_brands-{{ $filterBrand->id }}">{{$filterBrand->name}}</label>
                        </div>
                        @endforeach
                      @endisset
                      <div class="my-2 sub-title">
                        <span><i class="fas fa-arrow-alt-circle-right"></i> Model</span>
                      </div>
                      <div class="form-check ml-0 pl-0 " id="load-models">
                      </div>
                      @if (!empty($subcat) && !empty(json_decode($subcat->attributes, true)))
                        @foreach ($subcat->attributes as $key => $attr)
                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> {{$attr->name}}</span>
                        </div>
                          @if (!empty($attr->attribute_options))
                            @foreach ($attr->attribute_options as $key => $option)
                              <div class="form-check  ml-0 pl-0">
                                <input name="{{$attr->input_name}}[]" on class="form-check-input attribute-input" type="checkbox" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}">
                                <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                              </div>
                            @endforeach
                          @endif
                        @endforeach
                      @endif

                      @if (!empty($childcat) && !empty(json_decode($childcat->attributes, true)))
                        @foreach ($childcat->attributes as $key => $attr)
                        <div class="my-2 sub-title">
                          <span><i class="fas fa-arrow-alt-circle-right"></i> {{$attr->name}}</span>
                        </div>
                          @if (!empty($attr->attribute_options))
                            @foreach ($attr->attribute_options as $key => $option)
                              <div class="form-check  ml-0 pl-0">
                                <input name="{{$attr->input_name}}[]" class="form-check-input attribute-input" type="checkbox" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}">
                                <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                              </div>
                            @endforeach
                          @endif
                        @endforeach
                      @endif
                    </div>
                  </ul>
                </form>
              </div>
            </div>
            @else

            <div class="tags-area">
              <div class="header-area">
                <h4 class="title">
                    Filters
                </h4>
              </div>
              <div class="body-area">
                <form id="attrForm" action="{{route('front.devices', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">
                  <ul class="filter">
                    <div class="single-filter">

                      <div class="my-2 sub-title">
                        <span><i class="fas fa-arrow-alt-circle-right"></i> Brands</span>
                      </div>
                      @isset($deviceCats)

                        @foreach($deviceCats as $filterBrand)
                        <div class="form-check ml-0 pl-0">
                          <input name="filter_brands[]" class="form-check-input attribute-input filter_brands" type="checkbox" id="filter_brands-{{ $filterBrand->id }}" value="{{ $filterBrand->id }}" onchange='brandFilterChange(this);'>
                          <label class="form-check-label" for="filter_brands-{{ $filterBrand->id }}">{{$filterBrand->name}}</label>
                        </div>
                        @endforeach
                      @endisset
                      <div class="my-2 sub-title" id="load-models-after">
                        <span><i class="fas fa-arrow-alt-circle-right"></i> Model</span>
                      </div>
                      <div id="load-models">
                      </div>

                      <div class="my-2 sub-title">
                        <span><i class="fas fa-arrow-alt-circle-right"></i> Carriers</span>
                      </div>
                      @isset($deviceCarriers)

                        @foreach($deviceCarriers as $filterBrand)
                        <div class="form-check ml-0 pl-0">
                          <input name="filter_device_carriers[]" class="form-check-input attribute-input filter_device_carriers" type="checkbox" id="filter_device_carriers-{{ $filterBrand->id }}" value="{{ $filterBrand->id }}">
                          <label class="form-check-label" for="filter_device_carriers-{{ $filterBrand->id }}">{{$filterBrand->name}}</label>
                        </div>
                        @endforeach
                      @endisset

                      <div class="my-2 sub-title">
                        <span><i class="fas fa-arrow-alt-circle-right"></i> Storage</span>
                      </div>
                      <?php 
                      $storage  = 8;
                      for ($i=0; $i < 7; $i++) { 

                        ?>
                        <div class="form-check ml-0 pl-0">
                          <input name="filter_device_storage[]" class="form-check-input attribute-input filter_device_storage" type="checkbox" id="filter_device_storage-{{ $storage }}" value="{{ $storage }}">
                          <label class="form-check-label" for="filter_device_storage-{{ $storage }}">{{ $storage }} GB</label>
                        </div>
                        <?php
                        $storage  +=  $storage;
                      }
                      ?>

                      <div class="my-2 sub-title">
                        <span><i class="fas fa-arrow-alt-circle-right"></i>Phone Color</span>
                      </div>
                      <?php 
                      $colors  = [0 => 'Red', 1 => 'Blue', 2 => 'Black', 3 => 'Silver'];
                      for ($i=0; $i < count($colors); $i++) {

                        ?>
                        <div class="form-check ml-0 pl-0">
                          <input name="filter_device_color[]" class="form-check-input attribute-input" type="checkbox" id="filter_device_color-{{ $i }}" value="{{ $colors[$i] }}">
                          <label class="form-check-label" for="filter_device_color-{{ $i }}">{{ $colors[$i] }}</label>
                        </div>
                        <?php
                        $storage  +=  $storage;
                      }
                      ?>

                      <div class="my-2 sub-title">
                        <span><i class="fas fa-arrow-alt-circle-right"></i>Phone Conditions</span>
                      </div>
                      <?php 
                      $conditions  = [1 => 'A', 2 => 'A-', 3 => 'B', 4 => 'C', 5 => 'A/B'];
                      for ($i=1; $i <= count($conditions); $i++) {
                        ?>
                        <div class="form-check ml-0 pl-0">
                          <input name="filter_device_conditions[]" class="form-check-input attribute-input" type="checkbox" id="filter_device_conditions-{{ $i }}" value="{{ $i }}">
                          <label class="form-check-label" for="filter_device_conditions-{{ $i }}">{{ $conditions[$i] }}</label>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                  </ul>
                </form>
              </div>
            </div>
            @endif
            
            <div class="filter-result-area">
              <div class="header-area">
                <h4 class="title">
                  {{$langg->lang61}}
                </h4>
              </div>
              <div class="body-area">
                <form id="catalogForm" action="{{route('front.getCatModelpro', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')]) }}" method="GET">
                  @if (!empty(request()->input('search')))
                    <input type="hidden" name="search" value="{{ request()->input('search') }}">
                  @endif
                  @if (!empty(request()->input('sort')))
                    <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
                  @endif
                  {{--
                  <ul class="filter-list">
                    @foreach ($categories as $element)
                    <li>
                      <div class="content">
                          <a href="{{route('front.category', $element->slug)}}{{!empty(request()->input('search')) ? '?search='.request()->input('search') : ''}}" class="category-link"> <i class="fas fa-angle-double-right"></i> {{$element->name}}</a>
                          @if(!empty($cat) && $cat->id == $element->id && !empty($cat->subs))
                              @foreach ($cat->subs as $key => $subelement)
                              <div class="sub-content open">
                                <a href="{{route('front.category', [$cat->slug, $subelement->slug])}}{{!empty(request()->input('search')) ? '?search='.request()->input('search') : ''}}" class="subcategory-link"><i class="fas fa-angle-right"></i>{{$subelement->name}}</a>
                                @if(!empty($subcat) && $subcat->id == $subelement->id && !empty($subcat->childs))
                                  @foreach ($subcat->childs as $key => $childcat)
                                  <div class="child-content open">
                                    <a href="{{route('front.category', [$cat->slug, $subcat->slug, $childcat->slug])}}{{!empty(request()->input('search')) ? '?search='.request()->input('search') : ''}}" class="subcategory-link"><i class="fas fa-caret-right"></i> {{$childcat->name}}</a>
                                  </div>
                                  @endforeach
                                @endif
                              </div>
                              @endforeach

                            </div>
                          @endif


                    </li>
                    @endforeach

                  </ul>
                  --}}

                    <div class="price-range-block">
                      <div id="slider-range" class="price-filter-range" name="rangeInput"></div>
                      <div class="livecount">
                        <input type="number" min=0  name="min"  id="min_price" class="price-range-field" />
                        <span>{{$langg->lang62}}</span>
                        <input type="number" min=0  name="max" id="max_price" class="price-range-field" />
                      </div>
                    </div>

                    <button class="filter-btn" type="submit">{{$langg->lang58}}</button>
                </form>
              </div>
            </div>

            @if(!isset($vendor))

            {{-- <div class="tags-area">
                <div class="header-area">
                    <h4 class="title">
                        {{$langg->lang63}}
                    </h4>
                  </div>
                  <div class="body-area">
                    <ul class="taglist">
                      @foreach(App\Models\Product::showTags() as $tag)
                      @if(!empty($tag))
                      <li>
                        <a class="{{ isset($tags) ? ($tag == $tags ? 'active' : '') : ''}}" href="{{ route('front.tag',$tag) }}">
                            {{ $tag }}
                        </a>
                      </li>
                      @endif
                      @endforeach
                    </ul>
                  </div>
            </div> --}}


            @else

            <div class="service-center">
              <div class="header-area">
                <h4 class="title">
                    {{ $langg->lang227 }}
                </h4>
              </div>
              <div class="body-area">
                <ul class="list">
                  <li>
                      <a href="javascript:;" data-toggle="modal" data-target="{{ Auth::guard('web')->check() ? '#vendorform1' : '#comment-log-reg' }}">
                          <i class="icofont-email"></i> <span class="service-text">{{ $langg->lang228 }}</span>
                      </a>
                  </li>
                  <li>
                        <a href="tel:+{{$vendor->shop_number}}">
                          <i class="icofont-phone"></i> <span class="service-text">{{$vendor->shop_number}}</span>
                        </a>
                  </li>
                </ul>
              <!-- Modal -->
              </div>

              <div class="footer-area">
                <p class="title">
                  {{ $langg->lang229 }}
                </p>
                <ul class="list">


              @if($vendor->f_check != 0)
              <li><a href="{{$vendor->f_url}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
              @endif
              @if($vendor->g_check != 0)
              <li><a href="{{$vendor->g_url}}" target="_blank"><i class="fab fa-google"></i></a></li>
              @endif
              @if($vendor->t_check != 0)
              <li><a href="{{$vendor->t_url}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
              @endif
              @if($vendor->l_check != 0)
              <li><a href="{{$vendor->l_url}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
              @endif


                </ul>
              </div>
            </div>


            @endif


          </div>
        </div>