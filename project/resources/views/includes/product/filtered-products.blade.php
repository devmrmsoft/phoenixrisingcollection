			@if (count($prods) > 0)
					@foreach ($prods as $key => $prod)
									<div class="col-lg-4 col-md-4 col-6 remove-padding">
                                        <div class="productTab">
                                            <div class="productTab0">
                                            </div>
                                            <div class="productTab1">
                                                <a href="{{ route('front.product', $prod->slug) }}">{{ $prod->showName() }}</a>
                                            </div>
                                            <div class="productTab2">
                                                <a href="{{ route('front.product', $prod->slug) }}"><img src="{{ $prod->thumbnail ? asset('assets/images/thumbnails/'.$prod->thumbnail):asset('assets/images/noimage.png') }}"
                                                        class="img-fluid"></a>
                                            </div>
                                            <div class="productTab3">
                                                <h1>{{ $prod->showPrice($prod->id) }}</h1>
                                                <h2><del>{{ $prod->showPreviousPrice() }}</del></h2>
                                                <a tabindex="0" data-toggle="tooltip" title="Add to Cart!" class="add-to-cart add-to-cart-btn" data-href="{{ route('product.cart.add',$prod->id) }}"><i
                                                        class="fas fa-shopping-cart"></i></a>
                                            </div>
                                            <div class="productTab4">

											@if(Auth::guard('web')->check())
                                                <a href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" data-toggle="tooltip" data-placement="right" title="{{ $langg->lang54 }}" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
											@else
                                                <a href="javascript:;" rel-toggle="tooltip" title="{{ $langg->lang54 }}" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="right"><i class="far fa-heart"></i> Add to Wishlist</a>
                                            @endif
                                                <a href="javascript:;" class="add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}"  data-toggle="tooltip" data-placement="right" title="{{ $langg->lang57 }}" data-placement="right"><i class="fas fa-retweet"></i> Compare</a>
                                            </div>
                                        </div>
                                    </div>
				@endforeach
				<div class="col-lg-12">
					<div class="page-center mt-5">
						{!! $prods->appends(['search' => request()->input('search')])->links() !!}
					</div>
				</div>
			@else
				<div class="col-lg-12">
					<div class="page-center">
						 <h4 class="text-center">{{ $langg->lang60 }}</h4>
					</div>
				</div>
			@endif


@if(isset($ajax_check))


<script type="text/javascript">


// Tooltip Section


    $('[data-toggle="tooltip"]').tooltip({
      });
      $('[data-toggle="tooltip"]').on('click',function(){
          $(this).tooltip('hide');
      });




      $('[rel-toggle="tooltip"]').tooltip();

      $('[rel-toggle="tooltip"]').on('click',function(){
          $(this).tooltip('hide');
      });


// Tooltip Section Ends

</script>

@endif