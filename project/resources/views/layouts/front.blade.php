<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="{{ asset('assets/front/assets/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/assets/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/assets/css/style.responsive.css') }}">
    <link rel="icon" href="{{ asset('assets/front/assets/img/16x16.png') }}" type="image" sizes="16x16">
    <title>Phoenix Rising Collection</title>

</head>
<body>
@php
  $social = App\Models\Socialsetting::findOrFail(1);
@endphp
  @if(\Request::route()->getName() == 'front.index')





    <div class="preloader js-preloader flex-center" style="display: none;">

        <div class="spinner">

            <img src="{{ asset('assets/front/assets/img/preloader.gif') }}" class="img-fluid">

        </div>

    </div>





    @endif
    <!-- Preloader -->
    <header class="top-header">
        <div class="my-account">
            @if(!Auth::check())
            <a href="{{url('/login')}}">Login</a>
            <a href="{{url('/register')}}">Register</a>
            @else
            <h4>Welcome {{Auth::user()->name}}!</h4>
            <a href="{{route('user-dashboard')}}">My Account</a>
            <a href="{{url('/user/logout')}}">Logout</a>
            @endif
        </div>
        <p>TRY OUR NEW CUSTOM BODY oil + hair oil</p>
        <div class="my-account">
            <a href="{{url('/carts')}}">Cart</a>
        </div>
    </header>
    
    <!-- Top Header -->

    <header class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 hide-navigation navigation-column d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link">home</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/about')}}" class="nav-link">About</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/buildown')}}" class="nav-link">Build Your own</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link">
                                <img src="{{ asset('assets/front/assets/img/Phoenix-logo.png') }}" alt="Website Logo" class="img-fluid">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/shop')}}" class="nav-link">shop</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/blog')}}" class="nav-link">blog</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/contact')}}" class="nav-link">Contact</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 mobile-nav-column d-md-block d-lg-none navigation-column d-flex justify-content-center align-items-center">
                    <div class="mobile-nav-logo">
                        <a href="{{url('/')}}" class="nav-link">
                            <img src="{{ asset('assets/front/assets/img/Phoenix-logo.png') }}" alt="Website Logo" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 mobile-nav-column d-md-block d-lg-none navigation-column d-flex justify-content-center align-items-center mt-auto mb-auto">
                    <div id="mySidenav" class="sidenav">
                        <a href="{{url('/')}}" class="nav-link site-logo-mob">
                            <img src="{{ asset('assets/front/assets/img/Phoenix-logo.png') }}" alt="Website Logo" class="img-fluid">
                        </a>
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="{{url('/')}}" class="nav-link">home</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/about')}}" class="nav-link">About</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/buildown')}}" class="nav-link">Build Your own</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/shop')}}" class="nav-link">shop</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/blog')}}" class="nav-link">blog</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/contact')}}" class="nav-link">Contact</a>
                            </li>
                        </ul>
                    </div>
                    <span class="breadcrumb-navs" style="cursor:pointer" onclick="openNav()">
                        <svg preserveAspectRatio="none" viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
                            <g>
                                <rect class="bcrumb-top" x="5.016" y="9.541" fill="#000" width="21.969" height="2.176"></rect>
                                <rect class="bcrumb-middle" x="9.567" y="14.912" fill="#000" width="17.417" height="2.177"></rect>
                                <rect class="bcrumb-bottom" x="7.341" y="20.284" fill="#000" width="19.644" height="2.177"></rect>
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section ends -->

    @yield('content')
    <!-- footer-section starts -->
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 site-footer-column">
                    <div class="inner-footer-1">
                        <img src="{{ asset('assets/front/assets/img/footer-logo.png') }}" class="img-fluid">
                        <p>{{ $gs->footer }}</p>
                        <ul class="nav">
                           
                           @if($social->f_status == 1)
                                <li class="nav-item">
                                <a href="https://www.facebook.com/Phoenix-rising-collection-119021809940833" target="_blank" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                            </li>
                           @endif
                           
                            
                             @if($social->insta_status == 1)
                                <li class="nav-item">
                                <a href="https://www.instagram.com/phoenix_risingcollection/" target="_blank" class="nav-link"><i class="fab fa-instagram"></i></a>
                                </li>
                           @endif
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 site-footer-column d-flex justify-content-center">
                    <div class="inner-footer-2">
                        <h3>Locate Us</h3>
                        <p><a href="tel:{{ $gs->site_phone}}">{{ $gs->site_phone}}</a></p>
                        <p><a href="mailto:{{ $gs->site_email}}">{{ $gs->site_email}}</a></p>
                    </div>
                </div>
                <div class="col-md-3 site-footer-column d-flex justify-content-center">
                    <div class="inner-footer-3">
                        <h3>Profile</h3>
                        <ul class="nav flex-column">
                          
                             @if(!Auth::check())
            <li class="nav-item"><a class="nav-link" href="{{url('/login')}}">Login</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('/register')}}">Register</a></li>
            @else
            <li class="nav-item"><a class="nav-link" href="{{route('user-dashboard')}}">My Account</a></li>
             <li class="nav-item">
                                <a href="{{url('/carts')}}" class="nav-link">Checkout</a>
                            </li>
           <li class="nav-item"> <a class="nav-link" href="{{url('/user/logout')}}">Logout</a></li>
            @endif
                           
                            <li class="nav-item">
                                <a href="{{ route('privacy-policy')}}" class="nav-link">Privacy Policy</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('terms')}}" class="nav-link">Terms & Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 site-footer-column">
                    <div class="inner-footer-4">
                        <h3>Newsletter</h3>
                        <p>Subscribe to our newsletter</p>
                        <form action="" id="emailSubsForm">                 

                        <input type="email" name="subscribe_email" required="" id="subscribe_email" placeholder="Email">

                        <button type="submit" class="btn btn-sm subscribe-btn">Subscribe</button>

                        </form>
                    </div>
                    <div class="copyrights">
                        <p>{!! $gs->copyright !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer-section ends -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>


    <script src="{{ asset('assets/front/assets/js/jquery.preloadinator.min.js') }}"></script>
    <script src="{{ asset('assets/front/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/front/assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/front/assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/front/assets/js/main-slider.js') }}"></script>
    <script>
        new WOW().init();
        
        $(document).ready(function() {

    $("#emailSubsForm").validate({

        rules: {

            subscribe_email: {

                required: !0,

                email: !0

            }

        },

        highlight: function(e) {

            $(e).closest(".control-group").removeClass("success").addClass("error")

        },

        submitHandler: function(e) {

          $.ajax({

              url: "{{ route('front.subscribe')}}",

              type: "POST",

              headers: {

                'X-CSRF-Token': '{{ csrf_token() }}',

                },

              data: {

                email: $('input[name="subscribe_email"]').val()

              },

              success: function(data) {

               if(data.status == 0){

                                   alert(data.errors[0]);



                }

                else{

                alert(data.success);



                }

              },

              error: function() {

               alert('Something went wrong!!!!');                       

          }

         });

        }

    });

});
    </script>
    @yield('scripts')
</body>
</html>

    