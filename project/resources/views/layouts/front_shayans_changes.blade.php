<?php
use App\accessoryBrand as Brands;
use App\accessoryType as AType;
use App\Models\Category;
use App\Models\Subcategory as DeviceModel;

$brandsDropDown =   Brands::where('status',1)->get();

$devices = json_decode( DB::table('categories')->select('id','name','slug')->where('devices',1)->orderBy('id','asc')->take(8)->get());
$phoneDeviceBrands  =   Category::where('status', 1)->orderBy('id','desc')->take(8)->get();
//dd($phoneDeviceBrands);
$brands = json_decode( DB::table('categories')->select('id','name','slug')->where('brands',1)->orderBy('id','asc')->take(8)->get());
/*$carriers = json_decode(DB::table('categories')->select('id','name','slug')->where('carriers',1)->orderBy('id','asc')->take(8)->get());*/
$carriers   =   DB::table('carriers')->select('id','name','status')->orderBy('id','asc')->take(8)->get();
    
/*    foreach ($carriers as $key => $carrier) {
        $carrierProducts    =   DB::table('products')->whereRaw("find_in_set('{$carrier->id}',carrier_id)")->select('id','name','status','category_id')->groupBy('category_id')->get();
        foreach ($carrierProducts as $key => $product) {
            $carrierBrands = DB::table('categories')->find($product->category_id);
            echo '<pre>';print_r($carrierBrands->name);echo '</pre>';
        }
    }*/
  /*  foreach ($carrierProducts as $key => $value) {
        print_r($value);echo '<br>'.$key.'<br>';
    }die;*/


$accessories = json_decode(DB::table('categories')->select('id','name','slug')->where('accessories',1)->orderBy('id','asc')->take(8)->get());
$arrivals = json_decode(DB::table('categories')->select('id','name','slug')->where('arrivals',1)->orderBy('id','asc')->take(8)->get());

/*$carrierProducts = DB::table('categories')->join('products','categories.id','=','products.category_id')->join('carriers','carriers.id','=','products.carrier_id')->select('categories.id as CatId','categories.name as CatName','categories.slug as CatSlug','carriers.id as CarrId')->where('categories.status','=','1')->where('products.status','=','1')->orderBy('categories.id','desc')->groupBy('CatId','CarrId')->get();*/

 

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(isset($page->meta_tag) && isset($page->meta_description))
        <meta name="keywords" content="{{ $page->meta_tag }}">
        <meta name="description" content="{{ $page->meta_description }}">
        <title>{{$gs->title}}</title>
    @elseif(isset($blog->meta_tag) && isset($blog->meta_description))
        <meta name="keywords" content="{{ $blog->meta_tag }}">
        <meta name="description" content="{{ $blog->meta_description }}">
        <title>{{$gs->title}}</title>
    @elseif(isset($productt))
        <meta name="keywords" content="{{ !empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): '' }}">
        <meta name="description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}">
        <meta property="og:title" content="{{$productt->name}}" />
        <meta property="og:description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}" />
        <meta property="og:image" content="{{asset('assets/images/thumbnails/'.$productt->thumbnail)}}" />
        <meta name="author" content="GeniusOcean">
        <title>{{substr($productt->name, 0,11)."-"}}{{$gs->title}}</title>
    @else
        <meta name="keywords" content="{{ $seo->meta_keys }}">
        <meta name="author" content="Shayan">
        <title>{{$gs->title}}</title>
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"  type="image/x-icon" href="{{asset('assets/images/'.$gs->favicon)}}"/>
{{-- @if($langg->rtl == "1")
    <link rel="stylesheet" href="{{asset('assets/front/css/rtl/all.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/front/css/rtl/styles.php?color='.str_replace('#','',$gs->colors).'&amp;'.'header_color='.str_replace('#','',$gs->header_color).'&amp;'.'footer_color='.str_replace('#','',$gs->footer_color).'&amp;'.'copyright_color='.str_replace('#','',$gs->copyright_color).'&amp;'.'menu_color='.str_replace('#','',$gs->menu_color).'&amp;'.'menu_hover_color='.str_replace('#','',$gs->menu_hover_color)) }}">
@else 
    <link rel="stylesheet" href="{{asset('assets/front/css/all.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/front/css/styles.php?color='.str_replace('#','',$gs->colors).'&amp;'.'header_color='.str_replace('#','',$gs->header_color).'&amp;'.'footer_color='.str_replace('#','',$gs->footer_color).'&amp;'.'copyright_color='.str_replace('#','',$gs->copyright_color).'&amp;'.'menu_color='.str_replace('#','',$gs->menu_color).'&amp;'.'menu_hover_color='.str_replace('#','',$gs->menu_hover_color)) }}">
@endif --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="{{asset('assets/front/assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/front/assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/assets/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/assets/css/style.css')}}">
    @yield('styles')
</head>
<body>

@if($gs->is_loader == 1) 

@endif 

    <header class="top-header">
        <p>TRY OUR NEW CUSTOM BODY oil + hair oil</p>
    </header> 
    <header class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 navigation-column d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="#" class="nav-link">home</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">About</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Build Your own</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <img src="{{ asset('assets/front/assets/img/Phoenix-logo.png') }}" alt="Website Logo" class="img-fluid">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">shop</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">blog</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section -->

@yield('content')

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
    <script src="{{ asset('assets/front/assets/js/jquery.preloadinator.min.js') }}"></script>
    <script src="{{asset('assets/front/assets/js/wow.min.js')}}"></script>
    <script src="{{asset('assets/front/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/front/assets/js/main.js')}}"></script>
    <script src="{{asset('assets/front/assets/js/main-slider.js')}}"></script>
    <script>
        new WOW().init();
    </script>
</body>
</html>