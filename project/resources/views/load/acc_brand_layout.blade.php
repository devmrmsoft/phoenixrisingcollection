<select id="acc_brand" name="acc_brand_id[]" required="">
	<option value="">{{ __('Select Type') }}</option>
	@foreach($brands as $cat)
	<option data-href=""
		value="{{ $cat->id }}" @isset($lastid) @if($lastid == $cat->id ) selected @endif @endisset>{{$cat->name}}</option>
	@endforeach
	<option value="add-new-main-cat">Add New</option>
</select>