<select id="cat" name="category_id" required="">
	<option value="">{{ __('Select Type') }}</option>
	@foreach($cats as $cat)
	<option data-href="{{ route('admin-subcat-load',$cat->id) }}"
		value="{{ $cat->id }}" @isset($lastid) @if($lastid == $cat->id) selected @endif @endisset>{{$cat->name}}</option>
	@endforeach
	<option value="add-new-main-cat">Add New</option>
</select>