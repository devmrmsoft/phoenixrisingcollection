@extends('layouts.admin')
@section('styles')

<link href="{{asset('assets/admin/css/product.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/css/jquery.Jcrop.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/css/Jcrop-style.css')}}" rel="stylesheet" />

@endsection
@section('content')

<div class="content-area">
	<div class="mr-breadcrumb">
		<div class="row">
			<div class="col-lg-12">
				<h4 class="heading">{{ __('Physical Product') }} <a class="add-btn"
						href="{{ route('admin-prod-types') }}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a>
				</h4>
				<ul class="links">
					<li>
						<a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
					</li>
					<li>
						<a href="javascript:;">{{ __('Products') }} </a>
					</li>
					<li>
						<a href="{{ route('admin-prod-index') }}">{{ __('All Products') }}</a>
					</li>
					<li>
						<a href="{{ route('admin-prod-types') }}">{{ __('Add Product') }}</a>
					</li>
					<li>
						<a href="{{ route('admin-prod-physical-create') }}">{{ __('Physical Product') }}</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<form id="geniusform" action="{{route('admin-prod-store')}}" method="POST" enctype="multipart/form-data">
		{{csrf_field()}}	
	<div class="row">
		<div class="col-lg-8">
			<div class="add-product-content">
				<div class="row">
					<div class="col-lg-12">
						<div class="product-description">
							<div class="body-area">
		
								<div class="gocover"
									style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);">
								</div>
		
									@include('includes.admin.form-both')
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Product Name') }}* </h4>
												<p class="sub-heading">{{ __('(In Any Language)') }}</p>
											</div>
										</div>
										<div class="col-lg-12">
											<input type="text" class="input-field" placeholder="{{ __('Enter Product Name') }}"
												name="name" required="">
										</div>
									</div>

									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Build') }}* </h4>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="checkbox-wrapper">
												<input type="checkbox" name="build" class=""
													id="conditionCheck1" value="1">
												<label for="conditionCheck1">{{ __('Build') }}</label>
											</div>
											<input type="text" class="input-field" placeholder="{{ __('body or hair? leave empty for ready-made product') }}"
												name="build_type" >
										</div>
									</div>

									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Product Sku') }}* </h4>
											</div>
										</div>
										<div class="col-lg-12">
											<input type="text" class="input-field" placeholder="{{ __('Enter Product Sku') }}"
												name="sku" required=""
												value="{{ str_random(3).substr(time(), 6,8).str_random(3) }}">
		
										{{-- 	<div class="checkbox-wrapper">
												<input type="checkbox" name="product_condition_check" class="checkclick"
													id="conditionCheck" value="1">
												<label for="conditionCheck">{{ __('Allow Product Condition') }}</label>
											</div>
		 --}}
										</div>
									</div>

									

									<div class="showbox">
										<div class="row">
											<div class="col-lg-12">
												<div class="left-area">
													<h4 class="heading">{{ __('Product Condition') }}*</h4>
												</div>
											</div>
											<div class="col-lg-12">
												<select name="product_condition">
													<option value="2">{{ __('New') }}</option>
													<option value="1">{{ __('Refurbished') }}</option>
												</select>
											</div>
										</div>
									</div>
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Product Type') }}*</h4>
											</div>
										</div>
										<div class="col-lg-12" id="pro_type_div">
											<select id="pro_type" name="pro_type_id[]" required="">
												<option value="" selected hidden>{{ __('Select Type') }}</option>
												@foreach($types as $cat)
												<option data-href="{{ route('admin-catTypeChild-load',$cat->id) }}"
													value="{{ $cat->id }}">{{$cat->name}}</option>
												@endforeach
												{{-- <option value="add-new-pro-type">Add New</option> --}}
											</select>
										</div>
									</div>
								{{-- 	<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading pro_type_child_name">{{ __('Product Type Child') }}</h4>
											</div>
										</div>
										<div class="col-lg-12" id="pro_type_child_div">
											<select id="pro_type_child" name="pro_type_child[]" >
												<option value="">{{ __('Select Type') }}</option>
												<option value="add-catTypeChild-cat">Add New</option>
											</select>
										</div>
									</div> --}}
									<!-- <div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Product Brand') }}*</h4>
											</div>
										</div>
										<div class="col-lg-12" id="cat_div">
											<select id="cat" name="category_id" required="">
												<option value="">{{ __('Select Type') }}</option>
												@foreach($cats as $cat)
												<option data-href="{{ route('admin-subcat-load',$cat->id) }}"
													value="{{ $cat->id }}">{{$cat->name}}</option>
												@endforeach
												<option value="add-new-main-cat">Add New</option>
											</select>
										</div>
									</div>
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Product Model') }}*</h4>
											</div>
										</div>
										<div class="col-lg-12">
											<select id="subcat" name="subcategory_id" disabled="">
												<option value="">{{ __('Select Product Model') }}</option>
											</select>
										</div>
									</div>
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Child Category') }}</h4>
											</div>
										</div>
										<div class="col-lg-12">
											<select id="childcat" name="childcategory_id" disabled="">
												<option value="">{{ __('Select Child Category') }}</option>
											</select>
										</div>
									</div>
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Accessories Brand') }}</h4>
											</div>
										</div>
										<div class="col-lg-12" id="acc_brand_div">
											<select id="acc_brand" name="acc_brand_id[]">
												<option value="">{{ __('Select Accessories Brand') }}</option>
												@foreach($brands as $cat)
												<option data-href="{{ route('admin-acc_brand-load',$cat->id) }}"
													value="{{ $cat->id }}">{{$cat->name}}</option>
												@endforeach
												<option value="add-new-pro-type">Add New</option>
											</select>
										</div>
									</div>
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Accessory Types') }}</h4>
											</div>
										</div>
										<div class="col-lg-12" id="acc_type_id_div">
											<select id="acc_type_id" name="acc_type_id[]">
												<option value="">{{ __('Select Accessory Type') }}</option>
												@foreach($accTypes as $cat)
												<option data-href=""
													value="{{ $cat->id }}">{{$cat->name}}</option>
												@endforeach
												<option value="add-new-acc_type_id">Add New</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Carrier') }}</h4>
											</div>
										</div>
										<div class="col-lg-12" id="carrier_id_div">
											<select class="js-example-basic-multiple" name="carrier_id[]" multiple="multiple" >
												<option value="">{{ __('Select Carrier') }}</option>
												@foreach($carriers as $cat)
												<option data-href=""
													value="{{ $cat->id }}">{{$cat->name}}</option>
												@endforeach
											</select>
										</div>
									</div> -->
		
		
									<div id="catAttributes"></div>
									<div id="subcatAttributes"></div>
									<div id="childcatAttributes"></div>
		
		
		
			
		
		
		{{-- 
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
		
											</div>
										</div>
										<div class="col-lg-12">
											<ul class="list">
												<li>
													<input class="checkclick1" name="shipping_time_check" type="checkbox"
														id="check1" value="1">
													<label for="check1">{{ __('Allow Estimated Shipping Time') }}</label>
												</li>
											</ul>
										</div>
									</div>
		 --}}
		
		
									<div class="showbox">
		
										<div class="row">
											<div class="col-lg-12">
												<div class="left-area">
													<h4 class="heading">{{ __('Product Estimated Shipping Time') }}* </h4>
												</div>
											</div>
											<div class="col-lg-12">
												<input type="text" class="input-field"
													placeholder="{{ __('Estimated Shipping Time') }}" name="ship">
											</div>
										</div>
		
		
									</div>
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
		
											</div>
										</div>
										<div class="col-lg-12">
											<ul class="list">
												<li>
													<input name="size_check" type="checkbox" id="size-check" value="1">
													<label for="size-check">{{ __('Allow Product Sizes') }}</label>
												</li>
											</ul>
										</div>
									</div>
									<div class="showbox" id="size-display">
										<div class="row">
											<div class="col-lg-12">
											</div>
											<div class="col-lg-12">
												<div class="product-size-details" id="size-section">
													<div class="size-area">
														{{-- <span class="remove size-remove"><i class="fas fa-times"></i></span> --}}
														<div class="row">
															<div class="col-md-4 col-sm-6">
																<label>
																	{{ __('Size Name') }} :
																	<span>
																		{{ __('(eg. 10oz.,25ml, 50gm etc)') }}
																	</span>
																</label>
																<input type="text"  name="size[]" class="input-field"
																	placeholder="{{ __('Size Name') }}">
															</div>
															<div class="col-md-4 col-sm-6">
																<label>
																	{{ __('Size Qty') }} :
																	<span>
																		{{ __('(Number of quantity of this size)') }}
																	</span>
																</label>
																<input type="number" required="" name="size_qty[]" class="input-field"
																	placeholder="{{ __('Size Qty') }}" value="1" min="1">
															</div>
															<div class="col-md-4 col-sm-6 s-price d-none">
																<label>
																	{{ __('Size Price') }} :
																	<span>
																		{{ __('(This price will be added with base price)') }}
																	</span>
																</label>
																<input type="text" required="" name="size_price[]" class="input-field"
																	placeholder="{{ __('Size Price') }}" value="0" min="0">
															</div>
														</div>
													</div>
												</div>
		
												<a href="javascript:;" id="size-btn" class="add-more"><i
														class="fas fa-plus"></i>{{ __('Add More Size') }} </a>
											</div>
										</div>
									</div>
									{{-- <div class="row">
										<div class="col-lg-12">
											<div class="left-area">
		
											</div>
										</div>
										<div class="col-lg-12">
											<ul class="list">
												<li>
													<input class="checkclick1" name="color_check" type="checkbox" id="check3"
														value="1">
													<label for="check3">{{ __('Allow Product Colors') }}</label>
												</li>
											</ul>
										</div>
									</div>
		
									<div class="showbox">
		
										<div class="row">
											<div class="col-lg-12">
												<div class="left-area">
													<h4 class="heading">
														{{ __('Product Colors') }}*
													</h4>
													<p class="sub-heading">
														{{ __('(Choose Your Favorite Colors)') }}
													</p>
												</div>
											</div>
											<div class="col-lg-12">
												<div class="select-input-color" id="color-section">
													<div class="row">
														<div class="col-md-3">
															<input type="text" class="input-field" placeholder="Enter Color Name" name="color_name[0]">
														</div>
														<div class="col-md-3">																	
															<div class="color-area">
																<span class="remove color-remove"><i class="fas fa-times"></i></span>
																<div class="input-group colorpicker-component cp">
																	<input type="text" name="color[]" value="#000000"
																		class="input-field cp" />
																	<span class="input-group-addon"><i></i></span>
																</div>
															</div>
														</div>
														<div class="col-md-3 col-sm-6">
															<label>
																{{ __('Color Qty') }}
															</label>
															<input type="number" name="color_qty[]" class="input-field"
																placeholder="{{ __('Color Qty') }}" value="0" min="0">
														</div>
														<div class="col-md-3 col-sm-6">
															<label>
																{{ __('Color Price') }} 
															</label>
															<input type="number" name="color_price[]" class="input-field"
																placeholder="{{ __('Color Price') }}" value="0" min="0">
														</div>
														<div class="col-md-3">
															
															<a href="javascript:;" class="set-gallery" onclick="existingModalStarts('0')" id="colorGalleryModalStarts0"  data-toggle="modal">
																<i class="icofont-plus"></i> {{ __('Color Gallery') }}
															</a>
															<input type="file" name="uploadColorgallery[0][]" onchange="uploadInputColorGalleryImages('0')" class="hidden" id="uploadColorgallery0" accept="image/*"
													multiple>
														</div>
													</div>
												</div>
												<a href="javascript:;" id="color-btn" onclick="addMoreColor()" class="add-more mt-4 mb-3"><i
														class="fas fa-plus"></i>{{ __('Add More Color') }} </a>
											</div>
											<div class="col-lg-6">
											</div>
										</div>
		
									</div> --}}
		
									{{-- <div class="row">
										<div class="col-lg-12">
											<div class="left-area">
		
											</div>
										</div>
										<div class="col-lg-12">
											<ul class="list">
												<li>
													<input class="checkclick1" name="whole_check" type="checkbox"
														id="whole_check" value="1">
													<label for="whole_check">{{ __('Allow Product Whole Sell') }}</label>
												</li>
											</ul>
										</div>
									</div>
		
									<div class="showbox">
										<div class="row">
											<div class="col-lg-12">
												<div class="left-area">
		
												</div>
											</div>
											<div class="col-lg-12">
												<div class="featured-keyword-area">
													<div class="feature-tag-top-filds" id="whole-section">
														<div class="feature-area">
															<span class="remove whole-remove"><i
																	class="fas fa-times"></i></span>
															<div class="row">
																<div class="col-lg-6">
																	<input type="number" name="whole_sell_qty[]"
																		class="input-field"
																		placeholder="{{ __('Enter Quantity') }}" min="0">
																</div>
		
																<div class="col-lg-6">
																	<input type="number" name="whole_sell_discount[]"
																		class="input-field"
																		placeholder="{{ __('Enter Discount Percentage') }}"
																		min="0" />
																</div>
															</div>
														</div>
													</div>
		
													<a href="javascript:;" id="whole-btn" class="add-fild-btn"><i
															class="icofont-plus"></i> {{ __('Add More Field') }}</a>
												</div>
											</div>
										</div>
									</div> --}}
		
									<div class="row" id="stckprod">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Product Stock') }}*</h4>
												<p class="sub-heading">{{ __('(Leave Empty will Show \'Unlimted\')') }}</p>
											</div>
										</div>
										<div class="col-lg-12">
											<input name="stock" type="text" class="input-field"
												placeholder="{{ __('e.g 20') }}">
											{{-- <div class="checkbox-wrapper">
												<input type="checkbox" name="measure_check" class="checkclick"
													id="allowProductMeasurement" value="1">
												<label
													for="allowProductMeasurement">{{ __('Allow Product Measurement') }}</label>
											</div> --}}
										</div>
									</div>
		
		
		
								{{-- 	<div class="showbox">
		
										<div class="row">
											<div class="col-lg-6">
												<div class="left-area">
													<h4 class="heading">{{ __('Product Measurement') }}*</h4>
												</div>
											</div>
											<div class="col-lg-6">
												<select id="product_measure">
													<option value="">{{ __('None') }}</option>
													<option value="Gram">{{ __('Gram') }}</option>
													<option value="Kilogram">{{ __('Kilogram') }}</option>
													<option value="Litre">{{ __('Litre') }}</option>
													<option value="Pound">{{ __('Pound') }}</option>
													<option value="Custom">{{ __('Custom') }}</option>
												</select>
											</div>
											<div class="col-lg-6 hidden" id="measure">
												<input name="measure" type="text" id="measurement" class="input-field"
													placeholder="{{ __('Enter Unit') }}">
											</div>
										</div>
		
									</div>
		 --}}
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">
													{{ __('Product Description') }}*
												</h4>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="text-editor">
												<textarea class="nic-edit-p" name="details"></textarea>
											</div>
										</div>
									</div>
		
		
		{{-- 
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">
													{{ __('Product Buy/Return Policy') }}*
												</h4>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="text-editor">
												<textarea class="nic-edit-p" name="policy"></textarea>
											</div>
										</div>
									</div> --}}
		
		
								{{-- 	<div class="row">
										<div class="col-lg-12">
											<div class="checkbox-wrapper">
												<input type="checkbox" name="seo_check" value="1" class="checkclick"
													id="allowProductSEO" value="1">
												<label for="allowProductSEO">{{ __('Allow Product SEO') }}</label>
											</div>
										</div>
									</div>
		 --}}
		
		
									<div class="showbox">
										<div class="row">
											<div class="col-lg-12">
												<div class="left-area">
													<h4 class="heading">{{ __('Meta Tags') }} *</h4>
												</div>
											</div>
											<div class="col-lg-12">
												<ul id="metatags" class="myTags">
												</ul>
											</div>
										</div>
		
										<div class="row">
											<div class="col-lg-12">
												<div class="left-area">
													<h4 class="heading">
														{{ __('Meta Description') }} *
													</h4>
												</div>
											</div>
											<div class="col-lg-12">
												<div class="text-editor">
													<textarea name="meta_description" class="input-field"
														placeholder="{{ __('Meta Description') }}"></textarea>
												</div>
											</div>
										</div>
									</div>
		

		
		
									<div class="row">
										<div class="col-lg-12 text-center">
											<button class="addProductSubmit-btn"
												type="submit">{{ __('Create Product') }}</button>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="add-product-content">
				<div class="row">
					<div class="col-lg-12">
						<div class="product-description">
							<div class="body-area">
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Feature Image') }} *</h4>
											</div>
										</div>
										<div class="col-lg-12">
												<div class="panel panel-body">
													<div class="span4 cropme text-center" id="landscape"
														style="width: 100%; height: 285px; border: 1px dashed #ddd; background: #f1f1f1;">
														<a href="javascript:;" id="crop-image" class=" mybtn1" style="">
															<i class="icofont-upload-alt"></i> {{ __('Upload Image Here') }}
														</a>
													</div>
												</div>
										</div>
									</div>
		
									<input type="hidden" id="feature_photo" name="photo" value="">
		
									<input type="file" name="gallery[]" class="hidden" id="uploadgallery" accept="image/*"
										multiple>

									{{-- <div class="row mb-4">
										<div class="col-lg-12 mb-2">
											<div class="left-area">
												<h4 class="heading">
													{{ __('Product Gallery Images') }} *
												</h4>
											</div>
										</div>
										<div class="col-lg-12">
											<a href="javascript:;" class="set-gallery" data-toggle="modal" data-target="#setgallery">
												<i class="icofont-plus"></i> {{ __('Set Gallery') }}
											</a>
										</div>
									</div>
 --}}
		
							
									<div class="row c-price d-none">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">
													{{ __('Product Current Price') }}*
												</h4>
												<p class="sub-heading">
													({{ __('In') }} {{$sign->name}})
												</p>
											</div>
										</div>
										<div class="col-lg-12">
											<input name="price" type="text" class="input-field"
												placeholder="{{ __('e.g 20') }}"   min="0">
										</div>
									</div>
		
								{{-- 	<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Product Previous Price') }}*</h4>
												<p class="sub-heading">{{ __('(Optional)') }}</p>
											</div>
										</div>
										<div class="col-lg-12">
											<input name="previous_price" step="0.01" type="number" class="input-field"
												placeholder="{{ __('e.g 20') }}" min="0">
										</div>
									</div> --}}
									
							{{-- 		<div class="PriceVariations">
										@foreach($discounts as $discount)

										<div class="row">
											<div class="col-lg-12">
												<div class="left-area">
														<h4 class="heading">{{ ucwords($discount->name) }}*</h4>
														<p class="sub-heading">{{ __('(Optional)') }}</p>
												</div>
											</div>
											<div class="col-lg-12">
												<input name="discount_price[{{ $discount->id }}]" type="number" class="input-field" step="0.01" value="" min="0">
											</div>
										</div>
										@endforeach
									</div> --}}
		
									<!-- <div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Youtube Video URL') }}*</h4>
												<p class="sub-heading">{{ __('(Optional)') }}</p>
											</div>
										</div>
										<div class="col-lg-12">
											<input name="youtube" type="text" class="input-field"
												placeholder="{{ __('Enter Youtube Video URL') }}">
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
		
											</div>
										</div>
										<div class="col-lg-12">
											<div class="featured-keyword-area">
												<div class="left-area">
													<h4 class="heading">{{ __('Feature Tags') }}</h4>
												</div>
												<div class="feature-tag-top-filds" id="feature-section">
													<div class="feature-area">
														<span class="remove feature-remove"><i class="fas fa-times"></i></span>
														<div class="row">
															<div class="col-lg-6">
																<input type="text" name="features[]" class="input-field"
																	placeholder="{{ __('Enter Your Keyword') }}">
															</div>
		
															<div class="col-lg-6">
																<div class="input-group colorpicker-component cp">
																	<input type="text" name="colors[]" value="#000000"
																		class="input-field cp" />
																	<span class="input-group-addon"><i></i></span>
																</div>
															</div>
														</div>
													</div>
												</div>
		
												<a href="javascript:;" id="feature-btn" class="add-fild-btn"><i
														class="icofont-plus"></i> {{ __('Add More Field') }}</a>
											</div>
										</div>
									</div>
		
		
									<div class="row">
										<div class="col-lg-12">
											<div class="left-area">
												<h4 class="heading">{{ __('Tags') }} *</h4>
											</div>
										</div>
										<div class="col-lg-12">
											<ul id="tags" class="myTags">
											</ul>
										</div>
									</div> -->
									<input type="hidden" name="type" value="Physical">
		
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
	
</div>

<div class="modal fade" id="setgallery" tabindex="-1" role="dialog" aria-labelledby="setgallery" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">{{ __('Image Gallery') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="top-area">
					<div class="row">
						<div class="col-sm-6 text-right">
							<div class="upload-img-btn">
								<label for="image-upload" id="prod_gallery"><i
										class="icofont-upload-alt"></i>{{ __('Upload File') }}</label>
							</div>
						</div>
						<div class="col-sm-6">
							<a href="javascript:;" class="upload-done" data-dismiss="modal"> <i
									class="fas fa-check"></i> {{ __('Done') }}</a>
						</div>
						<div class="col-sm-12 text-center">( <small>{{ __('You can upload multiple Images.') }}</small>
							)</div>
					</div>
				</div>
				<div class="gallery-images">
					<div class="selected-image">
						<div class="row">


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="uploadColorgallery" tabindex="-1" role="dialog" aria-labelledby="uploadColorgallery" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">{{ __('Image Gallery') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="top-area">
					<div class="row">
						<div class="col-sm-6 text-right">
							<div class="upload-img-btn">
								<label for="image-upload" id="color_gallery"><i
										class="icofont-upload-alt"></i>Upload File</label>
							</div>
						</div>
						<div class="col-sm-6">
							<a href="javascript:;" class="upload-done" data-dismiss="modal"> <i class="fas fa-check"></i> {{ __('Done') }}</a>
						</div>
						<div class="col-sm-12 text-center">( <small>{{ __('You can upload multiple Images.') }}</small>
							)</div>
					</div>
				</div>
				<div class="gallery-images">
					<div class="selected-Colorimage">
						<div class="row">


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="idMyModal"></div>

	<a style="display: none;" data-href="{{route('admin-cat-procreate')}}/?sts=main-on-create-product" id="add-data" data-toggle="modal" data-target="#modal1"></a>
	<a class="add-subcat-data" data-href="{{route('admin-subcat-procreate')}}?sts=sub-on-create-product" id="add-subcat-data" data-toggle="modal" data-target="#modal1"></a>
	<a style="display: none;" data-href="{{route('admin-childcat-procreate')}}/?sts=main-on-create-product" id="add-childcat-data" data-toggle="modal" data-target="#modal1"></a>

	<a style="display: none;" data-href="{{route('admin-typecat-procreate')}}/?sts=main-on-create-product" id="add-proType-data" data-toggle="modal" data-target="#modal1"></a>

	<a style="display: none;" data-href="{{route('admin-catTypeChild-procreate')}}/?sts=main-on-create-product" id="add-catTypeChild-data" data-toggle="modal" data-target="#modal1"></a>

	<a style="display: none;" data-href="{{route('admin-brandData-procreate')}}/?sts=main-on-create-product" id="acc-brand-type-getData" data-toggle="modal" data-target="#modal1"></a>


	<a style="display: none;" data-href="{{route('admin-acc_type-procreate')}}/?sts=main-on-create-product" id="acc-acc_type-type-getData" data-toggle="modal" data-target="#modal1"></a>


	<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="submit-loader">
					<img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">
				</div>
				<div class="modal-header">
					<h5 class="modal-title"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

<script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>

<script type="text/javascript">

	// dynamic modal display starts
	// existing modal implement into dynamic way

	function existingModalStarts(len){


		var header = "Image Gallery";
		var content = "This is my dynamic content";
		var strSubmitFunc = "applyButtonFunc()";
		var btnText = "Just do it!";
		var modalId =	'dynamicImageGallery'+len;
		existingDoModal(modalId, header, content, strSubmitFunc, btnText,len);
	}

	function existingDoModal(placementId, heading, formContent, strSubmitFunc, btnText,len)
	{
		//alert(placementId);
		modalElement	=	$('#'+placementId+"").html();
		//alert(modalElement);
		if (typeof modalElement === 'undefined'){

	    html =  '<div class="modal fade" id="'+placementId+'" tabindex="-1" role="dialog" aria-labelledby="'+placementId+'" aria-hidden="true">';
	    html += '<div class="modal-dialog modal-dialog-centered  modal-lg" role="document"><div class="modal-content"> <div class="modal-header">';
	    html += '<h5 class="modal-title" id="exampleModalCenterTitle'+placementId+'">'+heading+'</h5>';
	    html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>';
	    html += '</div>';
	    html += '<div class="modal-body"><div class="top-area"><div class="row"><div class="col-sm-6 text-right"><div class="upload-img-btn">';
	    html += '<label for="image-upload" class="color-gallery" id="color_gallery'+placementId+'" onclick="colorGallery('+"'"+placementId+"','"+len+"'"+')">';
	    html += '<i class="icofont-upload-alt"></i>Upload File</label></div></div>';
	    html += '<div class="col-sm-6"><a href="javascript:;" class="upload-done" data-dismiss="modal">';
	    html += '<i class="fas fa-check"></i> Done </a>';
	    html += '</div>';  // dialog
	    html += '<div class="col-sm-12 text-center"><small>You can upload multiple Images.</small></div>';
	    html += '</div>';  // footer
	    html += '</div>';  // footer
	    html += '<div class="gallery-images"><div class="selected-Colorimage'+len+'"><div class="row">';
	    html += '<div class="row">';  // dynamic images
	    html += '</div>';  // 
	    html += '</div>';  // 
	    html += '</div>';  // 
	    html += '</div>';  // 
	    html += '</div>';  // 
	    html += '</div>';  // 
	    html += '</div>';  // modalWindow
	    $('body').append(html);
	    //$('#'+placementId).modal();
	    alert(placementId);
	    console.log($('#'+placementId).html());
	    //$('#'+placementId).modal('show');
	    $('#colorGalleryModalStarts'+len).attr('data-target','#'+placementId);
	    $('#colorGalleryModalStarts'+len).removeAttr('onclick');
	    $('#colorGalleryModalStarts'+len).click();
	    }else{
			$('#'+placementId).modal('show');
		}
	    /*$('#'+placementId).on('hidden.bs.modal', function (e) {
	        $(this).remove();
	    });*/
	}

	// existing modal implement into dynamic way ends

	// Gallery Section Insert uploadInputColorGallery

	$(document).on('click', '.remove-img', function () {
		var id = $(this).find('input[type=hidden]').val();
		$('#galval' + id).remove();
		$(this).parent().parent().remove();
	});

	$(document).on('click', '#prod_gallery', function () {
		$('#uploadgallery').click();
		$('.selected-image .row').html('');
		$('#geniusform').find('.removegal').val(0);
	});


	$("#uploadgallery").change(function () {
		var total_file = document.getElementById("uploadgallery").files.length;
		for (var i = 0; i < total_file; i++) {
			$('.selected-image .row').append('<div class="col-sm-6">' +
				'<div class="img gallery-img">' +
				'<span class="remove-img"><i class="fas fa-times"></i>' +
				'<input type="hidden" value="' + i + '">' +
				'</span>' +
				'<a href="' + URL.createObjectURL(event.target.files[i]) + '" target="_blank">' +
				'<img src="' + URL.createObjectURL(event.target.files[i]) + '" alt="gallery image">' +
				'</a>' +
				'</div>' +
				'</div> '
			);
			$('#geniusform').append('<input type="hidden" name="galval[]" id="galval' + i +
				'" class="removegal" value="' + i + '">')
		}

	});

	// Gallery Section Insert Ends

	// Gallery Section Insert for Product Colors starts 

	$(document).on('click', '.remove-img', function () {
		var id = $(this).find('input[type=hidden]').val();
		$('#colGalval' + id).remove();
		$(this).parent().parent().remove();
	});

	$(document).on('click', '#color_gallery', function () {
		$('#uploadInputColorGallery').click();
		$('.selected-Colorimage .row').html('');
		$('#geniusform').find('.removecolGal').val(0);
	});


	$("#uploadInputColorGallery").change(function () {
		var total_file = document.getElementById("uploadInputColorGallery").files.length;
		for (var i = 0; i < total_file; i++) {
			$('.selected-Colorimage .row').append('<div class="col-sm-6">' +
				'<div class="img ColorGallery-img">' +
				'<span class="remove-img"><i class="fas fa-times"></i>' +
				'<input type="hidden" value="' + i + '">' +
				'</span>' +
				'<a href="' + URL.createObjectURL(event.target.files[i]) + '" target="_blank">' +
				'<img src="' + URL.createObjectURL(event.target.files[i]) + '" alt="Color Gallery image">' +
				'</a>' +
				'</div>' +
				'</div> '
			);
			$('#geniusform').append('<input type="hidden" name="colGalval[]" id="colGalval' + i +
				'" class="removecolGal" value="' + i + '">')
		}

	});

	// Gallery Section Insert for Product Colors Ends

	//dynamic add more color functions starts

	function addMoreColor(){
		var numberOfColor 	=	$('.color-area').length;

	    $("#color-section").append(''+'<div class="row" id="colorSection'+numberOfColor+'">'+'<div class="col-md-3"><input type="text" class="input-field" placeholder="Enter Color Name" name="color_name['+numberOfColor+']" required=""></div>'+'<div class="col-md-3">'+
	                            '<div class="color-area">'+
	                                '<span class="remove color-remove"><i class="fas fa-times"></i></span>'+
	                                    '<div class="input-group colorpicker-component cp">'+
	                                        '<input type="text" name="color[]" value="#000000" class="input-field cp"/>'+
	                                        '<span class="input-group-addon"><i></i></span>'+
	                                    '</div></div>'+

	                            '</div>'+'<div class="col-md-3 col-sm-6"><label>Color Qty</label><input type="number" name="color_qty[]" class="input-field" placeholder="Color Qty" value="0" min="0"></div>'+'<div class="col-md-3 col-sm-6"><label>Color Price</label><input type="number" name="color_price[]" class="input-field" placeholder="Color Price" value="0" min="0"></div>'+
	                            '<div class="col-md-3">'+'<a href="javascript:;" class="set-gallery" onclick="existingModalStarts('+"'"+numberOfColor+"'"+')" data-toggle="modal" id="colorGalleryModalStarts'+numberOfColor+'"  data-target="#dynamicImageGallery'+numberOfColor+'">'+'<i class="icofont-plus"></i> Color Gallery' +'</a>'+'<input type="file" name="uploadColorgallery['+numberOfColor+'][]" class="hidden" onchange="uploadInputColorGalleryImages('+"'"+numberOfColor+"'"+')" id="uploadColorgallery'+numberOfColor+'" accept="image/*" multiple>'+'</div>'
	                            +'</div>'+'</div>'+'');
	    $('.cp').colorpicker();
	}

	//dynamic add more color functions ends


	// Gallery Section Insert for Product Colors function starts 

	function removeImage(){
		var id = $(this).find('input[type=hidden]').val();
		$('#colGalval' + id).remove();
		$(this).parent().parent().remove();
	}
	function colorGallery(id,len){
		var selectColorClass =	'selected-Colorimage'+len+'';
		$('#uploadColorgallery'+len).click();
		$('.'+selectColorClass+' .row').html('');
		$('#geniusform').find('.removecolGal').val(0);
	}

	function uploadInputColorGalleryImages(len){
		var selectColorClass =	'selected-Colorimage'+len+'';
		var id = "uploadColorgallery"+len+"";
		var total_file = document.getElementById(id).files.length;
		for (var i = 0; i < total_file; i++) {
			$('.'+selectColorClass+' .row').append('<div class="col-sm-6">' +
				'<div class="img ColorGallery-img'+len+'">' +
				'<span class="remove-img"><i class="fas fa-times"></i>' +
				'<input type="hidden" value="' + i + '">' +
				'</span>' +
				'<a href="' + URL.createObjectURL(event.target.files[i]) + '" target="_blank">' +
				'<img src="' + URL.createObjectURL(event.target.files[i]) + '" alt="Color Gallery image">' +
				'</a>' +
				'</div>' +
				'</div> '
			);
			$('#geniusform').append('<input type="hidden" name="colGalval['+len+'][]" id="colGalval' + i +
				'" class="removecolGal" value="' + i + '">')
		}
	}

	// Gallery Section Insert for Product Colors function Ends
</script>

<script type="text/javascript">
	$('.cropme').simpleCropper();


function checkSizesLength(){
			let count = $('#size-btn').siblings('.product-size-details').children('.size-area').length;
			return count;
		}
		
	$('#pro_type').on('change',function(){
		if($(this).val() == 1){
		$('.s-price').removeClass('d-none')
		$('.c-price').addClass('d-none')
		$('input[name="size_price[]"]').attr('required', true);
		$('input[name="price"]').attr('required', false);
		if(checkSizesLength() == 2){
					$('#size-btn').addClass('d-none')
				}
				else{
					$('#size-btn').removeClass('d-none')
				}
		}
		else{
		$('.s-price').addClass('d-none')
		$('.c-price').removeClass('d-none')
		$('input[name="size_price[]"]').attr('required', false);
		$('input[name="price"]').attr('required', true);
		if(checkSizesLength() == 1){
							$('#size-btn').addClass('d-none')

		}
		}
	});

$(document).on('click','#size-btn', function(){
			if(checkSizesLength() == 2){
				$('#size-btn').addClass('d-none')
					$('.s-price').removeClass('d-none')
		$('.c-price').addClass('d-none')
			}
			else{
				$('#size-btn').removeClass('d-none')
			}
});

$(document).on('click','.size-remove', function(){
			if(checkSizesLength() == 1){
				$('#size-btn').addClass('d-none')
			}
			else{
				$('#size-btn').removeClass('d-none')
			}
});



	// 	$('#size-btn, span.remove.size-remove').on('click',function(){
	// 		alert();
			
	// 		// console.log($(this).siblings('.product-size-details').andSelf().length);
	// });
	

$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script src="{{asset('assets/admin/js/product.js')}}"></script>
@endsection