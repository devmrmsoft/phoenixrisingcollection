@extends('layouts.load')

<style>
    .label_down{
        margin-bottom:-80px !important;
    }
</style>

@section('content')


            <div class="content-area">

              <div class="add-product-content1">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">
                        @include('includes.admin.form-error')  
                      <form id="geniusformdata" action="{{route('admin-cat-store')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Name') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="name" placeholder="{{ __('Enter Name') }}" required="" value="">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Slug') }} *</h4>
                                <p class="sub-heading">{{ __('In English') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="slug" placeholder="{{ __('Enter Slug') }}" required="" value="">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Set Icon') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <div class="img-upload">
                                <div id="image-preview" class="img-preview" style="background: url({{ asset('assets/admin/images/upload.png') }});">
                                    <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>{{ __('Upload Icon') }}</label>
                                    <input type="file" name="photo" accept=".jpeg,.jpg,.png,.svg" class="img-upload" id="image-upload">
                                  </div>
                            </div>

                          </div>
                        </div>

					{{-- 		<div class="row" class="label_down">
								<div class="col-lg-4">
									<div class="left-area">
									</div>
								</div>
								<div class="col-lg-4">
							         <div class="checkbox-wrapper">
							              <input type="checkbox" name="is_featured" class="checkclick" id="is_featured" value="1">
							              <label for="is_featured">{{ __('Allow Featured Category') }}</label>
							         </div>
								</div>
								<div class="col-lg-4">
							         <div class="checkbox-wrapper">
        			                    <input type="checkbox" name="devices" class="checkclick" id="devices" value="1">
        				                <label for="devices">{{ __('Navbar Devices') }}</label>
        							</div>
								</div>
							</div>
							
							<div class="row" class="label_down">
								<div class="col-lg-4">
									<div class="left-area">
									</div>
								</div>
								<div class="col-lg-4">
							         <div class="checkbox-wrapper">
        			                    <input type="checkbox" name="brands" class="checkclick" id="brands" value="1">
        				                <label for="brands">{{ __('Navbar Brands') }}</label>
        							</div>
								</div>
								<div class="col-lg-4">
							         <div class="checkbox-wrapper">
        			                    <input type="checkbox" name="carriers" class="checkclick" id="carriers" value="1">
        				                <label for="carriers">{{ __('Navbar Carrier') }}</label>
        							</div>
								</div>
							</div>
							
							<div class="row" class="label_down">
								<div class="col-lg-4">
									<div class="left-area">
									</div>
								</div>
								<div class="col-lg-4">
							         <div class="checkbox-wrapper">
        			                    <input type="checkbox" name="accessories" class="checkclick" id="accessories" value="1">
        				                <label for="accessories">{{ __('Navbar Accessory') }}</label>
        							</div>
								</div>
								<div class="col-lg-4">
							         <div class="checkbox-wrapper">
        			                    <input type="checkbox" name="arrivals" class="checkclick" id="arrivals" value="1">
        				                <label for="arrivals">{{ __('Navbar New Arrival') }}</label>
        							</div>
								</div>
							</div>
							
							 --}}

						            <div class="showbox">

                          <div class="row">
                            <div class="col-lg-4">
                              <div class="left-area">
                                <h4 class="heading">{{ __('Set Feature Image') }} *</h4>
                              </div>
                            </div>
                            <div class="col-lg-7">
                              <div class="img-upload">
                                <div id="image-preview" class="img-preview" style="background: url({{ asset('assets/admin/images/upload.png') }});">
                                  <label for="image-upload" class="img-label"><i class="icofont-upload-alt"></i>{{ __('Upload Image') }}</label>
                                  <input type="file" name="image" class="img-upload">
                                </div>
                              </div>
                            </div>
                          </div>

						            </div>
                        <?php 

                          if(isset($sts)){
                            ?>
                            <input type="hidden" name="sts" value="1">
                            <?php
                          }

                        ?>


                        <br>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <button class="addProductSubmit-btn" type="submit">{{ __('Create Category') }}</button>
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection