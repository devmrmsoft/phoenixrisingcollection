@extends('layouts.admin')
@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')

	<div class="content-area">
		<div class="mr-breadcrumb">
			<div class="row">
				<div class="col-lg-12">
						<h4 class="heading">{{ __("Edit Retailer") }} <a class="add-btn" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ __("Back") }}</a></h4>
						<ul class="links">
							<li>
								<a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }} </a>
							</li>
							<li>
								<a href="{{ route('admin-vendor-index') }}">{{ __("Retailers") }}</a>
							</li>
							<li>
								<a href="{{ route('admin-vendor-edit',$data->id) }}">{{ __("Edit") }}</a>
							</li>
						</ul>
				</div>
			</div>
		</div>


		<div class="add-product-content1">
			<div class="row">
				<div class="col-lg-12">
					<div class="product-description">
						<div class="body-area">
						<div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
							@include('includes.admin.form-both') 

						<form id="geniusform" action="{{ route('admin-vendor-edit',$data->id) }}" method="POST" enctype="multipart/form-data">
							{{csrf_field()}}

							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Email") }} *</h4>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="email" class="input-field" name="email" placeholder="{{ __("Email Address") }}" value="{{ $data->email }}" disabled="">
								</div>
							</div>


							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Shop Name") }} *</h4>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="shop_name" placeholder="{{ __("Shop Name") }}" required="" value="{{ $data->shop_name }}">
								</div>
							</div>




							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Shop Details") }} *</h4>
									</div>
								</div>
								<div class="col-lg-7">
								<textarea class="nic-edit" name="shop_details" placeholder="{{ __("Details") }}">{{ $data->shop_details }}</textarea> 
								</div>
							</div>

							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Owner Name") }} *</h4>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="owner_name" placeholder="{{ __("Owner Name") }}" required="" value="{{ $data->owner_name }}">
								</div>
							</div>


							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Shop Number") }} *</h4>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="shop_number" placeholder="{{ __("Shop Number") }}" required="" value="{{ $data->shop_number }}">
								</div>
							</div>

							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Shop Address") }} *</h4>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="shop_address" placeholder="{{ __("Shop Address") }}" required="" value="{{ $data->shop_address }}">
								</div>
							</div>


							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Registration Number") }} </h4>
											<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="reg_number" placeholder="Registration Number" value="{{ $data->reg_number }}">
								</div>
							</div>

							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Credit Line") }} </h4>
											<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
				                    <div class="checkbox">                    
				                        <input type="checkbox" name="yes_credit_line" class="" id="yes_credit_line" value="1" onchange='creditLineToggle();'>
				                        <label for="yes_credit_line">Activated</label>
				                    </div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("No Credit Line") }} </h4>
											<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
				                    <div class="checkbox">                    
				                        <input type="checkbox" name="no_credit_line" class="" id="no_credit_line" value="1" onchange='noCreditLineToggle();'>
				                        <label for="no_credit_line">Activated</label>
				                    </div>
								</div>
							</div>
							<div class="row credit_limit_row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Credit Limit") }} </h4>
											<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="credit_limit" placeholder="Credit Limit" value="{{ $data->credit_limit }}">
								</div>
							</div>
							<div class="row credit_usage_row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">Credit Usage </h4>
											<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="credit_usage" placeholder="Credit Usage" value="{{ $data->credit_usage }}">
								</div>
							</div>
@php $limit =	explode(',',$data->credit_line); @endphp
							<div class="row credit_line_in_days_row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Credit Line In Days") }} </h4>
											<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-3">
									<select name="credit_line[]" required="" @php print_r($limit ); @endphp>
										<option @isset($limit[0]) @if($limit[0] == '3') selected @endif @endisset value="1">0</option>
										<option @isset($limit[0]) @if($limit[0] == '3') selected @endif @endisset value="1">1</option>
									</select>
								</div>
								<div class="col-lg-4">
									<input type="text" class="input-field" name="credit_line[]" placeholder="Credit Line In Days" value="@isset($data->credit_line){{ $data->credit_line }}@endisset">
								</div>
							</div>

							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
										<h4 class="heading">{{ __("Secure Payment") }} </h4>
										<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
							        <select id="secure" class="example-basic-multiple input-field"  onChange="checkIfSecurePaymentIsSelected(this)" name="secure[]" multiple="multiple">
									@if($secures != Null)
							        	@foreach($secures as $key => $val)
							        		<option value="{{$key}}" selected="selected">{{$val}}</option>
							        	@endforeach
							        @endif
                                        @foreach($payment_s as $pms)
                                            <option value="{{$pms->id}}">{{$pms->title}}</option>
                                        @endforeach
                                    
                                    </select>
								</div>
							</div>
							
							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
										<h4 class="heading">{{ __("Non Secure Payment") }} </h4>
										<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
							        <select id="nonsecure" class="example-basic-multiple input-field" name="nonsecure[]"   onChange="checkIfNonSecurePaymentIsSelected(this)" multiple="multiple">
							        	
							        	@if($nonsecures != Null)
							        		@foreach($nonsecures as $key => $val)
							        			<option value="{{$key}}" selected="selected">{{$val}}</option>
							        		@endforeach
							        	@endif
                                        @foreach($payment_ns as $pmn)
                                            <option value="{{$pmn->id}}">{{$pmn->title}}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>


							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">{{ __("Message") }} </h4>
											<p class="sub-heading">{{ __("(This Field is Optional)") }}</p>
									</div>
								</div>
								<div class="col-lg-7">
									<input type="text" class="input-field" name="shop_message" placeholder="{{ __("Message") }}" value="{{ $data->shop_message }}">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">Personal ID / Driving License</h4>
									</div>
								</div>

								<div class="col-lg-7">
									<div class="img-upload full-width-img">
									    <div id="image-preview" class="img-preview" style="background: url({{ $data->shop_image ? asset('assets/images/vendorprofile/'.$data->personal_id):asset('assets/images/noimage.png') }});">
									        <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>{{ $langg->lang522 }}</label>
									        <input type="file" name="personal_id" class="tax-upload" id="image-upload">
									      </div>
									      <p class="text">{{ $langg->lang521 }}</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<div class="left-area">
											<h4 class="heading">Tax ID</h4>
									</div>
								</div>

								<div class="col-lg-7">
									<div class="img-upload full-width-img">
									    <div id="image-preview" class="img-preview" style="background: url({{ $data->shop_image ? asset('assets/images/vendorprofile/'.$data->tax_id):asset('assets/images/noimage.png') }});">
									        <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>{{ $langg->lang522 }}</label>
									        <input type="file" name="tax_id" class="img-upload" id="image-upload">
									      </div>
									      <p class="text">{{ $langg->lang521 }}</p>
									</div>
								</div>
							</div>


	                        <div class="row">
	                          <div class="col-lg-4">
	                            <div class="left-area">
	                                <h4 class="heading">{{ __('Discount List') }} *</h4>
	                            </div>
	                          </div>
	                          <div class="col-lg-7">
	                              <select name="discount_list" required="">
	                              	<option value="0">Please Select</option>
	                              	@foreach($discounts as $discount)
	                                <option value="{{ $discount->id }}" @if($discount->id == $data->discount_list) selected @endif>{{ $discount->name }}</option>
	                                @endforeach
	                              </select>
	                          </div>
	                        </div>

	                        <div class="row">
	                          <div class="col-lg-4">
	                            <div class="left-area">
	                              
	                            </div>
	                          </div>
	                          <div class="col-lg-7">
	                            <button class="addProductSubmit-btn" type="submit">{{ __("Submit") }}</button>
	                          </div>
	                        </div>

						</form>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script type="text/javascript">

    function creditLineToggle(){
        var yesCreditLine = document.getElementById('yes_credit_line');
        var noCreditLine = document.getElementById('no_credit_line');
        /*creditLine.checked = creditLine.checked;*/

        if(yesCreditLine.checked == false) {
            noCreditLine.checked = true;
            $('.credit_limit_row').addClass('d-none');
            $('.credit_line_days_row').addClass('d-none');
            $('.credit_usage_row').addClass('d-none');
            $('.credit_line_in_days_row').addClass('d-none');
            
        }
        else {
            if(yesCreditLine.checked == true) {
                noCreditLine.checked = false;
                $('.credit_limit_row').removeClass('d-none');
                $('.credit_line_days_row').removeClass('d-none');
                $('.credit_usage_row').removeClass('d-none');
            $('.credit_line_in_days_row').removeClass('d-none');
             }
        }
    }


    function noCreditLineToggle(){
        var yesCreditLine = document.getElementById('yes_credit_line');
        var noCreditLine = document.getElementById('no_credit_line');
        /*creditLine.checked = creditLine.checked;*/

        if(noCreditLine.checked == false) {
            yesCreditLine.checked = true; 
        }
        else {
            if(noCreditLine.checked == true) {
                yesCreditLine.checked = false;
             }
        }

        if(yesCreditLine.checked == false) {
            noCreditLine.checked = true;
            $('.credit_limit_row').addClass('d-none');
            $('.credit_line_days_row').addClass('d-none');
            $('.credit_usage_row').addClass('d-none');
            $('.credit_line_in_days_row').addClass('d-none');
        }
        else {
            if(yesCreditLine.checked == true) {
                noCreditLine.checked = false;
                $('.credit_limit_row').removeClass('d-none');
                $('.credit_line_days_row').removeClass('d-none');
                $('.credit_usage_row').removeClass('d-none');
            $('.credit_line_in_days_row').removeClass('d-none');
             }
        }

    }




    function checkIfSecurePaymentIsSelected(data){
        if($(data).val()){
            $('#nonsecure').attr('disabled', true);
            } else {
                /*alert('unselected');*/
                $('#nonsecure').attr('disabled', false);
            }
    }

    function checkIfNonSecurePaymentIsSelected(data){
        if($(data).val()){
            $('#secure').attr('disabled', true);
            } else {
                /*alert('unselected');*/
                $('#secure').attr('disabled', false);
            }
    }

	$(document).ready(function() {
	    $('.example-basic-multiple').select2();

       if($("#secure").val()){
                $('#secure').attr('disabled', false);
            } else {
                $('#secure').attr('disabled', true);
            }
       if($("#nonsecure").val()){
                $('#nonsecure').attr('disabled', false);
            } else {
                $('#nonsecure').attr('disabled', true);
            }
	});

</script>

@endsection('scripts')