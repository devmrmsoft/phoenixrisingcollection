@extends('layouts.front')
@section('content')

@if(isset($products) && !empty($products))
<section class="shop-page">
    <div class="container">
        <div class="creation-3-column">
            <div class="last-stage">
                <div class="added-product row">
                    @foreach($products as $product)
                    <div class="col-md-4">
                        <div class="product w-100">
                            <div class="product-title">
                                <div class="detail">
                                    <h4>{{ $product->name }}</h4>
                                </div>
                            </div>

                            <div class="product-image text-center">
                                @if($product->photo == 'No image found')
                                    <img src="{{ asset('assets/front/assets/img/banner.jpg') }}" class="img-fluid">
                                @else
                                    <img src="assets/images/products/{{ $product->photo }}" class="img-fluid">
                                @endif
                            </div> 

                            <div class="product-describtion ">
                                <p>{{ substr(strip_tags($product->details), 0 , 100) }}<a href="#" data-toggle="modal" data-target="#detail-popup{{$product->id}}">...read more </a></p>
                            </div>

                            <div class="detail product-end ">
                                <h3>Price: ${{ $product->price }}</h3>
                                <a href="{{url('/addtocart/'. $product->id)}}">ADD</a>
                            </div>

                        </div>
                        <div class="spacer"></div>
                    </div>
                    <!-- Column End Here -->
                    <div class="modal fade" id="detail-popup{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="detail-popup{{$product->id}}-title" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{ $product->name }} - Details</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! $product->details !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product Creation Level 3 -->
@endif

@endsection
