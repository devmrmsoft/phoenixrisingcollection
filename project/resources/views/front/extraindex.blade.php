@php

use App\Models\Subcategory as FeaturedCategoryModel;

@endphp
	@if($ps->best == 1)
		<!-- Phone and Accessories Area Start -->
		<section class="phone-and-accessories categori-item">
			<div class="container">
				<div class="row">
	                <div class="col-md-12 newestPhone-col">
	                    <div class="main-title">
	                        <h1>Newest phone models</h1>
	                    </div>
	                </div>
	            </div>
                <div class="row">
					@foreach(FeaturedCategoryModel::where('is_newest','=',1)->where('status','1')->latest('id')->take(8)->get() as $cat)
					<div class="col-md-3 col-sm-4 newestPhone-col pad-top pb-3">
                        <div class="newestPhone-image">
                            <a href="{{url('/devices/')}}/{{$cat->category->slug}}/{{$cat->slug}}"><img src="{{asset('assets/images/categories/'.$cat->photo) }}" class="img-fluid"></a>
                        </div>
                        <div class="description text-center">
                            <h5 class="margin-t-b-10 text-orange">{{ $cat->name }}</h5>
                        </div>
                    </div>
					@endforeach
				</div>
					{{--
					<div class="col-lg-3 remove-padding d-none d-lg-block">
						<div class="aside">
							<a class="banner-effect mb-10" href="{{ $ps->best_seller_banner_link }}">
								<img src="{{asset('assets/images/'.$ps->best_seller_banner)}}" alt="">
							</a>
							<a class="banner-effect" href="{{ $ps->best_seller_banner_link1 }}">
								<img src="{{asset('assets/images/'.$ps->best_seller_banner1)}}" alt="">
							</a>
						</div>
					</div>
					--}}
				</div>
			</div>
		</section>
		<!-- Phone and Accessories Area start-->
	@endif


	@if($ps->featured_category == 1)
		<!-- Electronics Area Start -->
		<section class="categori-item electronics-section">
			<div class="container">
				<div class="row">
	                <div class="col-md-12 newestPhone-col">
	                    <div class="main-title">
	                        <h1>POPULAR PHONE MODELS</h1>
	                    </div>
	                </div>
				</div>
				<div class="row">
					@foreach(FeaturedCategoryModel::where('is_featured','=',1)->where('status','1')->latest('id')->take(8)->get() as $cat)
					<div class="col-md-3 col-sm-4 pt-3">
						<div class="popular-detail text-center">
			                 <a href="{{ route('front.subcat',$cat->slug) }}"><img src="{{asset('assets/images/categories/'.$cat->photo) }}" class="img-fluid"></a>
			                 <a href="{{ route('front.category',$cat->slug) }}">       	
                                <h5 class="margin-t-b-10 text-orange">{{ $cat->name }}</h5>
			                 </a>
			            </div>
					</div>			
					@endforeach
				</div>
			</div>
		</section>
		<!-- Electronics Area start-->
	@endif
    
    <section class="product-section">
        <div class="product">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col">
                        <div class="product00">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
							  <li class="nav-item">
							    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Smart Phones</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="cases-tab" data-toggle="tab" href="#cases" role="tab" aria-controls="profile" aria-selected="false">Cases</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="headphones-tab" data-toggle="tab" href="#headphones" role="tab" aria-controls="contact" aria-selected="false">Headphones</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="chargers-tab" data-toggle="tab" href="#chargers" role="tab" aria-controls="contact" aria-selected="false">Chargers</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="screen-tab" data-toggle="tab" href="#screen" role="tab" aria-controls="contact" aria-selected="false">Screen</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="protectors-tab" data-toggle="tab" href="#protectors" role="tab" aria-controls="contact" aria-selected="false">Protectors</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="cables-tab" data-toggle="tab" href="#cables" role="tab" aria-controls="contact" aria-selected="false">Cables</a>
							  </li>
							</ul>
							<div class="tab-content" id="myTabContent">
						  		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						  			<div class="row">						  				
										@foreach($smart_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="cases" role="tabpanel" aria-labelledby="cases-tab">
									
						  			<div class="row">						  				
										@foreach($cases_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="headphones" role="tabpanel" aria-labelledby="headphones-tab">
									
						  			<div class="row">						  				
										@foreach($headphone_products as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="chargers" role="tabpanel" aria-labelledby="chargers-tab">
									
						  			<div class="row">						  				
										@foreach($chargers_products as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="screen" role="tabpanel" aria-labelledby="screen-tab">
									
						  			<div class="row">						  				
										@foreach($smart_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
						  		<div class="tab-pane fade" id="protectors" role="tabpanel" aria-labelledby="protectors-tab">
									
						  			<div class="row">						  				
										@foreach($smart_phones as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
							  	<div class="tab-pane fade" id="cables" role="tabpanel" aria-labelledby="cables-tab">
									
						  			<div class="row">						  				
										@foreach($cables_products as $prod)
											@include('includes.product.home-latest-product')
										@endforeach
						  			</div>
								</div>
							</div>
                        </div>
                    </div>
                    <!--Product Column-->
                </div>
                <!--Product Row-->
            </div>
            <!--Product Container-->
        </div>
    </section>

    <section class="aboutCommunication">
        <div class="container">
            <div class="aboutCampany">
                <h2>about horizon wireless</h2>
                <h2>communication</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                    and scrambled it to make a type specimen book. It has survived not only five centuries, but also the
                    leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
                    with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                    publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
            <div class="row">
                <div class="col-md-12 aboutCommunication-col pad-top">
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/01.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/02.png') }}" class="img-fluid" style="border: 1px solid #000;">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/03.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/04.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/05.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/06.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/07.png') }}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-12 aboutCommunication-col pt-3">
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/08.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/09.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/10.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/11.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/12.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/13.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/14.png') }}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-12 aboutCommunication-col pt-3">
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/15.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/16.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/17.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/18.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/19.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/20.png') }}" class="img-fluid">
                    </div>
                    <div class="brandsLogo">
                        <img src="{{ asset('/assets/front//assets/img/03.png') }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!---Communication About Company-->

	@if($ps->flash_deal == 1)
		<!-- Electronics Area Start -->
		<section class="categori-item electronics-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 remove-padding">
						<div class="section-top">
							<h2 class="section-title">
								{{ $langg->lang244 }}
							</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="flash-deals">
							<div class="flas-deal-slider">

								@foreach($discount_products as $prod)
									@include('includes.product.flash-product')
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Electronics Area start-->
	@endif

	@if($ps->large_banner == 1)
		<!-- Banner Area One Start -->
		<section class="banner-section">
			<div class="container">
				@foreach($large_banners->chunk(1) as $chunk)
					<div class="row">
						@foreach($chunk as $img)
							<div class="col-lg-12 remove-padding">
								<div class="img">
									<a class="banner-effect" href="{{ $img->link }}">
										<img src="{{asset('assets/images/banners/'.$img->photo)}}" alt="">
									</a>
								</div>
							</div>
						@endforeach
					</div>
				@endforeach
			</div>
		</section>
		<!-- Banner Area One Start -->
	@endif

	@if($ps->bottom_small == 1)
		<!-- Banner Area One Start -->
		<section class="banner-section">
			<div class="container">
				@foreach($bottom_small_banners->chunk(3) as $chunk)
					<div class="row">
						@foreach($chunk as $img)
							<div class="col-lg-4 remove-padding">
								<div class="left">
									<a class="banner-effect" href="{{ $img->link }}" target="_blank">
										<img src="{{asset('assets/images/banners/'.$img->photo)}}" alt="">
									</a>
								</div>
							</div>
						@endforeach
					</div>
				@endforeach
			</div>
		</section>
		<!-- Banner Area One Start -->
	@endif

	@if($ps->big == 1)
		<!-- Clothing and Apparel Area Start -->
		<section class="categori-item clothing-and-Apparel-Area">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 remove-padding">
						<div class="section-top">
							<h2 class="section-title">
								{{ $langg->lang29 }}
							</h2>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9">
						<div class="row">
							@foreach($big_products as $prod)
								@include('includes.product.home-product')
							@endforeach



						</div>
					</div>
					<div class="col-lg-3 remove-padding d-none d-lg-block">
						<div class="aside">
							<a class="banner-effect mb-10" href="{{ $ps->big_save_banner_link }}">
								<img src="{{asset('assets/images/'.$ps->big_save_banner)}}" alt="">
							</a>
							<a class="banner-effect" href="{{ $ps->big_save_banner_link1 }}">
								<img src="{{asset('assets/images/'.$ps->big_save_banner1)}}" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>
		<!-- Clothing and Apparel Area start-->
	@endif

	@if($ps->hot_sale == 1)
		<!-- hot-and-new-item Area Start -->
		<section class="hot-and-new-item">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="accessories-slider">
							<div class="slide-item">
								<div class="row">
									<div class="col-lg-3 col-sm-6">
										<div class="categori">
											<div class="section-top">
												<h2 class="section-title">
													{{ $langg->lang30 }}
												</h2>
											</div>
											<div class="hot-and-new-item-slider">
												@foreach($hot_products->chunk(3) as $chunk)
													<div class="item-slide">
														<ul class="item-list">
															@foreach($chunk as $prod)
																@include('includes.product.list-product')
															@endforeach
														</ul>
													</div>
												@endforeach
											</div>

										</div>
									</div>
									<div class="col-lg-3 col-sm-6">
										<div class="categori">
											<div class="section-top">
												<h2 class="section-title">
													{{ $langg->lang31 }}
												</h2>
											</div>

											<div class="hot-and-new-item-slider">

												@foreach($latest_products->chunk(3) as $chunk)
													<div class="item-slide">
														<ul class="item-list">
															@foreach($chunk as $prod)
																@include('includes.product.list-product')
															@endforeach
														</ul>
													</div>
												@endforeach

											</div>
										</div>
									</div>
									<div class="col-lg-3 col-sm-6">
										<div class="categori">
											<div class="section-top">
												<h2 class="section-title">
													{{ $langg->lang32 }}
												</h2>
											</div>


											<div class="hot-and-new-item-slider">

												@foreach($trending_products->chunk(3) as $chunk)
													<div class="item-slide">
														<ul class="item-list">
															@foreach($chunk as $prod)
																@include('includes.product.list-product')
															@endforeach
														</ul>
													</div>
												@endforeach

											</div>

										</div>
									</div>
									<div class="col-lg-3 col-sm-6">
										<div class="categori">
											<div class="section-top">
												<h2 class="section-title">
													{{ $langg->lang33 }}
												</h2>
											</div>

											<div class="hot-and-new-item-slider">

												@foreach($sale_products->chunk(3) as $chunk)
													<div class="item-slide">
														<ul class="item-list">
															@foreach($chunk as $prod)
																@include('includes.product.list-product')
															@endforeach
														</ul>
													</div>
												@endforeach

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Clothing and Apparel Area start-->
	@endif

	@if($ps->review_blog == 1)
		<!-- Blog Area Start -->
		<section class="blog-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="aside">
							<div class="slider-wrapper">
								<div class="aside-review-slider">
									@foreach($reviews as $review)
										<div class="slide-item">
											<div class="top-area">
												<div class="left">
													<img src="{{ $review->photo ? asset('assets/images/reviews/'.$review->photo) : asset('assets/images/noimage.png') }}" alt="">
												</div>
												<div class="right">
													<div class="content">
														<h4 class="name">{{ $review->title }}</h4>
														<p class="dagenation">{{ $review->subtitle }}</p>
													</div>
												</div>
											</div>
											<blockquote class="review-text">
												<p>
													{!! $review->details !!}
												</p>
											</blockquote>
										</div>
									@endforeach


								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						@foreach(DB::table('blogs')->orderby('views','desc')->take(2)->get() as $blogg)

							<div class="blog-box">
								<div class="blog-images">
									<div class="img">
										<img src="{{ $blogg->photo ? asset('assets/images/blogs/'.$blogg->photo):asset('assets/images/noimage.png') }}" class="img-fluid" alt="">
										<div class="date d-flex justify-content-center">
											<div class="box align-self-center">
												<p>{{date('d', strtotime($blogg->created_at))}}</p>
												<p>{{date('M', strtotime($blogg->created_at))}}</p>
											</div>
										</div>
									</div>

								</div>
								<div class="details">
									<a href='{{route('front.blogshow',$blogg->id)}}'>
										<h4 class="blog-title">
											{{mb_strlen($blogg->title,'utf-8') > 40 ? mb_substr($blogg->title,0,40,'utf-8')."...":$blogg->title}}
										</h4>
									</a>
									<p class="blog-text">
										{{substr(strip_tags($blogg->details),0,170)}}
									</p>
									<a class="read-more-btn" href="{{route('front.blogshow',$blogg->id)}}">{{ $langg->lang34 }}</a>
								</div>
							</div>

						@endforeach

					</div>
				</div>
			</div>
		</section>
		<!-- Blog Area start-->
	@endif

	@if($ps->service == 1)

	{{-- Info Area Start --}}
	{{--
	<section class="info-area">
			<div class="container">

					@foreach($services->chunk(4) as $chunk)
	
						<div class="row">
	
							<div class="col-lg-12 p-0">
								<div class="info-big-box">
									<div class="row">
										@foreach($chunk as $service)
											<div class="col-6 col-xl-3 p-0">
												<div class="info-box">
													<div class="icon">
														<img src="{{ asset('assets/images/services/'.$service->photo) }}">
													</div>
													<div class="info">
														<div class="details">
															<h4 class="title">{{ $service->title }}</h4>
															<p class="text">
																{!! $service->details !!}
															</p>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>
	
						</div>
	
					@endforeach
	
			</div>
	</section>
	--}}
		{{-- Info Area End  --}}

	@endif


    <section class="newsLetter-section">
        <div class="newsLetter">
        	<form action="{{route('front.subscribe')}}" id="subscribeform" method="POST">
	            <div class="container">
	                <div class="row d-flex align-items-center">
	                    <div class="col-md-5 d-flex align-items-center justify-content-center newsLetter-col">
	                        <div class="newsLetter-00">
	                            <h1>Sign Up For Newsletter</h1>
	                            <p>Get e-mail update about our latest shop and special offers.</p>
	                        </div>
	                    </div>
	                    <!--Column-->
	                    <div class="col-md-7 d-flex align-items-center justify-content-center newsLetter-col">
	                        <div class="newsLetter-01">
		                    		{{csrf_field()}}
		                            <div class="input-group">
		                                <div class="input-group-prepend">
		                                    <span class="input-group-text" id="basic-addon1"><i
		                                            class="far fa-envelope"></i></span>
		                                </div>
		                                <input  type="email" name="email" class="form-control" placeholder="Your e-mail address..." aria-label="Username" aria-describedby="basic-addon1" required="">
		                            </div>
		                            <a href="javascript:;" id="sub-btn" type="button" class="btn-default">subscribe</a>
	                        </div>
	                    </div>

	                    <!--Column-->
	                </div>
	                <!--Row-->
	            </div>
        	</form>
            <!--Container-->
        </div>
    </section>

    <!--SignUP News Letter Section-->
	<!-- main -->
	<script src="{{asset('assets/front/js/mainextra.js')}}"></script>