@extends('layouts.front')

@section('content')

<section class="product-creation">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Healing Oils</h1>
            <p>Add Flowers, Crystals and Essential Oils</p>
        </div>
        <div class="row">
            <div class="col-md-12 product-creation-column">
                <div class="choose-flowers">
                    <div class="quiz-text text-center">
                        <h1>Choose your healing flowers.</h1>
                        <h1>2 Max</h1>
                    </div>
                    <form class="flowers d-flex justify-content-center align-items-center" action="{{ url('/crystals')}}" method="get">
                        <input type="hidden" name="oils" value="{{$product_type}}">
                        <nav class="nav flower-container text-center">
                            @foreach($ingredients as $ingredient)
                            <li class="nav-item">
                                <input type="checkbox" name="flowers[]" id="{{$ingredient->id}}" value="{{$ingredient->id}}" class="input-hidden check"/>
                                <label for="{{$ingredient->id}}">
                                    @if($ingredient->photo == 'No image found')
                                    <img src="{{ asset('assets/front/assets/img/flower-1.jpg') }}" class="img-fluid  custome-images">
                                    @else
                                    <img src="assets/images/ingredients/{{ $ingredient->photo }}" class="img-fluid  custome-images">
                                    @endif
                                    <!-- <img src="./assets/img/flower-1.png" class="img-fluid custome-images"> -->
                                    <p class="d-block">{{$ingredient->title}}</p>
                                </label>
                                @if($ingredient->ingredients || $ingredient->description)
                                <p>
                                    @if($ingredient->ingredients)
                                    {{ substr($ingredient->ingredients, 0 , 30)}} 
                                    @endif
                                    @if($ingredient->description)
                                    <br>
                                    <a href="#" data-toggle="modal" data-target="#detail-popup{{$ingredient->id}}"> ...read more </a>
                                    @endif
                                </p>
                                @endif
                            </li>
                            <div class="modal fade" id="detail-popup{{$ingredient->id}}" tabindex="-1" role="dialog" aria-labelledby="detail-popup{{$ingredient->id}}-title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            {!! $ingredient->description !!}

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </nav>
                        <div class="col-md-12 product-creation-column">
                            <div class="next-page">
                                <button href="#" type="submit" class="next-btn border-0">Next</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</section>
<script type="text/javascript">
    var checks = document.querySelectorAll(".check");
var max = 2;
for (var i = 0; i < checks.length; i++)
  checks[i].onclick = selectiveCheck;
function selectiveCheck (event) {
  var checkedChecks = document.querySelectorAll(".check:checked");
  if (checkedChecks.length >= max + 1)
    return false;
}
</script>
@endsection