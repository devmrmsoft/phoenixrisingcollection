@extends('layouts.front')

@section('content')
<form method="post" action="{{url('/submitOils')}}">
    {{ csrf_field() }}
    <section class="creation-3">
        <div class="container">
            <div class="title text-center">
                <h1>Customize Your Healing Oils</h1>
                <p>Add Flowers, Crystals and Essential Oils</p>
            </div>

            <input type="hidden" name="oils" value="{{$product_type}}">
            @foreach($flowers as $flower)
            <input type="hidden" name="flowers[]" value="{{$flower}}">
            @endforeach
            @foreach($crystals as $crystal)
            <input type="hidden" name="crystals[]" value="{{$crystal}}">
            @endforeach
            @foreach($blends as $blend)
            <input type="hidden" name="blends[]" value="{{$blend}}">
            @endforeach
            <div class="col-md-12 creation-3-column customize-oill">
                <div class="last-stage">
                    <div class="quiz-text text-center">
                        <h1>Size</h1>
                    </div>
                    <div class="size d-flex justify-content-center align-items-center">
                        <ul class="nav">
                            @foreach($items as $key => $item)
                            <li class="nav-item size-item">
                                <input type="radio" name="bottle" value="{{$item.','.$prices[$key].','.$size_qty[$key]}}" id="{{$item}}" class="input-hidden" />
                                <label for="{{$item}}">
                                    <img src="{{ asset('assets/front/assets/img/bottle-4oz.png') }}" class="img-fluid">
                                    <div class="inner-container">
                                        <h6>{{$item}}</h6>
                                        <p>${{$prices[$key]}}</p>
                                    </div>
                                </label>
                            </li>
                            @endforeach
                            <!-- <li class="nav-item size-item">
                                <input type="radio" name="bottle" value="8" id="happy" class="input-hidden" />
                                <label for="happy">
                                    <img src="{{ asset('assets/front/assets/img/bottle-4oz.png') }}" class="img-fluid">
                                    <div class="inner-container">
                                        <h6>8.oz</h6>
                                        <p>$60.00</p>
                                    </div>
                                </label>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 creation-3-column customize-oill">
                <div class="last-stage">
                    <div class="quiz-text text-center">
                        <h1>Frequency</h1>
                    </div>
                    <div class="size d-flex justify-content-center align-items-center">
                        <ul class="nav">
                            <li class="nav-item frequency-item">
                                <input type="radio" name="duration" value="yearly" id="yearly" class="input-hidden" />
                                <label for="yearly">
                                    <p>Every Months</p>
                                </label>
                            </li>
                            <li class="nav-item frequency-item">
                                <input type="radio" name="duration" value="3-month" id="3-month" class="input-hidden" />
                                <label for="3-month">
                                    <p>Every 3 Months</p>
                                </label>
                            </li>
                            <li class="nav-item frequency-item">
                                <input type="radio" name="duration" value="6-month" id="6-month" class="input-hidden" />
                                <label for="6-month">
                                    <p>Every 6 Months</p>
                                </label>
                            </li>
                            <li class="nav-item frequency-item">
                                <input type="radio" name="duration" value="once" id="once" class="input-hidden" />
                                <label for="once">
                                    <p>Just Once</p>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 creation-3-column customize-oill">
                <div class="last-stage">
                    <div class="quiz-text text-center">
                        <h1>Boost Your Set?</h1>
                        <h3>Added Products</h3>
                    </div>
                    <div class="added-product d-flex justify-content-center align-items-start">
                        @foreach($products as $product)
                        <div class="product">
                            <div class="image text-center">
                                @if($product->photo == 'No image found')
                                <img src="{{ asset('assets/front/assets/img/banner.jpg') }}" class="img-fluid">
                                @else
                                <img src="assets/images/products/{{ $product->photo }}" width="50" class="img-fluid">
                                @endif
                                <h3>${{ $product->price }}</h3>
                            </div>
                            <div class="detail">
                                <h3>{{ $product->name }}</h3>
                                <p>{{ substr(strip_tags($product->details), 0 , 80)}} </p>
                                <a class="ajaxCart" href="{{url('/addcart/'.$product->id)}}">ADD</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-4 creation-3-column customize-oill offset-md-4"> 
                <div class="next-button text-center">
                    <div class="checkbox">
                        <input type="checkbox" name="gift" value="1" id="gift">
                        <label for="gift">
                            <h3 class="d-inline">is this a gift?</h3>
                            <img src="{{ asset('assets/front/assets/img/gift.png') }}" class="img-fluid">
                        </label>
                    </div>
                    <div class="gift-form" style="display: none;">
                        <div class="form-group text-left">
                            <label for="to">To</label>
                            <input type="input" name="to" value="" id="to" class="form-control">
                        </div>
                        <div class="form-group text-left">
                            <label for="to">From</label>
                            <input type="input" name="from" value="" id="from" class="form-control">
                        </div>
                        <div class="form-group text-left">
                            <label for="to">Message</label>
                            <textarea rows="3" class="form-control" name="message"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="next-btn border-0">NEXT</button>
                </div>
            </div>
        </div>
    </section>
</form>

<script type="text/javascript">
    // $('#gift').change(function() {
    //     if ($('#chShipAdd').prop('checked')) {
    //         $('#shipadddiv').show();
    //     } else {
    //         $('#shipadddiv').hide();
    //     }
    // });
</script>
@endsection