@extends('layouts.front')

@section('content')



    <section class="shopDetail_title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h1>{{ $productt->name }}</h1>
                    </div>
                    <div class="star-rating">
                        <span class="fa fa-star-o" data-rating="1"></span>
                        <span class="fa fa-star-o" data-rating="2"></span>
                        <span class="fa fa-star-o" data-rating="3"></span>
                        <span class="fa fa-star-o" data-rating="4"></span>
                        <span class="fa fa-star-o" data-rating="5"></span>
                        <input type="hidden" name="whatever1" class="rating-value" value="4.5">
                        <span class="counter">3.5 (250)</span>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="shopDetail_product">
        <div class="container product-details-page">
            <div class="row shopDetail_product_row right-area">

                  @if(!empty($productt->size))

                  <input type="hidden" id="stock" value="{{ $productt->size_qty[0] }}">
                  @else
                  @php
                  $stck = (string)$productt->stock;
                  @endphp
                  @if($stck != null)
                  <input type="hidden" id="stock" value="{{ $stck }}">
                  @elseif($productt->type != 'Physical')
                  <input type="hidden" id="stock" value="0">
                  @else
                  <input type="hidden" id="stock" value="">
                  @endif

                  @endif
                  <input type="hidden" id="product_price" value="{{ round($productt->vendorPrice() * $curr->value,2) }}">

                  <input type="hidden" id="product_id" value="{{ $productt->id }}">
                  <input type="hidden" id="curr_pos" value="{{ $gs->currency_format }}">
                  <input type="hidden" id="curr_sign" value="{{ $curr->sign }}">

                <div class="col-md-7 shopDetail_product_col">
                    <div id="carouselExampleIndicators" class="carousel slide" data-interval="false"
                        data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner " role="listbox">
                            <!-- Slide One - Set the background image for this slide in the line below -->
                            <div class="carousel-item active">
                                <a href="#"><img src="{{filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('assets/images/products/'.$productt->photo)}}" class="img-fluid"></a>
                            </div>

                            @foreach($productt->galleries as $gal)

                            <div class="carousel-item">
                                <a href="{{asset('assets/images/galleries/'.$gal->photo)}}"><img src="{{asset('assets/images/galleries/'.$gal->photo)}}" class="img-fluid"></a>
                            </div>

                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <!---Product Image Slider-->
                <div class="col-md-5 shopDetail_text product-info">
                    <div class="price">
                        <h1>{{ $productt->showPrice() }} <small><del>{{ $productt->showPreviousPrice() }}</del></small>
                        </h1>
                    </div>
                    {{--<div class="moneyback">
                        <span>$</span>
                        <span>Price Match Guarantee</span>
                    </div>--}}


                      @if(!empty($productt->color))
                      <div class="product-color">
                        <p class="title">{{ $langg->lang89 }} :</p>
                        <ul class="color-list">
                          @php
                          $is_first = true;
                          @endphp
                          @foreach($productt->color as $key => $data1)
                          <li class="{{ $is_first ? 'active' : '' }}">
                            <span class="box" data-color="{{ $productt->color[$key] }}" style="background-color: {{ $productt->color[$key] }}"></span>
                          </li>
                          @php
                          $is_first = false;
                          @endphp
                          @endforeach

                        </ul>
                      </div>
                      @endif
                   {{-- 
                    <div class="color">
                        <p>Color and Storage</p>

                        <span class="clr1"><img src="{{ asset('assets/front/assets/img/clr1.png') }}"></span>
                        <span class="clr1"><img src="{{ asset('assets/front/assets/img/clr2.png') }}"></span>
                        <span class="clr1"><img src="{{ asset('assets/front/assets/img/clr3.png') }}"></span>
                        <span class="clr1"><img src="{{ asset('assets/front/assets/img/clr4.png') }}"></span>
                        <span class="clr1"><img src="{{ asset('assets/front/assets/img/clr5.png') }}"></span>
                    </div>
                    
                    <div class="size">
                        <p>Size: 32GB</p>
                    </div>
                    --}}

                  @if(!empty($productt->size))
                  <div class="product-size">
                    <p class="title">{{ $langg->lang88 }} :</p>
                    <ul class="siz-list">
                      @php
                      $is_first = true;
                      @endphp
                      @foreach($productt->size as $key => $data1)
                      <li class="{{ $is_first ? 'active' : '' }}">
                        <span class="box">{{ $data1 }}
                          <input type="hidden" class="size" value="{{ $data1 }}">
                          <input type="hidden" class="size_qty" value="{{ $productt->size_qty[$key] }}">
                          <input type="hidden" class="size_key" value="{{$key}}">
                          <input type="hidden" class="size_price"
                            value="{{ round($productt->size_price[$key] * $curr->value,2) }}">
                        </span>
                      </li>
                      @php
                      $is_first = false;
                      @endphp
                      @endforeach
                      <li>
                    </ul>
                  </div>
                  @endif
                  <div class="info-meta-3">
                    
                    <ul class="meta-list">

                      @if($productt->product_type != "affiliate")
                        <li class="d-block count {{ $productt->type == 'Physical' ? '' : 'd-none' }}">
                          <p class="title">Quantity :</p>
                          <div class="qty">
                            <ul>
                              <li>
                                <span class="qtminus">
                                  <i class="icofont-minus"></i>
                                </span>
                              </li>
                              <li>
                                <span class="qttotal">1</span>
                              </li>
                              <li>
                                <span class="qtplus">
                                  <i class="icofont-plus"></i>
                                </span>
                              </li>
                            </ul>
                          </div>
                        </li>
                      @endif
                    </ul>
                  </div>
                    <div class="add-to-cart">
                        {{--
                       <li class="addtocart">
                              <a href="javascript:;" id="addcrt"><i class="icofont-cart"></i>Add to Cart</a>
                            </li>
                            --}}
                          
                          


                        <div class="box-area">

                      @if($productt->product_type == "affiliate")

                            <li class="addtocart">
                              <a href="{{ route('affiliate.product', $productt->slug) }}" target="_blank"><i
                                  class="icofont-cart"></i> {{ $langg->lang251 }}</a>
                            </li>
                            @else
                            @if($productt->emptyStock())
                            <li class="addtocart">
                              <a href="javascript:;" class="cart-out-of-stock">
                                <i class="icofont-close-circled"></i>
                                {{ $langg->lang78 }}</a>
                            </li>
                            @else
                            <a href="javascript:;" class="box side-one">Add to Cart</a>
                            <a href="javascript:;" id="addcrt" class="box side-two">Add to Cart</a>
                            @endif

                            @endif
                        </div>
                    </div>
                    <div class="show-more">
                        <a href="#" type="button">View All Phones</a>
                    </div>
                </div>
                <!--Product Detail-->
            </div>
        </div>
    </section>
    <!--Single Page Main Product-->

    <section class="reviews-tab">
        <div class="container">
            <div class="row reviews-tab-row">
                <div class="col-md-12 reviews-tab-col">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-overview-tab" data-toggle="pill" href="#pills-overview" role="tab" aria-controls="pills-overview" aria-selected="true">Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-features-tab" data-toggle="pill" href="#pills-features" role="tab" aria-controls="pills-features" aria-selected="false">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="false">Reviews</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-accessories-tab" data-toggle="pill" href="#pills-accessories" role="tab" aria-controls="pills-accessories" aria-selected="false">Accessories</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-overview" role="tabpanel" aria-labelledby="pills-overview-tab">
                            <div class="overview-container">
                                <div class="pr-1">
                                    <p>All-new dual 12MP cameras. The brightest, most colorful iPhone display ever. The fastest performance and best battery life in an iPhone. Water and splash resistant.1 And stereo speakers. Every bit as powerful as it looks—this is iPhone 7 Plus.</p>
                                </div>
                                <div class="pr-2">
                                    <h4>The iPhone 7 Plus offers:</h4>
                                    <ul>
                                        <li>5.5-inch (diagonal) Retina HD display with 1920-by-1080 resolution and wide color 3D Touch</li>
                                        <li>New 12MP cameras with optical zoom at 2x, digital zoom up to 10x, optical image stabilization, Quad-LED True Tone flash, and Live Photos Splash, water, and dust resistant1</li>
                                        <li>A10 Fusion chip with integrated M10 motion coprocessor</li>
                                        <li>4K video recording at 30 fps and slo-mo video recording for 1080p at 120 fps</li>
                                        <li>7MP FaceTime HD camera with Retina Flash</li>
                                        <li>Touch ID fingerprint sensor built into the new Home button</li>
                                        <li>LTE Advanced2 up to 450 Mbps and 802.11a/b/g/n/ac Wi-Fi with MIMO iOS 10 and iCloud</li>
                                        <li>Available in jet black, black, silver, gold, and rose gold3</li>
                                    </ul>
                                </div>
                                <div class="pr-3">
                                    <p>1Phone 7 and iPhone 7 Plus are splash, water, and dust resistant and were tested under controlled laboratory conditions with a rating of IP67 under IEC standard 60529. Splash, water, and dust resistance are not permanent conditions, and resistance might decrease as a result of normal wear. Do not attempt to charge a wet iPhone; refer to the user guide for cleaning and drying instructions. Liquid damage not covered under warranty.</p>
                                </br>
                                    <p>2Data plan required. LTE Advanced and LTE are available in select markets and through select carriers. Speeds are based on theoretical throughput and vary based on site conditions and carrier. For details on LTE support, contact your carrier and see www.apple.com/iphone/LTE</p>
                                </br>
                                    <p>3iPhone 7 and iPhone 7 Plus in jet black are only available in 128GB and 256GB models.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-features" role="tabpanel" aria-labelledby="pills-features-tab">
                            2
                        </div>
                        <div class="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                            3
                        </div>
                        <div class="tab-pane fade" id="pills-accessories" role="tabpanel" aria-labelledby="pills-accessories-tab">
                            4
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection


@section('scripts')

<script type="text/javascript">

  $(document).on("submit", "#emailreply1", function () {
    var token = $(this).find('input[name=_token]').val();
    var subject = $(this).find('input[name=subject]').val();
    var message = $(this).find('textarea[name=message]').val();
    var $type  = $(this).find('input[name=type]').val();
    $('#subj1').prop('disabled', true);
    $('#msg1').prop('disabled', true);
    $('#emlsub').prop('disabled', true);
    $.ajax({
      type: 'post',
      url: "{{URL::to('/user/admin/user/send/message')}}",
      data: {
        '_token': token,
        'subject': subject,
        'message': message,
        'type'   : $type
      },
      success: function (data) {
        $('#subj1').prop('disabled', false);
        $('#msg1').prop('disabled', false);
        $('#subj1').val('');
        $('#msg1').val('');
        $('#emlsub').prop('disabled', false);
        if(data == 0)
          toastr.error("Oops Something Goes Wrong !!");
        else
          toastr.success("Message Sent !!");
        $('.close').click();
      }

    });
    return false;
  });

</script>


<script type="text/javascript">

  $(document).on("submit", "#emailreply", function () {
    var token = $(this).find('input[name=_token]').val();
    var subject = $(this).find('input[name=subject]').val();
    var message = $(this).find('textarea[name=message]').val();
    var email = $(this).find('input[name=email]').val();
    var name = $(this).find('input[name=name]').val();
    var user_id = $(this).find('input[name=user_id]').val();
    var vendor_id = $(this).find('input[name=vendor_id]').val();
    $('#subj').prop('disabled', true);
    $('#msg').prop('disabled', true);
    $('#emlsub').prop('disabled', true);
    $.ajax({
      type: 'post',
      url: "{{URL::to('/vendor/contact')}}",
      data: {
        '_token': token,
        'subject': subject,
        'message': message,
        'email': email,
        'name': name,
        'user_id': user_id,
        'vendor_id': vendor_id
      },
      success: function () {
        $('#subj').prop('disabled', false);
        $('#msg').prop('disabled', false);
        $('#subj').val('');
        $('#msg').val('');
        $('#emlsub').prop('disabled', false);
        toastr.success("{{ $langg->message_sent }}");
        $('.ti-close').click();
      }
    });
    return false;
  });

</script>

@endsection