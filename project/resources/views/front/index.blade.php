@extends('layouts.front')
@section('content')

    <!-- Slider Section Start -->
    <section class="slider">
        <div class="container-fluid no-gutters">
            <div class="row">
                <div class="col-12 pl-0 pr-0">
                    <div class="your-class">
                        @foreach($sliders as $key => $slider)
                        <div class="image-wrapper">
                            <div class="caro-container">
                                <div class="text-cont">
                                    <div class="container">
                                        <div class="text-cont-heading animated fadeInRight">
                                            <h1>{{ $slider->subtitle_text }}</h1>
                                            <h1>{{ $slider->title_text }}</h1>
                                            <h1>{{ $slider->details_text }}</h1>
                                            <div class="text-btn animated fadeInRight">
                                                <a href=" {{ url($slider->link) }}" class="shop-btn">
                                                    @if($key == 0)
                                                    Customize Your Oil
                                                    @elseif($key == 1)
                                                    Our Shop
                                                    @else
                                                    Shop Now
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="i-wrap animated fadeInRight">
                                    @if($slider->photo == 'No image found')
                                    <img src="{{ asset('assets/front/assets/img/banner.jpg') }}" class="img-fluid">
                                    @else
                                    <img src="assets/images/sliders/{{ $slider->photo }}" class="img-fluid" width="100%">
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>            
                </div>
            </div>
        </div>
    </section>
    <!-- Slider Section End -->

    <!-- Customize Your Hair Oil Start -->
    <section class="customization">
        <div class="container">
            <div class="title text-center">
                <h1>Customize Your Healing Oils</h1>
                <p>Add Flowers, Crystals and Essential Oils</p>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <a href="{{url('/oil/body')}}">
                        <h1>Our Body Oil</h1>
                        </a>
                        <div class="inner-description">
                            <a href="{{url('/oil/body')}}">
                            <h3>All over body oil</h3>
                            </a>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">
                            <p>Our body oil is specially formulated to be lightweight and smooth on the skin. Especially

made with you in mind. Made with high-quality naturals oils that easily melt into the

skin to nourish and protect in every season... <a href="{{url('/oil/body')}}">read more.</a></p>
                        </div>
                        <a href="{{url('/flowers?oils=body')}}" class="product-btn" type="button">customize your oil</a>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="{{url('/oil/body')}}" class="hair-img d-block">
                        <img src="{{ asset('assets/front/assets/img/customize-1.webp') }}" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="{{url('/oil/hair')}}" class="hair-img d-block">
                        <img src="{{ asset('assets/front/assets/img/customize-2.webp') }}" class="img-fluid">
                    </a>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <a href="{{url('/oil/hair')}}">
                        <h1>Our Hair Oil</h1>
                        </a>
                        <div class="inner-description">
                            <a href="{{url('/oil/hair')}}">
                            <h3>Herb infused</h3>
                            </a>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">
                            <p>Our Hair Oils are special, not only because they are all-natural with organic and

high-quality ingredients but we also added special herbs for you to have beautiful results

in hair growth. We infused our oils with healthy hair growth... <a href="{{url('/oil/hair')}}">read more.</a></p>
                        </div>
                        <a href="{{url('/flowers?oils=hair')}}" class="product-btn" type="button">customize your oil</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Customize Your Hair Oil End -->

    <!-- Product Review Start --> 
    <section class="product-review">
        <div class="container-fluid no-gutters">
            <div class="row">
                <div class="col-md-4 offset-md-1 ml-auto mr-auto product-review-column">
                    <img src="{{ asset('assets/front/assets/img/reviews-sideimage.png') }}" class="img-fluid flower-img">
                </div>
                <div class="col-md-5 product-review-column d-flex justify-content-center align-items-center">
                    <div class="owl-carousel review owl-theme">
                        @foreach($reviews as $review)
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>Reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            @if($review->photo == 'No image found')
                                            <img src="{{ asset('assets/front/assets/img/client.png') }}" class="img-fluid">
                                            @else
                                            <img src="assets/images/reviews/{{ $review->photo }}" class="img-fluid" width="100%">
                                            @endif
                                            
                                            <h5>{{ $review->title }}</h5>
                                            <p>{{ $review->subtitle }}</p>
                                        </div>
                                        <div class="inner-2">
                                            <p>{{  substr(strip_tags($review->details), 0 , 200) }}... <a href="{{ route('reviews') }}"  class="">read more!</a></p>
                                        </div>
                                    </div>
                                </div>
                               <!--<div class="read-more-reviews">-->
                               <!--     <a href="{{ route('reviews') }}" type="button" class="review-btn">read More reviews</a>-->
                               <!-- </div> -->
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>
    <!-- Product Review End -->

    <!-- How it Works Start -->
  <section class="choose-oil">
        <div class="container">
            <div class="title text-center">
                <h1>How it Works</h1>
                <p>It's simple! You build it here!</p>
                <img src="{{ asset('assets/front/assets/img/big-zigzag.png') }}" class="img-fluid">
                <div class="paragraph">
                    <p>Build your perfect for you, Phoenix Healing body and hair oils. We package and send your order straight to your door! Start a monthly subscription for consistent delivery. Visit our shop for additional items to add to your Order.</p>
                </div>
            </div>
            <div class="spacer60"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row row1">
                        <div class="col-md-6 choose-oil-column d-flex flex-column align-items-start justify-content-between">
                            <div class="choose-oil-content">
                                <p><span>1/4</span></p>
                                <h3>{{ $services[0]->title }}</h3>
                                <p>{{ $services[0]->details }}</p>
                            </div>
                            <div class="customize-btn">
                                <a href="{{ route('front.buildown') }}" type="button" rel="dofollow">Customize your oil</a>
                            </div>
                        </div>
                        <div class="col-md-6 choose-oil-column">
                            <div class="choose-oil-image text-right">
                                <img src="{{ asset('assets/images/services').'/'.$services[0]->photo}}" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="spacer60"></div>
                    <div class="row row2">
                        <div class="col-md-5 choose-oil-column">
                            <div class="choose-oil-image text-left">
                                <img src="{{ asset('assets/images/services').'/'.$services[1]->photo}}" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5 choose-oil-column d-flex flex-column align-items-start justify-content-between">
                            <div class="choose-oil-content">
                                <p><span>2/4</span></p>
                                <h3>{{ $services[1]->title}}</h3>
                                <p>{{ $services[1]->details}}</p>
                            </div>
                            <div class="customize-btn">
                                <a href="{{ route('front.buildown') }}" type="button" rel="dofollow">Customize your oil</a>
                            </div>
                        </div>
                    </div>
                    <div class="spacer60"></div>
                    <div class="row row3">
                        <div class="col-md-6 choose-oil-column d-flex flex-column align-items-start justify-content-between">
                            <div class="choose-oil-content">
                                <p><span>3/4</span></p>
                                <h3>{{ $services[2]->title}}</h3>
                                <p>{{ $services[2]->details}}</p>
                            </div>
                            <div class="customize-btn">
                                <a href="{{ route('front.buildown') }}" type="button" rel="dofollow">Customize your oil</a>
                            </div>
                        </div>
                        <div class="col-md-6 choose-oil-column">
                            <div class="choose-oil-image text-right">
                                <img src="{{ asset('assets/images/services').'/'.$services[2]->photo}}" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="spacer60"></div>
                    <div class="row row4">
                        <div class="col-md-5 choose-oil-column">
                            <div class="choose-oil-image text-left">
                                <img src="{{ asset('assets/images/services').'/'.$services[3]->photo}}" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5 choose-oil-column d-flex flex-column align-items-start justify-content-between">
                            <div class="choose-oil-content">
                                <p><span>4/4</span></p>
                                <h3>{{ $services[3]->title}}</h3>
                                <p>{{ $services[3]->details}}</p>
                            </div>
                            <div class="customize-btn">
                                <a href="{{ route('front.buildown') }}" type="button" rel="dofollow">Customize your oil</a>
                            </div>
                        </div>
                    </div>
                    <div class="satisfaction-customer text-center">
                        <p>Thank you for choosing Phoenix Rising Collection to be a part of your beautiful healing journey.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>    

    <!-- How it Works End -->

    <!-- Clear Ingredients Start -->
    <section class="clear-ingredients">
            <div class="row">
              <div class="col-md-12">
                  <img class="img-fluid" src="{{ asset('assets/front/assets/img/clear-ingredients.webp')}}">
                  </div>  
            </div>
    </section>
    <!--<section class="clear-ingredients">-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-md-5 offset-md-1 m-auto clear-ingredients-column">-->
    <!--                <div class="inner-description text-center">-->
    <!--                    <h1>Clean, Science-Backed Ingredients</h1>-->
    <!--                    <p>Sed ut perspiciatis unde omnis iste natus error  voluptatem accusantium </p>-->
    <!--                </div>-->
    <!--                <div class="brands text-center">-->
    <!--                    <img src="{{ asset('assets/front/assets/img/brands.png') }}" class="img-fluid">-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-md-6 clear-ingredients-column">-->
    <!--                <div class="inner-image">-->
    <!--                    <img src="{{ asset('assets/front/assets/img/pink-bg.png') }}" class="img-fluid">-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- Clear Ingredients End -->

    <!-- Our Blogs Start -->
   
    <!-- Our Blogs End -->
    
    <!-- Discount Banner Start  -->
    <!--<section class="discount-section">-->
    <!--    <div class="container-fluid p-0">-->
    <!--        <div class="discount-offer">-->
    <!--            <div class="row h-100">-->
    <!--                <div class="col-md-6 offset-md-6 discount-offer-column">-->
    <!--                    <div class="inner-column">-->
    <!--                        <div class="text">-->
    <!--                            <h3>One day</h3>-->
    <!--                            <h1 class="d-inline">10 <span>%</span> </h1>-->
    <!--                            <h2 class="d-inline">discount</h2>-->
    <!--                        </div>-->
    <!--                        <a href="#" type="button">05/9 - 20/10/2020</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- Discount Banner End -->

    <!-- Instagram Feeds Start -->
    <section class="instagram-feed">
        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
        <div class="elfsight-app-79b8e780-f1eb-4e31-9266-0362947c0973"></div>
    </section>
    <!-- Instagram Feeds End -->


@endsection