@extends('layouts.front')


@section('content')

{{-- <section class="google-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.947564277273!2d-82.45432188491334!3d34.907765379608016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x885833c216ca1215%3A0xe09607386830f736!2sCounty%20Rd%20133%2C%20Greenville%2C%20SC%2029617%2C%20USA!5e0!3m2!1sen!2s!4v1602356064082!5m2!1sen!2s" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section> --}}

<section class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12 contact-form-column">
                <div class="title d-flex justify-content-center align-items-start flex-column">
                    <h1>Contact Us</h1>
                    <img src="{{ asset('assets/front/assets/img/big-zigzag.png')}}" class="img-fluid">
                </div>
            </div>
   
            <div class="col-md-12 contact-form-column pt-5">
                <div class="contact_form">
                  @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <form action="{{ route('front.contact.submit')}}" method="post">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form pb-0">
                                    <textarea type="text" id="message" name="message" rows="4" class="form-control md-textarea" autocomplete="off" required spellcheck="true" placeholder="Enter your message!"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 pt-4">
                                <div class="md-form mb-0">
                                    <input type="text" id="name" name="name" placeholder="Enter your name!" class="form-control" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="md-form mb-0">
                                    <input type="email" name="email" id="email" placeholder="Enter your email address!" class="form-control" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                        <div class="submit-btn">
                            <input type="submit" value="Submit">
                        </div>
                    </form>
                </div>
            </div>

           {{--    <div class="col-md-12 contact-form-column pt-5">
                <div class="inner-column">
                    <div class="address">
                        <h4>NewYork</h4>
                        <p>No 40 Lorem Ipsum 133/2 NewYork 13589</p>
                        <p>Email: Phoenix@example.com Phone: (800) 111 2323</p>
                    </div>
                    <div class="address">
                        <h4>Barcelona</h4>
                        <p>184 Main Lorem Ipsum 8007 Barselona 23765</p>
                        <p>Email: Phoenix@example.com Phone: (800) 111 2323</p>
                    </div>
                </div>
                <div class="social-icon">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="https://www.facebook.com/Phoenix-rising-collection-119021809940833" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li class="nav-item">
                            <a href="https://www.instagram.com/p/CIGZt7GH5Mg/?igshid=14o9op7pgy7rg" class="nav-link"><i class="fab fa-instagram"></i></a>
                        </li>                       
                    </ul>
                </div>
            </div> --}}


        </div>
    </div>
</section>

@endsection