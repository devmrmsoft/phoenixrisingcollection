@extends('layouts.front')


@section('content')

<section class="About_Banner">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-1 d-flex flex-column justify-content-center align-items-start">
                <h1 class="heading">About Us</h1>
                <a class="text_about_us" href="{{url('/')}}">Home <span>/</span> <span class="active">About</span></a>
            </div>
            <div class="col-md-6">
                <img src="{{ asset('assets/front//assets/img/Bottels.png') }}" class="img-fluid invisible img1">
            </div>
        </div>
    </div>
</section>
<!-- About Banner -->

<section class="about-tiles">
    <div class="container">
        <div class="row">          
            <div class="col-lg-8 section_col">
                <div class="col_pad d-flex">
                    
                    <div class="tiles">
                        <!-- <h1 class=" tile_head1">Lorem Ipsum</h1> -->
                        <p class="tile_para1">Welcome to Phoenix Rising Collection Inc, your number one source of the Phoenix
Rising Body Oil. We're dedicated to providing you the very best of our Phoenix Healing
Oils, with an emphasis on healing the mind, body and spirit.
With a personalised approach to our healing body oils. We realize that our customers
not only would enjoy healthy nurturing oils for skin and hair but also the opportunity to
build their own special oil blend uniquely formulated that incorporates the spiritual,
emotional, and physical healing properties of natural stones, crystals , flowers and
essential oil blends.</p>
                           <p class="tile_para1">Founded in late 2020 by Elizabeth Thompson, Phoenix Rising Collection Inc. was born out of a desire to bring healing to the collective admits a time where we are all waking up
to a new way of life. Needing to adjust, cleans and heal the body, mind and spirit. With
social, economic and moral changes in the world now more than ever,we are in a time
where there needs to be healing in our communities in our families and in ourselves.
Here at Phoenix rising collection we think about the deeply personal needs of our
customers concerning renewal of self and also the larger emphasis on the energies that
we all receive everyday around us. Formulating our products to give options for healing
and manifesting brings us great joy and satisfaction.</p>


                        <p class="tile_para1">Our products are suited for both women and men searching for a connection to
self-care, self-love, creativity or simply healing for their mind, body, and spirit. While
connecting with the source energy inside, our customers can cultivate consistency in
manifesting their desires such as love, abundance, joy, creativity and more.</p>
</div>
</div>
</div>

  <div class="col-lg-4 section_col">
                <div class="pt-5 text-center">
                    
                <!--    <div class="tiles">-->
                        <img class="img-fluid about-img" src="{{ asset('assets/front/assets/img/about-us.jpg') }}">
                        
<!--           </div>-->
</div>
</div>             
  <div class="col-lg-12  section_col">
                <div class="col_pad d-flex">
                    
                    <div class="tiles">
                      
<p class="tile_para1">We stand on a strong set of principles to guide our business activities which are:</p>

<ol>
    <li><p class="tile_para1">We ensure all our ingredients are naturally sourced from ethical producers.</p></li>
    <li><p class="tile_para1">We ensure to never sacrifice quality for the price.</p></li>
    <li><p class="tile_para1">We ensure to always put you (our customer) first.</p></li>
    <li><p class="tile_para1">We appreciate every customer, large and small and do our best to service them.</p></li>
</ol>

<p class="tile_para1">We hope you enjoy our products as much as we enjoy offering them to you. If you have
any questions or comments, please don't hesitate to contact us.</p>

<p class="tile_para1">Sincerely,<br>Elizabeth Thompson</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Tiles -->

{{-- <section class="video_progress">
    <div class="container">
        <div class="row">
            <div class="col-md-6 pl-0">
                <img src="{{ asset('assets/front/assets/img/video_image.jpg') }}" alt="" class=" img-fluid video_banner">
            </div>
            <div class="col-md-12 d-flex justify-content-start align-items-center">
                <div class="progress_div">
                    <h1 class="progress_head ">
                        What Have They
                    </h1>
                    <img src="{{ asset('assets/front/assets/img/zigzag_pro.jpg') }}" alt="" class="img-fluid zigzag2">
                    <div class="prgress1 d-flex">
                        <h2 class="progress_text ">
                            Natural extracts
                        </h2 class="progress_text1 ">
                        100%
                        </h2>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width:100%; "></div>
                    </div>
                    <div class="prgress1 d-flex">
                        <h2 class="progress_text ">
                           Quality of Products
                        </h2 class="progress_text1 ">
                        100%
                        </h2>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width:100%; "></div>
                    </div>
                    <div class="prgress1 d-flex">
                        <h2 class="progress_text ">
                            Clients Satisfaction
                        </h2 class="progress_text1 ">
                        100%
                        </h2>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 100%; "></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- Video Progess Bar -->

{{-- <section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel testimonials owl-theme">
                    <div class="item">
                        <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                            <img src="{{ asset('assets/front/assets/img/testimonials.jpg') }}" class="img-fluid clients">
                            <h3>Patricia Peterson</h3>
                            <div class="five-star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <p class="text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                            <img src="{{ asset('assets/front/assets/img/client.png') }}" class="img-fluid clients">
                            <h3>Patricia Peterson</h3>
                            <div class="five-star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <p class="text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                            <img src="{{ asset('assets/front/assets/img/testimonials.jpg') }}" class="img-fluid clients">
                            <h3>Patricia Peterson</h3>
                            <div class="five-star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <p class="text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- Testimonials -->

@endsection