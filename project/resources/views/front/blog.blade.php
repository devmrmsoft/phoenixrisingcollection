@extends('layouts.front')
@section('content')

<section class="blog-banner">
    <div class="container">
        <div class="row h-100">
            <div class="col-md-5 m-auto blog-banner-column d-flex justify-content-center align-items-center">
                <div class="inner-column">
                    <div class="blog-heading">
                        <h1>blog</h1>
                        <img src="{{ asset('assets/front/assets/img/zigzag.png')}}" class="img-fluid">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="{{url('/')}}" class="nav-link">Home</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/blog')}}" class="nav-link active">Blog</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7 blog-banner-column">
                {{-- <div class="dropper-bg"></div> --}}
            </div>
        </div>
    </div>
</section>
<!-- Blog Banner -->

<section class="blog-post">
    <div class="container">
        <div class="row">
        @foreach($posts as $post)
            <div class="col-md-4 blog-post-column">
                <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                    <div class="blog-upper-section text-center">
                        <img src="{{  asset('assets/images/blogs/'.$post->photo) }}" class="img-fluid featured-image">
                        <div class="blog-title text-center">
                            <h5>{{ $post->title }}</h5>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">
                            <p>{{ substr(strip_tags($post->details), 0 , 150) }}</p>
                        </div>
                    </div>
                    <div class="blog-bottom-section d-flex">
                        <p>{{ date('jS M Y',strtotime($post->created_at)) }}</p>
                        <a href="{{ url('/blog/'.$post->id) }}">READ MORE</a>
                    </div>
                </div>
            </div>
            @endforeach 
            <div class="col-md-12 pagination-col d-flex justify-content-center align-items-center pt-4">
                <ul class="nav">
                    {{ $posts->links() }}
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection
