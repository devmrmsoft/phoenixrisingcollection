@extends('layouts.front')
@section('content')
@php
	$partners = DB::table('partners')->get();
@endphp
	@if($ps->slider == 1)

		@if(count($sliders))
			@include('includes.slider-style')
		@endif
	@endif

	@if($ps->slider == 1)
		<section class="slider">
			<div class="container-fluid no-gutters">
				<div class="row">
					<div class="col-12 pl-0 pr-0">
						<div class="your-class">
							<div class="image-wrapper">
								<div class="caro-container">
									<div class="text-cont">
										<div class="container">
											<div class="text-cont-heading animated fadeInRight">
												<h1>Hair care.</h1>
												<h1>Body care.</h1>
												<h1>Essential oil.</h1>
												<div class="text-btn animated fadeInRight">
													<a href="#" class="shop-btn">Shop Now</a>
												</div>
											</div>
										</div>
									</div>
									<div class="i-wrap animated fadeInRight">
										<img src="{{ asset('assets/front/assets/img/banner.jpg') }}" class="img-fluid">
									</div>
								</div>
							</div>
							<div class="image-wrapper">
								<div class="caro-container">
									<div class="text-cont">
										<div class="container">
											<div class="text-cont-heading animated fadeInRight">
												<h1>Hair care.</h1>
												<h1>Body care.</h1>
												<h1>Essential oil.</h1>
												<div class="text-btn animated fadeInRight">
													<a href="#" class="shop-btn">Shop Now</a>
												</div>
											</div>
										</div>
									</div>
									<div class="i-wrap animated fadeInRight">
										<img src="{{ asset('assets/front/assets/img/banner.jpg') }}" class="img-fluid">
									</div>
								</div>
							</div>
							<div class="image-wrapper">
								<div class="caro-container">
									<div class="text-cont">
										<div class="container">
											<div class="text-cont-heading animated fadeInRight">
												<h1>Hair care.</h1>
												<h1>Body care.</h1>
												<h1>Essential oil.</h1>
												<div class="text-btn animated fadeInRight">
													<a href="#" class="shop-btn">Shop Now</a>
												</div>
											</div>
										</div>
									</div>
									<div class="i-wrap animated fadeInRight">
										<img src="{{ asset('assets/front/assets/img/banner.jpg') }}" class="img-fluid">
									</div>
								</div>
							</div>
						</div>            
					</div>
				</div>
			</div>
		</section>
		<!-- Slider Section -->		
	@endif

	<section class="customization">
        <div class="container">
            <div class="title text-center">
                <h1>Customize Your Oil</h1>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Body Oil</h1>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <h3>Organic oil</h3>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="#" class="product-btn" type="button">customize your product</a>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="#" class="hair-img d-block">
                        <img src="{{ asset('assets/front/assets/img/customize-2.png') }}" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="#" class="hair-img d-block">
                        <img src="{{ asset('assets/front/assets/img/customize-1.png') }}" class="img-fluid">
                    </a>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Hair Oil</h1>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <h3>Organic oil</h3>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="#" class="product-btn" type="button">customize your product</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Customize Your Hair Oil -->  

	<section class="product-review">
        <div class="container-fluid no-gutters">
            <div class="row">
                <div class="col-md-4 offset-md-1 ml-auto mr-auto product-review-column">
                    <img src="{{ asset('assets/front/assets/img/flower-oil.png') }}" class="img-fluid flower-img">
                </div>
                <div class="col-md-5 product-review-column d-flex justify-content-center align-items-center">
                    <div class="owl-carousel review owl-theme">
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>10,000 five-star reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            <img src="{{ asset('assets/front/assets/img/client.png') }}" class="img-fluid">
                                            <h5>Emma Walker</h5>
                                            <p>Owner</p>
                                        </div>
                                        <div class="inner-2">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more-reviews">
                                    <a href="#" type="button" class="review-btn">read More reviews</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>10,000 five-star reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            <img src="{{ asset('assets/front/assets/img/client.png') }}" class="img-fluid">
                                            <h5>Emma Walker</h5>
                                            <p>Owner</p>
                                        </div>
                                        <div class="inner-2">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more-reviews">
                                    <a href="#" type="button" class="review-btn">read More reviews</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>10,000 five-star reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            <img src="{{ asset('assets/front/assets/img/client.png') }}" class="img-fluid">
                                            <h5>Emma Walker</h5>
                                            <p>Owner</p>
                                        </div>
                                        <div class="inner-2">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more-reviews">
                                    <a href="#" type="button" class="review-btn">read More reviews</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>
    <!-- Product Review -->

    <!---Featured Product Section-->
	@if($ps->featured_category == 1)

	{{-- Slider buttom Category Start --}}
	{{-- 
	<section class="slider-buttom-category d-none d-md-block">
		<div class="container-fluid">
			<div class="row">
				@foreach($categories->where('is_featured','=',1) as $cat)
					<div class="col-xl-2 col-lg-3 col-md-4 sc-common-padding">
						<a href="{{ route('front.category',$cat->slug) }}" class="single-category">
							<div class="left">
								<h5 class="title">
									{{ $cat->name }}
								</h5>
								<p class="count">
									{{ count($cat->products) }} {{ $langg->lang4 }}
								</p>
							</div>
							<div class="right">
								<img src="{{asset('assets/images/categories/'.$cat->image) }}" alt="">
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</section>
	--}}
	{{-- Slider buttom banner End --}}

	@endif
{{--
	@if($ps->featured == 1)
		<!-- Trending Item Area Start -->
		<section  class="trending">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 remove-padding">
						<div class="section-top">
							<h2 class="section-title">
								{{ $langg->lang26 }}
							</h2>
							<!--  <a href="#" class="link">View All</a>  -->
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 remove-padding">
						<div class="trending-item-slider">
							@foreach($feature_products as $prod)
								@include('includes.product.slider-product')
							@endforeach
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- Tranding Item Area End -->
	@endif
--}}
	@if($ps->small_banner == 1)

		<!-- Banner Area One Start -->
		<section class="banner-section">
			<div class="container">
				@foreach($top_small_banners->chunk(2) as $chunk)
					<div class="row">
						@foreach($chunk as $img)
							<div class="col-lg-6 remove-padding">
								<div class="left">
									<a class="banner-effect" href="{{ $img->link }}" target="_blank">
										<img src="{{asset('assets/images/banners/'.$img->photo)}}" alt="">
									</a>
								</div>
							</div>
						@endforeach
					</div>
				@endforeach
			</div>
		</section>
		<!-- Banner Area One Start -->
	@endif

	<section id="extraData">
		<div class="text-center">
			<img src="{{asset('assets/images/'.$gs->loader)}}">
		</div>
	</section>


@endsection

@section('scripts')
	<script>
        $(window).on('load',function() {

            setTimeout(function(){

                $('#extraData').load('{{route('front.extraIndex')}}');

            }, 500);
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-prev').text('<');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-prev').addClass('adjust-arrow');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-next').text('>');
        	$('.featuredProduct-Slider > .owl-controls > .owl-nav .owl-next').addClass('adjust-arrow');
        });


	</script>
@endsection