@extends('layouts.front')
@section('content')

  <!-- Blog Details Area Start -->
  <section class="blog-details" id="blog-details">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="blog-title">
            {{ $blog->title }}
          </h3>
          <div class="blog-content">
            <div class="feature-image">
              <img class="img-fluid" src="{{ asset('assets/images/blogs/'.$blog->photo) }}" alt="">
            </div>
            <div class="content">
              <div class="blog-detail">
                {!! $blog->details !!}
              </div>
             {{--  <script async src="https://static.addtoany.com/menu/page.js"></script> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  <!-- Blog Details Area End-->


@endsection
