@extends('layouts.front')

@section('content')

<!-- Customize Your Hair Oil Start -->
    <section class="customization">
        <div class="container">
            <div class="title text-center">
                <h1>Customize Your Healing Oils</h1>
                <p>Add Flowers, Crystals and Essential Oils</p>
            </div>
            @if($type == 'body')
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Our Body Oil</h1>
                        <div class="inner-description">
                            <h3>All over body oil</h3>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">
                            <p>Our body oil is specially formulated to be lightweight and smooth on the skin. Especially

made with you in mind. Made with high-quality naturals oils that easily melt into the

skin to nourish and protect in every season. Focused on healing the body, mind , and

spirit, we created a body oil that can be used all over the body. On the hair and scalp,

and on the face and skin that feels soft and soothing without causing breakouts or

oiliness.</p>
<p>With ingredients like Safflower oil that absorbs quickly into the skin and rosehip oil that

is great for aging skin and sensitive skin, we create an oil that is for all skin types.</p>
<h3>Ingredients and benefits</h3>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">

<p><b>Safflower oil, Argan oil, Rosehip oil, Vitamin E, Sweet almond oil</b></p>
<h6><strong>Safflower oil:</strong></h6>
<p>The antioxidant-rich vitamins in safflower oil are important in keeping

your cells in good health. As a topical moisturizer, the linoleic acid in

safflower oil is thought to help maintain the integrity of the outer layer of

your skin by preventing flaking.</p>
<h6><strong>Argon oil:</strong></h6>
<p>Argan oil contains antioxidants, like polyphenols and vitamin E, these

ingredients can help brighten your skin tone and protect skin cells from

free radical damage (the precursor to everything from acne to early signs

of aging).</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                        <img src="{{ asset('assets/front/assets/img/customize-1.webp') }}" class="img-fluid">
                </div>
            </div>

            <div class="row">
                <div  class="col-md-11 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center">
                       <div class="description">
                        <div class="inner-description m-0">                        
<h6><strong>Rosehip oil:</strong></h6>
<p>Not only do the fatty acids and Vitamin A in rosehip oil moisturize the

skin, but they also promote skin regeneration, and can improve skin

flexibility and permeability. This means that rosehip oil products can

improve skin texture and even reduce the appearance of acne scars or

stretch marks.</p>
<h6><strong>Vitamin E:</strong></h6>
<p>Vitamin E is most commonly known for its benefits for skin health and

appearance. Vitamin E oil contains benefits for a wide range of skin and

nail conditions, including treating dry skin, preventing skin cancer,

treating psoriasis and it reduces UV damage to the skin .</p>
<h6><strong>Sweet almond oil:</strong></h6>
<p>More than soothing dry skin, almond oil can improve complexion and skin

tone. It's highly emollient, which means it helps to balance the absorption

of moisture and water loss. Because it is antibacterial and full of vitamin A,

almond oil can be used to treat acne.</p>
<h3>How to use</h3>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">                        

<p>Phoenix rising body oil is all-natural and it can be used from head to toes. After a

shower, on damp skin, squeeze a dropper full in palm. Emulsify in hands and apply all

over your body. Same for hair and scalp. It can be used on dry hair for shine and

nourishment. Feel the energies from the crystals and potions in the oil and speak your

intentions of the day or mantra for manifestations and desires.
</p>
<p>For energizing healing oils, place your bottle at the window in new moon light or full

moon light overnight to amplify properties in oil. Use cleansing tools such as selenite

ward or sage that you can find in <a href="{{route('front.index')}}">our store</a> for refreshing energies.</p>
                        <a href="{{url('/flowers?oils=body')}}" class="product-btn" type="button">customize your oil</a>
                    </div>
                    </div>
                </div>
            </div>
            @else
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                        <img src="{{ asset('assets/front/assets/img/customize-2.webp') }}" class="img-fluid">
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Our Hair Oil</h1>
                        <div class="inner-description">
                            <h3>Herb infused</h3>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">
                            <p>Our Hair Oils are special, not only because they are all-natural with organic and

high-quality ingredients but we also added special herbs for you to have beautiful results

in hair growth. We infused our oils with healthy hair growth herbs which comes with

benefits such as thickness, luster, grey prevention, and dandruff prevention. Creating an

oil that benefits the crown chakra. Giving clarity for meditation and intuition when used

as a base to add your own special mix of healing ingredients.</p>

<h3>Infused Ingredients (herbs):</h3>
                            <img src="{{ asset('assets/front/assets/img/zigzag.png') }}" class="img-fluid">

<p><b>Valampuri, Edampuri, Kadukkai, Khus Khus, Rose petals, Neli, Athimaduram, Vasambu, Carboga arisi, Samangi Vithai, Karuveppilai, Makilampoo, Maruthani vital, Karunjeerakam, Vebalam pattai, Pulankilangu, Velankuchi Ver, Vendhayam</b></p>

<p>If you choose not to add these herbs, you can choose our all-over body oil that can be

used in hair also. Some of the oils added to hair oils are Castor oil infused with 18 herbs

for more healing and growth properties, Jojoba oil, Avocado oil, Sweet almond oil,

Safflower oil, and Olive oil.</p>

<h5><strong>Benefits:</strong></h5>
<h6><strong>Jojoba oil:</strong></h6>
<p>Jojoba is rich in vitamins and minerals that nourish hair, including

vitamin C, B vitamins, vitamin E, copper, and zinc. Because it strengthens

hair, it can also prevent hair loss and promote hair thickness. In addition,

it also prevents the buildup of bacteria.</p>
                        </div>
                        <!-- <a href="{{url('/flowers?oils=hair')}}" class="product-btn" type="button">customize your oil</a> -->
                    </div>
                </div>
            </div>
             <div class="row">
                <div  class="col-md-11 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center">
                       <div class="description">
                        <div class="inner-description m-0">                        
<h6><strong>Avocado oil:</strong></h6>
<p>Avocado oil is extremely rich in oleic acid and monounsaturated fats,

making it one of few oils that can actually penetrate the hair shaft and

moisturize your hair, rather than sitting on top and coating your hair. It

also strengthens the hair shaft and helps prevent breakage.</p>

<h6><strong>Sweet almond oil:</strong></h6>

<p>Sweet almond oil is rich in magnesium, calcium, and zinc, all of which are

important for healthy hair! Using this beautiful oil in your hair and on

your scalp can help improve the condition of your scalp, reducing dryness

and flakiness as well and improving the texture of your hair and even the

amount of hair you lose.</p>

<h6><strong>Safflower oil:</strong></h6>
<p>Safflower oil can be used for hair loss and even baldness. Safflower oil is

also amazing for naturals that are shampoo-less because it nourishes and

penetrates hair follicles while protecting your hair from outside free

radicals.</p>

<h6><strong>Olive oil:</strong></h6>
<p>Olive Oil is rich in Vitamin E which makes your hair strong and prevents

hair fall. It also treats dandruff, moisturizes your hair, and reduces scalp

irritation, which further reduces dandruff.</p>

                        <a href="{{url('/flowers?oils=hair')}}" class="product-btn" type="button">customize your oil</a>
                    </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
    <!-- Customize Your Hair Oil End -->

@endsection