<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catTypeChild extends Model
{
    
    protected $fillable = ['cat_type_id','name','slug'];
    public $timestamps = false;

    // public function childs()
    // {
    //  return $this->hasMany('App\Models\Childcategory')->where('status','=',1);
    // }

    public function productType()
    {
        return $this->belongsTo('App\catType','cat_type_id','id')->withDefault(function ($data) {
            foreach($data->getFillable() as $dt){
                $data[$dt] = __('Deleted');
            }
        });
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','pro_type_child','id');
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_replace(' ', '-', $value);
    }

    public function attributes() {
        return $this->morphMany('App\Models\Attribute', 'attributable');
    }
}
