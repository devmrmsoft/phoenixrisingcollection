<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Generalsetting;
use App\Models\User;
use App\Classes\GeniusMailer;
use App\Models\Notification;
use Auth;
use Illuminate\Support\Facades\Input;
use Validator;

class UserfrontController extends Controller
{
	public function __construct()
    {
      $this->middleware('guest:web', ['except' => ['logout']]);
    }

	public function showRegisterForm(){
		return view('front.register');
	}
    public function register(Request $request)
    {
        //--- Validation Section
    	$gs = Generalsetting::findOrFail(1);
        $rules = [
	        'email'   => 'required|email|unique:users',
	        'name'   => 'required',
	        'password' => 'required|confirmed',
        ];
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        $user = new User;
        $input = $request->all();        
        $input['password'] = bcrypt($request['password']);
        $token = md5(time().$request->name.$request->email);
        $input['verification_link'] = $token;
        $input['affilate_code'] = md5($request->name.$request->email);
		$user->fill($input)->save();
        if($gs->is_verification_email == 1)
        {
	        $to = $request->email;
          $name = $request->name;
	        $subject = 'Verify your email address.';
	        // $msg = "Dear Customer,<br> We noticed that you need to verify your email address. <a href=".url('user/register/verify/'.$token).">Simply click here to verify. </a>";
          $msg = "Dear Customer,<br> We noticed that you need to verify your email address. <a href=".url('user/register/verify/'.$token).">Simply click here to verify. </a>";
          $link = url('user/register/verify/'.$token);
	        //Sending Email To Customer
	        if($gs->is_smtp == 1)
	        {
		        $data = [
		            'to' => $to,
                'name' => $name,
		            'subject' => $subject,
		            'body' => $msg,
                'link' => $link,
		        ];

		        $mailer = new GeniusMailer();
		        $mailer->sendCustomMail($data);
	        }
	        else
	        {
		        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
            $headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		        mail($to,$subject,$msg,$headers);
	        }
          	return response()->json('We need to verify your email address. We have sent an email to '.$to.' to verify your email address. Please click link in that email to continue.');
	    }
	    else {

            $user->email_verified = 'Yes';
            $user->update();
	        $notification = new Notification;
	        $notification->user_id = $user->id;
	        $notification->save();
            // print_r(Auth::guard('web')->login($user));die; 
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
          		return response()->json(1);
          	}
          	return response()->json(array('errors' => [ 0 => 'Credentials Doesn\'t Match !' ]));
	    }

    }

    public function token($token)
    {
        $gs = Generalsetting::findOrFail(1);

        if($gs->is_verification_email == 1)
	    {    	
	        $user = User::where('verification_link','=',$token)->first();
	        if(isset($user))
	        {
	            $user->email_verified = 'Yes';
	            $user->update();
		        $notification = new Notification;
		        $notification->user_id = $user->id;
		        $notification->save();
	            Auth::guard('web')->login($user); 
	            return redirect()->route('front.index')->with('success','Email Verified Successfully');
	        }
		}
		else {
			return redirect()->back();	
		}
    }

    public function showLoginForm(){
		return view('front.login');
	}


	public function login(Request $request)
    {
        //--- Validation Section
        $rules = [
                  'email'   => 'required|email',
                  'password' => 'required'
                ];

        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

      // Attempt to log the user in
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        // if successful, then redirect to their intended location

        // Check If Email is verified or not
          if(Auth::guard('web')->user()->email_verified == 'No')
          {
            Auth::guard('web')->logout();
            return response()->json(array('errors' => [ 0 => 'Your Email is not Verified!' ]));   
          }

          if(Auth::guard('web')->user()->ban == 1)
          {
            Auth::guard('web')->logout();
            return response()->json(array('errors' => [ 0 => 'Your Account Has Been Banned.' ]));   
          }

          // Login Via Modal
          if(!empty($request->modal))
          {
             // Login as Vendor
            if(!empty($request->vendor))
            {
              return response()->json(1);
            }
          // Login as User
          return response()->json(1);          
          }
          // Login as User
          return response()->json(1);     
      }

      // if unsuccessful, then redirect back to the login with the form data
          return response()->json(array('errors' => [ 0 => 'Credentials Doesn\'t Match !' ]));     
    }
}