<?php

namespace App\Http\Controllers\Admin;

use Datatables;
use App\Models\Category;
use App\Models\Subcategory; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\accessoryBrand;
use App\catType;
use App\catTypeChild;
use App\accessoryType;


class CatTypeChildController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
         $datas = catTypeChild::orderBy('id','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('category', function(catTypeChild $data) {
                                //dd($data->productType->id);
                                return $data->productType->name;
                            })
                            ->addColumn('status', function(catTypeChild $data) {
                                $class = $data->status == 1 ? 'drop-success' : 'drop-danger';
                                $s = $data->status == 1 ? 'selected' : '';
                                $ns = $data->status == 0 ? 'selected' : '';
                                return '<div class="action-list"><select class="process select droplinks '.$class.'"><option data-val="1" value="'. route('admin-cattypechild-status',['id1' => $data->id, 'id2' => 1]).'" '.$s.'>Activated</option><<option data-val="0" value="'. route('admin-cattypechild-status',['id1' => $data->id, 'id2' => 0]).'" '.$ns.'>Deactivated</option>/select></div>';
                            })
                            ->addColumn('attributes', function(catTypeChild $data) {
                                $buttons = '<div class="action-list"><a data-href="' . route('admin-attr-createForCatTypeChild', $data->id) . '" class="attribute" data-toggle="modal" data-target="#attribute"> <i class="fas fa-edit"></i>Create</a>';
                                if ($data->attributes()->count() > 0) {
                                  $buttons .= '<a href="' . route('admin-attr-manage', $data->id) .'?type=subcategory' . '" class="edit"> <i class="fas fa-edit"></i>Manage</a>';
                                }
                                $buttons .= '</div>';

                                return $buttons;
                            })
                            ->addColumn('action', function(catTypeChild $data) {
                                return '<div class="action-list"><a data-href="' . route('admin-cattypechild-edit',$data->id) . '" class="edit" data-toggle="modal" data-target="#modal1"> <i class="fas fa-edit"></i>Edit</a><a href="javascript:;" data-href="' . route('admin-cattypechild-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>';
                            })
                            ->rawColumns(['status','attributes','action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    

    //*** GET Request
    public function index()
    {
        return view('admin.cattypechild.index');
    }

    

    //*** GET Request
    public function create()
    {
      	$cats = catType::all();

        if(isset($_GET['sts'])){
            $data   =   [
                            'sts'   =>  'true',
                            'cats'  =>  $cats

            ];
            return view('admin.cattypechild.create',$data);
        }
        return view('admin.cattypechild.create',compact('cats'));
    }

    public function proCreate(){
        $cats = catType::all();

        if(isset($_GET['sts'])){
            $data   =   [
                            'sts'   =>  'true',
                            'cats'  =>  $cats

            ];
            return view('admin.cattypechild.create',$data);
        }
        return view('admin.cattypechild.create',compact('cats'));

    }


    public function Acc_typeCreate(){
        $cats = accessoryBrand::all();

        if(isset($_GET['sts'])){
            $data   =   [
                            'sts'   =>  'true',
                            'cats'  =>  $cats

            ];
            return view('admin.cattypechild.acc_brand_type_create',$data);
        }
        return view('admin.cattypechild.acc_brand_create',compact('cats'));

    }

    public function catTypeChildCreate(){
        $cats = catType::all();
        $catTypeChildCreate =   'catTypeChildCreate';
        if(isset($_GET['sts'])){
            $data   =   [
                            'sts'   =>  'true',
                            'cats'  =>  $cats,
                            'catTypeChildCreate'    =>  $catTypeChildCreate

            ];
            return view('admin.cattypechild.acc_brand_create',$data);
        }
        return view('admin.cattypechild.acc_brand_create',compact('cats'));
    }
    //*** POST Request
    public function store(Request $request)
    {
        //--- Validation Section
        $rules = [
            'slug' => 'unique:subcategories|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new catTypeChild();
        $input = $request->all();
        $data->fill($input)->save();
        //--- Logic Section Ends

        if(isset($input['sts'])){
            if($input['sts'] == '1'){
                $msg = 'New Cat Added';
                return response()->json(array('msg' => $msg, 'cat_id'   =>  $data->id,'sub' =>  '1'));
            }
        }
        //--- Redirect Section
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }


    public function storebrandData(Request $request)
    {
        //--- Validation Section
        $rules = [
            'slug' => 'unique:subcategories|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new accessoryType();
        $input = $request->all();
        $data->fill($input)->save();
        //--- Logic Section Ends

        if(isset($input['sts'])){
            if($input['sts'] == '1'){
                $msg = 'New Cat Added';
                $atypes = accessoryType::all();
                $returnHTML = view('load.add_acc_type_layout')->with('atypes', $atypes)->render();
                 return response()->json(array('success' => true,'acc_brand' => '1', 'html'=>$returnHTML));
            }
        }
        //--- Redirect Section
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    public function Acc_typeStore(Request $request)
    {
        //--- Validation Section
        $rules = [
            'slug' => 'unique:subcategories|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new accessoryType();
        $input = $request->all();
        $data->fill($input)->save();
        //--- Logic Section Ends

        if(isset($input['sts'])){
            if($input['sts'] == '1'){
                $msg = 'New Cat Added';
                $types = accessoryType::all();
                $returnHTML = view('load.add_acc_type_child_layout')->with('atypes', $types)->render();
                 return response()->json(array('success' => true,'acc_type_brand' => '1', 'html'=>$returnHTML));
            }
        }
        //--- Redirect Section
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    public function catTypeChildStore(Request $request)
    {
        //--- Validation Section
        $rules = [
            'slug' => 'unique:subcategories|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new catTypeChild();
        $input = $request->all();
        $data->fill($input)->save();
        //--- Logic Section Ends

        if(isset($input['sts'])){
            if($input['sts'] == '1'){
                $msg = 'New Cat Added';
                $types = catTypeChild::all();
                $catTypeChildCreate =   'catTypeChildCreate';
                $data   =   [
                                'atypes'                 =>  $types,
                                'catTypeChildCreate'    =>  $catTypeChildCreate

                        ];
                $returnHTML = view('load.add_acc_type_layout',$data)->render();
                 return response()->json(array('success' => true,'catTypeChildCreate' => '1', 'html'=>$returnHTML));
            }
        }
        //--- Redirect Section
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($id)
    {
    	$cats = Category::all();
        $data = catTypeChild::findOrFail($id);
        return view('admin.cattypechild.edit',compact('data','cats'));
    }

    //*** POST Request
    public function update(Request $request, $id)
    {
        //--- Validation Section
        $rules = [
            'slug' => 'unique:subcategories,slug,'.$id.'|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = catTypeChild::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

      //*** GET Request Status
      public function status($id1,$id2)
        {
            $data = catTypeChild::findOrFail($id1);
            $data->status = $id2;
            $data->update();
        }

    //*** GET Request
    // public function load($id)
    // {
    //     $cat = Category::findOrFail($id);
    //     return view('load.cattypechild',compact('cat'));
    // }

    // public function catTypeChildload($id){

    //     $cat = catType::findOrFail($id);
    //     $catTypeChild   =   '1';
    //     return view('load.cattypechild',compact('cat','catTypeChild'));
    // }

    //*** GET Request Delete
    public function destroy($id)
    {
        $data = catTypeChild::findOrFail($id);
        //dd($data);

        if($data->attributes->count()>0)
        {
        //--- Redirect Section
        $msg = 'Remove the Attributes first !';
        return response()->json($msg);
        //--- Redirect Section Ends
        }
        // if($data->childs->count()>0)
        // {dd("2");
        // //--- Redirect Section
        // $msg = 'Remove the Child Categories first !';
        // return response()->json($msg);
        // //--- Redirect Section Ends
        // }
        if($data->products->count()>0)
        {
        //--- Redirect Section
        $msg = 'Remove the products first !';
        return response()->json($msg);
        //--- Redirect Section Ends
        }

        
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
