<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
	protected $fillable = ['title', 'slug'];
    public $timestamps = true;
    protected $table = 'durations';    

    public function order_duration()
    {
        return $this->belongsTo('App\Models\OrderDuration','id');
    }

}
