<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDuration extends Model
{
	protected $fillable = ['user_id','product_id','duration_id','order_id'];
    protected $table = 'order_duration';

    public function duration()
    {
        return $this->hasOne('App\Models\Duration','id','duration_id');
    }

      public function order()
    {
        return $this->belongsTo('App\Models\Order','id');
    }

}
