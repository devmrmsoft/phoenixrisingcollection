<?php

namespace App\Models;
use Session;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

// **************** ADD TO CART *******************
    public function add($item, $id, $size,$color, $cost = null, $size_qty, $keys = null, $values = null) {
        // dd($item );
        $size_cost = 0;
        $storedItem = ['qty' => 0,'size_key' => 0, 'size_qty' =>  $size_qty,'size_price' => $item->size_price, 'cost' => $cost,  'size' => $item->size, 'color' => $item->color, 'stock' => $item->stock, 'category_id' => $item->category_id , 'price' => $item->price, 'item' => $item, 'license' => '', 'dp' => '0','keys' => $keys, 'values' => $values];

        // dd($storedItem);
        // die;

        if($item->type == 'Physical')
        {
            if ($this->items) {
                if (array_key_exists($id.$size.$color.str_replace(str_split(' ,'),'',$values), $this->items)) {
                    $storedItem = $this->items[$id.$size.$color.str_replace(str_split(' ,'),'',$values)];
                }
            }            
        }
        else {
            if ($this->items) {
                if (array_key_exists($id.$size.$color.str_replace(str_split(' ,'),'',$values), $this->items)) {
                    $storedItem = $this->items[$id.$size.$color.str_replace(str_split(' ,'),'',$values)];
                    $storedItem['dp'] = 1;
                }
            }
        }
        $storedItem['qty']++;
        $stck = (string)$item->stock;
        if($stck != null){
                $storedItem['stock']--;
        }            
        if(!empty($item->size)){ 
        $storedItem['size'] = $item->size[0];
        }  
        if(!empty($size)){
        $storedItem['size'] = $size;  
        $sizeKey = 0;
        foreach ($item->size as $key => $s) {
            if ($s == $size) {
                $sizeKey = $key;
            }
        }
        } 
        // if(!empty($item->size_qty) && $sizeKey){ 
        $storedItem['size_qty'] = $storedItem['size_qty'];
        // }  

        // if($item->size_price != null && $sizeKey){ 
        // $storedItem['size_price'] = $item->size_price[$sizeKey];
        $size_cost = $storedItem['cost'];
        // } 
        if(!empty($color)){
        $storedItem['color'] = $color;    
        } 


        if(!empty($keys)){
        $storedItem['keys'] = $keys;    
        }
        if(!empty($values)){
        $storedItem['values'] = $values;    
        }
        $item->cost += $size_cost;
        if(!empty($item->whole_sell_qty))
        {
            foreach(array_combine($item->whole_sell_qty,$item->whole_sell_discount) as $whole_sell_qty => $whole_sell_discount)
            {
                if($storedItem['qty'] == $whole_sell_qty)
                {   
                    $whole_discount[$id.$size.$color.str_replace(str_split(' ,'),'',$values)] = $whole_sell_discount;
                    Session::put('current_discount',$whole_discount);
                    break;
                }                  
            }
            if(Session::has('current_discount')) {
                    $data = Session::get('current_discount');
                if (array_key_exists($id.$size.$color.str_replace(str_split(' ,'),'',$values), $data)) {
                    $discount = $item->cost * ($data[$id.$size.$color.str_replace(str_split(' ,'),'',$values)] / 100);
                    $item->cost = $item->cost - $discount;
                }
            }
        }
        $price = ($storedItem['cost']) ? $storedItem['cost'] : $item->cost;
        // print_r($price);die;
        $storedItem['cost'] = $item->cost * $storedItem['qty'];
        $this->items[$id.$size.$color.str_replace(str_split(' ,'),'',$values)] = $storedItem;
        // dd($this->items);
        $this->totalQty++;
    }

// **************** ADD TO CART ENDS *******************



// **************** ADD TO CART MULTIPLE *******************

    public function addnum($item, $id, $qty, $size, $color, $size_qty, $color_qty, $size_price, $color_price, $size_key,$color_key, $keys, $values) {
        //dd($color_qty.'line');
        $size_cost = 0;
        $storedItem = ['qty' => 0,'size_key' => 0,'color_key' => 0, 'size_qty' =>  $item->size_qty, 'color_qty' =>  $item->color_qty,'size_price' => $item->size_price,'color_price' => $item->color_price, 'size' => $item->size, 'color' => $item->color, 'stock' => $item->stock, 'price' => $item->price, 'item' => $item, 'license' => '', 'dp' => '0','keys' => $keys, 'values' => $values];
        if($item->type == 'Physical')
        {
            if ($this->items) {
                if (array_key_exists($id.$size.$color.str_replace(str_split(' ,'),'',$values), $this->items)) {
                    $storedItem = $this->items[$id.$size.$color.str_replace(str_split(' ,'),'',$values)];
                }
            }            
        }
        else {
            if ($this->items) {
                if (array_key_exists($id.$size.$color.str_replace(str_split(' ,'),'',$values), $this->items)) {
                    $storedItem = $this->items[$id.$size.$color.str_replace(str_split(' ,'),'',$values)];
                    $storedItem['dp'] = 1;
                }
            }
        }
        $storedItem['qty'] = $storedItem['qty'] + $qty;
        $stck = (string)$item->stock;
        if($stck != null){
                $storedItem['stock']--;
        }              
        if(!empty($item->size)){ 
        $storedItem['size'] = $item->size[0];
        }  
        if(!empty($size)){
        $storedItem['size'] = $size;    
        }
        if(!empty($size_key)){
        $storedItem['size_key'] = $size_key;    
        }
        if(!empty($color_key)){
        $storedItem['color_key'] = $color_key;
        }
        if(!empty($item->size_qty)){ 
        $storedItem['size_qty'] = $item->size_qty [0];
        }
        if(!empty($size_qty)){
        $storedItem['size_qty'] = $size_qty;    
        }
        //size quantity
        if(!empty($item->color_qty)){ 
        $storedItem['color_qty'] = $item->color_qty[0];
        }
        if(!empty($color_qty)){
            //dd($color_qty);
        $storedItem['color_qty'] = $color_qty;
        }

        if(!empty($item->size_price)){ 
        $storedItem['size_price'] = $item->size_price[0];
        //$size_cost = $item->size_price[0];
        }  
        if(!empty($size_price)){
        $storedItem['size_price'] = $size_price;    
        $size_cost = $size_price;
        //dd($size_cost);
        $item->price += $size_cost;
        }
        if(!empty($item->color_price)){ 
        $storedItem['color_price'] = $item->color_price[0];
        $color_cost = $item->color_price[0];
        }  
        if(!empty($color_price)){
        $storedItem['color_price'] = $color_price;    
        $color_cost = $color_price;
        $item->price += $color_price;
        }
        if(!empty($item->color)){ 
        $storedItem['color'] = $item->color[0];
        }  
        if(!empty($color)){
        $storedItem['color'] = $color;    
        }
        if(!empty($keys)){
        $storedItem['keys'] = $keys;    
        }
        if(!empty($values)){
        $storedItem['values'] = $values;    
        }

        if(!empty($item->whole_sell_qty))
        {
            foreach(array_combine($item->whole_sell_qty,$item->whole_sell_discount) as $whole_sell_qty => $whole_sell_discount)
            {
                if($storedItem['qty'] == $whole_sell_qty)
                {   
                    $whole_discount[$id.$size.$color.str_replace(str_split(' ,'),'',$values)] = $whole_sell_discount;
                    Session::put('current_discount',$whole_discount);
                    break;
                }                  
            }
            if(Session::has('current_discount')) {
                    $data = Session::get('current_discount');
                if (array_key_exists($id.$size.$color.str_replace(str_split(' ,'),'',$values), $data)) {
                    $discount = $item->price * ($data[$id.$size.$color.str_replace(str_split(' ,'),'',$values)] / 100);
                    $item->price = $item->price - $discount;
                }
            }
        }

        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id.$size.$color.str_replace(str_split(' ,'),'',$values)] = $storedItem;
        $this->totalQty++;
    }


// **************** ADD TO CART MULTIPLE ENDS *******************


// **************** ADDING QUANTITY *******************

    public function adding($item, $id, $size_qty, $size_price) {
        $storedItem = ['qty' => 0,'size_key' => 0, 'size_qty' =>  $item->size_qty,'size_price' => $item->size_price, 'size' => $item->size, 'color' => $item->color, 'stock' => $item->stock, 'price' => $item->price, 'item' => $item, 'license' => '', 'dp' => '0','keys' => '', 'values' => ''];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty']++;

            if($item->stock != null){
                $storedItem['stock']--;
            }          
        $item->price = (double)$size_price;   
        if(!empty($item->whole_sell_qty))
        {
            foreach(array_combine($item->whole_sell_qty,$item->whole_sell_discount) as $whole_sell_qty => $whole_sell_discount)
            {
                if($storedItem['qty'] == $whole_sell_qty)
                {   
                    $whole_discount[$id] = $whole_sell_discount;
                    Session::put('current_discount',$whole_discount);
                    break;
                }                  
            }
            if(Session::has('current_discount')) {
                    $data = Session::get('current_discount');
                if (array_key_exists($id, $data)) {
                    $discount = $item->price * ($data[$id] / 100);
                    $item->price = $item->price - $discount;
                }
            }
        }

        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty++;
    }

// **************** ADDING QUANTITY ENDS *******************


// **************** REDUCING QUANTITY *******************

    public function reducing($item, $id, $size_qty, $size_price) {
        $storedItem = ['qty' => 0,'size_key' => 0, 'size_qty' =>  $item->size_qty,'size_price' => $item->size_price, 'size' => $item->size, 'color' => $item->color, 'stock' => $item->stock, 'price' => $item->price, 'item' => $item, 'license' => '', 'dp' => '0','keys' => '', 'values' => ''];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty']--;
            if($item->stock != null){
                $storedItem['stock']++;
            }            

        $item->price = (double)$size_price;   
        if(!empty($item->whole_sell_qty))
        {
            $len = count($item->whole_sell_qty);
            foreach($item->whole_sell_qty as $key => $data1)
            {
                if($storedItem['qty'] < $item->whole_sell_qty[$key])
                {   
                    if($storedItem['qty'] < $item->whole_sell_qty[0])
                    {   
                        Session::forget('current_discount');
                        break;
                    }  

                    $whole_discount[$id] = $item->whole_sell_discount[$key-1];
                    Session::put('current_discount',$whole_discount);
                    break;
                }      


            }
            if(Session::has('current_discount')) {
                    $data = Session::get('current_discount');
                if (array_key_exists($id, $data)) {
                    $discount = $item->price * ($data[$id] / 100);
                    $item->price = $item->price - $discount;
                }
            }
        }

        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty--;
    }

// **************** REDUCING QUANTITY ENDS *******************

    public function updateLicense($id,$license) {

        $this->items[$id]['license'] = $license;
    }

    public function updateColor($item, $id,$color) {

        $this->items[$id]['color'] = $color;
    }

    public function removeItem($id) {

        $id = base64_decode($id);
        // dd($id);
        // echo '<pre>';print_r($this->items);die;
        $this->totalQty -= $this->items[$id]['qty'];
        $this->totalPrice -= $this->items[$id]['cost'];
        unset($this->items[$id]);
            if(Session::has('current_discount')) {
                    $data = Session::get('current_discount');
                if (array_key_exists($id, $data)) {
                    unset($data[$id]);
                    Session::put('current_discount',$data);
                }
            }

    }
}
