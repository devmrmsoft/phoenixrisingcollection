<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = ['type','title','product_type','photo', 'ingredients', 'description', 'user_id'];
    public $timestamps = false;
}
