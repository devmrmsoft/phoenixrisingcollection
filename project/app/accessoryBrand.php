<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class accessoryBrand extends Model
{
    
    protected $fillable = ['name','slug','photo','is_featured','image'];
    public $timestamps = false;

    public function subs()
    {
        return $this->hasMany('App\accessoryType','accessory_brand_id','id')->where('status','=',1);
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','acc_brand_id','id');
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_replace(' ', '-', $value);
    }

    public function attributes() {
        return $this->morphMany('App\Models\Attribute', 'attributable');
    }
}
