<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class accessoryType extends Model
{ 
    
    protected $fillable = ['accessory_brand_id','name','slug'];
    public $timestamps = false;

    // public function childs()
    // {
    //  return $this->hasMany('App\Models\Childcategory')->where('status','=',1);
    // }

    public function category()
    {
        return $this->belongsTo('App\accessoryBrand','accessory_brand_id','id')->withDefault(function ($data) {
            foreach($data->getFillable() as $dt){
                $data[$dt] = __('Deleted');
            }
        });
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','acc_type_id','id');
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_replace(' ', '-', $value);
    }

    public function attributes() {
        return $this->morphMany('App\Models\Attribute', 'attributable');
    }
}
