<?php
/**
 * Created by PhpStorm.
 * User: ShaOn
 * Date: 11/29/2018
 * Time: 12:49 AM
 */

namespace App\Classes;

use App\Models\EmailTemplate;
use App\Models\Generalsetting;
use App\Models\Order;
use PDF;
use Illuminate\Support\Facades\Mail;
use Config;

class GeniusMailer
{
    public function __construct()
    {
        $gs = Generalsetting::findOrFail(1);
        if($gs->header_email == 'smtp') {
            $mail_driver = 'smtp';
        }
        else{
            if($gs->header_email == 'sendmail') {
                $mail_driver = 'sendmail';
            }
            else {
                $mail_driver = 'smtp';
            }
        }
        Config::set('mail.driver', $mail_driver);
        Config::set('mail.host', $gs->smtp_host);
        Config::set('mail.port', $gs->smtp_port);
        Config::set('mail.encryption', $gs->email_encryption);
        Config::set('mail.username', $gs->smtp_user);
        Config::set('mail.password', $gs->smtp_pass);
                \Artisan::call('config:clear');
                \Artisan::call('cache:clear');

    }

    public function sendAutoOrderMail(array $mailData,$id)
    {
        $emails = [$mailData['to'], 'info@phoenixrisingcollection.com'];


       /* var_dump(Config::all());
        die;*/
        $setup = Generalsetting::find(1);
        $temp = EmailTemplate::where('email_type','=',$mailData['type'])->first();
        // echo '<pre>';
        // var_dump(Config::all());
        // die;
        $body = preg_replace("/{customer_name}/", $mailData['cname'] ,$temp->email_body);
        $body = preg_replace("/{order_amount}/", $mailData['oamount'] ,$body);
        $body = preg_replace("/{admin_name}/", $mailData['aname'] ,$body);
        $body = preg_replace("/{admin_email}/", $mailData['aemail'] ,$body);
        $body = preg_replace("/{order_number}/", $mailData['onumber'] ,$body);
        $body = preg_replace("/{website_title}/", $setup->title ,$body);
        //dd($setup->from_email);
        $data = [
            'email_body' => $body
        ];


        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $setup->from_email;
        $objDemo->title = $setup->from_name;
        $objDemo->subject = $temp->email_subject;
// var_dump('here');
// die;
          
        try{
                $order = Order::findOrFail($id);

             $record = [
            'order' => $order,
            'cart' => unserialize(bzdecompress(utf8_decode($order->cart)))
        ];

                // $cart = unserialize(bzdecompress(utf8_decode($order->cart)));


// echo '<pre>';
// print_r($cart);
// die;

                // $cart = unserialize(bzdecompress(utf8_decode($order->cart)));

            // var_dump('here111');
            // die;
  Mail::send('print.order',$record, function ($message) use ($objDemo,$id, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
                $order = Order::findOrFail($id);
                $cart = unserialize(bzdecompress(utf8_decode($order->cart)));
                // $fileName = public_path('assets/temp_files/').str_random(4).time().'.pdf';
                $pdf = PDF::loadView('print.order', compact('order', 'cart'));
                  // $pdf =PDF::loadView('file.viewFile',compact('data'));
         $content = $pdf->output();
         $fileName= public_path('assets/temp_files/').str_random(4).time().'.pdf';
         file_put_contents($fileName, $content);

      // return $pdf->stream(" anyname.pdf");
                $message->attach($fileName);
            });
        }
        catch (\Exception $e){
             die($e->getMessage());
        }

        $files = glob('assets/temp_files/*'); //get all file names
        foreach($files as $file){
            if(is_file($file))
            unlink($file); //delete file
        }
    }

    public function sendAutoMail(array $mailData)
    {
               $emails = [$mailData['to'], 'info@phoenixrisingcollection.com'];

        $setup = Generalsetting::find(1);
        $temp = EmailTemplate::where('email_type','=',$mailData['type'])->first();
        $body = preg_replace("/{customer_name}/", $mailData['cname'] ,$temp->email_body);
        $body = preg_replace("/{order_amount}/", $mailData['oamount'] ,$body);
        $body = preg_replace("/{admin_name}/", $mailData['aname'] ,$body);
        $body = preg_replace("/{admin_email}/", $mailData['aemail'] ,$body);
        $body = preg_replace("/{order_number}/", $mailData['onumber'] ,$body);
        $body = preg_replace("/{website_title}/", $setup->title ,$body);

        $data = [
            'email_body' => $body
        ];

        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $setup->from_email;
        $objDemo->title = $setup->from_name;
        $objDemo->subject = $temp->email_subject;

        try{
            Mail::send('admin.email.mailbody',$data, function ($message) use ($objDemo, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            // die($e->getMessage());
        }
    }


  public function sendDeclinedMail(array $mailData)
    {
        
                       $emails = [$mailData['to'], 'info@phoenixrisingcollection.com'];

        // var_dump($mailData);
        // die;
        $setup = Generalsetting::find(1);

        $data = [
            'email_body' => $mailData['body'],
        ];

        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $setup->from_email;
        $objDemo->title = $setup->from_name;
        $objDemo->subject = $mailData['subject'];

        try{
            Mail::send('admin.email.mailbody',$data, function ($message) use ($objDemo, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            die($e->getMessage());
            // return $e->getMessage();
        }
        return true;
    }

    public function sendCustomMail(array $mailData)
    {
        
                       $emails = [$mailData['to'], 'info@phoenixrisingcollection.com'];

       $data = [
            'name' => $mailData['name'],
            'subject' => $mailData['subject'],
            'email_body' => $mailData['body'],
        ];

        // $data = [
        //     'email_body' => $data
        // ];

        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $mailData['from'];
        $objDemo->title = $mailData['name'];
        $objDemo->subject =  $mailData['subject'];

        try{
            Mail::send('admin.email.mailbody',$data, function ($message) use ($objDemo, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            // die($e->getMessage());
        }
    }

     public function sendContactMail(array $mailData)
    {        
        $emails = ['info@phoenixrisingcollection.com'];
         $data = [
            'name' => $mailData['name'],
            'subject' => $mailData['subject'],
            'email_body' => $mailData['body'],
        ];
        $objDemo = new \stdClass();
        $objDemo->from = $mailData['from'];
        $objDemo->title = $mailData['name'];
        $objDemo->subject =  $mailData['subject'];

        try{
            Mail::send('admin.email.mailbody',$data, function ($message) use ($objDemo, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            // die($e->getMessage());
        }
    } 

      public function sendSubsChangeMail(array $mailData)
    {        
         $data = [
            'email_body' => $mailData['body'],
        ];
        $objDemo = new \stdClass();
        $objDemo->to = $mailData['to'];
        $objDemo->from = $mailData['from'];
        $objDemo->title = $mailData['name'];
        $objDemo->subject =  $mailData['subject'];

        try{
            Mail::send('admin.email.mailbody',$data, function ($message) use ($objDemo) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($objDemo->to);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            // die($e->getMessage());
        }
    }

  public function sendOrderAdminMail(array $mailData)
    {
        
                       $emails = ['info@phoenixrisingcollection.com'];

        // var_dump($mailData);
        // die;
        $setup = Generalsetting::find(1);

        $data = [
            'email_body' => $mailData['body'],
        ];

        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $setup->from_email;
        $objDemo->title = $setup->from_name;
        $objDemo->subject = $mailData['subject'];

        try{
            Mail::send('admin.email.mailbody',$data, function ($message) use ($objDemo, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            die($e->getMessage());
            // return $e->getMessage();
        }
        return true;
    }

        public function sendWelcomeMail(array $mailData)
    {
        
                               $emails = [$mailData['to'], 'info@phoenixrisingcollection.com'];

        // var_dump($mailData);
        // die;
        $setup = Generalsetting::find(1);

        $data = [
            'name' => $mailData['name']
        ];

        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $setup->from_email;
        $objDemo->title = $setup->from_name;
        $objDemo->subject = $mailData['subject'];
        try{            
            // Welcome Email

              Mail::send('admin.email.welcome-email',$data, function ($message) use ($objDemo, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            die($e->getMessage());
            // return $e->getMessage();
        }
        return true;
    }    
        public function sendConfirmationMail(array $mailData)
    {
        
                                       $emails = [$mailData['to'], 'info@phoenixrisingcollection.com'];

        // var_dump($mailData);
        // die;
        $setup = Generalsetting::find(1);

        $data = [
            'name' => $mailData['name']
        ];

        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $setup->from_email;
        $objDemo->title = $setup->from_name;
        $objDemo->subject = $mailData['subject'];
        try{            
            // Welcome Email

              Mail::send('admin.email.order-comfirm',$data, function ($message) use ($objDemo,  $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            die($e->getMessage());
            // return $e->getMessage();
        }
        return true;
    }  

      public function sendDeliveredMail(array $mailData)
    {
                                               $emails = [$mailData['to'], 'info@phoenixrisingcollection.com'];

        // var_dump($mailData);
        // die;
        $setup = Generalsetting::find(1);

        $data = [
            'name' => $mailData['name']
        ];

        $objDemo = new \stdClass();
        // $objDemo->to = $mailData['to'];
        $objDemo->from = $setup->from_email;
        $objDemo->title = $setup->from_name;
        $objDemo->subject = $mailData['subject'];
        try{            
            // Welcome Email

              Mail::send('admin.email.delivered-mail',$data, function ($message) use ($objDemo, $emails) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($emails);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            die($e->getMessage());
            // return $e->getMessage();
        }
        return true;
    }

}