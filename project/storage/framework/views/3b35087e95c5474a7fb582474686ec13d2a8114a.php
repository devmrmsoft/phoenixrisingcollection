


<?php $__env->startSection('content'); ?>

<section class="About_Banner">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-1 d-flex flex-column justify-content-center align-items-start">
                <h1 class="heading">Privacy Policy</h1>
                <a class="text_about_us" href="<?php echo e(url('/')); ?>">Home <span>/</span> <span class="active">Privacy Policy</span></a>
            </div>
            <div class="col-md-6">
                <img src="<?php echo e(asset('assets/front//assets/img/Bottels.png')); ?>" class="img-fluid img1">
            </div>
        </div>
    </div>
</section>
<!-- About Banner -->

<section class="about-tiles privacy">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">Your privacy is important to us</h3>
                        <br>
                        <p class="">It is Phoenix Rising Collection Inc.'s policy to respect your privacy regarding any information we may collect while operating our phoenixrisingcollection. This Privacy Policy applies to (www.phoenixrisingcollection.com) (hereinafter, "us", "we", or "www.phoenixrisingcollection.com"). We respect your privacy and are committed to protecting personally identifiable information you may provide us through the Website. We have adopted this privacy policy ("Privacy Policy") to explain what information may be collected on our Website, how we use this information, and under what circumstances we may disclose the information to third parties. This Privacy Policy applies only to information we collect through the Website and does not apply to our collection of information from other sources.
                        </p>
                        <br>
                        <p>This Privacy Policy, together with the Terms of service posted on our Website, set forth the general rules and policies governing your use of our Website. Depending on your activities when visiting our Website, you may be required to agree to additional terms of service.</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="table-of-content">
                        <h4 class="">Contents</h4>
                        <ol>
                            <li><a href="#content1">Website Visitors</a></li>
                            <li><a href="#content2">Personally-Identifying Information</a></li>
                            <li><a href="#content3">Security</a></li>
                            <li><a href="#content4">Advertisements</a></li>
                            <li><a href="#content5">Phoenix Rising Collection Inc. uses Google Adwords for remarketing</a></li>
                            <li><a href="#content6">Protection of Certain Personally-Identifying Information</a></li>
                            <li><a href="#content7">Aggregated Statistics</a></li>
                            <li><a href="#content8">Cookies</a></li>
                            <li><a href="#content9">E-commerce</a></li>
                            <li><a href="#content10">Privacy Policy Changes</a></li>
                            <li><a href="#content11">Contact Information & Credit</a></li>
                            
                        </ol>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content1">
                        <h4 class="">1. Website Visitors</h4>
                        <p class="">Like most website operators, Phoenix Rising Collection Inc. collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Phoenix Rising Collection Inc.'s purpose in collecting non-personally identifying information is to better understand how Phoenix Rising Collection Inc.'s visitors use its website. From time to time, Phoenix Rising Collection Inc. may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.
                        </p>
                        <br>
                        <p>Phoenix Rising Collection Inc. also collects potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on www.phoenixrisingcollection.com blog posts. Phoenix Rising Collection Inc. only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.</p> <small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content2">
                        <h3 class="">2. Personally-Identifying Information</h3>
                        <p class="">Certain visitors to Phoenix Rising Collection Inc.'s websites choose to interact with Phoenix Rising Collection Inc. in ways that require Phoenix Rising Collection Inc. to gather personally-identifying information. The amount and type of information that Phoenix Rising Collection Inc. gathers depend on the nature of the interaction. For example, we ask visitors who leave a comment at www.phoenixrisingcollection.com to provide a username and email address.</p><small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
           <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content3">
                        <h3 class="">3. Security</h3>
                        <p class="">The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.
                        </p>  <small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content4">
                        <h3 class="">4. Advertisements</h3>
                        <p class="">Ads appearing on our website may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by Phoenix Rising Collection Inc. and does not cover the use of cookies by any advertisers.
                        </p> <small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content5">
                        <h3 class="">5. Phoenix Rising Collection Inc. uses Facebook and Google AdWords for remarketing</h3>
                        <p class="">Phoenix Rising Collection Inc. uses remarketing services to advertise on third party websites (including Facebook and Google) to previous visitors to our site. It could mean that we advertise to previous visitors who haven't completed a task on our site, for example using the contact form to make an inquiry. This could be in the form of an advertisement on the Google search results page or a site in the Google Display Network. Third-party vendors, including Google, use cookies to serve ads based on someone's past visits. Of course, any data collected will be used in accordance with our own privacy policy and Google's privacy policy.
                        </p>
                        <br>
                        <p>You can set preferences for how Google advertises to you using the Google Ad Preferences page, and if you want to you can opt-out of interest-based advertising entirely by cookie settings or permanently using a browser plugin.
</p> <small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
           <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content6">
                        <h3 class="">6. Protection of Certain Personally-Identifying Information</h3>
                        <p class="">Phoenix Rising Collection Inc. discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on Phoenix Rising Collection Inc.'s behalf or to provide services available at Phoenix Rising Collection Inc.'s website, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using Phoenix Rising Collection Inc.'s website, you consent to the transfer of such information to them. Phoenix Rising Collection Inc. will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, Phoenix Rising Collection Inc. discloses potentially personally-identifying and personally-identifying information only in response to a subpoena, court order or other governmental requests, or when Phoenix Rising Collection Inc. believes in good faith that disclosure is reasonably necessary to protect the property or rights of Phoenix Rising Collection Inc., third parties or the public at large.
                        </p>
                        <br>
                        <p>If you are a registered user of www.phoenixrisingcollection.com and have supplied your email address, Phoenix Rising Collection Inc. may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what's going on with Phoenix Rising Collection Inc. and our products. We primarily use our blog to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. Phoenix Rising Collection Inc. takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.
</p><small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content7">
                        <h3 class="">7. Aggregated Statistics</h3>
                        <p class="">Phoenix Rising Collection Inc. may collect statistics about the behavior of visitors to its website. Phoenix Rising Collection Inc. may display this information publicly or provide it to others. However, Phoenix Rising Collection Inc. does not disclose your personally-identifying information.
                        </p> <small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content8">
                        <h3 class="">8. Cookies</h3>
                        <p class="">To enrich and perfect your online experience, Phoenix Rising Collection Inc. uses "Cookies", similar technologies and services provided by others to display personalized content, appropriate advertising and store your preferences on your computer.
                        </p>
                        <br>
                        <p>A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. Phoenix Rising Collection Inc. uses cookies to help Phoenix Rising Collection Inc. identify and track visitors, their usage of www.phoenixrisingcollection.com, and their website access preferences. Phoenix Rising Collection Inc. visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using Phoenix Rising Collection Inc.'s websites, with the drawback that certain features of Phoenix Rising Collection Inc.'s websites may not function properly without the aid of cookies.
</p>
<br>
<p>By continuing to navigate our website without changing your cookie settings, you hereby acknowledge and agree to Phoenix Rising Collection Inc.'s use of cookies.
</p><small><a href="#table-of-content">Back to table of contents</a></small> 
                    </div>
                    
                </div>
            </div>
         
        </div>
        
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content9">
                        <h3 class="">9. E-commerce</h3>
                        <p class="">Those who engage in transactions with Phoenix Rising Collection Inc. – by purchasing Phoenix Rising Collection Inc.'s services or products, are asked to provide additional information, including as necessary the personal and financial information required to process those transactions. In each case, Phoenix Rising Collection Inc. collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor's interaction with Phoenix Rising Collection Inc.. Phoenix Rising Collection Inc. does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.</p> <small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
           <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles" id="content10">
                        <h3 class="">10. Privacy Policy Changes</h3>
                        <p class="">Although most changes are likely to be minor, Phoenix Rising Collection Inc. may change its Privacy Policy from time to time, and in Phoenix Rising Collection Inc.'s sole discretion. Phoenix Rising Collection Inc. encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p>  <small><a href="#table-of-content">Back to table of contents</a></small>
                    </div>
                    
                </div>
            </div>
         
        </div>
    </div>
</section>
<!-- About Tiles -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>