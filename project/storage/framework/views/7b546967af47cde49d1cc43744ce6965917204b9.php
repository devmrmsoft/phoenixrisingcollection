<link rel="stylesheet" type="text/css" href="<?php echo e(url('assets/front/assets/css/dashboard.css')); ?>">
        <div class="col-lg-4">
          <div class="user-profile-info-area">
            <ul class="links">
                <?php 

                  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
                  {
                    $link = "https"; 
                  }
                  else
                  {
                    $link = "http"; 
                      
                    // Here append the common URL characters. 
                    $link .= "://"; 
                      
                    // Append the host(domain name, ip) to the URL. 
                    $link .= $_SERVER['HTTP_HOST']; 
                      
                    // Append the requested resource location to the URL 
                    $link .= $_SERVER['REQUEST_URI']; 
                  }      

                ?>
              <li class="<?php echo e($link == route('user-dashboard') ? 'active':''); ?>">
                <a href="<?php echo e(route('user-dashboard')); ?>">
                  <?php echo e($langg->lang200); ?>

                </a>
              </li>
              
              <?php if(Auth::user()->IsVendor()): ?>
                <li>
                  <a href="<?php echo e(route('vendor-dashboard')); ?>">
                    <?php echo e($langg->lang230); ?>

                  </a>
                </li>
              <?php endif; ?>

              <li class="<?php echo e($link == route('user-orders') ? 'active':''); ?>">
                <a href="<?php echo e(route('user-orders')); ?>">
                  <?php echo e($langg->lang201); ?>

                </a>
              </li>

              <?php if($gs->is_affilate == 1): ?>


              <?php endif; ?>


              <!-- <li class="<?php echo e($link == route('user-order-track') ? 'active':''); ?>">
                  <a href="<?php echo e(route('user-order-track')); ?>"><?php echo e($langg->lang772); ?></a>
              </li> -->

              <li class="<?php echo e($link == route('user-profile') ? 'active':''); ?>">
                <a href="<?php echo e(route('user-profile')); ?>">
                  <?php echo e($langg->lang205); ?>

                </a>
              </li>

              <li class="<?php echo e($link == route('user-reset') ? 'active':''); ?>">
                <a href="<?php echo e(route('user-reset')); ?>">
                 <?php echo e($langg->lang206); ?>

                </a>
              </li>

              <li>
                <a href="<?php echo e(route('user-logout')); ?>">
                  <?php echo e($langg->lang207); ?>

                </a>
              </li>

            </ul>
          </div>
          <?php if($gs->reg_vendor == 1): ?>
          
          <?php endif; ?>
        </div>