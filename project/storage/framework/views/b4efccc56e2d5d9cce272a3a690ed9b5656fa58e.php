
<?php $__env->startSection('content'); ?>

  <!-- Blog Details Area Start -->
  <section class="blog-details" id="blog-details">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="blog-title">
            <?php echo e($blog->title); ?>

          </h3>
          <div class="blog-content">
            <div class="feature-image">
              <img class="img-fluid" src="<?php echo e(asset('assets/images/blogs/'.$blog->photo)); ?>" alt="">
            </div>
            <div class="content">
              <div class="blog-detail">
                <?php echo $blog->details; ?>

              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  <!-- Blog Details Area End-->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>