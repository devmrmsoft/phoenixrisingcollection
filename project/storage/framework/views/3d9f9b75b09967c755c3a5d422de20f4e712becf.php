

<?php $__env->startSection('content'); ?>

<!-- Customize Your Hair Oil Start -->
    <section class="customization">
        <div class="container">
            <div class="title text-center">
                <h1>Customize Your Oil</h1>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
            </div>
            <?php if($type == 'body'): ?>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Body Oil</h1>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <h3>Organic oil</h3>
                            <img src="<?php echo e(asset('assets/front/assets/img/zigzag.png')); ?>" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="<?php echo e(url('/flowers?oils=body')); ?>" class="product-btn" type="button">customize your oil</a>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                        <img src="<?php echo e(asset('assets/front/assets/img/customize-2.png')); ?>" class="img-fluid">
                </div>
            </div>
            <?php else: ?>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                        <img src="<?php echo e(asset('assets/front/assets/img/customize-1.png')); ?>" class="img-fluid">
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Hair Oil</h1>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <h3>Organic oil</h3>
                            <img src="<?php echo e(asset('assets/front/assets/img/zigzag.png')); ?>" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="<?php echo e(url('/flowers?oils=hair')); ?>" class="product-btn" type="button">customize your oil</a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <!-- Customize Your Hair Oil End -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>