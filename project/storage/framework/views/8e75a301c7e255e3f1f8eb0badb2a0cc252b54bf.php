

<?php $__env->startSection('content'); ?>

<section class="creation-1">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Oil</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>
        <div class="row">
            <div class="col-md-12 creation-1-column">
                <div class="choose-oil">
                    <div class="quiz-text text-center">
                        <h1>Choose Oil</h1>
                    </div>
                    <form action="<?php echo e(url('/flowers')); ?>" method="get">
                        <div class="oil d-flex justify-content-center align-items-center">
                            <ul class="nav w-100">
                                <li class="nav-item text-center">
                                    <input type="radio" name="oils" required="" value="hair" id="hair" class="input-hidden" />
                                    <label for="hair">
                                        <img src="<?php echo e(asset('assets/front/assets/img/hair.png')); ?>" class="img-fluid">
                                        <h5>HAIR</h5>
                                    </label>
                                </li>
                                <li class="nav-item text-center">
                                    <input type="radio" name="oils" value="body" required="" id="body" class="input-hidden" />
                                    <label for="body">
                                        <img src="<?php echo e(asset('assets/front/assets/img/body.png')); ?>" class="img-fluid"/>
                                        <h5>BODY</h5>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="next-button text-center">
                            <button type="submit" class="next-btn border-0" style="visibility: hidden">next</a>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Creation Level 1 --> 
<script src="//code.jquery.com/jquery-3.5.1.js"></script>
<script>
    $('.creation-1 .creation-1-column .choose-oil .next-button').css('padding',0);
    $('input[name="oils"]').on('click', function(){
        $('.creation-1 .creation-1-column .choose-oil .next-button button').trigger('click');
    })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>