


<?php $__env->startSection('content'); ?>

<section class="About_Banner">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-1 d-flex flex-column justify-content-center align-items-start">
                <h1 class="heading">Terms & Conditions</h1>
                <a class="text_about_us" href="<?php echo e(url('/')); ?>">Home <span>/</span> <span class="active">Terms & Conditions</span></a>
            </div>
            <div class="col-md-6">
                <img src="<?php echo e(asset('assets/front//assets/img/Bottels.png')); ?>" class="img-fluid img1">
            </div>
        </div>
    </div>
</section>
<!-- About Banner -->

<section class="about-tiles">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <!--<h1 class="">Lorem Ipsum</h1>-->
                        <p class="">Welcome to the Phoenix Rising Collection Inc website (the “Site”).  These Terms of Service are a contract between you and Phoenix Rising Collection Inc. (“the Company,” “we,” “our,” and “us”) (the “Agreement”).  The Agreement governs your purchase and use of the Site, as well as any services provided to you by us.  <b>PLEASE READ THIS AGREEMENT.</b>
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">Limitation of Product Liability:</h3>
                        <p class="">By purchasing raw materials or curated products on this site you recognize that it is your responsibility to stay within the recommended dosage and the guide provided on the site. Phoenix Rising Collection Inc’s liability is limited to the product cost of the product you purchase on this site. Any claims, issues, complaints, etc. for the finished products you curated using the ingredients on this site are the responsibility of you as the manufacturer.  It is your responsibility to read our guides and follow our dosage to complete all of your own product mixings prior to shipping the finished product.
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">Ship times may vary:</h3>
                        <p class="">By purchasing products on this site you recognize that any lead times quoted are best estimates and Phoenix Rising Collection Inc is not liable for any delays.  Phoenix Rising Collection Inc will do its best to ship products in a timely fashion but from time to time extenuating circumstances may make it difficult to meet these processing time estimates.  By purchasing on this site you recognize that Phoenix Rising Collection Inc can not be held liable in any way for any potential delays.
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">REVIEWS AND COMMENTS</h3>
                        <p class="">Except as otherwise provided elsewhere in this Agreement or on the Site, anything that you submit or post to the Site and/or provide us, including without limitation, ideas, know-how, techniques, questions, reviews, comments, and suggestions (collectively, “Submissions”) is and will be treated as non-confidential and non-proprietary, and we shall have the royalty-free, worldwide, perpetual, irrevocable and transferable right to use, copy, distribute, display, publish, perform, sell, lease, transmit, adapt, create derivative works from such Submissions by any means and in any form, and to translate, modify, reverse-engineer, disassemble, or decompile such Submissions.  All Submissions shall automatically become our sole and exclusive property and shall not be returned to you.
                        </p>
                        <p>In addition to the rights applicable to any Submission, when you post comments or reviews to the Site, you also grant us the right to use the name that you submit with any review, comment, or other Content, if any, in connection with such review, comment, or other content.  You represent and warrant that you own or otherwise control all of the rights to the reviews, comments and other Content that you post on this Site and that use of your reviews, comments, or other Content by us will not infringe upon or violate the rights of any third party.  You shall not use a false email address, pretend to be someone other than yourself or otherwise mislead us or third-parties as to the origin of any Submissions or Content.  We may, but shall not be obligated to, remove or edit any Submissions (including comments or reviews) for any reason.</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
           <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">INTELLECTUAL PROPERTY</h3>
                        <p class="">All text, graphics, button icons, images, audio clips, and software (collectively, “Content”), belongs exclusively to Phoenix Rising Collection Inc., or its affiliates.  The collection, arrangement, and assembly of all Content on this Site (the “Compilation”) belongs exclusively to Phoenix Rising Collection Inc. or its affiliates.  All software used on this Site (the “Software”) is the property of Phoenix Rising Collection Inc., its affiliates or its Software suppliers.  The Content, the Compilation and the Software are all protected by U.S. and international copyright laws.  The use of any of our trademarks or service marks without our express written consent is strictly prohibited.  You may not use our trademarks or service marks in connection with any product or service in any way that is likely to cause confusion.  You may not use our trademarks or service marks in any manner that disparages or discredits us.  You may not use any of our trademarks or service marks in meta tags without prior explicit consent.
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">PRIVACY POLICY</h3>
                        <p class="">Please review our Privacy Policy to understand our practices.  Our Privacy Policy is available at www.phoenixrisingcollection.com/privacy-policy.  While our Privacy Policy includes information about how we may use your personal information and about our data security, we make no express or implied promise regarding the security used by the Site or that your information will not be accessed in an unauthorized manner despite the security we provide.
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">TERMINATION AND EFFECT OF TERMINATION</h3>
                        <p class="">In addition to any other legal or equitable remedies, we may, without prior notice to you, immediately terminate the Agreement or revoke any or all of your rights granted under this Agreement.  Upon any termination of this Agreement, you shall immediately cease all access to and use of the Site and we shall, in addition to any other legal or equitable remedies, immediately revoke all password(s) and account identification issued to you and deny your access to and use of this Site in whole or in part.  Any termination of this Agreement shall not affect the respective rights and obligations (including without limitation, payment obligations) of the parties arising before the date of termination.
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
           <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">INTERNATIONAL ACCESS</h3>
                        <p class="">This Site may be accessed from countries other than the United States.  This Site may contain products or references to products that are not available outside of the United States.  Any such references do not imply that such products will be made available outside the United States.  If you access and use this Site outside the United States you are responsible for complying with your local laws and regulations.
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">PAYMENT OPTIONS ACCEPTED</h3>
                        <p class="">We accept all major payment options. Payment may be made via Visa, MasterCard and American Express credit cards or by bank transfer or cash deposit into the Phoenix Rising Collection Inc bank account, the details of which will be provided during the checkout process.
                        </p>
                    </div>
                    
                </div>
            </div>
         
        </div>
          <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">RETURN AND REFUNDS POLICY</h3>
                        <p class="">The provision of goods and services by Phoenix Rising Collection Inc is subject to availability. In cases of unavailability, Phoenix Rising Collection Inc will contact you with options.
                        </p>
                        <p>An order can be cancelled if payment has not been made yet for the order by the customer. Once payment has been made by the customer for an order, the order can only be cancelled before the order has been dispatched by Phoenix Rising Collection Inc.
                        </p>
                        <p>We offer no refund unless damage was caused by our faults. If a product is damaged or broken when you receive it due to our faults, you must inform Phoenix Rising Collection Inc within one working day of receiving it. We will then make arrangements for a replacement product to be sent to you on our account or to refund you in full for the damaged or broken product (provided the damage is caused by our faults).
</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
        <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">TYPOGRAPHICAL ERRORS</h3>
                        <p class="">In the event a product is listed at an incorrect price or with incorrect information due to typographical error or error in pricing or product information received from our suppliers, we shall have the right to refuse or cancel any orders placed for product listed at the incorrect price. We shall have the right to refuse or cancel any such orders whether or not the order has been confirmed and your payment card charged.  If your payment card has already been charged for the purchase and your order is canceled, we shall immediately issue a credit to your payment card account in the amount of the charge.</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
           <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">LINKS</h3>
                        <p class="">This Site may contain links to other sites on the Internet that are owned and operated by third parties.  You acknowledge that we are not responsible for the operation of or content located on or through any such site.</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
              <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">COPYRIGHT COMPLAINTS</h3>
                        <p class="">If you believe that your work has been copied in a way that constitutes copyright infringement, please contact us.</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
           <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">APPLICABLE LAW</h3>
                        <p class="">This Site is created and controlled by us in the United States.  As such, the laws of the United States will govern any and all claims related to or arising out of your use of the Site or these disclaimers, terms, and conditions, without giving effect to any principles of conflicts of laws.</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
             <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">LIMITATION OF CLAIMS</h3>
                        <p class="">You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of, related to or connected with the use of the Site or this Agreement must be filed within one (1) year after such claim or cause of action arose or be forever banned.  This provision only applies if you purchased products from the Site for resale to others (that is, it does not apply to consumer transactions).</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
        
             <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">SEVERABILITY</h3>
                        <p class="">If any of these provisions shall be deemed invalid, void, or for any reason unenforceable, that condition shall be deemed several and shall not affect the validity and enforceability of any remaining provision.</p>
                    </div>
                    
                </div>
            </div>
         
        </div>

             <div class="row">
            <div class="col-md-12 section_col">
                <div class="col_pad ">
                    <div class="tiles">
                        <h3 class="">OUR ADDRESS</h3>
                        <p class="">Please send any questions or comments (including all inquiries unrelated to copyright infringement) regarding this Site to:</p>
                    </div>
                    
                </div>
            </div>
         
        </div>
    </div>
</section>
<!-- About Tiles -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>