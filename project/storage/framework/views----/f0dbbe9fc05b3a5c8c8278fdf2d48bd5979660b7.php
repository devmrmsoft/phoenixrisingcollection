 

<?php $__env->startSection('content'); ?>  
					<div class="content-area">
						<div class="mr-breadcrumb">
							<div class="row">
								<div class="col-lg-12">
									<h4 class="heading"><?php echo e(__("Discounts")); ?></h4>
									<ul class="links">
										<li>
											<a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo e(__("Dashboard")); ?> </a>
										</li>
										<li>
											<a href="javascript:;"><?php echo e(__("Discounts")); ?></a>
										</li>
										<li>
											<a href="<?php echo e(route('admin-vendor-index')); ?>"><?php echo e(__("Discounts List")); ?></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="product-area">
							<div class="row">
								<div class="col-lg-12">


									<div class="mr-table allproduct">
										<?php echo $__env->make('includes.admin.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
										<div class="table-responsiv">
											<table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th>Serial</th>
	                                                  <th><?php echo e(__("Discount Name")); ?></th>
	                                                  <th><?php echo e(__("Status")); ?></th>
													</tr>
												</thead>
												<tbody>
													<?php $serial = 1; ?>
													<?php $__currentLoopData = $discounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $discount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<tr>
														<td> #<?php echo e($serial); ?></td>
														<td> <?php echo e($discount->name); ?></td>
														<td> <a href="<?php echo e(route('admin-discount-edit',[$discount->id])); ?>"> <i class="fas fa-edit"></i> Edit</a> || <a href="<?php echo e(route('admin-discount-delete',[$discount->id])); ?>"> <i class="fas fa-trash-alt"></i> Delete</a></td>
													</tr>
													<?php $serial += 1; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>




<?php $__env->stopSection(); ?>    

<?php $__env->startSection('scripts'); ?>


    <script type="text/javascript">

		var table = $('#geniustable').DataTable({
			   ordering: false,
               processing: true,
               serverSide: true,
               ajax: '<?php echo e(route('admin-discount-datatables')); ?>',
               columns: [
                        { data: 'shop_name', name: 'shop_name' },
                        { data: 'email', name: 'email' },
                        { data: 'shop_number', name: 'shop_number' },
                        { data: 'status', searchable: false, orderable: false},
            			{ data: 'action', searchable: false, orderable: false }
                     ],
               language : {
                	processing: '<img src="<?php echo e(asset('assets/images/'.$gs->admin_loader)); ?>">'
                },
				drawCallback : function( settings ) {
	    				$('.select').niceSelect();	
				}
            });

	    				$('.select1').niceSelect();	
																
    </script>


<script type="text/javascript">


      	$(function() {
        $(".btn-area").append('<div class="col-sm-4 table-contents">'+
        	'<a class="add-btn" href="<?php echo e(route('admin-discount-create')); ?>">'+
          '<i class="fas fa-plus"></i> <span class="remove-mobile"><?php echo e(__("Add New Discount List")); ?><span>'+
          '</a>'+
          '</div>');
      });			

</script>


    
<?php $__env->stopSection(); ?>   
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>