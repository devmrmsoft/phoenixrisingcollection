<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/css/animate.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/css/owl.theme.default.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/css/style.css')); ?>">
    <link rel="icon" href="<?php echo e(asset('assets/front/assets/img/16x16.png')); ?>" type="image" sizes="16x16">
    <title>Phoenix Rising Collection</title>

</head>
<body>

    <div class="preloader js-preloader flex-center">
        <div class="spinner">
            <img src="<?php echo e(asset('assets/front/assets/img/preloader.gif')); ?>" class="img-fluid">
        </div>
    </div>
    <!-- Preloader -->
    <header class="top-header">
        <div class="my-account">
            <?php if(!Auth::check()): ?>
            <a href="<?php echo e(url('/login')); ?>">Login</a>
            <a href="<?php echo e(url('/register')); ?>">Register</a>
            <?php else: ?>
            <a href="<?php echo e(route('user-dashboard')); ?>">My Account</a>
            <a href="<?php echo e(url('/user/logout')); ?>">Logout</a>
            <?php endif; ?>
        </div>
        <p>TRY OUR NEW CUSTOM BODY oil + hair oil</p>
        <div class="my-account">
            <a href="<?php echo e(url('/carts')); ?>">Cart</a>
        </div>
    </header>
    
    <!-- Top Header -->

    <header class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 navigation-column d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="<?php echo e(url('/')); ?>" class="nav-link">home</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(url('/about')); ?>" class="nav-link">About</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(url('/buildown')); ?>" class="nav-link">Build Your own</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(url('/')); ?>" class="nav-link">
                                <img src="<?php echo e(asset('assets/front/assets/img/Phoenix-logo.png')); ?>" alt="Website Logo" class="img-fluid">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(url('/shop')); ?>" class="nav-link">shop</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(url('/blog')); ?>" class="nav-link">blog</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(url('/contact')); ?>" class="nav-link">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section ends -->

    <?php echo $__env->yieldContent('content'); ?>
    <!-- footer-section starts -->
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 site-footer-column">
                    <div class="inner-footer-1">
                        <img src="<?php echo e(asset('assets/front/assets/img/footer-logo.png')); ?>" class="img-fluid">
                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</p>
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="" class="nav-link"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 site-footer-column d-flex justify-content-center">
                    <div class="inner-footer-2">
                        <h3>Locate Us</h3>
                        <p>No 40 Baria Sreet 133/2</p>
                        <p>+ (156) 1800-366-6666</p>
                        <p>Liz-82@example.com</p>
                    </div>
                </div>
                <div class="col-md-3 site-footer-column d-flex justify-content-center">
                    <div class="inner-footer-3">
                        <h3>Profile</h3>
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="#" class="nav-link">My Account</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Checkout</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Order Tracking</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Help & Suppport</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 site-footer-column">
                    <div class="inner-footer-4">
                        <h3>Newsletter</h3>
                        <p>Subscribe to our newsletter</p>
                        <input type="email" name="email" id="email" placeholder="Email">
                    </div>
                    <div class="copyrights">
                        <p>@ 2020 Phoenix Rising Collection</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer-section ends -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
    <script src="<?php echo e(asset('assets/front/assets/js/jquery.preloadinator.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/assets/js/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/assets/js/wow.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/assets/js/main.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/front/assets/js/main-slider.js')); ?>"></script>
    <script>
        new WOW().init();
    </script>
    <?php echo $__env->yieldContent('scripts'); ?>
</body>
</html>

    