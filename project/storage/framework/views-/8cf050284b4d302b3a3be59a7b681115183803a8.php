

<?php $__env->startSection('content'); ?>

<section class="product-creation">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Oil</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>
        <div class="row">
            <div class="col-md-12 product-creation-column">
                <div class="choose-flowers">
                    <div class="quiz-text text-center">
                        <h1>Choose Essential Oil Blends</h1>
                        <h1>1 Max</h1>
                    </div>
                    <form class="flowers d-flex justify-content-center align-items-center" action="<?php echo e(url('/final')); ?>" method="get">
                        <input type="hidden" name="oils" value="<?php echo e($product_type); ?>">
                        <?php $__currentLoopData = $flowers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flower): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <input type="hidden" name="flowers[]" value="<?php echo e($flower); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $crystals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $crystal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <input type="hidden" name="crystals[]" value="<?php echo e($crystal); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <nav class="nav flower-container text-center">
                            <?php $__currentLoopData = $ingredients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ingredient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="nav-item">
                                <input type="checkbox" name="blends[]" id="<?php echo e($ingredient->id); ?>" value="<?php echo e($ingredient->id); ?>" class="input-hidden check"/>
                                <label for="<?php echo e($ingredient->id); ?>">
                                    <?php if($ingredient->photo == 'No image found'): ?>
                                    <img src="<?php echo e(asset('assets/front/assets/img/flower-1.jpg')); ?>" class="img-fluid  custome-images">
                                    <?php else: ?>
                                    <img src="assets/images/ingredients/<?php echo e($ingredient->photo); ?>" class="img-fluid  custome-images">
                                    <?php endif; ?>
                                    <!-- <img src="./assets/img/flower-1.png" class="img-fluid custome-images"> -->
                                    <p class="d-block"><?php echo e($ingredient->title); ?></p>
                                </label>
                                <?php if($ingredient->ingredients || $ingredient->description): ?>
                                <p>
                                    <?php if($ingredient->ingredients): ?>
                                    Ingredients: <?php echo e($ingredient->ingredients); ?> 
                                    <?php endif; ?>
                                    <?php if($ingredient->description): ?>
                                    <a href="#" data-toggle="modal" data-target="#detail-popup<?php echo e($ingredient->id); ?>">read more </a>
                                    <?php endif; ?>
                                </p>
                                <?php endif; ?>
                            </li>
                            <div class="modal fade" id="detail-popup<?php echo e($ingredient->id); ?>" tabindex="-1" role="dialog" aria-labelledby="detail-popup<?php echo e($ingredient->id); ?>-title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <?php echo $ingredient->description; ?>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </nav>
                        <div class="col-md-12 product-creation-column">
                            <div class="next-page">
                                <button href="#" type="submit" class="next-btn border-0">Next</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</section>

<script type="text/javascript">
    var checks = document.querySelectorAll(".check");
var max = 1;
for (var i = 0; i < checks.length; i++)
  checks[i].onclick = selectiveCheck;
function selectiveCheck (event) {
  var checkedChecks = document.querySelectorAll(".check:checked");
  if (checkedChecks.length >= max + 1)
    return false;
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>