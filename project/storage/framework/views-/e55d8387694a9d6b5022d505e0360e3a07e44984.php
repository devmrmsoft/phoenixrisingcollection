

<?php $__env->startSection('content'); ?>
<form method="post" action="<?php echo e(url('/submitOils')); ?>">
	<?php echo e(csrf_field()); ?>

<section class="creation-3">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Oil</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>
        <input type="hidden" name="oils" value="<?php echo e($product_type); ?>">
        <?php $__currentLoopData = $flowers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flower): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <input type="hidden" name="flowers[]" value="<?php echo e($flower); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $crystals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $crystal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <input type="hidden" name="crystals[]" value="<?php echo e($crystal); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $blends; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blend): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <input type="hidden" name="blends[]" value="<?php echo e($blend); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-12 creation-3-column">
            <div class="last-stage">
                <div class="quiz-text text-center">
                    <h1>Size</h1>
                </div>
                <div class="size d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="nav-item size-item">
                            <input type="radio" name="bottle" value="<?php echo e($item); ?>" id="<?php echo e($item); ?>" class="input-hidden" />
                            <label for="<?php echo e($item); ?>">
                                <img src="<?php echo e(asset('assets/front/assets/img/bottle-4oz.png')); ?>" class="img-fluid">
                                <div class="inner-container">
                                    <h6><?php echo e($item); ?></h6>
                                    <p>$<?php echo e($prices[$key]); ?></p>
                                </div>
                            </label>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <!-- <li class="nav-item size-item">
                            <input type="radio" name="bottle" value="8" id="happy" class="input-hidden" />
                            <label for="happy">
                                <img src="<?php echo e(asset('assets/front/assets/img/bottle-4oz.png')); ?>" class="img-fluid">
                                <div class="inner-container">
                                    <h6>8.oz</h6>
                                    <p>$60.00</p>
                                </div>
                            </label>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12 creation-3-column">
            <div class="last-stage">
                <div class="quiz-text text-center">
                    <h1>Frequency</h1>
                </div>
                <div class="size d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" value="yearly" id="yearly" class="input-hidden" />
                            <label for="yearly">
                                <p>Every Months</p>
                            </label>
                        </li>
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" value="3-month" id="3-month" class="input-hidden" />
                            <label for="3-month">
                                <p>Every 3 Months</p>
                            </label>
                        </li>
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" value="6-month" id="6-month" class="input-hidden" />
                            <label for="6-month">
                                <p>Every 6 Months</p>
                            </label>
                        </li>
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" value="once" id="once" class="input-hidden" />
                            <label for="once">
                                <p>Just Once</p>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12 creation-3-column">
            <div class="last-stage">
                <div class="quiz-text text-center">
                    <h1>Boost Your Set?</h1>
                    <h3>Added Products</h3>
                </div>
                <div class="added-product d-flex justify-content-center align-items-start">
                	<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="product">
                        <div class="image text-center">
                            <?php if($product->photo == 'No image found'): ?>
                            <img src="<?php echo e(asset('assets/front/assets/img/banner.jpg')); ?>" class="img-fluid">
                            <?php else: ?>
                            <img src="assets/images/products/<?php echo e($product->photo); ?>" width="50" class="img-fluid">
                            <?php endif; ?>
                            <h3>$<?php echo e($product->price); ?></h3>
                        </div>
                        <div class="detail">
                            <h3><?php echo e($product->name); ?></h3>
                            <p><?php echo e(strip_tags($product->details)); ?></p>
                            <a class="ajaxCart" href="<?php echo e(url('/addcart/'.$product->id)); ?>">ADD</a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-12 creation-3-column"> 
            <div class="next-button text-center">
                <div class="checkbox">
                    <input type="checkbox" name="gift" value="1" id="gift">
                    <label for="gift">
                        <h3 class="d-inline">is this a gift?</h3>
                        <img src="<?php echo e(asset('assets/front/assets/img/gift.png')); ?>" class="img-fluid">
                    </label>
                </div>
                <button type="submit" class="border-0">NEXT</button>
            </div>
        </div>
    </div>
</section>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>