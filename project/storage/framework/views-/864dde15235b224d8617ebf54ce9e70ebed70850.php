<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/front/assets/css/style.css')); ?>">
    <link rel="icon" href="<?php echo e(asset('assets/front/assets/img/16x16.png')); ?>" type="image" sizes="16x16">
    <title>Login</title>
</head>
<body>

    <section class="login-form">
    <div class="container">
        <div class="row">
            <div class="div col-md-5 offset-md-7 m-auto login-form-column">
                <div class="inner-container-login">
                    <form class="login" action="<?php echo e(route('front.loginsubmit')); ?>" method="POST">
                        <?php echo e(csrf_field()); ?>

                        <h1 class="text-center text-capitalize">login to continue</h1>
                        <div class="md-form mb-0">
                            <input type="email" id="login-user" name="email" class="form-control" placeholder="Email" required>
                        </div>
                        <div class="md-form mb-0">
                            <input type="password" id="pass-user" name="password" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="forgot-remember">
                            <div class="inner-container">
                                <input type="checkbox" name="remember" id="remember-me">
                                <label for="remember-me">
                                    <p>Remember me</p>
                                </label>
                            </div>
                            <div class="inner-container">
                                <a href="#">Forgot Password?</a>
                            </div>
                        </div>
                        <button type="submit" class="login-btn mt-4 w-100 border-0">signup</button>
                    </form>
                    <div class="alert-success alert" style="display: none;">
                            <p></p>
                        </div>
                        <div class="alert-danger alert" style="display: none;">
                            <p></p>
                        </div>
                    <div class="signup-social-icons text-center">
                        <a href="#">or sign up using</a>
                        <ul class="nav justify-content-center mt-3">
                            <li class="nav-item">
                                <a href="#" class="">
                                    <img src="<?php echo e(asset('assets/front/assets/img/facebook.png')); ?>">
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="">
                                    <img src="<?php echo e(asset('assets/front/assets/img/gmail.png')); ?>">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    </section>




    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
    <script type="text/javascript">
        $("form.login").on('submit', function (e) {
      e.preventDefault();
      var $this = $(this).parent();
      $this.find('button').prop('disabled', true);
      // $this.find('.alert-info').show();
      // var processdata = $this.find('.mprocessdata').val();
      // $this.find('.alert-info p').html(processdata);
      $.ajax({
        method: "POST",
        url: $(this).prop('action'),
        data: new FormData(this),
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
          if (data == 1) {
            window.location = '<?php echo e(route('front.index')); ?>';
          } else {

            if ((data.errors)) {
              $this.find('.alert-success').hide();
              // $this.find('.alert-info').hide();
              $this.find('.alert-danger').show();
              $this.find('.alert-danger ul').html('');
              for (var error in data.errors) {
                $this.find('.alert-danger p').html(data.errors[error]);
              }
              $this.find('button').prop('disabled', false);
            } else {
              // $this.find('.alert-info').hide();
              $this.find('.alert-danger').hide();
              $this.find('.alert-success').show();
              $this.find('.alert-success p').html(data);
              $this.find('button').prop('disabled', false);
            }
          }

          $('.refresh_code').click();

        }
      });

    });
    </script>
</body>
</html>