
<?php $__env->startSection('content'); ?>


<section class="">
    <div class="container">
        <div class="creation-3-column">
            <div class="last-stage">
                <div class="added-product row">
                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="product w-100 mb-4">
                            <div class="image text-center">
                                <?php if($product->photo == 'No image found'): ?>
                                    <img src="<?php echo e(asset('assets/front/assets/img/banner.jpg')); ?>" class="img-fluid">
                                    <?php else: ?>
                                    <img src="assets/images/products/<?php echo e($product->photo); ?>" class="img-fluid">
                                <?php endif; ?>
                                <h3>$<?php echo e($product->price); ?></h3>
                            </div>
                            <div class="detail">
                                <h3><?php echo e($product->name); ?></h3>
                                <p><?php echo e(strip_tags($product->details)); ?></p>
                                <a href="<?php echo e(url('/addtocart/'. $product->id)); ?>">ADD</a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="creation-30"></div>
</div>
</section>
<!-- Product Creation Level 3 -->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>