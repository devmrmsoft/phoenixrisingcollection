 

<?php $__env->startSection('content'); ?>  


<input type="hidden" id="headerdata" value="<?php echo e(__('Subscriptions')); ?>">

                    <div class="content-area">
                        <div class="mr-breadcrumb">
                            <div class="row">
                                <div class="col-lg-12">
                                        <h4 class="heading"><?php echo e(__('Subscriptions')); ?></h4>
                                        <ul class="links">
                                            <li>
                                                <a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo e(__('Dashboard')); ?> </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;"><?php echo e(__('Orders')); ?></a>
                                            </li>
                                            <li>
                                                <a href="<?php echo e(route('admin-order-subscription')); ?>"><?php echo e(__('Subscriptions')); ?></a>
                                            </li>
                                        </ul>
                                </div>
                            </div>
                        </div>
                        <div class="product-area">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mr-table allproduct">
                                        <?php echo $__env->make('includes.admin.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
                                        <div class="table-responsiv">
                                        <div class="gocover" style="background: url(<?php echo e(asset('assets/images/'.$gs->admin_loader)); ?>) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                                                <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th><?php echo e(__('Customer Email')); ?></th>
                                                            <th><?php echo e(__('Order Number')); ?></th>
                                                            <th><?php echo e(__('Product')); ?></th>
                                                            <th><?php echo e(__('Duration')); ?></th>
                                                            <th><?php echo e(__('Total Cost')); ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php
                                                            $cart = unserialize(bzdecompress(utf8_decode($data->cart)));
                                                            ?>
                                                                <?php $__currentLoopData = $cart->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if(!empty($item['keys'])): ?>
                                                                    <?php
                                                                    $keys = explode(',', $item['keys']);
                                                                    $values = explode('|-', $item['values']);
                                                                    foreach ($keys as $k => $ke) {
                                                                        if ($ke == 'duration') {
                                                                            $duration = $values[$k];
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo e($data->customer_email); ?></td>
                                                                        <td><?php echo e($data->order_number); ?></td>
                                                                        <td><?php echo e($item['item']['name']); ?></td>
                                                                        <td style="text-transform: capitalize;"><?php echo e(ucfirst(str_replace('-', ' ', $duration))); ?></td>
                                                                        <td><?php echo e($item['price']); ?></td>
                                                                    </tr>
                                                                    <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




<?php $__env->stopSection(); ?>    

<?php $__env->startSection('scripts'); ?>



    <script type="text/javascript">
        var table = $('#geniustable').DataTable();
        // var table = $('#geniustable').DataTable({
        //        ordering: false,
        //        processing: true,
        //        serverSide: true,
        //        ajax: '<?php echo e(route('admin-order-datatables','none')); ?>',
        //        columns: [
        //                 { data: 'customer_email', name: 'customer_email' },
        //                 { data: 'id', name: 'id' },
        //                 { data: 'totalQty', name: 'totalQty' },
        //                 { data: 'pay_amount', name: 'pay_amount' },
        //                 { data: 'action', searchable: false, orderable: false }
        //              ],
        //        language : {
        //             processing: '<img src="<?php echo e(asset('assets/images/'.$gs->admin_loader)); ?>">'
        //         },
        //         drawCallback : function( settings ) {
        //                 $('.select').niceSelect();  
        //         }
        //     });
                                                                
    </script>


    
<?php $__env->stopSection(); ?>   
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>