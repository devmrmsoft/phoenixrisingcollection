
<?php $__env->startSection('content'); ?>

    <!-- Slider Section Start -->
	<section class="slider">
        <div class="container-fluid no-gutters">
            <div class="row">
                <div class="col-12 pl-0 pr-0">
                    <div class="your-class">
                        <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="image-wrapper">
                            <div class="caro-container">
                                <div class="text-cont">
                                    <div class="container">
                                        <div class="text-cont-heading animated fadeInRight">
                                            <h1><?php echo e($slider->subtitle_text); ?></h1>
                                            <h1><?php echo e($slider->title_text); ?></h1>
                                            <h1><?php echo e($slider->details_text); ?></h1>
                                            <div class="text-btn animated fadeInRight">
                                                <a href=" <?php echo e(url($slider->link)); ?>" class="shop-btn">
                                                    <?php if($key == 0): ?>
                                                    Cusstomize Your Oil
                                                    <?php elseif($key == 1): ?>
                                                    Our Shop
                                                    <?php else: ?>
                                                    Shop Now
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="i-wrap animated fadeInRight">
                                    <?php if($slider->photo == 'No image found'): ?>
                                    <img src="<?php echo e(asset('assets/front/assets/img/banner.jpg')); ?>" class="img-fluid">
                                    <?php else: ?>
                                    <img src="assets/images/sliders/<?php echo e($slider->photo); ?>" class="img-fluid" width="100%">
                                    <?php endif; ?>
                                    
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>            
                </div>
            </div>
        </div>
    </section>
    <!-- Slider Section End -->

    <!-- Customize Your Hair Oil Start -->
    <section class="customization">
        <div class="container">
            <div class="title text-center">
                <h1>Customize Your Oil</h1>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <a href="<?php echo e(url('/oil/body')); ?>">
                        <h1>Body Oil</h1>
                        </a>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <a href="<?php echo e(url('/oil/body')); ?>">
                            <h3>Organic oil</h3>
                            </a>
                            <img src="<?php echo e(asset('assets/front/assets/img/zigzag.png')); ?>" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="<?php echo e(url('/flowers?oils=body')); ?>" class="product-btn" type="button">customize your oil</a>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="<?php echo e(url('/oil/body')); ?>" class="hair-img d-block">
                        <img src="<?php echo e(asset('assets/front/assets/img/customize-2.png')); ?>" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="<?php echo e(url('/oil/hair')); ?>" class="hair-img d-block">
                        <img src="<?php echo e(asset('assets/front/assets/img/customize-1.png')); ?>" class="img-fluid">
                    </a>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <a href="<?php echo e(url('/oil/hair')); ?>">
                        <h1>Hair Oil</h1>
                        </a>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <a href="<?php echo e(url('/oil/hair')); ?>">
                            <h3>Organic oil</h3>
                            </a>
                            <img src="<?php echo e(asset('assets/front/assets/img/zigzag.png')); ?>" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="<?php echo e(url('/flowers?oils=hair')); ?>" class="product-btn" type="button">customize your oil</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Customize Your Hair Oil End -->

    <!-- Product Review Start --> 
    <section class="product-review">
        <div class="container-fluid no-gutters">
            <div class="row">
                <div class="col-md-4 offset-md-1 ml-auto mr-auto product-review-column">
                    <img src="<?php echo e(asset('assets/front/assets/img/flower-oil.png')); ?>" class="img-fluid flower-img">
                </div>
                <div class="col-md-5 product-review-column d-flex justify-content-center align-items-center">
                    <div class="owl-carousel review owl-theme">
                        <?php $__currentLoopData = $reviews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>10,000 five-star reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            <?php if($review->photo == 'No image found'): ?>
                                            <img src="<?php echo e(asset('assets/front/assets/img/client.png')); ?>" class="img-fluid">
                                            <?php else: ?>
                                            <img src="assets/images/reviews/<?php echo e($review->photo); ?>" class="img-fluid" width="100%">
                                            <?php endif; ?>
                                            
                                            <h5><?php echo e($review->title); ?></h5>
                                            <p><?php echo e($review->subtitle); ?></p>
                                        </div>
                                        <div class="inner-2">
                                            <p><?php echo e(strip_tags($review->details)); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more-reviews">
                                    <a href="#" type="button" class="review-btn">read More reviews</a>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>
    <!-- Product Review End -->

    <!-- How it Works Start -->
    <section class="how-it-works">
        <div class="container">
            <div class="row">
                <div class="col-md-12 work-column">
                    <div class="title d-flex justify-content-center align-items-center flex-column">
                        <h1>How It Works</h1>
                        <img src="<?php echo e(asset('assets/front/assets/img/big-zigzag.png')); ?>" class="img-fluid">
                    </div>
                </div>
                <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3 work-column working-slider review pt-5">
                    <div class="item">
                        <div class="inner-container d-flex justify-content-center align-items-center flex-column" onClick="window.location.href = '<?php echo e(url('/buildown')); ?>'" style="cursor: pointer;">
                            <div class="working-detail <?php if($key == 1): ?> active <?php endif; ?> text-center" style="background: url('assets/images/services/<?php echo e($service->photo); ?>')">
                                <h5 class="d-inline-block"><?php echo e(($key+1)); ?></h5>
                                <h2 class="text-capitalize"><?php echo e($service->title); ?></h2>
                                <p><?php echo e($service->details); ?></p>
                            </div>
                            <!-- <div class="working-btn">
                                <a href="<?php echo e(url('/buildown')); ?>" type="button" class="read-more-btn">Read More</a>
                            </div> -->
                        </div>
                    </div>
                </div>       
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>         
                <!-- <div class="col-md-4 work-column working-slider review pt-5">
                    <div class="item">
                        <div class="inner-container d-flex justify-content-center align-items-center flex-column">
                            <div class="working-detail active center text-center">
                                <h5 class="d-inline-block">02</h5>
                                <h2 class="text-capitalize">Lorem ipsum</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo in</p>
                            </div>
                            <div class="working-btn">
                                <a href="#" type="button" class="read-more-btn">LOREM IPSUM</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 work-column working-slider review pt-5">
                    <div class="item">
                        <div class="inner-container d-flex justify-content-center align-items-center flex-column">
                            <div class="working-detail text-center">
                                <h5 class="d-inline-block">03</h5>
                                <h2 class="text-capitalize">Lorem ipsum</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo in</p>
                            </div>
                            <div class="working-btn">
                                <a href="#" type="button" class="read-more-btn">LOREM IPSUM</a>
                            </div>
                        </div>
                    </div>
                </div> -->
                
            </div>
        </div>
    </section>
    <!-- How it Works End -->

    <!-- Clear Ingredients Start -->
    <section class="clear-ingredients">
        <div class="container">
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto clear-ingredients-column">
                    <div class="inner-description text-center">
                        <h1>Clean, Science-Backed Ingredients</h1>
                        <p>Sed ut perspiciatis unde omnis iste natus error  voluptatem accusantium </p>
                    </div>
                    <div class="brands text-center">
                        <img src="<?php echo e(asset('assets/front/assets/img/brands.png')); ?>" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 clear-ingredients-column">
                    <div class="inner-image">
                        <img src="<?php echo e(asset('assets/front/assets/img/pink-bg.png')); ?>" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Clear Ingredients End -->

    <!-- Our Blogs Start -->
    <section class="our-blogs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 our-blogs-column">
                    <div class="title d-flex justify-content-center align-items-center flex-column">
                        <h1>Our Blogs</h1>
                        <img src="<?php echo e(asset('assets/front/assets/img/big-zigzag.png')); ?>" class="img-fluid">
                    </div>
                    <div class="owl-carousel blogs-slider review owl-theme pt-5">
                    <?php                                
                        function str_limit_htmltest($value, $limit = 100)
                            {
                                if (mb_strwidth($value, 'UTF-8') <= $limit) {
                                    return $value;
                                }
                                // Strip text with HTML tags, sum html len tags too.
                                // Is there another way to do it?
                                do {
                                    $len          = mb_strwidth($value, 'UTF-8');
                                    $len_stripped = mb_strwidth(strip_tags($value), 'UTF-8');
                                    $len_tags     = $len - $len_stripped;
                                    $value = mb_strimwidth($value, 0, $limit + $len_tags, '', 'UTF-8');
                                } while ($len_stripped > $limit);
                                // Load as HTML ignoring errors
                                $dom = new DOMDocument();
                                @$dom->loadHTML('<?xml encoding="utf-8" ?>'.$value, LIBXML_HTML_NODEFDTD);
                                // Fix the html errors
                                $value = $dom->saveHtml($dom->getElementsByTagName('body')->item(0));
                                // Remove body tag
                                $value = mb_strimwidth($value, 6, mb_strwidth($value, 'UTF-8') - 13, '', 'UTF-8'); // <body> and </body>
                                // Remove empty tags
                                return preg_replace('/<(\w+)\b(?:\s+[\w\-.:]+(?:\s*=\s*(?:"[^"]*"|"[^"]*"|[\w\-.:]+))?)*\s*\/?>\s*<\/\1\s*>/', '', $value);
                            }
                    ?>
                    <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item">
                            <div class="blog-image">
                                <img src="<?php echo e(asset('assets/images/blogs/'.$post->photo)); ?>" class="img-fluid">
                            </div>
                            <div class="blog-writing">
                                <p> <?php echo str_limit_htmltest($post->details, 70); ?> </p>
                                <a href="<?php echo e(url('/blog/'.$post->id)); ?>" target="_blank" type="button">READ MORE</a>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <!-- Blog Slider Item 1 -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Our Blogs End -->
	
    <!-- Discount Banner Start  -->
    <section class="discount-section">
        <div class="container">
            <div class="discount-offer">
                <div class="row h-100">
                    <div class="col-md-6 offset-md-6 discount-offer-column">
                        <div class="inner-column">
                            <div class="text">
                                <h3>One day</h3>
                                <h1 class="d-inline">10 <span>%</span> </h1>
                                <h2 class="d-inline">discount</h2>
                            </div>
                            <a href="#" type="button">05/9 - 20/10/2020</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Discount Banner End -->

    <!-- Instagram Feeds Start -->
    <section class="instagram-feed">
        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
        <div class="elfsight-app-60368ae8-fda4-4a19-8764-fa1ccb2c97ec"></div>
    </section>
    <!-- Instagram Feeds End -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>