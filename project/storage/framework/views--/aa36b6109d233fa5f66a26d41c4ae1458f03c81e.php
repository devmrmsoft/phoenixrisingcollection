
<?php $__env->startSection('content'); ?>

<section class="shopping-cart">
        <div class="container">
            <div class="title text-center">
                <h2>Shopping Cart</h2>
            </div>
            <div class="row">
                <div class="col-md-7 offset-md-1 ml-auto mr-auto shopping-cart-column">
                    <?php if(!Auth::check()): ?>
                    <div class="account-login text-center">
                        <p class="d-inline">Have an account already?<a href="<?php echo e(url('/login')); ?>"> Log In Here</a></p>
                    </div>
                    <?php endif; ?>
                    <?php if(Session::has('cart')): ?>
                      <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(explode(',', $product['keys'])[0] == 'product_type'): ?>
                          <div class="cart-details d-flex">
                              <div class="inner-col-1">
                                  <div class="add-items">
                                      <h3>Flowers:</h3>
                                      <ul class="nav flex-column">
                                        <?php $__currentLoopData = json_decode(explode('|-', $product['values'])[1]); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ingredient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <li class="nav-item">
                                              <a href="#" class="nav-link">
                                                <?php if($ingredient->photo == 'No image found'): ?>
                                                <img src="<?php echo e(asset('assets/front/assets/img/flower-1.jpg')); ?>" class="img-fluid  custome-images">
                                                <?php else: ?>
                                                <img src="assets/images/ingredients/<?php echo e($ingredient->photo); ?>" class="img-fluid  custome-images">
                                                <?php endif; ?>
                                                <span><?php echo e($ingredient->title); ?></span>
                                              </a>
                                              <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">×</span>
                                              </button> -->
                                          </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </ul>
                                  </div>
                                  <div class="add-items">
                                      <h3>Crystals:</h3>
                                      <ul class="nav flex-column">
                                          <?php $__currentLoopData = json_decode(explode('|-', $product['values'])[2]); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ingredient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <li class="nav-item">
                                              <a href="#" class="nav-link">
                                                <?php if($ingredient->photo == 'No image found'): ?>
                                                <img src="<?php echo e(asset('assets/front/assets/img/flower-1.jpg')); ?>" class="img-fluid  custome-images">
                                                <?php else: ?>
                                                <img src="assets/images/ingredients/<?php echo e($ingredient->photo); ?>" class="img-fluid  custome-images">
                                                <?php endif; ?>
                                                <span><?php echo e($ingredient->title); ?></span>
                                              </a>
                                              <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">×</span>
                                              </button> -->
                                          </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </ul>
                                  </div>
                                  <div class="add-items">
                                      <h3>Essential Oil Blends:</h3>
                                      <ul class="nav flex-column">
                                          <?php $__currentLoopData = json_decode(explode('|-', $product['values'])[3]); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ingredient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <li class="nav-item">
                                              <a href="#" class="nav-link">
                                                <?php if($ingredient->photo == 'No image found'): ?>
                                                <img src="<?php echo e(asset('assets/front/assets/img/flower-1.jpg')); ?>" class="img-fluid  custome-images">
                                                <?php else: ?>
                                                <img src="assets/images/ingredients/<?php echo e($ingredient->photo); ?>" class="img-fluid  custome-images">
                                                <?php endif; ?>
                                                <span><?php echo e($ingredient->title); ?></span>
                                              </a>
                                              <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">×</span>
                                              </button> -->
                                          </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </ul>
                                  </div>
                              </div>
                              <div class="inner-col-2">
                                  <div class="text-center">
                                      <img src="<?php echo e(asset('assets/front/assets/img/main-bottle.png')); ?>" class="img-fluid">
                                      <p><?php echo e(App\Models\Product::convertPrice($product['item']['price'],$product['item']['id'])); ?>     </p>
                                  </div>
                              </div>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <div class="creation-3-column">
                        <div class="last-stage">
                          <div class="added-product row">
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(explode(',', $product['keys'])[0] != 'product_type'): ?>
                              <div class="col-md-6">
                                  <div class="product w-100 mb-4">
                                      <div class="image text-center">
                                          <?php if($product['item']['photo'] == 'No image found'): ?>
                                              <img src="<?php echo e(asset('assets/front/assets/img/banner.jpg')); ?>" class="img-fluid">
                                              <?php else: ?>
                                              <img src="assets/images/products/<?php echo e($product['item']['photo']); ?>" class="img-fluid">
                                          <?php endif; ?>
                                          <h3><?php echo e(App\Models\Product::convertPrice($product['item']['price'],$product['item']['id'])); ?> </h3>
                                      </div>
                                      <div class="detail">
                                          <h3><?php echo e($product['item']['name']); ?></h3>
                                          <p><?php echo e(mb_substr(strip_tags($product['item']['details']),0,45,'utf-8')); ?></p>
                                          <a href="<?php echo e(route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']))); ?>">REMOVE</a>
                                      </div>
                                  </div>
                              </div>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>

                </div>
                <div class="col-md-4 shopping-cart-column sticky-top">
                    <div class="order-summary">
                        <div class="inner-column-summary">
                            <h2>Order summary</h2>
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item-oz d-flex">
                                <div class="images d-flex">
                                    <?php if($product['item']['photo'] == 'No image found'): ?>
                                      <img src="<?php echo e(asset('assets/front/assets/img/banner.jpg')); ?>" class="img-fluid">
                                    <?php else: ?>
                                      <img src="assets/images/products/<?php echo e($product['item']['photo']); ?>" class="img-fluid">
                                    <?php endif; ?>
                                    <!-- <img src="./assets/img/shampoo-1.png"> -->
                                </div>
                                <div class="details">
                                    <p><?php echo e($product['item']['name']); ?></p>
                                    <p><?php echo e(App\Models\Product::convertPrice($product['item']['price'],$product['item']['id'])); ?></p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="inner-column-total">
                            <div class="pricing d-flex">
                                <p>Subtotal</p>
                                <h6><?php echo e(Session::has('cart') ? App\Models\Product::convertPrice($totalPrice) : '0.00'); ?></h6>
                            </div>
                            <div class="pricing d-flex">
                                <p>Shipping</p>
                                <h6><?php echo e(App\Models\Product::convertPrice(0)); ?></h6>
                            </div>
                            <div class="pricing d-flex">
                                <p>Tax</p>
                                <h6><?php echo e(App\Models\Product::convertPrice(0)); ?></h6>
                            </div>
                            <div class="pricing d-flex">
                                <p>Total</p>
                                <h6><?php echo e(Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00'); ?></h6>
                            </div>
                        </div>
                    </div>
                     <div class="next-button text-center mt-3">
                      <a href="<?php echo e(url('/checkout')); ?>">
                       <button type="submit" class="border-0 rounded w-100">Proceed to Checkout</button>
                       </a>
                     </div>
                </div>
            </div>
        </div>
    </section>

<!-- Breadcrumb Area Start -->
<div class="breadcrumb-area d-none">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="pages">
          <li>
            <a href="<?php echo e(route('front.index')); ?>">
              <?php echo e($langg->lang17); ?>

            </a>
          </li>
          <li>
            <a href="<?php echo e(route('front.cart')); ?>">
              <?php echo e($langg->lang121); ?>

            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Breadcrumb Area End -->

<!-- Cart Area Start -->
<section class="cartpage d-none">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="left-area">
          <div class="cart-table">
            <table class="table">
              <?php echo $__env->make('includes.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <thead>
                    <tr>
                      <th><?php echo e($langg->lang122); ?></th>
                      <th width="30%"><?php echo e($langg->lang539); ?></th>
                      <th><?php echo e($langg->lang125); ?></th>
                      <th><?php echo e($langg->lang126); ?></th>
                      <th><i class="icofont-close-squared-alt"></i></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(Session::has('cart')): ?>

                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(explode(',', $product['keys'])[0] != 'product_type'): ?>
                    <tr class="cremove<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>">
                      <td class="product-img">
                        <div class="item">
                          <img src="<?php echo e($product['item']['photo'] ? asset('assets/images/products/'.$product['item']['photo']):asset('assets/images/noimage.png')); ?>" alt="">
                          <p class="name"><a href="<?php echo e(route('front.product', $product['item']['slug'])); ?>"><?php echo e(mb_strlen($product['item']['name'],'utf-8') > 35 ? mb_substr($product['item']['name'],0,35,'utf-8').'...' : $product['item']['name']); ?></a></p>
                        </div>
                      </td>
                                            <td>
                                                <?php if(!empty($product['size'])): ?>
                                                <b><?php echo e($langg->lang312); ?></b>: <?php echo e($product['item']['measure']); ?><?php echo e(str_replace('-',' ',$product['size'])); ?> <br>
                                                <?php endif; ?>
                                                <?php if(!empty($product['color'])): ?>
                                                <div class="d-flex mt-2">
                                                <b><?php echo e($langg->lang313); ?></b>:  <span id="color-bar" style="border: 10px solid #<?php echo e($product['color'] == "" ? "white" : $product['color']); ?>;"></span>
                                                </div>
                                                <?php endif; ?>

                                                    <?php if(!empty($product['keys'])): ?>

                                                    <?php $__currentLoopData = array_combine(explode(',', $product['keys']), explode(',', $product['values'])); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                        <b><?php echo e(ucwords(str_replace('_', ' ', $key))); ?> : </b> <?php echo e($value); ?> <br>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                    <?php endif; ?>

                                                  </td>




                      <td class="unit-price quantity">
                        <p class="product-unit-price">
                          <?php echo e(App\Models\Product::convertPrice($product['item']['price'],$product['item']['id'])); ?>                        
                        </p>
          <?php if($product['item']['type'] == 'Physical'): ?>

                          <div class="qty">
                              <ul>
              <input type="hidden" class="prodid" value="<?php echo e($product['item']['id']); ?>">  
              <input type="hidden" class="itemid" value="<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>">     
              <input type="hidden" class="size_qty" value="<?php echo e($product['size_qty']); ?>">     
              <input type="hidden" class="size_price" value="<?php echo e($product['item']['price']); ?>">   
                                <li>
                                  <span class="qtminus1 reducing">
                                    <i class="icofont-minus"></i>
                                  </span>
                                </li>
                                <li>
                                  <span class="qttotal1" id="qty<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>"><?php echo e($product['qty']); ?></span>
                                </li>
                                <li>
                                  <span class="qtplus1 adding">
                                    <i class="icofont-plus"></i>
                                  </span>
                                </li>
                              </ul>
                          </div>
        <?php endif; ?>


                      </td>

                            <?php if($product['size_qty']): ?>
                            <input type="hidden" id="stock<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>" value="<?php echo e($product['size_qty']); ?>">
                            <?php elseif($product['item']['type'] != 'Physical'): ?> 
                            <input type="hidden" id="stock<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>" value="1">
                            <?php else: ?>
                            <input type="hidden" id="stock<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>" value="<?php echo e($product['stock']); ?>">
                            <?php endif; ?>

                      <td class="total-price">
                        <p id="prc<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>">
                          <?php echo e(App\Models\Product::convertPrice($product['price'],$product['item']['id'])); ?>                 
                        </p>
                        <span class="removecart cart-remove" test-data="<?php echo e(str_replace(str_split(' ,'),'',$product['values'])); ?>" data-class="cremove<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>" data-href="<?php echo e(route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']))); ?>"><i class="icofont-ui-delete"></i> </span>
                      </td>
                      <td>
                        
                      </td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                  </tbody>
            </table>
          </div>
        </div>
      </div>
      <?php if(Session::has('cart')): ?>
      <div class="col-lg-4">
        <div class="right-area">
          <div class="order-box">
            <h4 class="title"><?php echo e($langg->lang127); ?></h4>
            <ul class="order-list">
              <li>
                <p>
                  <?php echo e($langg->lang128); ?>

                </p>
                <P>
                  <b class="cart-total <?php echo e($totalPrice); ?>"><?php echo e(Session::has('cart') ? App\Models\Product::convertPrice($totalPrice) : '0.00'); ?></b>
                </P>
              </li>
              <li>
                <p>
                  <?php echo e($langg->lang129); ?>

                </p>
                <P>
                  <b class="discount"><?php echo e(App\Models\Product::convertPrice(0)); ?></b>
                  <input type="hidden" id="d-val" value="<?php echo e(App\Models\Product::convertPrice(0)); ?>">
                </P>
              </li>
              <li>
                <p>
                  <?php echo e($langg->lang130); ?>

                </p>
                <P>
                  <b><?php echo e($tx); ?>%</b>
                </P>
              </li>
            </ul>
            <div class="total-price">
              <p>
                  <?php echo e($langg->lang131); ?>

              </p>
              <p>
                  <span class="main-total"><?php echo e(Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00'); ?></span>
              </p>
            </div>
            <div class="cupon-box">
              <div id="coupon-link">
                  <?php echo e($langg->lang132); ?>

              </div>
              <form id="coupon-form" class="coupon">
                <input type="text" placeholder="<?php echo e($langg->lang133); ?>" id="code" required="" autocomplete="off">
                <input type="hidden" class="coupon-total" id="grandtotal" value="<?php echo e(Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00'); ?>">
                <button type="submit"><?php echo e($langg->lang134); ?></button>
              </form>
            </div>
            <a href="<?php echo e(route('front.checkout')); ?>" class="order-btn">
              <?php echo e($langg->lang135); ?>

            </a>
          </div>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<!-- Cart Area End -->
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>