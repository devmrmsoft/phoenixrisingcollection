-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 19, 2020 at 08:31 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_phoenix_staging`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessory_brands`
--

CREATE TABLE `accessory_brands` (
  `id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `image` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `accessory_types`
--

CREATE TABLE `accessory_types` (
  `id` int(191) NOT NULL,
  `accessory_brand_id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(191) NOT NULL DEFAULT '0',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `role_id`, `photo`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `shop_name`) VALUES
(1, 'Admin', 'admin@gmail.com', '1112223333', 0, '16039023631603802508Phoenix-logo.png', '$2y$10$p35S2FczpEfpbe41CX4j4.XE548tHBtF5weGLPxZ56MX5dsOFtaCC', 1, 'jl9bUUXP7h5X5dlYcucszoM6MlgQBVB1IZH5rQElHTnFDtpzBnCSY0hbWBhY', '2018-02-28 23:27:08', '2020-10-28 15:26:03', 'Phoenix Rising Collection');

-- --------------------------------------------------------

--
-- Table structure for table `admin_languages`
--

CREATE TABLE `admin_languages` (
  `id` int(191) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rtl` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_languages`
--

INSERT INTO `admin_languages` (`id`, `is_default`, `language`, `file`, `name`, `rtl`) VALUES
(1, 1, 'English', '1567232745AoOcvCtY.json', '1567232745AoOcvCtY', 0),
(2, 0, 'RTL English', '1584887310NzfWDhO8.json', '1584887310NzfWDhO8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_conversations`
--

CREATE TABLE `admin_user_conversations` (
  `id` int(191) NOT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` enum('Ticket','Dispute') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_messages`
--

CREATE TABLE `admin_user_messages` (
  `id` int(191) NOT NULL,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `attributable_id` int(11) DEFAULT NULL,
  `attributable_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `input_name` varchar(255) DEFAULT NULL,
  `price_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `details_status` int(3) NOT NULL DEFAULT '1' COMMENT '0 - hide, 1- show	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(191) NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Large','TopSmall','BottomSmall') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `photo`, `link`, `type`) VALUES
(7, '1605103345image-after.jpg', 'blank', 'Large');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(191) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `meta_tag` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `title`, `details`, `photo`, `source`, `views`, `status`, `meta_tag`, `meta_description`, `tags`, `created_at`) VALUES
(27, 9, 'Et harum quidem rerum', '<span style=\"color: rgb(102, 102, 102); font-family: Raleway, sans-serif; text-align: center;\">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat.</span><br>', '1603811421blog-post-2.png', 'test', 0, 1, NULL, NULL, 'posts', '2020-10-27 10:10:21'),
(30, 9, 'Sed ut perspiciatis unde', '<span style=\"color: rgb(102, 102, 102); font-family: Raleway, sans-serif; text-align: center;\">But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you.</span><br>', '1603814366blog-post-1.png', '', 0, 1, NULL, NULL, 'Blogs', '2020-10-27 10:59:26'),
(31, 9, 'Temporibus autem quib', '<span style=\"color: rgb(102, 102, 102); font-family: Raleway, sans-serif; text-align: center;\">On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized.</span><br>', '1603814498blog-post-3.png', '', 3, 1, NULL, NULL, 'Blogs', '2020-10-27 11:01:38'),
(32, 9, 'There are many variations', '<span style=\"color: rgb(102, 102, 102); font-family: Raleway, sans-serif; text-align: center;\">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden.</span><br>', '1603814583blog-post-4.png', '', 37, 1, NULL, NULL, 'blogs', '2020-10-27 11:03:03'),
(33, 9, 'All the Lorem Ipsum', '<span style=\"color: rgb(102, 102, 102); font-family: Raleway, sans-serif; text-align: center;\">It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate.</span><br>', '1603814691blog-post-5.png', '', 0, 1, NULL, NULL, 'blogs', '2020-10-27 11:04:51'),
(34, 9, 'Contrary to popular', '<span style=\"color: rgb(102, 102, 102); font-family: Raleway, sans-serif; text-align: center;\">Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin.</span><br>', '1603814718blog-post-6.png', '', 2, 1, NULL, NULL, 'blogs', '2020-10-27 11:05:18'),
(35, 9, 'Et harum quidem rerum', '<span style=\"color: rgb(102, 102, 102); font-family: Raleway, sans-serif; text-align: center;\">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat.</span><br>', '1603818371blog-post-2.png', '', 8, 1, NULL, NULL, 'blogs', '2020-10-27 12:06:11'),
(36, 9, 'ABC Test', '<span style=\"color: rgb(123, 136, 152); font-family: &quot;Mercury SSm A&quot;, &quot;Mercury SSm B&quot;, Georgia, Times, &quot;Times New Roman&quot;, &quot;Microsoft YaHei New&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, 宋体, SimSun, STXihei, 华文细黑, serif; font-size: 26px; background-color: rgb(85, 98, 113);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore&nbsp;</span><br>', '1607018341Flower2.png', '', 3, 1, NULL, NULL, 'blog', '2020-12-03 22:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`) VALUES
(9, 'blog', 'blogs');

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT '0',
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devices` int(10) DEFAULT '0',
  `brands` int(10) DEFAULT '0',
  `carriers` int(10) DEFAULT '0',
  `accessories` int(10) DEFAULT '0',
  `arrivals` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `image`, `devices`, `brands`, `carriers`, `accessories`, `arrivals`) VALUES
(43, 'test test', 'test3', 1, '1605172826blog-4.png', 0, NULL, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cat_types`
--

CREATE TABLE `cat_types` (
  `id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat_types`
--

INSERT INTO `cat_types` (`id`, `name`, `slug`, `status`, `photo`, `is_featured`, `image`) VALUES
(19, 'test', 'test', 1, '1605172805blog-bg-oil.png', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cat_type_children`
--

CREATE TABLE `cat_type_children` (
  `id` int(191) NOT NULL,
  `cat_type_id` int(191) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `childcategories`
--

CREATE TABLE `childcategories` (
  `id` int(191) NOT NULL,
  `subcategory_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(191) NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(191) NOT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) NOT NULL,
  `recieved_user` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `counters`
--

CREATE TABLE `counters` (
  `id` int(11) NOT NULL,
  `type` enum('referral','browser') NOT NULL DEFAULT 'referral',
  `referral` varchar(255) DEFAULT NULL,
  `total_count` int(11) NOT NULL DEFAULT '0',
  `todays_count` int(11) NOT NULL DEFAULT '0',
  `today` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counters`
--

INSERT INTO `counters` (`id`, `type`, `referral`, `total_count`, `todays_count`, `today`) VALUES
(1, 'referral', 'www.facebook.com', 5, 0, NULL),
(2, 'referral', 'geniusocean.com', 2, 0, NULL),
(3, 'browser', 'Windows 10', 1809, 0, NULL),
(4, 'browser', 'Linux', 223, 0, NULL),
(5, 'browser', 'Unknown OS Platform', 408, 0, NULL),
(6, 'browser', 'Windows 7', 422, 0, NULL),
(7, 'referral', 'yandex.ru', 15, 0, NULL),
(8, 'browser', 'Windows 8.1', 536, 0, NULL),
(9, 'referral', 'www.google.com', 7, 0, NULL),
(10, 'browser', 'Android', 538, 0, NULL),
(11, 'browser', 'Mac OS X', 533, 0, NULL),
(12, 'referral', 'l.facebook.com', 1, 0, NULL),
(13, 'referral', 'codecanyon.net', 6, 0, NULL),
(14, 'browser', 'Windows XP', 2, 0, NULL),
(15, 'browser', 'Windows 8', 1, 0, NULL),
(16, 'browser', 'iPad', 4, 0, NULL),
(17, 'browser', 'Ubuntu', 9, 0, NULL),
(18, 'browser', 'iPhone', 23, 0, NULL),
(19, 'referral', 'webewox.com', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CD', 'Democratic Republic of the Congo'),
(50, 'CG', 'Republic of Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DK', 'Denmark'),
(58, 'DJ', 'Djibouti'),
(59, 'DM', 'Dominica'),
(60, 'DO', 'Dominican Republic'),
(61, 'TP', 'East Timor'),
(62, 'EC', 'Ecuador'),
(63, 'EG', 'Egypt'),
(64, 'SV', 'El Salvador'),
(65, 'GQ', 'Equatorial Guinea'),
(66, 'ER', 'Eritrea'),
(67, 'EE', 'Estonia'),
(68, 'ET', 'Ethiopia'),
(69, 'FK', 'Falkland Islands (Malvinas)'),
(70, 'FO', 'Faroe Islands'),
(71, 'FJ', 'Fiji'),
(72, 'FI', 'Finland'),
(73, 'FR', 'France'),
(74, 'FX', 'France, Metropolitan'),
(75, 'GF', 'French Guiana'),
(76, 'PF', 'French Polynesia'),
(77, 'TF', 'French Southern Territories'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germany'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GK', 'Guernsey'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'IM', 'Isle of Man'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran (Islamic Republic of)'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'CI', 'Ivory Coast'),
(109, 'JE', 'Jersey'),
(110, 'JM', 'Jamaica'),
(111, 'JP', 'Japan'),
(112, 'JO', 'Jordan'),
(113, 'KZ', 'Kazakhstan'),
(114, 'KE', 'Kenya'),
(115, 'KI', 'Kiribati'),
(116, 'KP', 'Korea, Democratic People\'s Republic of'),
(117, 'KR', 'Korea, Republic of'),
(118, 'XK', 'Kosovo'),
(119, 'KW', 'Kuwait'),
(120, 'KG', 'Kyrgyzstan'),
(121, 'LA', 'Lao People\'s Democratic Republic'),
(122, 'LV', 'Latvia'),
(123, 'LB', 'Lebanon'),
(124, 'LS', 'Lesotho'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libyan Arab Jamahiriya'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lithuania'),
(129, 'LU', 'Luxembourg'),
(130, 'MO', 'Macau'),
(131, 'MK', 'North Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MW', 'Malawi'),
(134, 'MY', 'Malaysia'),
(135, 'MV', 'Maldives'),
(136, 'ML', 'Mali'),
(137, 'MT', 'Malta'),
(138, 'MH', 'Marshall Islands'),
(139, 'MQ', 'Martinique'),
(140, 'MR', 'Mauritania'),
(141, 'MU', 'Mauritius'),
(142, 'TY', 'Mayotte'),
(143, 'MX', 'Mexico'),
(144, 'FM', 'Micronesia, Federated States of'),
(145, 'MD', 'Moldova, Republic of'),
(146, 'MC', 'Monaco'),
(147, 'MN', 'Mongolia'),
(148, 'ME', 'Montenegro'),
(149, 'MS', 'Montserrat'),
(150, 'MA', 'Morocco'),
(151, 'MZ', 'Mozambique'),
(152, 'MM', 'Myanmar'),
(153, 'NA', 'Namibia'),
(154, 'NR', 'Nauru'),
(155, 'NP', 'Nepal'),
(156, 'NL', 'Netherlands'),
(157, 'AN', 'Netherlands Antilles'),
(158, 'NC', 'New Caledonia'),
(159, 'NZ', 'New Zealand'),
(160, 'NI', 'Nicaragua'),
(161, 'NE', 'Niger'),
(162, 'NG', 'Nigeria'),
(163, 'NU', 'Niue'),
(164, 'NF', 'Norfolk Island'),
(165, 'MP', 'Northern Mariana Islands'),
(166, 'NO', 'Norway'),
(167, 'OM', 'Oman'),
(168, 'PK', 'Pakistan'),
(169, 'PW', 'Palau'),
(170, 'PS', 'Palestine'),
(171, 'PA', 'Panama'),
(172, 'PG', 'Papua New Guinea'),
(173, 'PY', 'Paraguay'),
(174, 'PE', 'Peru'),
(175, 'PH', 'Philippines'),
(176, 'PN', 'Pitcairn'),
(177, 'PL', 'Poland'),
(178, 'PT', 'Portugal'),
(179, 'PR', 'Puerto Rico'),
(180, 'QA', 'Qatar'),
(181, 'RE', 'Reunion'),
(182, 'RO', 'Romania'),
(183, 'RU', 'Russian Federation'),
(184, 'RW', 'Rwanda'),
(185, 'KN', 'Saint Kitts and Nevis'),
(186, 'LC', 'Saint Lucia'),
(187, 'VC', 'Saint Vincent and the Grenadines'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'Sao Tome and Principe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SK', 'Slovakia'),
(198, 'SI', 'Slovenia'),
(199, 'SB', 'Solomon Islands'),
(200, 'SO', 'Somalia'),
(201, 'ZA', 'South Africa'),
(202, 'GS', 'South Georgia South Sandwich Islands'),
(203, 'SS', 'South Sudan'),
(204, 'ES', 'Spain'),
(205, 'LK', 'Sri Lanka'),
(206, 'SH', 'St. Helena'),
(207, 'PM', 'St. Pierre and Miquelon'),
(208, 'SD', 'Sudan'),
(209, 'SR', 'Suriname'),
(210, 'SJ', 'Svalbard and Jan Mayen Islands'),
(211, 'SZ', 'Swaziland'),
(212, 'SE', 'Sweden'),
(213, 'CH', 'Switzerland'),
(214, 'SY', 'Syrian Arab Republic'),
(215, 'TW', 'Taiwan'),
(216, 'TJ', 'Tajikistan'),
(217, 'TZ', 'Tanzania, United Republic of'),
(218, 'TH', 'Thailand'),
(219, 'TG', 'Togo'),
(220, 'TK', 'Tokelau'),
(221, 'TO', 'Tonga'),
(222, 'TT', 'Trinidad and Tobago'),
(223, 'TN', 'Tunisia'),
(224, 'TR', 'Turkey'),
(225, 'TM', 'Turkmenistan'),
(226, 'TC', 'Turks and Caicos Islands'),
(227, 'TV', 'Tuvalu'),
(228, 'UG', 'Uganda'),
(229, 'UA', 'Ukraine'),
(230, 'AE', 'United Arab Emirates'),
(231, 'GB', 'United Kingdom'),
(232, 'US', 'United States'),
(233, 'UM', 'United States minor outlying islands'),
(234, 'UY', 'Uruguay'),
(235, 'UZ', 'Uzbekistan'),
(236, 'VU', 'Vanuatu'),
(237, 'VA', 'Vatican City State'),
(238, 'VE', 'Venezuela'),
(239, 'VN', 'Vietnam'),
(240, 'VG', 'Virgin Islands (British)'),
(241, 'VI', 'Virgin Islands (U.S.)'),
(242, 'WF', 'Wallis and Futuna Islands'),
(243, 'EH', 'Western Sahara'),
(244, 'YE', 'Yemen'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `price` double NOT NULL,
  `times` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(191) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `sign`, `value`, `is_default`) VALUES
(1, 'USD', '$', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(191) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test 1', '2020-12-03 22:59:25', '2020-12-03 22:59:25', NULL),
(2, 'Test 1', '2020-12-03 22:59:26', '2020-12-03 22:59:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `email_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_subject` mediumtext COLLATE utf8_unicode_ci,
  `email_body` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_type`, `email_subject`, `email_body`, `status`) VALUES
(1, 'new_order', 'Order', 'TEst order welcome', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorite_sellers`
--

CREATE TABLE `favorite_sellers` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `vendor_id` int(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `product_id`, `photo`) VALUES
(249, 186, '1591173870featured_1.jpg'),
(250, 186, '1591173870featured_2.jpg'),
(251, 186, '1591173870featured_3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `generalsettings`
--

CREATE TABLE `generalsettings` (
  `id` int(191) NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_email` text COLLATE utf8mb4_unicode_ci,
  `header_phone` text COLLATE utf8mb4_unicode_ci,
  `footer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `colors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loader` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_loader` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_talkto` tinyint(1) NOT NULL DEFAULT '1',
  `talkto` text COLLATE utf8mb4_unicode_ci,
  `is_language` tinyint(1) NOT NULL DEFAULT '1',
  `is_loader` tinyint(1) NOT NULL DEFAULT '1',
  `map_key` text COLLATE utf8mb4_unicode_ci,
  `is_disqus` tinyint(1) NOT NULL DEFAULT '0',
  `disqus` longtext COLLATE utf8mb4_unicode_ci,
  `is_contact` tinyint(1) NOT NULL DEFAULT '0',
  `is_faq` tinyint(1) NOT NULL DEFAULT '0',
  `guest_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_check` tinyint(1) NOT NULL DEFAULT '0',
  `cod_check` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_key` text COLLATE utf8mb4_unicode_ci,
  `stripe_secret` text COLLATE utf8mb4_unicode_ci,
  `currency_format` tinyint(1) NOT NULL DEFAULT '0',
  `withdraw_fee` double NOT NULL DEFAULT '0',
  `withdraw_charge` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `smtp_host` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_smtp` tinyint(1) NOT NULL DEFAULT '0',
  `is_comment` tinyint(1) NOT NULL DEFAULT '1',
  `is_currency` tinyint(1) NOT NULL DEFAULT '1',
  `add_cart` text COLLATE utf8mb4_unicode_ci,
  `out_stock` text COLLATE utf8mb4_unicode_ci,
  `add_wish` text COLLATE utf8mb4_unicode_ci,
  `already_wish` text COLLATE utf8mb4_unicode_ci,
  `wish_remove` text COLLATE utf8mb4_unicode_ci,
  `add_compare` text COLLATE utf8mb4_unicode_ci,
  `already_compare` text COLLATE utf8mb4_unicode_ci,
  `compare_remove` text COLLATE utf8mb4_unicode_ci,
  `color_change` text COLLATE utf8mb4_unicode_ci,
  `coupon_found` text COLLATE utf8mb4_unicode_ci,
  `no_coupon` text COLLATE utf8mb4_unicode_ci,
  `already_coupon` text COLLATE utf8mb4_unicode_ci,
  `order_title` text COLLATE utf8mb4_unicode_ci,
  `order_text` text COLLATE utf8mb4_unicode_ci,
  `is_affilate` tinyint(1) NOT NULL DEFAULT '1',
  `affilate_charge` int(100) NOT NULL DEFAULT '0',
  `affilate_banner` text COLLATE utf8mb4_unicode_ci,
  `already_cart` text COLLATE utf8mb4_unicode_ci,
  `fixed_commission` double NOT NULL DEFAULT '0',
  `percentage_commission` double NOT NULL DEFAULT '0',
  `multiple_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `multiple_packaging` tinyint(4) NOT NULL DEFAULT '0',
  `vendor_ship_info` tinyint(1) NOT NULL DEFAULT '0',
  `reg_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `cod_text` text COLLATE utf8mb4_unicode_ci,
  `paypal_text` text COLLATE utf8mb4_unicode_ci,
  `stripe_text` text COLLATE utf8mb4_unicode_ci,
  `header_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin_loader` tinyint(1) NOT NULL DEFAULT '0',
  `menu_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_hover_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `is_verification_email` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instamojo_text` text COLLATE utf8mb4_unicode_ci,
  `is_instamojo` tinyint(1) NOT NULL DEFAULT '0',
  `instamojo_sandbox` tinyint(1) NOT NULL DEFAULT '0',
  `is_paystack` tinyint(1) NOT NULL DEFAULT '0',
  `paystack_key` text COLLATE utf8mb4_unicode_ci,
  `paystack_email` text COLLATE utf8mb4_unicode_ci,
  `paystack_text` text COLLATE utf8mb4_unicode_ci,
  `wholesell` int(191) NOT NULL DEFAULT '0',
  `is_capcha` tinyint(1) NOT NULL DEFAULT '0',
  `error_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_popup` tinyint(1) NOT NULL DEFAULT '0',
  `popup_title` text COLLATE utf8mb4_unicode_ci,
  `popup_text` text COLLATE utf8mb4_unicode_ci,
  `popup_background` text COLLATE utf8mb4_unicode_ci,
  `invoice_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_secure` tinyint(1) NOT NULL DEFAULT '0',
  `is_report` tinyint(1) NOT NULL,
  `paypal_check` tinyint(1) DEFAULT '0',
  `paypal_business` text COLLATE utf8mb4_unicode_ci,
  `footer_logo` text COLLATE utf8mb4_unicode_ci,
  `email_encryption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paytm_merchant` text COLLATE utf8mb4_unicode_ci,
  `paytm_secret` text COLLATE utf8mb4_unicode_ci,
  `paytm_website` text COLLATE utf8mb4_unicode_ci,
  `paytm_industry` text COLLATE utf8mb4_unicode_ci,
  `is_paytm` int(11) NOT NULL DEFAULT '1',
  `paytm_text` text COLLATE utf8mb4_unicode_ci,
  `paytm_mode` enum('sandbox','live') CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `is_molly` tinyint(1) NOT NULL DEFAULT '0',
  `molly_key` text COLLATE utf8mb4_unicode_ci,
  `molly_text` text COLLATE utf8mb4_unicode_ci,
  `is_razorpay` int(11) NOT NULL DEFAULT '1',
  `razorpay_key` text COLLATE utf8mb4_unicode_ci,
  `razorpay_secret` text COLLATE utf8mb4_unicode_ci,
  `razorpay_text` text COLLATE utf8mb4_unicode_ci,
  `show_stock` tinyint(1) NOT NULL DEFAULT '0',
  `is_maintain` tinyint(1) NOT NULL DEFAULT '0',
  `maintain_text` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generalsettings`
--

INSERT INTO `generalsettings` (`id`, `logo`, `favicon`, `title`, `header_email`, `header_phone`, `footer`, `copyright`, `colors`, `loader`, `admin_loader`, `is_talkto`, `talkto`, `is_language`, `is_loader`, `map_key`, `is_disqus`, `disqus`, `is_contact`, `is_faq`, `guest_checkout`, `stripe_check`, `cod_check`, `stripe_key`, `stripe_secret`, `currency_format`, `withdraw_fee`, `withdraw_charge`, `tax`, `shipping_cost`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `from_email`, `from_name`, `is_smtp`, `is_comment`, `is_currency`, `add_cart`, `out_stock`, `add_wish`, `already_wish`, `wish_remove`, `add_compare`, `already_compare`, `compare_remove`, `color_change`, `coupon_found`, `no_coupon`, `already_coupon`, `order_title`, `order_text`, `is_affilate`, `affilate_charge`, `affilate_banner`, `already_cart`, `fixed_commission`, `percentage_commission`, `multiple_shipping`, `multiple_packaging`, `vendor_ship_info`, `reg_vendor`, `cod_text`, `paypal_text`, `stripe_text`, `header_color`, `footer_color`, `copyright_color`, `is_admin_loader`, `menu_color`, `menu_hover_color`, `is_home`, `is_verification_email`, `instamojo_key`, `instamojo_token`, `instamojo_text`, `is_instamojo`, `instamojo_sandbox`, `is_paystack`, `paystack_key`, `paystack_email`, `paystack_text`, `wholesell`, `is_capcha`, `error_banner`, `is_popup`, `popup_title`, `popup_text`, `popup_background`, `invoice_logo`, `user_image`, `vendor_color`, `is_secure`, `is_report`, `paypal_check`, `paypal_business`, `footer_logo`, `email_encryption`, `paytm_merchant`, `paytm_secret`, `paytm_website`, `paytm_industry`, `is_paytm`, `paytm_text`, `paytm_mode`, `is_molly`, `molly_key`, `molly_text`, `is_razorpay`, `razorpay_key`, `razorpay_secret`, `razorpay_text`, `show_stock`, `is_maintain`, `maintain_text`) VALUES
(1, '1603802508Phoenix-logo.png', '160380253316x16.png', 'Phoenix Rising Collection', 'sendmail', '0123 456789', 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.', '©<font color=\"#666666\" face=\"Raleway, sans-serif\">&nbsp;2020 Phoenix Rising Collection&nbsp;</font>', '#e7c949', '1592224445loading.gif', '1592224443loading.gif', 0, '<script type=\"text/javascript\">\r\nvar Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n(function(){\r\nvar s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];\r\ns1.async=true;\r\ns1.src=\'https://embed.tawk.to/5bc2019c61d0b77092512d03/default\';\r\ns1.charset=\'UTF-8\';\r\ns1.setAttribute(\'crossorigin\',\'*\');\r\ns0.parentNode.insertBefore(s1,s0);\r\n})();\r\n</script>', 1, 0, 'AIzaSyB1GpE4qeoJ__70UZxvX9CTMUTZRZNHcu8', 0, '<div id=\"disqus_thread\">         \r\n    <script>\r\n    /**\r\n    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.\r\n    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/\r\n    /*\r\n    var disqus_config = function () {\r\n    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page\'s canonical URL variable\r\n    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page\'s unique identifier variable\r\n    };\r\n    */\r\n    (function() { // DON\'T EDIT BELOW THIS LINE\r\n    var d = document, s = d.createElement(\'script\');\r\n    s.src = \'https://junnun.disqus.com/embed.js\';\r\n    s.setAttribute(\'data-timestamp\', +new Date());\r\n    (d.head || d.body).appendChild(s);\r\n    })();\r\n    </script>\r\n    <noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>\r\n    </div>', 1, 1, 1, 0, 1, 'pk_test_UnU1Coi1p5qFGwtpjZMRMgJM', 'sk_test_QQcg3vGsKRPlW6T3dXcNJsor', 0, 5, 5, 0, 5, 'smtp.mailtrap.io', '2525', 'be9550e06de09a', '6bf9a31b6ecb6b', 'devmrmsoft@gmail.com', 'Horizon Wireless', 1, 1, 1, 'Successfully Added To Cart', 'Out Of Stock', 'Add To Wishlist', 'Already Added To Wishlist', 'Successfully Removed From The Wishlist', 'Successfully Added To Compare', 'Already Added To Compare', 'Successfully Removed From The Compare', 'Successfully Changed The Color', 'Coupon Found', 'No Coupon Found', 'Coupon Already Applied', 'THANK YOU FOR YOUR PURCHASE.', 'We\'ll email you an order confirmation with details and tracking info.', 1, 8, '15587771131554048228onepiece.jpeg', 'Already Added To Cart', 5, 5, 1, 1, 1, 1, 'Pay with cash upon delivery.', 'Pay via your PayPal account.', 'Pay via your Credit Card.', '#ffffff', '#fef9e6', '#666666', 0, '#ff5500', '#02020c', 0, 0, 'test_172371aa837ae5cad6047dc3052', 'test_4ac5a785e25fc596b67dbc5c267', 'Pay via your Instamojo account.', 0, 1, 0, 'pk_test_162a56d42131cbb01932ed0d2c48f9cb99d8e8e2', 'example@gmail.com', 'Pay via your Paystack account.', 6, 1, '1566878455404.png', 1, 'NEWSLETTER', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita porro ipsa nulla, alias, ab minus dummy.', '1584934329adv-banner.jpg', '1589598709logo.png', '1567655174profile.jpg', '#666666', 0, 1, 1, 'example@gmail.com', '1571567309footers.png', 'tls', 'tkogux49985047638244', 'LhNGUUKE9xCQ9xY8', 'WEBSTAGING', 'Retail', 0, 'Pay via your Paytm account.', 'sandbox', 0, 'test_5HcWVs9qc5pzy36H9Tu9mwAyats33J', 'Pay with Molly Payment.', 0, 'rzp_test_xDH74d48cwl8DF', 'cr0H1BiQ20hVzhpHfHuNbGri', 'Pay via your Razorpay account.', 1, 0, '<div style=\"text-align: center;\"><font size=\"5\"><br></font></div><h1 style=\"text-align: center;\"><font size=\"6\">UNDER MAINTENANCE</font></h1>');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `product_type` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `ingredients` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `user_id`, `title`, `type`, `product_type`, `photo`, `ingredients`, `description`) VALUES
(2, 0, 'Chamomile', 'flowers', 'hair,body', '1608322676chamomile.jpg', 'Chamomile flowers belong to the daisy family and have', '<h3>Chamomile</h3>\r\n<p> Chamomile flowers belong to the daisy family and have white petals surrounding a yellow center. They are fragrant with aromatic and volatile oil, used for several healings.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>Chamomile symbolizes relaxation, so it helps you relax while it attracts abundance. Its scent allows you to concentrate on your meditation, opening you to the healing love of God and the universe.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Chamomile flower calms your mind to help you sleep better. This effect is because it contains an antioxidant called Apigenin, which binds to some brain receptors, which promote sleep. It significantly reduces anxiety and depression symptoms.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>Chamomile oil has anti-inflammatory, analgesic, and antiseptic properties. It relieves skin conditions like eczema and hastens wound healing. The oil is used in beauty products to eliminate acne, protect against sunburn, and make the skin glow.</p>'),
(3, 0, 'Rose', 'flowers', 'hair,body', '1608321916flower15.png', 'Rose is the most popular flower with over three hundred', '<h3>Rose</h3>\r\n<p>Rose is the most popular flower with over three hundred species, identified by the sharp prickles. It has five petals, five sepals, and comes in varying beautiful colors.</p>\r\n\r\n<h3>For the spirit</h3>\r\n<p>The sweet scent of a rose flower symbolizes the sweetness of God\'s love. During prayer or meditation, angels use rose scent to register their presence because they both have powerful energy fields. Roses have a high frequency that brings inspiration, deep healing, luck, protection, prosperity, and intuition.</p>\r\n\r\n<h3>For the mind</h3>\r\n<p>Rose flower comes in several significant colors. The white rose symbolizes purity and innocence; the Pink and red roses represent love, romance, admiration, joy, gratitude, and grace, while the yellow rose symbolizes friendship. Generally, it fights anxiety, cleanses the mind, gives room for positivity and hopefulness.</p>\r\n\r\n<h3>For the body</h3>\r\n<p>Rose oil is used for healing because of the anti-inflammatory and antioxidant properties. It soothes irritation, treats wounds, and heals scars from burns and cuts. It removes acne, hydrates the skin, prevents aging, and improves skin quality. Also, rose flowers can heal headaches, sore throats, and stomach upsets.</p>'),
(4, 0, 'Jasmin', 'flowers', 'hair,body', '1608321943flower12.png', 'Jasmin is a climbing plant from the Olive family grown', '<h3>Jasmin</h3>\r\n<p>Jasmin is a climbing plant from the Olive family grown for its exotic fragrance and healing property.</p>\r\n\r\n<h3>For the spirit</h3>\r\n<p>Jasmine is symbolic of purity and grace. It inspires and invokes blessings, happiness, confidence, and inner beauty. Using a bit of Jasmin oil during meditation or yoga can increase the quality of dreams and sleep. Jasmin\'s scent increases the feeling of love in the atmosphere.</p>\r\n\r\n<h3>For the mind</h3>\r\n<p>Jasmin has a soothing effect, alleviates symptoms of anxiety and depression. The fragrant scent directly affects a brain chemical called GABA, which calms the nerve and facilitates rest. It dispels depression, anxiety, worries while it gives a sense of compassion and love. It impacts mental health positively.</p>\r\n\r\n<h3>For the body</h3>\r\n<p>Jasmin oil contains natural antibiotic properties that strengthen the immune system and protect the skin from assault. It helps to reduce and lighten scars from acne, wounds, eruptions, and stretch marks. It hydrates the skin and increases its elasticity.<br>\r\nThe oil also prevents hair thinning, breakage while strengthening and softening it.</p>'),
(5, 0, 'Onyx', 'crystals', 'hair,body', '1608323069onyx.jpg', 'Onyx is a banded, marble-like crystal widely accepted for', '<h3>Onyx</h3>\r\n<p>Onyx is a banded, marble-like crystal widely accepted for its ability to give strength and support. It comes in various colors like black, gray, white, blue, yellow, brown, and red. </p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>The Onyx crystal brings relaxation, relief, liberty, increased sensitivity, and aids connection to higher powers for higher guidance access. It imparts the strength to be the master of one\'s destiny.<br>\r\nIt is a powerful stone of protection against negative energies, absorbing them and transforming them into positive ones.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Onyx crystal symbolizes emotional strength aiding rational thinking, power of reasoning, logic, and control. It boosts self-confidence and a sense of responsibility bringing flexibility, stability, and rest. It is a mental tonic that helps to deal with overwhelming worries and grief. It also helps in making wise decisions.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>Onyx crystal sharpens the ears and aids the healing of inner ear diseases. It helps heal ailments of the teeth, liver, gallbladder, bone marrow, blood, joints, discs, and bones. Onyx crystal oil\'s infusion into a hair care regimen treats baldness, itchy scalp, dandruff, and other hair problems.</p>'),
(6, 0, 'Amethyst', 'crystals', 'hair,body', '1608322862amethyst.jpg', 'The Amethyst crystal is a violet crystal quartz in various', '<h3>Amethyst</h3>\r\n<p>The Amethyst crystal is a violet crystal quartz in various shades from light to dark that symbolizes kindness, inner peace, and reconciliation. Amethyst is a beneficial crystal to the spirit, mind, and body.</p> \r\n\r\n<h3>For the Spirit</h3>\r\n<p>The Amethyst crystal is a powerful crystal that can protect from attacks or harm of any kind. It brings forth spiritual wisdom, insights, humility, impartiality, and makes dreams vivid and understandable. It increases peace, spiritual awareness, and clarity during meditation.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Amethyst crystal calms your mind and promotes better sleep. It improves focus, concentration, sense of judgment, productive thinking, and memory. It also helps in reconciliation with self and others. Amethyst oil has a sobering effect, which helps with addictions. It helps to overcome psychological pain, alleviates mood, and wards off negativity.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>This Amethyst crystal is a tranquilizer, relieving headaches, migraines, tension, stress, and strain. It heals skin injuries, bruises, and inflammation. When Amethyst oil is used on the hair, it makes it soft, shiny and protects the hair from heat damage during styling. It nourishes the hair and prevents dry scalp.</p>'),
(7, 0, 'Citrine', 'crystals', 'hair,body', '1608322975citrine.jpg', 'Citrine is a yellow quartz crystal associated with illumination', '<h3>Citrine</h3>\r\n<p>Citrine is a yellow quartz crystal associated with illumination and brightness. Based on its ability to give light, it is described as the Vitamin C of the soul. Also, it contains natural oils that are healthy for your skin.</p> \r\n\r\n<h3>For the Spirit</h3>\r\n<p>Citrine quartz crystal gives the courage to face life. It energizes the entire being. It helps to balance and transfer energy into daily living. It helps to realign the solar plexus and sacral chakras when they go out of balance. Sleeping with some citrine enables one to remember every detail in a dream.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Citrine helps to deal with depression and encourages self-expression. It imparts joy, vibrance, and positivity. With Citrine, confidence and self-esteem are boosted. It improves intellect as it stimulates the brain. It motivates and unlocks human creativity. Citrine crystal dispels emotions like fear, phobias, and other negative traits as it gently releases the healing energy.</p> \r\n\r\n<h3>For the Body</h3>\r\n<p>Citrine oil contains antioxidants to protect the body from harmful influences. It heals skin problems such as eczema and acne, to bring out the glow. It brightens and balances all skin tones.<br>\r\nAlso, Citrine quartz heals indigestion, eye problems, and many other infections.</p>'),
(8, 0, 'Sweet Joy', 'blends', 'hair,body', '16083201311608138534Image-Preview.jpg', 'Jasmine, Neroli, Gardenia, Vanilla, and Sandalwood', '<h3>Sweet Joy</h3>\r\n<h3>Essential oil blend</h3> \r\n<p>Brings out the best in the wearer. Talents increase, brings optimistic thoughts, and attracts pleasure.</p>\r\n<h3>Ingredients</h3>\r\n<p>Jasmine, neroli, gardenia, vanilla,  sandalwood</p>'),
(9, 0, 'Creating Happiness', 'blends', 'hair,body', '16083206101608138534Image-Preview.jpg', 'Rose, Patchouli, Orange, Basil, and Rosemary', '<h3>Creating Happiness </h3>\r\n<h3>Essential oil blend</h3> \r\n<p>Creates happiness in one’s environment and spirit.</p>\r\n<h3>Ingredients</h3>\r\n<p>Rose, Patchouli, Orange, Basil, and Rosemary</p>'),
(10, 0, 'Inner Child', 'blends', 'hair,body', '16083207471608138534Image-Preview.jpg', 'Orange or Tangerine, Jasmine, Lemon Grass, Lang Lang , Spruce, Sandalwood with Neroli', '<h3>Inner Child </h3>\r\n<h3>Essential oil blend</h3> \r\n<p>Helps bring out focus and amplifies your best qualities, so the healing can take place.  For emotional balance,  helpful to victims of abuse.</p>\r\n<h3>Ingredients</h3>\r\n<p>Orange or Tangerine, Jasmine, Lemon Grass, Lang Lang , Spruce, Sandalwood with Neroli</p>'),
(13, 0, 'Calendula', 'flowers', 'hair,body', '1608321958flower1.png', 'Calendula is a plant whose flower is popular for its flower', '<h3>Calendula</h3>\r\n<p>Calendula is a plant whose flower is popular for its flower and medicinal oil.</p>\r\n\r\n<h3>For the spirit</h3>\r\n<p>Calendula is known as a flower of joy. It symbolizes thankfulness and serenity. It also has powerful psychic and spiritual powers. Calendula oil is used in spiritual baths to cleanse the spirit, spark the inner fire, get the guidance, enhance reflection, restore energy, and protect against evil spirits.</p>\r\n\r\n<h3>For the mind</h3>\r\n<p>Calendula brings calmness and soothing effect to mind. It infuses warmth, compassion, and an ability to relate with other people well. The bright yellow flowers provide relief for pain and energize the mind. It also provides healing from hurts and disappointments.</p> \r\n\r\n<h3>For the body</h3>\r\n<p>Calendula oil has antifungal, antibacterial, antiseptic, and anti-inflammatory properties. These properties are used to heal wounds, soothe eczema, clear acne, and protect the skin. The oil can lighten the skin by decreasing the melanin pigment.<br>\r\nIt is useful for the hair to hydrate the scalp and treat dandruff.</p>'),
(14, 0, 'Lavender', 'flowers', 'hair,body', '1608321975flower14.png', 'Lavenders are members of the mint family that are widely', '<h3>Lavender</h3>\r\n<p>Lavenders are members of the mint family that are widely used for the unique scent and oil. It comes in blue, lilac or violet colors.</p>\r\n\r\n<h3>For the spirit</h3>\r\n<p>Using Lavender during meditation aids receptivity to angel\'s guidance by creating a channel to connect with them and increasing your awareness. It raises vibrations and balances the energy centers while preparing for spiritual work. Lavender oil dispels negative energy and attracts peaceful, positive, and loving energy.</p>\r\n\r\n<h3>For the mind</h3>\r\n<p>Lavender is said to increase dopamine and serotonin chemical levels in the brain. Hence, it causes happiness. It helps to ease symptoms of depression and anxiety while it promotes happiness. It releases energy to quit unhealthy relationships, disconnect from past hurtful events and frees the mind\'s stress.</p>\r\n\r\n<h3>For the body</h3>\r\n<p>Lavender\'s essential oil is used to heal skin problems such as acne, dry skin, eczema, etc. It removes wrinkles, moisturizes, and brightens the skin. The oil has anti-inflammatory property that helps soothe the skin when irritated or bitten by insects. Its antiseptic properties make it suitable for wound cleaning and care of burns.</p>'),
(15, 0, 'Amaranthus', 'flowers', 'hair,body', '1608321991flower13.png', 'Amaranthus is popularly known as a plant bearing the', '<h3>Amaranthus</h3>\r\n<p>Amaranthus is popularly known as a plant bearing the never-fading flower, which has been in use since ancient times up till today.</p>\r\n\r\n<h3>For the spirit</h3>\r\n<p>Amaranthus symbolizes immortality. It has been shown to cause multiplication because of its never-dying ability. It can also be used in magic spells to mend a broken heart. The daily use of amaranthus opens one up to unlimited abilities. </p>\r\n\r\n<h3>For the mind</h3>\r\n<p>Amaranthus oil contains essential substances needed for a coordinated function of the nervous system. It relieves insomnia, headache, depression, and other emotional disorders. It strengthens nerve impulse transmission, improves brain function and memory. It increases the level of happiness and fights exhaustion caused by stress.</p>\r\n\r\n<h3>For the body</h3>\r\n<p>Amaranthus oil is an antioxidant that protects against damages that cause degenerative diseases that lead to paralyzing health conditions. It heals diseases or infections of the skin and slows down the aging process.<br>\r\nAlso, Amaranthus oil stimulates hair growth by strengthening the hair, moisturizing, and nourishing the hair follicles.</p>'),
(16, 0, 'Rose Quartz', 'crystals', 'hair,body', '1608322290crystal.png', 'Rose Quartz is a crystal with a soft pink color known for its', '<h3>Rose Quartz</h3>\r\n<p>Rose Quartz is a crystal with a soft pink color known for its ability to produce empathy and increase sensitivity. The crystal gives an oil that soothes the skin and slows down the aging process.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>Rose Quartz is the crystal symbolizing unconditional love and infinite peace. It purifies the heart, blocks negativity, and protects it from environmental pollution. Going into meditation with Rose quartz causes one to be wrapped by love energy. It is easier to love others when self-love has been learned.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Rose Quartz is the perfect crystal for managing a midlife crisis as it draws negative energy out and draws in loving vibes. It encourages self-forgiveness, acceptance, trust, and harmony. Rose Quartz helps to promote love and deep friendship with others. It also brings calm, reassurance, and comfort in times of difficulty and grief.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>Rose quartz oil maintains a healthy complexion, youthful glow and reduces wrinkles. It alleviates burns and blisters then smoothen the face.\r\nRose quartz also strengthens the heart and improves blood circulation. It heals infections of the chest, lung, kidneys, and adrenal glands. It helps deal with age-related diseases such as Alzheimer\'s, Parkinson\'s, etc.</p>'),
(17, 0, 'Rutilated quartz', 'crystals', 'hair,body', '1608322309crystal.png2.png', 'Rutilated quartz, also known as Angel\'s hair, is an energy', '<h3>Rutilated quartz</h3>\r\n<p>Rutilated quartz, also known as Angel\'s hair, is an energy integrator crystal. The crystal appears colorless or smoky with reddish, black, or golden brown strands.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>The Rutilated Quartz is an illuminator for the soul, which promotes spiritual growth. It takes negative energy away and pulls down spiritual barriers for a smooth connection to the highest spiritual guidance. It cleanses and energizes one\'s aura. It opens up the well of compassion and empathy and guides against psychic attack.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>The Rutilated quartz crystal brings calmness and blocks out negativity. It works as an antidepressant by alleviating dark moods. It also relieves anxiety, phobias, and fears. Rutilated quartz crystal reveals personal needs and the desires of other persons. It increases receptivity to loving and more romantic relationships.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>Rutilated quartz symbolizes vitality. It deals with exhaustion, energy depletion, treats the respiratory system\'s chronic conditions, facilitates cell growth, regeneration, and repair in tissues. The Rutilated quartz oil brightens all skin tones and gives luscious hair. It reduces aging signs, helps you remain youthful, and opens up every clogged skin pore.</p>'),
(18, 0, 'Black Obsidian', 'crystals', 'hair,body', '16083223311608137150Image-Preview.png', 'Black Obsidian crystal is a powerful, protective, and creative', '<h3>Black Obsidian</h3>\r\n<p>Black Obsidian crystal is a powerful, protective, and creative crystal.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>Black Obsidian is used in all types of shamanic healing to get rid of physical disorders. It serves as a meditation and a scrying aid. The crystal vitalizes the soul and anchors the spirit to the body. It opens the consciousness and aids entry into the unknown realm with confidence and incredible ease.</p> \r\n\r\n<h3>For the Mind</h3>\r\n<p>The Black Obsidian can boost creativity and boost intuition, causing improved awareness. It helps to ignore fears or discouragement caused by insults, negative comments, and false accusations. With this ability, it repels negativity and erases unloving thoughts. It brings mental clarity and eliminates confusion. It promotes compassion and strength qualities.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>It stops bleeding, helps in wound healing, reduces arthritic pain and joint problems. It also dissolves blockages and tension in hardened arteries and can even shrink an enlarged prostate.<br>\r\nBlack Obsidian oil softens, brightens, and hydrates the skin. It opens blocked pores, prevents breakouts on the face, prevents inflammation, and stimulates collagen.</p>'),
(19, 0, 'Black Tourmaline', 'crystals', 'hair,body', '16083223411608137150Image-Previews.png', 'Black Tourmaline crystal, among other variants of Tourmaline,', '<h3>Black Tourmaline</h3>\r\n<p>Black Tourmaline crystal, among other variants of Tourmaline, is protective, defensive, and stimulatory. It is also known as Schorl and has been beneficial to humans.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>This dark crystal has protective power against physical and ethereal pollution. It shields against psychic attacks. It helps to maintain a spiritual consciousness even when people around do not believe. It attracts compassion, altruism and inspiration, tolerance, and prosperity.\r\nIt helps to focus and concentrate during meditation.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Black Tourmaline ensures calmness during upsetting situations. It heals anxiety and stress. It balances the right and left hemispheres of the mind. It can connect the spirit to the earth and see the world with a clear but objective mind.<br>\r\nIt increases mental alertness, promotes self-confidence, and dispels fear.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>Black Tourmaline increases physical vitality, strengthens the immune system, and helps with detoxification. It heals adrenal fatigue, spinal, muscular problems, and toxin-related illnesses.<br>\r\nBlack Tourmaline oil is one of the components used in many beauty masks to remove dead skin cells. It improves blood circulation to the face and makes it vibrant.</p>'),
(20, 0, 'Mookait', 'crystals', 'hair,body', '16083223621608137150Image-Previewss.png', 'The Mookait crystal is a variety of Australian Jasper in a', '<h3>Mookait</h3>\r\n<p>The Mookait crystal is a variety of Australian Jasper in a peculiar patterned red and cream color. Its oil is useful in repairing broken skin and for maintaining a radiant glow.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>The Mookait crystal is a protective crystal with a unique calming ability, provides a shield against dangerous situations while filtering external influences. It reveals any concealed matter and creates a healthy balance between inner and outer experiences. It imparts a desire to experience new things and a depth of calmness to face them.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Mookait crystal makes the mind flexible. It helps to foresee lots of possibilities and to choose the best one. It also encourages versatility, creativity, and self-esteem. It gives the needed enthusiasm to achieve excellence. It promotes friendship and effective communication. It dispels fear, depression, and loneliness while it brings peace.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>Mookait crystal is a physically stabilizing crystal that helps with thyroid imbalance, hernia, water retention, weight loss, and stomach disorders. It enhances the immune system, purifies the blood, and aids wound healing. Mookait crystal oil strengthens hair and prevents hair loss. It stimulates hair growth and promotes youthful skin glow.</p>'),
(21, 0, 'Peacock Ore', 'crystals', 'hair,body', '16083223801608137150Image-Previewssss.png', 'The Peacock Ore crystal is a crystal of joy, acceptance', '<h3>Peacock Ore</h3> \r\n<p>The Peacock Ore crystal is a crystal of joy, acceptance, and appreciation. It attracts comfort, prosperity, and abundance. It can be found globally in copper ores.</p> \r\n\r\n<h3>For the Spirit</h3>\r\n<p>Peacock Ore crystal develops and strengthens inner vision. That crystal helps meditation and enhances the sense of perception by removing any energy blockage. It allows missing or lost objects to be found. The crystal also helps to release and free any spiritual energy patterns that have become stuck.</p> \r\n\r\n<h3>For the Mind</h3>\r\n<p>It is a crystal symbolic of happiness and joy. It can uplift moods and help to spread joy to others. It allows one to see the joy and appreciate the love of every moment. Yet, it aids in accepting the things that cannot be changed. It fosters inner security and inner knowing.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>Peacock ore stimulates adrenaline, aids calcium production in the body, improves physical energy, and balances salt levels. It aids cellular metabolism, increases alkalinity, lowers fever, relieves inflammation, and treats arthritic conditions. Peacock Ore oil is a potent magic oil for skin beauty that cleanses and nourishes it. It promotes hair growth.</p>'),
(22, 0, 'Rhodonite', 'crystals', 'hair,body', '16083223991608137150Image-Previe.png', 'Rhodonite crystal comes in pink or red color with a small', '<h3>Rhodonite</h3>\r\n<p>Rhodonite crystal comes in pink or red color with a small and mottled appearance. It is globally known for its forgiveness and emotion-balancing activity.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>Rhodonite crystal assists with mantra-based meditation and helps in aligning with the vibrations. Meditating with the Rhodonite crystal helps to clear old patterns and uncovers the heart\'s real passions. It resonates within the Heart Chakra and is useful for self-esteem and self-confidence. It is potent to help through trauma and turmoil.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Rhodonite crystal is symbolic of compassion and love. It heals pain by removing wounds, hurts, and scars from previous trauma. It nurtures love, enhances mutual understanding, deepens friendship, heals shock, and dispels confusion. It clears the heart and helps it to remain calm in extreme situations.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>This Rhodonite crystal stops bleeding wounds and relieves insect bites. It strengthens the heart, improves blood circulation, improves wound healing, relieves joint inflammation, and arthritic pain. Rhodonite oil cleanses the skin, makes it firmer, reverses sagging, and reduces dark circles. It brightens it and makes it glow.</p>'),
(23, 0, 'Clear quartz', 'crystals', 'hair,body', '16083224111608137150Image-Previ.png', 'Clear crystal quartz, also known as rock crystal, stands', '<h3>Clear quartz</h3>\r\n<p>Clear crystal quartz, also known as rock crystal, stands for neutrality and clarity. It can be found all over the world.</p>\r\n\r\n<h3>For the Spirit</h3>\r\n<p>Clear quartz is known as the stone of power, and it expands any energy or intention. It amplifies energy by absorbing, storing, regulating, and releasing it. It protects against negativity and connects a person to the higher self. It cleanses, activates, opens the Chakras, balances all the planes, and enhances psychic abilities.</p>\r\n\r\n<h3>For the Mind</h3>\r\n<p>Just like its name, Clear quartz brings lucidity, light, reflection, and amplification. It also boosts inspiration and creativity. Clear Quartz aids concentration while studying. It is a crystal of harmony and can make romantic relationships blossom. It facilitates a deep soul cleansing, connecting the physical dimension with the mind.</p>\r\n\r\n<h3>For the Body</h3>\r\n<p>The Clear quartz crystal cleanses all the organs, especially the brain. It heals diseases affecting the teeth, heart, kidneys, thyroid, and skin. Clear quartz oil helps to cleanse the scalp and remove any impurities preventing hair growth. Also, it soothes the skin when used for massage or bath. It prevents aging in all skin types.</p>'),
(24, 0, 'Consider Love', 'blends', 'hair,body', '16083208551608138534Image-Preview.jpg', 'Rose, Vanilla, Lemon, Almond, and Gold leaf', '<h3>Consider Love</h3>\r\n<h3>Love and attraction</h3> \r\n<h3>Essential oil blend</h3>\r\n<p>Helps attract spiritual partner and soulmate</p>\r\n<h3>Ingredients</h3>\r\n<p>Rose, Vanilla, Lemon, Almond, and Gold leaf</p>'),
(25, 0, 'Phoenix Rising', 'blends', 'hair,body', '16083210021608138534Image-Preview.jpg', 'Carnation, Violet and Sandalwood', '<h3>Phoenix Rising</h3>\r\n<h3>Healing oil</h3> \r\n<h3>Essential oil blend</h3>\r\n<p>Promotes healing of the mind, body, and spirit.</p>\r\n<h3>Ingredients</h3>\r\n<p>Carnation, Violet and Sandalwood</p>'),
(26, 0, 'True Manifestation', 'blends', 'hair,body', '16083233811608138534Image-Preview.jpg', 'Allspice, White Mask, Orange, and Sweet Pea.', '<h3>True Manifestation</h3>\r\n<h3>Abundance oil</h3> \r\n<h3>Essential oil blend</h3>\r\n<p>Attacts business, money, success.</p>\r\n<h3>Ingredients</h3>\r\n<p>Allspice, White Mask, Orange, and Sweet Pea.</p>'),
(27, 0, 'Sacred love', 'blends', 'hair,body', '16083212971608138534Image-Preview.jpg', 'Lilac, Violet,  and Rain Fragrance Oil, Rose, Gardenia, Vanilla, and Yasmin', '<h3>Sacred love</h3>\r\n<h3>Self love</h3> \r\n<h3>Essential oil blend</h3>\r\n<p>Find sacred love for yourself</p>\r\n<h3>Ingredients</h3>\r\n<p>Lilac, Violet,  and Rain Fragrance Oil, Rose, Gardenia, Vanilla, and Yasmin</p>');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(191) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `is_default`, `language`, `file`) VALUES
(1, 1, 'English', '1579926860LzpDa1Y7.json'),
(2, 0, 'RTL English', '1579927527QjLMUGyj.json');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(191) NOT NULL,
  `conversation_id` int(191) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_user` int(191) DEFAULT NULL,
  `recieved_user` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(191) NOT NULL,
  `order_id` int(191) UNSIGNED DEFAULT NULL,
  `user_id` int(191) DEFAULT NULL,
  `vendor_id` int(191) DEFAULT NULL,
  `product_id` int(191) DEFAULT NULL,
  `conversation_id` int(191) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `order_id`, `user_id`, `vendor_id`, `product_id`, `conversation_id`, `is_read`, `created_at`, `updated_at`) VALUES
(29, NULL, 32, NULL, NULL, NULL, 1, '2020-11-12 06:51:57', '2020-12-03 17:03:06'),
(30, NULL, 33, NULL, NULL, NULL, 1, '2020-11-12 06:52:46', '2020-12-03 17:03:06'),
(31, NULL, 35, NULL, NULL, NULL, 1, '2020-11-12 07:02:34', '2020-12-03 17:03:06'),
(32, NULL, 36, NULL, NULL, NULL, 1, '2020-11-12 07:03:13', '2020-12-03 17:03:06'),
(33, NULL, 37, NULL, NULL, NULL, 1, '2020-11-12 07:05:01', '2020-12-03 17:03:06'),
(34, NULL, 38, NULL, NULL, NULL, 1, '2020-11-12 07:06:28', '2020-12-03 17:03:06'),
(35, NULL, 39, NULL, NULL, NULL, 1, '2020-11-12 07:07:53', '2020-12-03 17:03:06'),
(36, NULL, 40, NULL, NULL, NULL, 0, '2020-12-17 02:39:26', '2020-12-17 02:39:26'),
(37, NULL, 41, NULL, NULL, NULL, 0, '2020-12-17 19:48:12', '2020-12-17 19:48:12'),
(38, 1, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:46:22', '2020-12-17 20:46:22'),
(39, 2, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:50:13', '2020-12-17 20:50:13'),
(40, 3, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:56:10', '2020-12-17 20:56:10'),
(41, 4, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:57:42', '2020-12-17 20:57:42'),
(42, 5, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:58:12', '2020-12-17 20:58:12'),
(43, 6, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:58:42', '2020-12-17 20:58:42'),
(44, 7, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:59:08', '2020-12-17 20:59:08'),
(45, 8, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:59:31', '2020-12-17 20:59:31'),
(46, 9, NULL, NULL, NULL, NULL, 0, '2020-12-17 20:59:58', '2020-12-17 20:59:58'),
(47, 10, NULL, NULL, NULL, NULL, 0, '2020-12-17 21:02:58', '2020-12-17 21:02:58'),
(48, 11, NULL, NULL, NULL, NULL, 0, '2020-12-17 21:03:39', '2020-12-17 21:03:39'),
(49, 12, NULL, NULL, NULL, NULL, 0, '2020-12-17 21:04:22', '2020-12-17 21:04:22'),
(50, 13, NULL, NULL, NULL, NULL, 0, '2020-12-17 21:04:45', '2020-12-17 21:04:45'),
(51, 14, NULL, NULL, NULL, NULL, 0, '2020-12-18 00:17:27', '2020-12-18 00:17:27'),
(52, 15, NULL, NULL, NULL, NULL, 0, '2020-12-18 00:45:24', '2020-12-18 00:45:24'),
(53, 16, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:05:58', '2020-12-18 01:05:58'),
(54, 17, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:10:15', '2020-12-18 01:10:15'),
(55, 18, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:11:51', '2020-12-18 01:11:51'),
(56, 19, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:15:22', '2020-12-18 01:15:22'),
(57, 20, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:17:16', '2020-12-18 01:17:16'),
(58, 21, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:19:10', '2020-12-18 01:19:10'),
(59, 22, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:20:09', '2020-12-18 01:20:09'),
(60, 23, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:22:26', '2020-12-18 01:22:26'),
(61, 24, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:40:37', '2020-12-18 01:40:37'),
(62, 25, NULL, NULL, NULL, NULL, 0, '2020-12-18 01:41:17', '2020-12-18 01:41:17'),
(63, NULL, 42, NULL, NULL, NULL, 0, '2020-12-18 03:19:29', '2020-12-18 03:19:29'),
(64, 26, NULL, NULL, NULL, NULL, 0, '2020-12-19 01:39:25', '2020-12-19 01:39:25'),
(65, 27, NULL, NULL, NULL, NULL, 0, '2020-12-19 01:43:10', '2020-12-19 01:43:10'),
(66, 28, NULL, NULL, NULL, NULL, 0, '2020-12-19 01:43:36', '2020-12-19 01:43:36'),
(67, 29, NULL, NULL, NULL, NULL, 0, '2020-12-19 01:52:15', '2020-12-19 01:52:15'),
(68, 30, NULL, NULL, NULL, NULL, 0, '2020-12-19 01:55:23', '2020-12-19 01:55:23'),
(69, 31, NULL, NULL, NULL, NULL, 0, '2020-12-19 01:58:55', '2020-12-19 01:58:55'),
(70, 32, NULL, NULL, NULL, NULL, 0, '2020-12-19 02:02:14', '2020-12-19 02:02:14'),
(71, 33, NULL, NULL, NULL, NULL, 0, '2020-12-19 02:03:02', '2020-12-19 02:03:02'),
(72, 34, NULL, NULL, NULL, NULL, 0, '2020-12-19 02:05:39', '2020-12-19 02:05:39'),
(73, 35, NULL, NULL, NULL, NULL, 0, '2020-12-19 22:12:37', '2020-12-19 22:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `cart` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pickup_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalQty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_amount` float NOT NULL,
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subs_id` varchar(255) NOT NULL,
  `order_number` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(255) NOT NULL,
  `customer_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `coupon_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_discount` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `affilate_user` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `affilate_charge` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_sign` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_value` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `packing_cost` double NOT NULL DEFAULT '0',
  `tax` int(191) NOT NULL,
  `dp` tinyint(1) NOT NULL DEFAULT '0',
  `pay_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_shipping_id` int(191) NOT NULL DEFAULT '0',
  `vendor_packing_id` int(191) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `cart`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `subs_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(1, 41, 'BZh91AY&SY­ÛÜ\0ß@\0Pý+K÷¿ÿÿþ`	òø\0B4\"j¢¨¢9À&\0L&L\0\0&M4s	L\0L&\0\0Lhæ\00M0\0\04Ð0Ð¦E=CÔ\0\0\0\0\0QMSÓTg¦§¤Ñ\Z Ó=FR\"M=(ÄÕÔÔ=G©¦\Z\Z=#õORAJîqP{!P÷òÀØô;$Ay(ô=I µ°§­å\nÞb¾ñæÖG1ÄKXï¡5îõò&¦ù³mv41c}ëZ@c-«ZA£<RNøy­\n¦çµØz-k[\'äö/âÙà·mÌ;0ìe{Âôyµ4j·ôh¶ÊtZy¼inÆê+Çf·h¸ÝM0ÿ¦ûÛûÞ\rXkó%¾6S£.NïiïG×UUr!óJº}d#s£ÑÈð?øz)üsü|ZÇd¥Hýª÷ª&*í§T³äm¨ô)¢KJ.¦4¹3µó\\+º¦\"Gí*HQH©\Z©Å8¦õ®b7uu93Ê±*ÊRÍÖ1©|gmyü4NgÃrãQQID§ù«iÎ¼kmÎú8åI¢¤ÙB¥%D©ISãkÌ0Ã¶ÊSÉroH¥ùÛÛ{N4jþå-^*à®Íbo(TqyÝºÞÅX´ÒÖî7`bÚ[©·9çNi»eÍ\n*J^ºã;jËu¬·F­M[F­X;0¹4	«gcE)fîå«F\"ÍYæÜ,¶ÄÖi*q6sa95jjæÕ¹£g:i.·$ÍÈÆ#\'\rBÍYjäÞ­	hËF\r7®ØLdhÕÈ$s.YY2qc\rÌ8näkçQï<äBÐV ]Á_Ê×)*¥UÒéA1-R©L(ñ©UrL®EÁG%Ir\"¥P¢J$]¬¢R+Å»Á~·Ë0ÌÇpH\n`áTP%C¢L\",Í·ÉnÜá¼Ë£ºÖÛÓVÖg\r÷¶væÌ8ËJ¦jMhæÓM-9£f¸ybÜä×\\µÉ[agb¦ÕÌsfTãÙ7ã3w6ÑLÛìªÌsN1h&Ú[C ¡G´pöF#xó Í07,*u3-³(È*oU^Aqhm\rLB÷0|´Ò6Ë`JF[e	-nS#A$,ej¹k5Yut6Þ·?Kz¡³æÒy¦ò¦ðTýÊÕÜaõ·|sÒY×.¬öð5}ûVúNäênÌ)Ý;S»ÁZ§õIúgE~5+üÏ\'Æ`C$ê\Z2@ì\\¨éMGay©»Yâós1;4{áÏÑÒJ*ª{jU%)-km1)Ìåê¦f¯Wgiá87b/Ê\\Ôv¿gÝõJSÍObLZlþÇÖM*ç¶ç(ÐZp&õT1rTªe5	ö·4þïwV\nuL1[ö¼³&Îhûkg¹µSÒfÜMÝ]kÊ)|>¶³gúøóE]gñÞC#ÑØtÕ±ÀÎ\'ØP©È(ù5.vWq zMË4Ä7 ø5\rÛ¾OWÜüÊªÕöN^È¥I-©éU\"c3ã>ÉoGäÓÝJ£«âålìhÌU\'YÄÚndÞ\Z)ú°ör¸êGg7´\"Cv´ ù@üàYÜùZÁ=ËQµÓ±3¼¨ÒnÎ«ºe¸Ð?ñîÌÎí>U3_ª©ðm:ÍZ:°VuI!w*Ø³B\\nÂTâQàÉ&)¬&U:Ã¢{|ØÈø\'YÜ9%LJ[i¢>NÙäîÌú¡þCÛn¯z¤í?	Äø#õO/7½êÜL=£YôÃô9ê=ÇB^Æ\"\"2Qô¬)J£r.yò©í{W8Xwf/ÛùÄ0ÜªÔoF¡0J\ZNÌ·Qï}KªÖô»t_»ÃãÆy¥TÂÖe£ßçO?à¥%DUJT¯¾¥\"]­ µ\"ª!t%ÐK   [LGÂÊ~cõ33Äáä¹_\"¥1&Y\\¹ëån=jÌ³\r&%¥Úq16lÞ-*ijRY«)ÝÓ7KÒhO]\\CHê©¤\\¹`¾FúøÂlé	êyºº`îwOËM¦ÐÌÌÒ.wb~ÅOYí[IIËctéWGzq0ÃÌ²¨~æóDýTlÎÜ;­}ð>Ccò<ÛÇÍô!\0Ê·ø»)ÂhÞà', 'Stripe', '30', NULL, '1', 30, 'txn_1HzMoiJlIV5dN9n7C1R7zyd7', 'ch_1HzMoiJlIV5dN9n7U4eQFWxA', 'sub_IaXu2RKiBab60C', 'kTVo1608212779', 'Completed', 'itssohail97@gmail.com', 'Sohail', 'Bahrain', '34234', 'test add', 'test city', '324234', 'test', 'Antarctica', NULL, '34234234', 'test address', 'test city', '23123', NULL, NULL, NULL, 'pending', '2020-12-17 20:46:22', '2020-12-17 20:46:22', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(2, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzMsRJlIV5dN9n7pKOJFdJp', 'ch_1HzMsRJlIV5dN9n7nlODTwWr', 'sub_IaXywcP6pfR7tU,sub_IaXyBsgJvW7s0Y', 'RnqI1608213006', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:50:13', '2020-12-17 20:50:13', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(3, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzMyCJlIV5dN9n7nqBVQh0D', 'ch_1HzMyCJlIV5dN9n7YS5H1aSU', 'sub_IaY4S14vXWWJSC,sub_IaY4pYZjI8vnRR', 'Of151608213364', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:56:10', '2020-12-17 20:56:10', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(4, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzMzgJlIV5dN9n7bvIag2d0', 'ch_1HzMzgJlIV5dN9n72LSVflBE', 'sub_IaY5AzJYS1InzX,sub_IaY5Ki2MhLdtXG', 'dxTZ1608213457', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:57:42', '2020-12-17 20:57:42', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(5, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN0AJlIV5dN9n7NaAEXuxw', 'ch_1HzN0AJlIV5dN9n7ZRLJ8Al7', 'sub_IaY6Uf57LOet8b,sub_IaY61FqeyIxaxf', 'Aygo1608213487', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:58:12', '2020-12-17 20:58:12', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(6, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN0eJlIV5dN9n7wNniPMqV', 'ch_1HzN0eJlIV5dN9n7UNpvWmjH', 'sub_IaY6holPsngkqU,sub_IaY6slGM42Q3U2', 'eCTm1608213516', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:58:42', '2020-12-17 20:58:42', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(7, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN14JlIV5dN9n7ZQOUgBgi', 'ch_1HzN14JlIV5dN9n7TBBhqCvx', 'sub_IaY7mwxnUemhHs,sub_IaY7YR19b41V5R', '3Fxg1608213541', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:59:08', '2020-12-17 20:59:08', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(8, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN1RJlIV5dN9n7VBjVPZ5o', 'ch_1HzN1RJlIV5dN9n7ywEZQtg1', 'sub_IaY7PDm1tjzPpF,sub_IaY7BrabDmtpx3', 'i8zy1608213565', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:59:31', '2020-12-17 20:59:31', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(9, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN1sJlIV5dN9n7ynYTH4tI', 'ch_1HzN1sJlIV5dN9n7FKbB4frw', 'sub_IaY8e0gLaucd17,sub_IaY8lz9fQ1Rjgz', 'fZSY1608213592', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 20:59:58', '2020-12-17 20:59:58', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(10, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN4mJlIV5dN9n7rB4guTrq', 'ch_1HzN4lJlIV5dN9n7IgdEDLYI', 'sub_IaYBejA8oerP4N,sub_IaYBoblYRJsw2j', 'qhAx1608213771', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 21:02:58', '2020-12-17 21:02:58', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(11, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN5RJlIV5dN9n7YRlKJKUn', 'ch_1HzN5RJlIV5dN9n7xHrJtKVs', 'sub_IaYB1meNRKvWHL,sub_IaYBcrROB0MbpT', '7kh31608213814', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 21:03:39', '2020-12-17 21:03:39', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(12, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN68JlIV5dN9n71jrGLIZK', 'ch_1HzN68JlIV5dN9n7nnCQrUON', 'sub_IaYCEGtMbZFIPh,sub_IaYCBbNLB5cV7I', 'v5QQ1608213856', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 21:04:22', '2020-12-17 21:04:22', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(13, 41, 'BZh91AY&SYo\'\0\rwß@\0Pý+O÷¿ÿÿþ`\r\0  T\0P $\0@æ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14Àæ0#\0#	Á\r14ÀD¢O\'¡O(=å0Ò\0hh@4Ð*DA4 dÐ	©4óTií$a=M£M4e-Àô}ßãñ³±0CðIi0øQù?¹ýÿL`jWáULcñ2`Þ,ÐÁÔÕøé9ÍfÒ¦(þ²¤Üæo¦ÌË%L´ZÝTÂ£ÖËW,VÑ4tv3\r[2Ë\n7v2Ë\n7h§&Ì,üÒQÂRÔ©Øx`ôYeNÓ#ï=¿a±ØY×q©¨ÉÐï5\Z\ZCBÍ9cJ;ÎÕK:Ê¾Ú­L74.Få\Z0~&òêÁoàh50jÁºdYê(Ø£æÍûeû#»àI$¢\"bÅWwÀÃæ~½ÁZ@´ýC%Ìúÿæª*)JªÉ¯Ø§ÁeËåØJHùüQE\"bfMEÈÖFX}l¤W¤[XIH¤ªV\Z±²Ù59[ÔTfÀ¨BD  ¬	a©+(ÞC´GXÑÉ»¢(àS+2Á(¢¤Ý7.BÒ¡RL4Kre³W´ÈÐ8HäÜZrQ±)J(ýÅ0ÃG\r\\¬ÙMÓ7ml.TÞTD¤JJ%\'58a£g2Z`Á°Y²w-&ôE$²Ù[7\\ýªqÜVÍYB¤ÿ\"¥)ÚSDS«ct©%%	³U¹¹°èÕ¡ÉæhL¥­­¼ìsvww:¹:ô)$åQ%Í7V]Z6h³seÉBRkm0Ëf³3yrÍíÙ«FÍÍÍGCVÙU.oâhR÷ÏÍL\"Ñ±pYeÑ&©¢Rp&¤´¤Ñ7L¦FäMNDäÌÜÈ³n&È©fSsfÔÝ6L)iÝ5e0Za-0&¨ÝªhFÍÅ¦)¸²r5%k&RÍÓfñ¹¼o\08EÞ¨~Ýò	UP)R%(\\\n}ö\\Q)IJµ4 \"ªU¡a T²LË.$¸¤¥I ÂâE¬¹!j ¥DS¶nì+±:öÉ$¾Ta«gjÆÊ50¸ÞÔj£Cµ\Z²³2YÕ»æg(pµ©ªÍÔjl±\rÛ¬ÎÜ,`äÌÄBUFj\nQª¤áFê4­((p£*r;YÃ#V«ÉªÅ6`ÂaFX,Â¨Ãp±£%G&ÌÜMç4(ÊÎ¬R²s`a³[µÝZ(Ì¡©ªá<ÇÚ\\;÷\n·AÌÔ,Â÷¥wÃ2\ZC¾õsrÚd÷_¤§ävè?4ÉèOÞOvRôî))û9)K0`É\"K´¸±©rZ¨6J7St´ú<¢%I6¤Ñ;ÉÁ°ú>Àõè]¡[Ûc@xÇ®PÕè}íHpÓ;ØÕW¬0lçôÖK2OuTñ;%5\'jy¸\'ÎSîT;K~¹=bðï&Ê\'dÔ´ï)ôZË:ÔÚ$¤¢;åÍS¶wÎ	ÔP-4Ä0rIÌzÍf`\\EAïa²Ë	NÒy2Ï9Û<&ÉÜÉi¼Â-èKC«{¼|3ç9IEOIG¸í2bóÚß	I­·%·rk0ËCbn¢Ê9`ÊÚÊCJ\Z)Qjù§)?B~s¶yND¡IHTèLL1*ç	î;\'VRls4Yò6³e(ðL®rMÎ3¾^eM¥jì{l³SÚv§Ø÷¼%\rÃ x±ùbPÁØ;^3iÜ\ZÜ¤(kO\'¬´0Öv¦f³ê)ÒpiÚOU}o|-)5Or|	Ü%Èä|Ë~§ÏúER¤dù\'ÂTIdQIéAÒQ|¦Sö§Ô=\'ó4{PxÍÊØnqw¹yG89D!Ärã%Âà!f?®±¨à/¨G`?¹ìO¥OtæÙ;$ø?UÓ$Ñ9§Õ5DìIìRlµ|éPÑ7#)Ð´¦äÌÜ4ü)ËIN©ê:J2§êQ¬©î6N¬Òt0O£«X×,Bå$ì6M\Z&%¦ì&©PËÂa.èk,5I´ÊEÄø;êOL±þ¦Òy:§¨I\"Ó)Ræé©ÒêOAç2>×4ä¢<:ùI:§Ñ?ÓøÍJ;Þá=g)$bzgäjÒ?#ÜZtBá>Óã¢¢¢rîÉÚÒª;p±O!S	D ´ïq)<giÉ:LO$Êx&É\\çP|¹É©&ªIÂtP©hh:7({O|´q<¥Ë¹Î[Öì³.R£2V±kpXñ |1\"H±<¤hA1\"`J4Y\0hB)\ZAT¡ ÅFEMîrZ\Z\'ÐÌ³dõÎrÅ¤.[J032ÒÓg»tRäÀ¢Æ\r	CpóSa6M¢äT8båKMM\\*xôZk\Z$ÂO&³8\ZHjÖh¶4¤>¹Ò¡Ø(Qrzç$èO0ñ$ý3Á£dÙ¦SHZxÌ-GÕ)<Ó.e(L§©S:*ÔJðQÌÌ¤?É4\'ë(\Z8^ÃxD`üÍ¾Ú	ü{:CEðCaJ­Cþ.äp¡!\"Þ>N', 'Stripe', '30', NULL, '2', 30, 'txn_1HzN6VJlIV5dN9n7hffw2G4u', 'ch_1HzN6VJlIV5dN9n7bWeAuVIm', 'sub_IaYCstNCtEPySk,sub_IaYCcme7YRSzKQ', 'eKBz1608213879', 'Completed', 'itssohail97@gmail.com', 'test', 'Bahamas', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-17 21:04:45', '2020-12-17 21:04:45', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(14, 40, 'BZh91AY&SYHÅÄ\0íß@\0Pý+O÷¿ÿÿþ`¿|\nT¥Q\0P\0\0\0\0\n\09LÈÀÄÄÂa0CLM09LÈÀÄÄÂa0CLM09LÈÀÄÄÂa0CLM09LÈÀÄÄÂa0CLM0!A4\nb\0Ðõ@\0PhA2\Zd£ÔÓ$ÛE õ<£1O5M\nl)Ý<ßÍ{%/Üþ¯øilcfÍUÎ@6Ü0±±þÐ4\'æ68QxB64>ðÖA±è:¾\Z0Ô6÷	Îç0ªºfÐP@ÃFmÍ¦u}:!£Ôõ´Ðð;:GW´qÓ»Öã°wtcÉÙ¶ùc AáÓ\0ce÷/òKÞ´´´üÙx¥ÁíZ»Çaì\Z½Á±ë°pmâ64è½ár´¿áitX¼4\\1{¥nal×¦1ÊÙnº®¡ÕAÑ±ðKÑ±§Ì>1àÔfÇÅbê±t7^jð·kæq÷P«ëÝ$ BI$I)Uð\"§ xà:ðËô¼ÞÜ1ä½þÇü ÁüÍó`ìì@òºÌ5ÛÍ2ªÊ_ìÅ*6ËZkºä¶W*àØ¶?ZtÜ÷1\r45¤H(A¦¸Æ¬$Çfï.Åº~ÂÕö,É[&@ÆRbeTîÈrÝV¤Ý4²W\"\nr(ÐnÇ&hèðÚnucÄ»üUÜ²î¸58¶&,P\0Üh{à¥¡ê%¥<8îô¼/Ú·NÅá]ªìu-Gc@Ê±,ÿe«kc½ÍÚæÒèaÔx}÷+¬\\u1LEB!\"siáÇGgÂ4;ÁHÁX0TÙÇfÜvwh:ÈpwØ5°cAeè²à2ò:-;duS#ésSÍæÛÔê Á ÈbB:´ã­Åû×ày®ïSÔyþ^%Jwe&©ãy[ÜÜ].\r.«~§#$ÈÛWK¥µ½ÒäÜêhÐÑæpnhÃcG\'&ÃÐØÔ wat:dåaEÙs×wuÑÊÚ­E¿{KºÒÐÅwcÈï#hï\Z#¬o¼vãÞ8ÖçTv«²ÕvÎ±Ò:+\r-ãªétË¬tËÞ5ÇXæÞ6Z£Q´lYGÉu¹tºñ´oKHì¹8å7.±ÒátNÒ9uOõ?,ÉlËXeFX&Ö­BËô¡¥FS#*8Â`A±=\ZªiÂ\rBÖ¥Ý1ue[ÕVFX,DÈ·µ!«KIZ°,¡Ùæ¼ÓËÒÂ¯âbbÚâØº^¥Q¶¸;0t`è=¶6:¸Ðîà=ÓÁ¶ÃMVv£³@ãc»»CpÐ6<B@2 Æ¬ì!¤áðÁÆ<µ¶ÕÕ£VÍ¡llhm8bÒðÐº81y6Î»ÉÄ7\Z4<;´f+Ã`Û`ìØã»®ì58MGV>%ûVúbýÇê?ú¶^ÓÐÿ3eÁCø*OË/Ånz¥ùt³ÀØì{WÁz-×Íþf_Ñz#ó/í¯d~ô}#Ñoc¹ÑGçvy1ÇÆÇÇÂÝhËQ¡¢åi5e«éºÙuGæ]ãÁCèõ£¬dqô2?zÈþKÞ-ê¸¡`õÇ¶4¤u6=Odl~ãþæ£ÔxÇþüÏÐÚ[ÆÇ÷-ªû	zãÒÞôyX{×(ôzº>3ÀË÷Y/E«ùª>Ó¢?¬½hÚ4±g+QëY~v­-/}2F&*õºÇ¡ë;ÆÑäX£í¥²ìj?Üÿ3ó{oyR`Ë,#¢íF<cÄø¬7Oè{NêÑ¤R«O¨. =öÏ\'Ã0Ì0Áòhö%®Ïµ\"êìÆM;¼BÜtRÃv\rCÇmí®L¥½¨Âåa\rÑÄ~øc²cÐø\'°ñFÆÖÆhïG©yVòt^Ô¿Ðù.X½±½£´u^+ÁzÍnaÐÅ½Íç}ÚZ\\­t=#ýOáw^Â¸<#Ò>Óùßòei|d|cæ}Oå¯iÞËW¶?²Ý|mHäôÎO©ÇÜ~ÕèÅÏÔ|å¨ÁÉûãðGª8²ì¿ZÑþ+õÿ%dÝ~1ÜöËRiV,u±~&ñùGÖ4{þWÛbÊ²>§à§Éä~G³cü£Í-åwÕW2u¥ÁüÑ´uÚöT|KþWÛBËÂûÏ¤y§Ð±~#Ü§áS<ÅöX:Z±m~(Þë.#ª­ãÅj2êÎ¢à/ýL·î¼£ì<L[Ù~&zé1ÉÁâ¶Gæyw¶Ít7¸Í£«hÞ2[½¦Ñ©zÕriÉÐ72­#ð<àòG¸Ñsa{ªù£Ê>À¾îÚ86;GE_zÈð>1í_#xúÒý§vde£Å~Iås½>ª·_+,^»Ü}µvPØ÷Ñs|Ñ|Ö£ÅVGÐú\Z­cYea;\Z¨ù¨ô¸{²õF,»,>ÑÁj=ws#Þ{Ö£´xÞ=±Ò6£öeGÒ<\rºG&ônlhÚ<KY,5Ky-×U÷/©w>ÝZ<\r_+Îü®Ö÷{Y[-\Z\Z²ÓOÈøYÃ+?e°aXVDÖ$4°¬²RÖ$k\rYK\"f(äöTãß\Z¥Ä{Od|Hùã´v=KQZ5wXb2Xm&ææ£QÐø^­Þñ²ÒlX´[.#hÂê/å£hèjÐdsÊ´bÄhr¹;­{×¬Z¹®°¾\'x;ñ28.N7IxFRÿR¿Æ+Î;ØX«÷ÇÈí1ÜøÞ^ôÆõ]×HéKxÞ8£ÞmhÅõ2>î4oLGu°Þ>ÃÈñf¬Þ64ÙbÝnne/ÔvúuøXtÄòåQü_7ýZ«Y¯ìú¦&?ôºb?½A0Û@Km?ñw$S	\\@', 'Stripe', '30', NULL, '3', 90, 'txn_1HzQ6zJlIV5dN9n70r0xbPCf', 'ch_1HzQ6zJlIV5dN9n7I0yPvbwx', 'sub_IabJQk7oH7evef,sub_IabJJ7Vdzk3pIh', 'WkLe1608225440', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1111', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 00:17:27', '2020-12-18 00:17:27', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(15, 40, 'BZh91AY&SYýGÑã\0\ZK_@\0Pý+O÷¿ÿÿþ`_\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0G0	L&i¦0	L&i¦0	L&i¦0	L&i¦0	L&i¦JH 	m\ZIèÚ#Q§©äÀ©¥KeK¹yÄ}	ÙkùÚ­¦[[Z­QüA lò0q°þù\rKÿÃa¸`Ø9ürË\ròÌn6cC°òÏá9&M±ÜÈ¼6gÖ¶Ñcy©ã2nàÑ£ÈÃc#ÀÑ©¹Éñnn<æhÐî:#ssc¡Ôó777ÜÐêlaØèlhSå6{ÇpÐÐ×»>Æ´ÜxÏXä?xöt¡¡éÔ6aèÑa£ì¡üC Áà2Ðq`öC&o06µé\\ã¨àj§QaüÆÃÿ#a£ôpîrã¡°ì[Ä`ê0t7hð6Ý·Êmõ O­ËYÆffff¢GøR®Àùª\\IÔ<vç{1arA~ctüAÀÁÿCôÿ­LYffã¡Ñìj4ÌZíæ,¦ûþ¶Kl5¦ÆãÚÃ°l¿ItÝ{X©«E§44V\"²ªÑ¾çCmìÌàêlxw\rËõ#GÒ2olX¤Ã*XY*¼§-ÄÑ[[²M(å2U00N¦Ìààîl­LXòáÁ;NãuªÜ°Ø¬LWDê4?.JØ²+sÙhînu:ëñxGbv:²ÃXt&?0Ãsc¹ÉØäÐè±uO§ÁÜðs¢r150r°ë2VÂXX,Vð65ÜÃ¡à-ÛãqÐ=fÎ¸&hÜèlnt:lw½c:.Vâ`è¯ü4°ÆÃÈè4vÂêY+èrhð<\r§²ÐÔßÜ°X94nrp~Ñæ¼ïz¯Rñò;¯\r¼jÃ\n»²+S©xFç\'C¡ÔoÑ²æ00àÑo£¡¹±¹Ðæo:ÍM&§âo52q6Ng3dôMÄ¤î`tO2¹`Ðv÷íÃC£±4§A¿sC¸ÐÑ`Ãw.KÂî]bîXZ,.©n[¹v-ËÜ¸.Æó¨»°Ôuí®ÅÔº£&åØu:áØºáÁnZ-Ë©rn[\rÅ¢Ø¶.ä:t:å±n]CBì9s-ËC©t8Ðº%¥ñ+ïª¿´ûói#DÃ(°Â[\Z5ß¡¨ÁaÑ©­12U7816f`ô0åØfÚ4¥ÃE5¹`ìÂmo©hÀd¥hÔ©£CRAHÃÒwy|ËËÒªðª÷Ì\rM©ÐÐt0r65LÁÐÁÁqäll6Må¡äw6Vó¹°l`w4iÌÎM¦GCA¹°êu47éÜÐl;Í©e7È0ÁÉî`ê`ã.0]Ìæ\rÌ;Q±¡ÜÜ995n94t6Æ\rÍÆ\rÌ;-æÁ¸Èìl\rÃ©ØÜºÎææs© Ã7\'s°ll\rçS¦\\æ\ræ#UO~±©\\²~yüÃÙ=ô&.÷TWßÜ7^¡å4>þxMÓØ=ãÐn>BçðQè/À?¹n>ý¢ûKÐnaÖ^¡ûÆ\rÃq°Ü08L4Z¦©hÃFPèX:u-àË¾¤É]\'ù¬]KÿiûFðàm>C¨âüË,bzËØZÐº­§©ôÓöOþM¨ñ/ÿùÏÆm\rËiþb~IáYs-\rÈÉîÜ<æÐ½äõq|ÃöA£ø¨¾©Ð_¹Ö±lZ/9ÈÑzÆ£CCÄ_K ÆBÅ`¹¬ä½\'®w-È0\rê#aÚh¿Ý¢üÃÀSÈ¬L0Â÷X`°d\Z\Z±lXñ/ð7.gÂzOdè^¢ìZ¬Ø>Ôr#½·×{½í÷·Ý¸0döá¸õAºÚÃþ\rdÉnrt0ÑØÑÔìr¶78U´ê`ÐÁØ`ØlÜØæa,FL2ûî:iyé=óÆpXXFOm66©Ü¶/PóFåtSùÇÜ\'Ät0Áì-ÍNÅÔxëÞdé0nryCCµIè_¡|ó¹`ú$pX¼Ð¾©üOé24/¬°¾ò5ò<ìÌ4{ì\rÇÄhMí.g¡o90·/Ý?XôÝeù×Ê\Z,Nfë/°^¢à°§aØ~¥þG#ôÿ\nÌ27qwÈ`È­	ÚCÆ`>é¹}åó-Ohÿ¡Áõ0_5öJà¹SïfÓò·\ráIä]Ë¡uºÄÉüæÓØP}O À? úí0{Ï®xÎ_h`üL¶Wà_9Ê>©¡ÐÑcîçRÈp]DÜ¼F¢Þu\0|ö0Ü·8yÒxÌ~#3\'Ö:s8#a~ÉsNw6¦æ:MËç¼Øº¶-Ë!»Ù6-CÖ\'3BÚt9+¤Ì&ª}Ì§½³Aþ&Àöä/\"ú@ûW§ûiØº	õ/	ð/`øÍËæGë<³\n}&§û&ä_Ý_17³Û>ÙõÒ¦ÓÛ?¨ä¾²Ô|ÄL/¶}³Eó`ÃL¥ÚhÊzÐâ\rÌ£aê,wZ=ábÁ`4^³¼Â÷OpÑv/´÷åì.´5WêÈ_ixMMÝx·&±l^!¬MÂ<ã¨Àüå4ç¾jj{4jxMÌûÎÆçs\r\rã.ÔhÑÜàýõ2eXIVLýFMÄÊ¦FHÉµ5B±#k(ZÅTÑbd¥ôNÔWr÷à½ÐWÀ_Ð¾3¹v.ÓÔ4Q©£¸ÅÄdØ­æóE¢è½ç«iá-Ï\rK`Á Øp[P|\'v-¢º:Re)ÞyF¦Êî52{\Z9°=ç3¹NáÂ¼fåÌá9:CÀ°Ñüå^eÜÁ`Á?i|gbñ.óàp{ùF7Üpt.n[Ñ{¦ÆÂ÷¶jn¬q²n_JÉØ^,Ñï2;ÓVÃõ¼Þaì\\ñqöº#à¼øÂýïÿ¤Ökû?ê±XýÃÐéûË&eaUÆ66oØ]ÉáBCõG', 'Stripe', '30', NULL, '4', 150, 'txn_1HzQY2JlIV5dN9n7npxGtPYJ', 'ch_1HzQY2JlIV5dN9n7ffTRMxDA', 'sub_IablzGChCjY991,sub_IabllI1Ija9FqZ', 'abpf1608227117', 'Completed', 'max@gmail.com', 'Max', 'Afghanistan', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 00:45:24', '2020-12-18 00:45:24', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(16, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzQrxJlIV5dN9n7nJnrh3yE', 'ch_1HzQrwJlIV5dN9n7wiceikQd', 'sub_Iac6jOp0njvcZ3,sub_Iac6lwJwPyTmHe', 'WZUK1608228352', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:05:58', '2020-12-18 01:05:58', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(17, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzQw6JlIV5dN9n7z2iNJkEg', 'ch_1HzQw6JlIV5dN9n7zpqBE1Co', 'sub_IacAoMEFUxYZQe,sub_IacALKG6wcZfHg', 'kfbu1608228609', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:10:15', '2020-12-18 01:10:15', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0);
INSERT INTO `orders` (`id`, `user_id`, `cart`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `subs_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(18, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzQxdJlIV5dN9n7sHy34YaJ', 'ch_1HzQxdJlIV5dN9n7lqwDDknT', 'sub_IacB6DnWcXhATL,sub_IacBUAZ6g79iVh', 'cq2B1608228705', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:11:51', '2020-12-18 01:11:51', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(19, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzR13JlIV5dN9n75WILunIO', 'ch_1HzR12JlIV5dN9n7m9LAUe6o', 'sub_IacF2YhfdrkdDr,sub_IacFV2xme7o4ew', 'NBsE1608228916', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:15:22', '2020-12-18 01:15:22', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(20, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzR2sJlIV5dN9n7k7Cy7CS7', 'ch_1HzR2sJlIV5dN9n7LRA6tztz', 'sub_IacHhNIxHinXod,sub_IacHImOEQk2lkj', 'WKQu1608229030', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:17:16', '2020-12-18 01:17:16', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(21, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzR4iJlIV5dN9n7ocEC8J3p', 'ch_1HzR4iJlIV5dN9n7EjCi43iE', 'sub_IacJGKcwbxowVh,sub_IacJW5RfUqwAhA', 'dojC1608229144', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:19:10', '2020-12-18 01:19:10', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(22, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzR5gJlIV5dN9n7MwCswwmJ', 'ch_1HzR5gJlIV5dN9n7uGQlKHRv', 'sub_IacKu4SYq8CwES,sub_IacKzdIHmNq6CL', 'FRB01608229204', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:20:09', '2020-12-18 01:20:09', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(23, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzR7sJlIV5dN9n7fW7Vkoqe', 'ch_1HzR7sJlIV5dN9n7ctsXUUOg', 'sub_IacM7L6XhuPXFA,sub_IacMDZgbJ06Aei', '0HSJ1608229339', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:22:26', '2020-12-18 01:22:26', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(24, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzRPTJlIV5dN9n78s8Ny0Yc', 'ch_1HzRPTJlIV5dN9n7ityHCJ0d', 'sub_IacenoNbhqvMsE,sub_IacefBhT3r7Q8a', 'nr2j1608230431', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:40:37', '2020-12-18 01:40:37', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(25, 40, 'BZh91AY&SY\nª_³\0 Ð_@\0Pý+O÷¿ÿÿþ`ßª\0\0D\0H¤ @¨ªDH(U©`20110LÓL`20110LÓL`20110LÓL`20110LÓLÕ$Bh	OÉOQ¡¦ÓP£L\nRDÈ)&TôÊ{T~ ÔôFÒi õ6§¡=ÔÑÄÕæ¾ôöÔOÜéüFË\rFÐïI¤û&\'â\'óOÁ4ô&äÄØ&>öYdÙ4$Ý6N´ÒtMÉ8_Ãá9&M°ÿ©×Yá3Vm¥cu¥êX·:MMO)iñ54·\\¯n·Òàp¼W¡ij]åÑpS¼Þm0Î³Îo7LN³¤ÉÚs6LþS¢ÄïVML&-éOyR{ÓI¤×¿>¯mlèKÚîOzj~tå=¤õõ&ÉëMÖMÓiïLMS¢|ÉÁ:\'	¤þ)Ñ4¦\'ÃDéb|Éé1i<ºÂmièË.dÝ:§DÑ]S¤Ù?lÖyMSìOw\\Ý9\'j·&èLN©ÉºxMÄÛ|Í·ÕB/ØÆ0Áff1jÙ&E|:RêO	â®Éù/3ÜÁàÕ}krüèòì±ÂÉ1§#/E¦0içßd²¥òLý&&@ÙMk1·N	±8\'A±6b¹n>fjÒ´á&¨b¡L]÷Í·²XÌé:Í§¹7Wë«ß, ¶$É«#¯	Í½&·7²Ë´°.iEÆ\"Ôê±wXvYp¸]æÐÒìò`ÃÈÉÒ.äÅÝ7\ZMêÉ±XNKªi?;$mVIo8VÑ©Þo:ÎgûSut©à»EÙu&ªì2k\'\"È±V\nÄýIy´é;Î\'iÄÒr1:«ÃûóÃÂÖbp2uXFU1V@Ê±VCOS¼ÞtÏ[¶«tÝ9TÒr±1{¢ã®\"ÄÂ1yÌÚo9fïbcÀÜ\'!ýIC&2z&Ndò\\¦lFR°e¥à¼ËÅrZ¸jµ-Mòã!ÊÒÝr¸êcÂóN÷®õ¯Wî<6õ#&R;ÙPÒê¯TòÎ\'IÌè4Sw-«Âd\\bj­õ9Í¦óÂÜë55MODâo52q6Ng3j½ËT:E.ë	ÊêyÂbÄÑ;&¸ïÛkIÉÂmå7ï4å©j«%;GqÐl;\Z8Fãz\\\rÇa¸æ]Ç²Þue;KTëÛ]QÖ,ã´º®¸»z\\\rÆã¨ån6ÃCaµ,4º®GZYKq°Üu¥¨v3Õn5.£¢â]3 è9uWàJþSð³\"Ú¡dÅLª²`MMBdû´,,-Ml²id,Do5:L1dÅ²dÌÚ´\',2cmMàÕKPO\nÄîá¨·¢YVL:i&MIZMÔÂ&LÉè¼ißÎ¯?JGR/¹`bm8¬ò\'3Új]q:ÌN&\'DòMdâo4¦á¤ó&ÉÖ·^&ÄÚa<MM1ÄÒu\'3DÞlgY¤ßóDÙ;MëeUc%»\"dÄâ`wf\'Lº1Wyï17;\'´ÒwÄÕºq4LÍj¶ÍE²Én²[Wu¡²îµ\'ybv¹:ÎÓzº®óIÑ17Nó¬Ñ2fñw&ÄÚlNfÉ¼ë8upæ&ëÂq4Uóö¦úb~åõ¯Á6Orô_¡lWð~$ÜzÓÉi?O3<-eîOz&éõ*ãíY?¢z*ûIøÕº{Gå óëKÔ=2Áö.²É¼Ý4&Éºl	Ñ7MLZ\ZMRæZU¥K	t.«Q¡øÒî2ZºI®óT:Ù0~RÁÿ2öÔ¶:uNêcÙWº­*õNjê6^»ìOÝ?ÎhzkÄÇ¢~d·OçKh¾âOe\\yL^õ^S½<æ.aæ=Õ=0ÞÂbýËóï¥éléòº¥êÃRÈz\'2ÐõKâ´µ-KÆû¤±d¬õLYÈó©Ül<©eRÑt_D¦ÉÙj¯ô }¯tð\'ò¡Å¹`ÅÉd¥ª©v¼GøK&ãðs×:Hì5\r³e4¾Á¢æ¥ä»Öß#ßî·7Üß{t¾dÄ÷¦éëODÞ¶0ùçï¼âs2jvgiÂÚo:&ÈÙuLNÉ´¶n¶\\ÌEºÐÊ\\ËXÃ÷¾ºÕ_?öyÏtñ¥dñÓe´ÍNãaé¢y-èrEõ§É\\¯ÌÌê·]ªê¤ðÅ­Ö.V&óç>&¥Ì¶¬]\'ýu|ïrî2_d.Uà<ÇÛ?zÿÉ¨|ùÏ­_5ä9®wX´½cîÒÞ_jaôÏ1¼æ}hn<gz~ÙyÃï¬¬Ïë«æSùºÀÅ²}¡ÿÉÂ}âÉStùS¼õ¥ÁV¥2X=//¾n?õ\ZÉ¬¸_jÉb>µ}Ô®3Ê~Âmm?PôCyÄ·KIä;¬§%]Qq2	µ{jº¤úØ9?}2b|Êx.ÑUô¥ýCÙJàx­pCÍ\'Ð´\\ÍLM§ÉVó­X¬¦ãÆZºÃyÖ¥ÅRøUü¦-ÆëÈ{ëÆd·X¿Id¹>Rè<G3ã-¡øÕåW1Îëdµ0«Ñ.qÅ¼ào6mãÞõÍõRs5\r§Idä§)n²-ñq^J¾e¢e°¯b8y}RúUãQýóaÀÉ´ì:J|¥Â|®_¸ú¢ý³Àv±G½jxËîUä?üêîWÖSy|V,©{\'Ò}²íHÚ{\'ñ#ä£øËç-\ZLIôZed±aÈ;-@ú®¯IÑ&ó*l±ÅÞ­RÉî¥aÊ©w=³Û-ÃÆm=ÃqëÉi/íÂ>ÂjotÖá¼9XU°ØxÒÖ%H¸·YeJ}KTw_¥¥îÔÒðµ?4ó¦vÎó&SdÒh¦èçIþQ?à±b2%F,¦~¹R,¦HÂÈY1Jµ©ªeY#RÈX±\"Õ5+S%0°C0«í\\¯]ôö\"àzçØUðÆ]ÆwiéjiwUÁdØ«y½j­UÈøO^ËÀo=S&bbi-¥ÀØe.µ/òÃ¤®Qi,w^TÒÄÅZ.Ý4±{ÓÇI©©Å:$Ù\'Âp»ÁÞ\n¼fææqNWD¼\"ýj?<O@î²,üÆv#¼øT½´½°ýRËråÂè:\"Ün8KCÛ6Z«%õ=ÃÙ57+!Þ[Sqï«\'hxÙi`÷,u-[&&ãuºÉO¬íWE_À\'tøÌÔùÇÍ*ûQûÍRÓ4üB¿\"ê1þ	è¹Å_)ùâÆY1¶,Ùm_þ.äp¡ T¿f', 'Stripe', '30', NULL, '5', 150, 'txn_1HzRQ7JlIV5dN9n7AaFhwXJW', 'ch_1HzRQ7JlIV5dN9n7P2zpZ6X0', 'sub_Iacf4AzZXX2xuF,sub_IacfIMwWdpx1oi', 'cRhs1608230471', 'Completed', 'max@gmail.com', 'Max', 'Andorra', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-18 01:41:17', '2020-12-18 01:41:17', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(26, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1HznrNJlIV5dN9n7b8ER1OVJ', 'ch_1HznrNJlIV5dN9n7Q71QtPtL', '', 'jvCJ1608316764', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 01:39:25', '2020-12-19 01:39:25', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(27, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1Hznv0JlIV5dN9n7QYOkDpW2', 'ch_1Hznv0JlIV5dN9n7Mrj3f0xO', '', 'q0Jc1608316989', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 01:43:10', '2020-12-19 01:43:10', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(28, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1HznvQJlIV5dN9n7qMqNlF85', 'ch_1HznvQJlIV5dN9n7lA7z5pvj', '', 'MvXb1608317015', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 01:43:36', '2020-12-19 01:43:36', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(29, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1Hzo3nJlIV5dN9n72YvLeS6F', 'ch_1Hzo3nJlIV5dN9n7MNEGWVUw', '', 'fRqo1608317534', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 01:52:15', '2020-12-19 01:52:15', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(30, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1Hzo6pJlIV5dN9n75sLm3KVX', 'ch_1Hzo6oJlIV5dN9n7NkwBohM7', '', 'YRls1608317722', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 01:55:23', '2020-12-19 01:55:23', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(31, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1HzoAEJlIV5dN9n7L8A8KHRa', 'ch_1HzoAEJlIV5dN9n7Hphr3t4l', '', 'i0Lg1608317933', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 01:58:55', '2020-12-19 01:58:55', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(32, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1HzoDSJlIV5dN9n7vEXz0yxu', 'ch_1HzoDSJlIV5dN9n7lh0RW01W', '', 'v6dP1608318133', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 02:02:14', '2020-12-19 02:02:14', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(33, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1HzoEDJlIV5dN9n7zWYHX2XO', 'ch_1HzoEDJlIV5dN9n7d5TdRjwC', '', 'CsV81608318180', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 02:03:02', '2020-12-19 02:03:02', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0),
(34, 40, 'BZh91AY&SYîoëÑ\05_@\0Pý+O÷¿ÿÿþ`	¿ô\0J\0\0è\04\0Q&4CM\ZhÈÐ\Z1@\09LÈÀÄÄÂa0CLM0\"U< \0\0\Z4\0\09LÈÀÄÄÂa0CLM0Q56\0\0\0\0\0\0\nM&i!¦§Qµ<§M3T²F	;Í!>ÿõú[\nÆ-#òE£ý)û0ÿ/Ígøa5~VÂßôeòÚ0ê×ôÑÍ«e1SÿáwZánt­\'ZCiå(-Dø\r hNO°2%·\0dÜq§ëZSWB©±âöCÅkx­ÍÜÉø<ó·v­±n®Ça)à¦£ÌÔÑªßE¶S¬Ògm-ÔÊ_n¬2Ý¢Óu4ÃôaþúáÑçpÔÙØr-éSu0æ·?dw}Rªª®D?tT\rÑî$i\'#âëáRµ<Ï¥Äh§þ?©ýe±±N©ÊT­Pþ´¥½©¤ÖhÁô`\'5U FU¥¦½(XPú\nF$R¥\"0JÕ#BR\Z®r®+tã%¹::Ò¨à®\Z/IX¥&ë|õ&%EêÂøÎûiÏíhÔá¤nØ¹ºJ%>¥c\Zr×}m²¶+EIR¢T¤©½myræll¥;&ôPo±··.æ­%5ì«S¢´JèÝq´¡R®Çç¬hÝ¤Ä¤­o\ZëðupêãÕæurqÍ:ÔY»§\\ë¦Ú-»6ÑEIRï]qveºÖ[½£+SFÑ«VÖ&mØ4RKáÆ£<[ÖRFóY Å(^0ã ® ¼«¸¾x¯NIR¨dêòÊe¬I	\\@ä+¤ÁSI¤ÔßY¢6Ü£33rÓVµ2:±7)*¤¥n>MÔ¨&d		T»¿\\¤®ÅËÅ®¡T2ôaGm.ªîÒTJÔ¥IhB(ipk\"èT+gb»\'^Ù#ðQLk^Û6¦¬\\ì¦ôÖ;q\ZæÜ²[®øo2ã(âîµ¶ôÕµÃ}í·\ZrËI3MhâÔ8£m\\Ø·5ÖòÖÊãLS8[Í1Å.cM2©ËÙ7å»h¦mÆöUf8§<Á¶ã}µbÕ­Ò}«?Z}ï­ù°ðv¾\Z)³ûO¦¾f]Î«};W6\'k/jk÷+õv§Ü~Ó/4ø\'ºv³TïÊömÊ³°Ã,2Q£+«,ÕrêêJo[ËqÄ¥¼PÙü4é¼©¤û>\nÄaínÓêTwÏ	iÓi»4·ÞþëOýèø±óÜÄ{ïÌ)âiâìV©Û<re=3¾úÕþGtõ¶M£¹.Z«EÎå}·ksM^UGrµç¡AsGªr\\û;¿ÑÒJ*ªyêU%)-ko19Îo%15y;fÓ¶r7b/¾\\Ôuã×ßwvSÌ§Îîv³1^VøWÈ©v«å{ò×Ñ\réjrSg\Zª¹FªªM\'É6Â~ÎçIEJ9¦Â­ÊbvººfMÖÑô¾Sg¥µSÍ3nSwG7rò¦Êg^ÊSG®¶vÏ©çå)ÞÊq*Ç>á Ý(6Ëør¡¡,ÁA86PºuvÌµ|¤Ìèàû©î¯¡òEÊ5R{\'µ;¦¢rr}K\r_þP^ùÃÂ)RKE*yàè¤SÞÌú\'¾[Îÿ¶ºTT÷¾Q¤ÕÕô9­óÎÄÃ,ÅRuM¦èÖMá¢£twÄôåêâ©ãìq´ê{|hx\r\'9ïjª=KQµÓ2gyQ¤Ý+tËq zéYÑ]g75ñSU=§I«GFôujkEªNÆÌÌ30Ò\\Ä¨ð\\ÌwE¦\Z©¬&U=®ÈSÎ³÷`yãäN³Ð+¡>Æ&r#Ø©ÍéS3ÝøsÑnä©:ÏýÜG½zªþw¹ër;õk>H«Ú¹Ð=Ïrç½Tª¥IÉq=®éÛ ÍFÒÂÊyÄ¤¡s¿Oç)ÑQgfPN°²\ru#¢*PÅIØT¸iYl£Øö®NK[ÂíÍ~®Ï§x¥SYhõÝñ§ó÷)IP©_]-\n)%JE(.JETI*P¢*«ÌÙÝ\'Æ\\44õ6§äî\\¯)*)2ÊåÍ]Øs3Ò¬ÁK0ÒbQ¸ôºÎScfñiP:ËRÍZ¸Z.z]/Y¢0-\\DàÒ:*i\Z2i´sæ?NÉÊõ9Ns¤x)ó³#M¡¬\\ñbÔ÷ªyO³0fzäO*9L/\nueCyø¨3Éí¦±ètÓ§³áþïö!ñn£û;ZÒ{þhERbÃÿÅÜN$;úô@', 'Stripe', '30', NULL, '2', 150, 'txn_1HzoGkJlIV5dN9n7YM0fkfUC', 'ch_1HzoGkJlIV5dN9n7GsNOwKbc', '', 'qDUz1608318338', 'Completed', 'max@gmail.com', 'Max', 'Australia', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 02:05:39', '2020-12-19 02:05:39', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0);
INSERT INTO `orders` (`id`, `user_id`, `cart`, `method`, `shipping`, `pickup_location`, `totalQty`, `pay_amount`, `txnid`, `charge_id`, `subs_id`, `order_number`, `payment_status`, `customer_email`, `customer_name`, `customer_country`, `customer_phone`, `customer_address`, `customer_city`, `customer_zip`, `shipping_name`, `shipping_country`, `shipping_email`, `shipping_phone`, `shipping_address`, `shipping_city`, `shipping_zip`, `order_note`, `coupon_code`, `coupon_discount`, `status`, `created_at`, `updated_at`, `affilate_user`, `affilate_charge`, `currency_sign`, `currency_value`, `shipping_cost`, `packing_cost`, `tax`, `dp`, `pay_id`, `vendor_shipping_id`, `vendor_packing_id`) VALUES
(35, 40, 'BZh91AY&SYß¢XÁ\0\r«ß@\0Pý+{ÿ¿ÿÿþ`\rß@(\0\0 P\0PPQ%JAU*\"PÍ101¦&sFLL\0LFi#ÉFÑ\0\Z`AÁ2`4dÄÀÄ`Fb0L`Djb\rMGâ)£Ô=MFF@õ\ZdH	4ÒOõ\Z\ZSÔ{RõÔÓi3Ò~©hÙò|çêý_iúæeÎrù!2È£Sqò.OÈÁF¥9S#ó2`Þ,ÐÁÔ,W½\rkøb´2iB*ÑÍd/z©GE­¬0w-Ë£«8`àÙ©ÜÙXPÙ»«,°£vpÙú¦¥-AJ<Qä¨²ÏqgIàd~G ·¼ÜìY°³©ÞwQè(Ám9¨ÐÔ³þÍ6(èUyÎÊÁÔi(enÍ74,hÁù?gV}ç¤Ðj`ÕÉÐYì(Ü£BÎ}2¼6É	$!$DCà!\\92O¡Ñ9>sSA©÷3T¢z?öåó<Ãîç¼ó;ÉÞýÊ(¢OyØh	gØûËØ¦ëÄÚ\"R$©!jåL0S-w)«ð5-í42lY\"ÊI(î;yþFæC	>âåGb;!ªTG6ô\'e;ÔêìÑ»\"Í`¥\nJn5a¹IªS(µF©e¡iP©&-»-·~4¶S%9B\"Ì©k,Q)J(ï)»\rµpÕg¨¢bS¥ÒD¤JJ%&ÊjÃVíÒY¡I©bÎJC4E$7lÃ-®pð,AýÅ9QÜQÁ±ÜÂl$¤¡9h·GFÍ\\e©¡±WJCD¢Se²Õ³âjru:>Ýâ²ÜÃuH¤ÉË£-\Z5QÔ©3(RJLÃFe«VÓIß.Z\\óÍfeÈÙ«F÷Cf£­«Aoj,\\\Z n73§&MjÏ8,²\Z#dÕ4JMÓdÂr&éÈÑ2&SSÑ8fnNÁgn¦è©fS&Í©ºl\Z&RÓ)ºjÊ`´ÂZa0)4MQ»TÐL&SqdàÖh¬K7M&³dÙ,8|xx-UBDQakOÊË%))k¨)RIÌR¥)7)ÙLR©t¹K¤9)p(YI,¤¥IÂâE¬¹j ¥ANÓ¼ï:SÒCÊTa«gejai½¨ÕFf0jÊÎÎ­Ø739`aC­MVn£Se°nÝfÜµ8fb$*5T¨ÝHr¡Êàðag,Z­UrÁÂ°YQË)i+\Z2TpÀÙ»SyG*4(ÊÎ[¬R²Tt`a³[·lÔåFe\Z®HöÞ\\~|gîÑ>¹gÞï99?\"øBàÀõS¨67!ôâMmÀt>aí=MÉ¯ÆSô<	üì<SÒO­;R2xü8S,`ÁE\r¥¥¥KKRÔÉFênÊQg®BTió\'M÷Ï¥-?IýÄÉ¹£Á<édèÕ?Á¸=#Gî}!öæú¡´jü\0¨Ä<I¬e\n$ê©âwÊjNÉë\'2OiÒSýÕÅ¿ ðO)±6-,¢u?¬³¡5=j¡DxJ>dí<Ó¢a:£	¬òCIiàà;§rJJRÐ¤¥e\rGÅÜ¨ÙÜévºHâ4Q[½.5®÷»\"Ê<ÇìjdÂüTú%&Z¶RÜ-»¦h`bn¢Ê8(ÁXpm0J@BnÃÈà8Kñüv;ÛÆÄ(4njUz£Ú\ZÝEÂæÆçÐrk=§E(ó¦W8MÎã¡á/2¦Òµw¨£CÉM§dô·J<\'A£jå~£É@xÁâò&¡°t¹¡Ò?I\'°²¤¢©FÜÃ²i5q29¹Ø\ZÁí!Þ Á³Þ<AØ7Ä1a@øÜP;G\'©dQIéAÝ(>©÷ëKñ4o A óC!xÙÚ÷½\rñÔ\r[ä!Ò9ÀÂà!{kW¨j8	æDõ{dú(¯SÊrÙ:Éõ\n>jz$\'Dúæ¤PöË)6Z,û	éPÂnFS¸´¦äÌÜ4­0fS¹9£*|ÏÑ,ÚTò7NäÚi;û4u#\\±¼Úe0e14KL$C­ ÜÑ/h\r[l.J\\Á(Ë°Ié¬\ný¦CÔ é =Ã¬õFñ_qà	Ä æï°÷·p¤îN\'±s©÷ÊIÞá?cÞFOZMÏsÀ1U*õ¾@8r\nbAæóh=ÀB`¸´äìeàJQJ\"¦Aiæs>Å§ñ(9¯`Ü>#¸q V÷mE,RMS	Ô]B¥¡£¼ðhnPút´t¹rç¦×;¥¼äá*)F,d¦Öå£áâ*©Q<HÐbEÅF0AdT I!VA¡ ÅFEK=Nh¹h!xõH»Á°ïqXZBå½Gn¢¦5ËKL5x¼ÙÒeÕK)«TÊP°öNôàj:ÁHªdêJ0 49;M¢ÚÆa³9\ZéI¤4MC¢R?½#½8Q(¢?{g	Ñ9Ðñ$ûLÄrhÙ6C)Ö©û\'iCÑ(¹ÿÆ©dèhaSCZÖ¨å1-ùHp\'ÎP2ä÷(Õì9IíD¤úóµªDù?äí6¢{ßj¤¥B°³*Ô?ñw$S	\rú%', 'Stripe', '30', NULL, '2', 150, 'txn_1I076nJlIV5dN9n7VzsuxXSv', 'ch_1I076nJlIV5dN9n7OPhT8SO5', '', 'YdZs1608390756', 'Completed', 'max@gmail.com', 'Max', 'Algeria', '1121212121212', 'test add', 'test city', '324234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2020-12-19 22:12:37', '2020-12-19 22:12:37', NULL, NULL, '$', 1, 30, 0, 0, 0, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_tracks`
--

CREATE TABLE `order_tracks` (
  `id` int(191) NOT NULL,
  `order_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tracks`
--

INSERT INTO `order_tracks` (`id`, `order_id`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:46:22', '2020-12-17 20:46:22'),
(2, 2, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:50:13', '2020-12-17 20:50:13'),
(3, 3, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:56:10', '2020-12-17 20:56:10'),
(4, 4, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:57:42', '2020-12-17 20:57:42'),
(5, 5, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:58:12', '2020-12-17 20:58:12'),
(6, 6, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:58:42', '2020-12-17 20:58:42'),
(7, 7, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:59:08', '2020-12-17 20:59:08'),
(8, 8, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:59:31', '2020-12-17 20:59:31'),
(9, 9, 'Pending', 'You have successfully placed your order.', '2020-12-17 20:59:58', '2020-12-17 20:59:58'),
(10, 10, 'Pending', 'You have successfully placed your order.', '2020-12-17 21:02:58', '2020-12-17 21:02:58'),
(11, 11, 'Pending', 'You have successfully placed your order.', '2020-12-17 21:03:39', '2020-12-17 21:03:39'),
(12, 12, 'Pending', 'You have successfully placed your order.', '2020-12-17 21:04:22', '2020-12-17 21:04:22'),
(13, 13, 'Pending', 'You have successfully placed your order.', '2020-12-17 21:04:45', '2020-12-17 21:04:45'),
(14, 14, 'Pending', 'You have successfully placed your order.', '2020-12-18 00:17:27', '2020-12-18 00:17:27'),
(15, 15, 'Pending', 'You have successfully placed your order.', '2020-12-18 00:45:24', '2020-12-18 00:45:24'),
(16, 16, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:05:58', '2020-12-18 01:05:58'),
(17, 17, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:10:15', '2020-12-18 01:10:15'),
(18, 18, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:11:51', '2020-12-18 01:11:51'),
(19, 19, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:15:22', '2020-12-18 01:15:22'),
(20, 20, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:17:16', '2020-12-18 01:17:16'),
(21, 21, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:19:10', '2020-12-18 01:19:10'),
(22, 22, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:20:09', '2020-12-18 01:20:09'),
(23, 23, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:22:26', '2020-12-18 01:22:26'),
(24, 24, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:40:37', '2020-12-18 01:40:37'),
(25, 25, 'Pending', 'You have successfully placed your order.', '2020-12-18 01:41:17', '2020-12-18 01:41:17'),
(26, 26, 'Pending', 'You have successfully placed your order.', '2020-12-19 01:39:25', '2020-12-19 01:39:25'),
(27, 27, 'Pending', 'You have successfully placed your order.', '2020-12-19 01:43:10', '2020-12-19 01:43:10'),
(28, 28, 'Pending', 'You have successfully placed your order.', '2020-12-19 01:43:36', '2020-12-19 01:43:36'),
(29, 29, 'Pending', 'You have successfully placed your order.', '2020-12-19 01:52:15', '2020-12-19 01:52:15'),
(30, 30, 'Pending', 'You have successfully placed your order.', '2020-12-19 01:55:23', '2020-12-19 01:55:23'),
(31, 31, 'Pending', 'You have successfully placed your order.', '2020-12-19 01:58:55', '2020-12-19 01:58:55'),
(32, 32, 'Pending', 'You have successfully placed your order.', '2020-12-19 02:02:14', '2020-12-19 02:02:14'),
(33, 33, 'Pending', 'You have successfully placed your order.', '2020-12-19 02:03:02', '2020-12-19 02:03:02'),
(34, 34, 'Pending', 'You have successfully placed your order.', '2020-12-19 02:05:39', '2020-12-19 02:05:39'),
(35, 35, 'Pending', 'You have successfully placed your order.', '2020-12-19 22:12:37', '2020-12-19 22:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(191) NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `header` tinyint(1) NOT NULL DEFAULT '0',
  `footer` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pagesettings`
--

CREATE TABLE `pagesettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `contact_success` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_title` text COLLATE utf8mb4_unicode_ci,
  `contact_text` text COLLATE utf8mb4_unicode_ci,
  `side_title` text COLLATE utf8mb4_unicode_ci,
  `side_text` text COLLATE utf8mb4_unicode_ci,
  `street` text COLLATE utf8mb4_unicode_ci,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `fax` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `site` text COLLATE utf8mb4_unicode_ci,
  `slider` tinyint(1) NOT NULL DEFAULT '1',
  `service` tinyint(1) NOT NULL DEFAULT '1',
  `featured` tinyint(1) NOT NULL DEFAULT '1',
  `small_banner` tinyint(1) NOT NULL DEFAULT '1',
  `best` tinyint(1) NOT NULL DEFAULT '1',
  `top_rated` tinyint(1) NOT NULL DEFAULT '1',
  `large_banner` tinyint(1) NOT NULL DEFAULT '1',
  `big` tinyint(1) NOT NULL DEFAULT '1',
  `hot_sale` tinyint(1) NOT NULL DEFAULT '1',
  `partners` tinyint(1) NOT NULL DEFAULT '0',
  `review_blog` tinyint(1) NOT NULL DEFAULT '1',
  `best_seller_banner` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link` text COLLATE utf8mb4_unicode_ci,
  `bottom_small` tinyint(1) NOT NULL DEFAULT '0',
  `flash_deal` tinyint(1) NOT NULL DEFAULT '0',
  `best_seller_banner1` text COLLATE utf8mb4_unicode_ci,
  `best_seller_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner1` text COLLATE utf8mb4_unicode_ci,
  `big_save_banner_link1` text COLLATE utf8mb4_unicode_ci,
  `featured_category` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pagesettings`
--

INSERT INTO `pagesettings` (`id`, `contact_success`, `contact_email`, `contact_title`, `contact_text`, `side_title`, `side_text`, `street`, `phone`, `fax`, `email`, `site`, `slider`, `service`, `featured`, `small_banner`, `best`, `top_rated`, `large_banner`, `big`, `hot_sale`, `partners`, `review_blog`, `best_seller_banner`, `best_seller_banner_link`, `big_save_banner`, `big_save_banner_link`, `bottom_small`, `flash_deal`, `best_seller_banner1`, `best_seller_banner_link1`, `big_save_banner1`, `big_save_banner_link1`, `featured_category`) VALUES
(1, '', 'itssohail97@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(191) NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateways`
--

CREATE TABLE `payment_gateways` (
  `id` int(191) NOT NULL,
  `subtitle` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(199) DEFAULT NULL,
  `status` tinyint(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_gateways`
--

INSERT INTO `payment_gateways` (`id`, `subtitle`, `title`, `details`, `type`, `status`) VALUES
(47, 'Paypal Express', 'Paypal Express', '<font size=\"3\"><b style=\"\">Paypal Express</b><br></font>', 'secure', 1),
(48, 'Cash On Delivery', 'Cash On Delivery', '<font size=\"3\"><b style=\"\">Cash On Delivery</b><br></font>', 'nonsecure', 1),
(49, 'Bank Transfer', 'Bank Transfer', '<font size=\"3\"><b style=\"\">Bank Transfer</b><b>&nbsp;No: 6528068515</b><br><br></font>', 'secure', 1),
(50, 'Money Order / Cheque Payment', 'Cheque Payment', '<font size=\"3\"><b style=\"\">Cheque Payment</b><br></font>', 'secure', 1),
(52, 'Wire Transfer', 'Wire Transfer', '<font size=\"3\"><b style=\"\">Wire Transfer</b><br></font>', 'secure', 1),
(53, 'Company Cheque', 'Company Cheque', '<font size=\"3\"><b style=\"\">Company Cheque</b><br></font>', 'nonsecure', 1),
(54, 'Open Shipment', 'Open Shipment', '<font size=\"3\"><b style=\"\">Open Shipment<br></font>', 'nonsecure', 1),
(55, 'Personal Cheque', 'Personal Cheque', '<font size=\"3\"><b style=\"\">Personal Cheque<br></font>', 'nonsecure', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pickups`
--

CREATE TABLE `pickups` (
  `id` int(191) UNSIGNED NOT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(191) UNSIGNED NOT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `build` varchar(255) DEFAULT NULL,
  `build_type` varchar(255) DEFAULT NULL,
  `product_type` enum('normal','affiliate') NOT NULL DEFAULT 'normal',
  `affiliate_link` text,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `category_id` int(191) UNSIGNED NOT NULL,
  `subcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `childcategory_id` int(191) UNSIGNED DEFAULT NULL,
  `attributes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_qty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `color_qty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color_price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `previous_price` double DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `stock` int(191) DEFAULT NULL,
  `policy` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `views` int(191) UNSIGNED NOT NULL DEFAULT '0',
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text,
  `colors` text,
  `product_condition` tinyint(1) NOT NULL DEFAULT '0',
  `ship` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_meta` tinyint(1) NOT NULL DEFAULT '0',
  `meta_tag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `youtube` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Physical','Digital','License') NOT NULL,
  `license` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `license_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `platform` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `licence_type` varchar(255) DEFAULT NULL,
  `measure` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `best` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `top` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `hot` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `latest` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `big` tinyint(10) UNSIGNED NOT NULL DEFAULT '0',
  `trending` tinyint(1) NOT NULL DEFAULT '0',
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_date` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_qty` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `whole_sell_discount` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_catalog` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_id` int(191) NOT NULL DEFAULT '0',
  `color_gallery` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_prices` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `pro_type_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `pro_type_child` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `acc_brand_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `acc_type_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `carrier_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `cases` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `headphones` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `chargers` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cables` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `build`, `build_type`, `product_type`, `affiliate_link`, `user_id`, `category_id`, `subcategory_id`, `childcategory_id`, `attributes`, `name`, `slug`, `photo`, `thumbnail`, `file`, `size`, `size_qty`, `size_price`, `color`, `color_qty`, `color_price`, `price`, `previous_price`, `details`, `stock`, `policy`, `status`, `views`, `tags`, `features`, `colors`, `product_condition`, `ship`, `is_meta`, `meta_tag`, `meta_description`, `youtube`, `type`, `license`, `license_qty`, `link`, `platform`, `region`, `licence_type`, `measure`, `featured`, `best`, `top`, `hot`, `latest`, `big`, `trending`, `sale`, `created_at`, `updated_at`, `is_discount`, `discount_date`, `whole_sell_qty`, `whole_sell_discount`, `is_catalog`, `catalog_id`, `color_gallery`, `vendor_prices`, `pro_type_id`, `pro_type_child`, `acc_brand_id`, `acc_type_id`, `carrier_id`, `cases`, `headphones`, `chargers`, `cables`) VALUES
(249, 'QZw3436EYr', '', '', 'normal', NULL, 0, 43, 69, NULL, NULL, 'test test', 'test-test-qzw3436eyr', '160517346485hojsuP.png', '1605173464xbnP4hKt.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 0, '<span style=\"color: rgb(0, 0, 0); font-family: &quot;El Messiri&quot;, sans-serif; font-size: 16px; font-weight: 600;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span><br>', 30, '<br>', 0, 1, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-12 04:31:04', '2020-12-03 16:55:52', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', 'Select Accessory', '', NULL, 0, 0, 0, 0),
(250, 'Epj6234hWi', '1', 'hair', 'normal', NULL, 0, 43, 69, NULL, NULL, 'Hair Oil', 'hair-epj6234hwi', 'hair.png', '1605276523UE7VmA2P.jpg', NULL, '4.oz,8.oz', '0,1000', '30,60', NULL, NULL, NULL, 0, 0, '<br>', NULL, '<br>', 1, 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-13 09:08:43', '2020-12-19 22:12:37', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', '', '', NULL, 0, 0, 0, 0),
(251, '61x671684T', '1', 'body', 'normal', NULL, 0, 43, 69, NULL, NULL, 'Body Oil', 'body-61x671684t', 'body.png', '1605276801yspJ0XkV.jpg', NULL, '4.oz,8.oz', '1000,1000', '30,60', NULL, NULL, NULL, 0, 0, '<br>', NULL, '<br>', 1, 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'Physical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, '2020-11-13 09:13:21', '2020-11-13 09:13:21', 0, NULL, NULL, NULL, 0, 0, '[]', NULL, '19', '', '', '', NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_clicks`
--

CREATE TABLE `product_clicks` (
  `id` int(191) NOT NULL,
  `product_id` int(191) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_clicks`
--

INSERT INTO `product_clicks` (`id`, `product_id`, `date`) VALUES
(515, 249, '2020-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `product_discounts`
--

CREATE TABLE `product_discounts` (
  `id` int(11) NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `discount_id` int(10) UNSIGNED NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `product_id` int(191) NOT NULL,
  `review` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rating` tinyint(2) NOT NULL,
  `review_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(11) NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `comment_id` int(191) UNSIGNED NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `product_id` int(192) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `photo`, `title`, `subtitle`, `details`) VALUES
(7, '1605168717client.png', 'Emma Walker', 'Owner', '<span style=\"color: rgb(255, 255, 255); font-family: Consolas, &quot;Lucida Console&quot;, &quot;Courier New&quot;, monospace; font-size: 12px; white-space: pre-wrap; background-color: rgb(36, 36, 36);\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam </span><br>');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seotools`
--

CREATE TABLE `seotools` (
  `id` int(10) UNSIGNED NOT NULL,
  `google_analytics` text COLLATE utf8mb4_unicode_ci,
  `meta_keys` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seotools`
--

INSERT INTO `seotools` (`id`, `google_analytics`, `meta_keys`) VALUES
(1, '<script>//Google Analytics Scriptfffffffffffffffffffffffssssfffffs</script>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `user_id`, `title`, `details`, `photo`) VALUES
(10, 0, 'Choose Body or Hair Oil', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.', '16083187983.png'),
(11, 0, 'Choose Flower', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.', '16083188212.png'),
(12, 0, 'Choose Stones', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.', '16083188301.png'),
(13, 0, 'Choose Essentials', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.', '16083188364.png');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` int(11) NOT NULL,
  `user_id` int(191) NOT NULL DEFAULT '0',
  `title` text,
  `subtitle` text,
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `user_id`, `title`, `subtitle`, `price`) VALUES
(1, 0, 'FedEx Overnight', '1day', 30),
(2, 0, 'Free Shipping', '3 Day', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(191) UNSIGNED NOT NULL,
  `subtitle_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle_size` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_color` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle_anime` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `title_size` varchar(50) DEFAULT NULL,
  `title_color` varchar(50) DEFAULT NULL,
  `title_anime` varchar(50) DEFAULT NULL,
  `details_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details_size` varchar(50) DEFAULT NULL,
  `details_color` varchar(50) DEFAULT NULL,
  `details_anime` varchar(50) DEFAULT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `subtitle_text`, `subtitle_size`, `subtitle_color`, `subtitle_anime`, `title_text`, `title_size`, `title_color`, `title_anime`, `details_text`, `details_size`, `details_color`, `details_anime`, `photo`, `position`, `link`) VALUES
(11, 'Hair care.', '20', '#cf1313', 'fadeIn', 'Body care.', '20', '#5735db', 'fadeIn', 'Essential oil.', '10', '#000000', 'fadeIn', '1606850122Slider 2.jpg', 'slide-two', '/buildown'),
(12, 'Phoenix 1', '20', '#000000', 'fadeIn', 'Phoenix 2', '20', '#000000', 'fadeIn', 'Shop', '20', '#000000', 'fadeIn', '1606843548chakra-06.jpg', 'slide-two', '/shop');

-- --------------------------------------------------------

--
-- Table structure for table `socialsettings`
--

CREATE TABLE `socialsettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gplus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dribble` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_status` tinyint(4) NOT NULL DEFAULT '1',
  `g_status` tinyint(4) NOT NULL DEFAULT '1',
  `t_status` tinyint(4) NOT NULL DEFAULT '1',
  `l_status` tinyint(4) NOT NULL DEFAULT '1',
  `d_status` tinyint(4) NOT NULL DEFAULT '1',
  `f_check` tinyint(10) DEFAULT NULL,
  `g_check` tinyint(10) DEFAULT NULL,
  `fclient_id` text COLLATE utf8mb4_unicode_ci,
  `fclient_secret` text COLLATE utf8mb4_unicode_ci,
  `fredirect` text COLLATE utf8mb4_unicode_ci,
  `gclient_id` text COLLATE utf8mb4_unicode_ci,
  `gclient_secret` text COLLATE utf8mb4_unicode_ci,
  `gredirect` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socialsettings`
--

INSERT INTO `socialsettings` (`id`, `facebook`, `gplus`, `twitter`, `linkedin`, `dribble`, `f_status`, `g_status`, `t_status`, `l_status`, `d_status`, `f_check`, `g_check`, `fclient_id`, `fclient_secret`, `fredirect`, `gclient_id`, `gclient_secret`, `gredirect`) VALUES
(1, 'https://www.facebook.com/Phoenix-rising-collection-119021809940833', 'https://www.instagram.com/p/CIGZt7GH5Mg/?igshid=14o9op7pgy7rg', '#', '#', '#', 1, 1, 0, 0, 0, 0, 1, '503140663460329', 'f66cd670ec43d14209a2728af26dcc43', 'https://localhost/wireless/auth/facebook/callback', '536622682580-n50patemn5m6knmmvjl9dgqot0qhtf5o.apps.googleusercontent.com', 'LvTUnxm9JnyWwguZCgL_UlRl', 'http://localhost/wireless/auth/google/callback');

-- --------------------------------------------------------

--
-- Table structure for table `social_providers`
--

CREATE TABLE `social_providers` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `provider_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stripe_keys`
--

CREATE TABLE `stripe_keys` (
  `id` int(11) NOT NULL,
  `product_key` varchar(255) DEFAULT NULL,
  `stripe_product_id` varchar(255) DEFAULT NULL,
  `stripe_plan_id` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stripe_keys`
--

INSERT INTO `stripe_keys` (`id`, `product_key`, `stripe_product_id`, `stripe_plan_id`) VALUES
(1, 'Hair Oil 4.oz yearly', 'prod_IaXp1sD9yWsVY1', 'plan_IaXpGVCnU7qRkC'),
(2, 'Hair Oil 8.oz yearly', 'prod_IaXyUqF8hL8Ocm', 'plan_IaXyMyx4CRXYJ4');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(191) UNSIGNED NOT NULL,
  `category_id` int(191) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` text,
  `is_featured` tinyint(4) DEFAULT '0',
  `is_newest` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `name`, `slug`, `status`, `photo`, `is_featured`, `is_newest`) VALUES
(69, 43, 'test', 'test5', 1, '1605172898blog-post-1.png', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(191) NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `title`, `currency`, `currency_code`, `price`, `days`, `allowed_products`, `details`) VALUES
(5, 'Standard', '$', 'NGN', 60, 45, 25, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>'),
(6, 'Premium', '$', 'USD', 120, 90, 90, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(7, 'Unlimited', '$', 'USD', 250, 365, 0, '<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><br>'),
(8, 'Basic', '$', 'USD', 0, 30, 0, '<ol><li>Lorem ipsum dolor sit amet<br></li><li>Lorem ipsum dolor sit ame<br></li><li>Lorem ipsum dolor sit am<br></li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_provider` tinyint(10) NOT NULL DEFAULT '0',
  `status` tinyint(10) NOT NULL DEFAULT '0',
  `verification_link` text COLLATE utf8mb4_unicode_ci,
  `email_verified` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `affilate_code` text COLLATE utf8mb4_unicode_ci,
  `affilate_income` double NOT NULL DEFAULT '0',
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `owner_name` text COLLATE utf8mb4_unicode_ci,
  `shop_number` text COLLATE utf8mb4_unicode_ci,
  `shop_address` text COLLATE utf8mb4_unicode_ci,
  `reg_number` text COLLATE utf8mb4_unicode_ci,
  `shop_message` text COLLATE utf8mb4_unicode_ci,
  `shop_details` text COLLATE utf8mb4_unicode_ci,
  `shop_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personal_id` text COLLATE utf8mb4_unicode_ci,
  `tax_id` text COLLATE utf8mb4_unicode_ci,
  `f_url` text COLLATE utf8mb4_unicode_ci,
  `g_url` text COLLATE utf8mb4_unicode_ci,
  `t_url` text COLLATE utf8mb4_unicode_ci,
  `l_url` text COLLATE utf8mb4_unicode_ci,
  `is_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `f_check` tinyint(1) NOT NULL DEFAULT '0',
  `g_check` tinyint(1) NOT NULL DEFAULT '0',
  `t_check` tinyint(1) NOT NULL DEFAULT '0',
  `l_check` tinyint(1) NOT NULL DEFAULT '0',
  `mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_cost` double NOT NULL DEFAULT '0',
  `current_balance` double NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  `discount_list` int(10) UNSIGNED DEFAULT NULL,
  `credit_limit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `credit_line` text COLLATE utf8mb4_unicode_ci,
  `credit_usage` int(11) DEFAULT '0',
  `usage_date` date DEFAULT NULL,
  `last_usage_date` date DEFAULT NULL,
  `secure` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nonsecure` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `zip`, `city`, `country`, `address`, `phone`, `fax`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_provider`, `status`, `verification_link`, `email_verified`, `affilate_code`, `affilate_income`, `shop_name`, `owner_name`, `shop_number`, `shop_address`, `reg_number`, `shop_message`, `shop_details`, `shop_image`, `personal_id`, `tax_id`, `f_url`, `g_url`, `t_url`, `l_url`, `is_vendor`, `f_check`, `g_check`, `t_check`, `l_check`, `mail_sent`, `shipping_cost`, `current_balance`, `date`, `ban`, `discount_list`, `credit_limit`, `credit_line`, `credit_usage`, `usage_date`, `last_usage_date`, `secure`, `nonsecure`) VALUES
(32, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@gmail.com', '$2y$10$9ZN23VTLZidPsgMoDKPjlOYyP1AJTL1aDvCd1Wt2Wo6hzk1RU/Xeu', NULL, '2020-11-12 06:51:57', '2020-11-12 06:51:57', 0, 0, '1fe98eff042ffa9e4e67201961072d7f', 'Yes', '2eb91e0fe2bed746d8525d3d9040c948', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(33, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test1@gmail.com', '$2y$10$Za6QnTvJq82W4JK2PS233.x9Ce2ca13LWwYB.1xJVETL1lGnJ9K7q', NULL, '2020-11-12 06:52:45', '2020-11-12 06:52:46', 0, 0, 'e7a6bfb8281cf1993cd2021f819c16e2', 'Yes', '9eb7bcda3ab18a37799d03fe56da709c', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(34, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing3@gmail.com', '$2y$10$eu/irzPTEzuuWp7meob0AunnlYxGMx2s0jE8o3o3xXAmWI/nARllm', 'f0GXjfINpECDkXaCoX3sTwZiHoXWLvNxwgDcYOwAWnRBxGSAgsCsvMNO3mko', '2020-11-12 07:00:33', '2020-11-12 07:00:33', 0, 0, '7dfce47de8cd4507f021f908c2ee1308', 'No', 'bb13f056a88b97f96b2da7a0b1031974', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(35, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing5@gmail.com', '$2y$10$6pJ2zseT97xsKwZcAYHyO./4pS2nX8YPiTOuT11VWIqWbvD3OfHMO', NULL, '2020-11-12 07:02:33', '2020-11-12 07:02:34', 0, 0, '62167ad3760aa0e8e77179887fafd299', 'Yes', '3737b316c613b3815cd68bdcc26ae4e7', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(36, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing9@gmail.com', '$2y$10$4JLO32aRooItQ5cixbKSCOIWRU8zWzbPSejN.xcTxlrXyktrgziIS', NULL, '2020-11-12 07:03:13', '2020-11-12 07:03:13', 0, 0, 'fadd2f232495d414c00efbdae39aa750', 'Yes', '7da108d65ada9a6b1ff69d19a9ba5a20', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(37, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing33@gmail.com', '$2y$10$FIn0HbyLtkUNHAQnoqx.4e5JvMxdxVVduggegfsk7.VGpJ8ZWBHo2', 'mFGQ8zZWze5Nnvo9BfeJD3ETwo5YGNCBCZ9eSLQMV699MIzOUQGkFGQpOdDW', '2020-11-12 07:05:00', '2020-11-12 07:05:01', 0, 0, '93b8972e3106fd235591d7ae1a164a1b', 'Yes', '5c8f520c7b0395da6fc3fdc214ab1390', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(38, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing4321@gmail.com', '$2y$10$/Sw.oR9D0H1NlT9VyYci8em4KDO6seEuLJyNPPBPYEOuytx5Oin9.', NULL, '2020-11-12 07:06:28', '2020-11-12 07:06:28', 0, 0, '5205c533ace14d571d6792dbde92ed8a', 'Yes', '73bcd927d64a46e347a4a411311d3a8e', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(39, 'test test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing7654@gmail.com', '$2y$10$08vt2KoTfW2rabyJNu7KHOVkzEcbcurHlejvQhoMiRuto9v1NRFie', '1UcZbpDR8JGtfswwtqKhbilGUAqIH64bM5p6iqH8v9VkCfcQo6UefnPiz7ZV', '2020-11-12 07:07:53', '2020-11-12 07:07:53', 0, 0, 'f281e12a142b0a37d228426e7ea5c8fd', 'Yes', '4f366a76cd9def8d9cef0420e2e574c7', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(40, 'Max', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'max@gmail.com', '$2y$10$PvW9Rr0C9hV7Y80A1RhDd.sg80P1MZMj80.hvifXZbjdhWf2guSBO', 'XKEOwRlnz1qgellWHidcF8qV75NBfXSgdwCJDWKBzqWFD53lUnvOZTCyxe7L', '2020-12-17 02:39:26', '2020-12-17 02:39:26', 0, 0, '453925d06d73f661f194d5ac6c9267bd', 'Yes', '525d1d71b4c0c0b71d9ba9f2254e4b68', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(41, 'Sohail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'itssohail97@gmail.com', '$2y$10$eupBRlnr1.XFPZvzux6mZe2px.HOEOGDvJg98lskKwYwKTRUptnlK', '4IOWfaya1zlz3napA7eKn73ZP9hzZ6RJHN5uEjvKCwslE6gEgytYMcr8RBgC', '2020-12-17 19:48:12', '2020-12-17 19:48:12', 0, 0, 'e4020edb72f9aa7d12369c0b324a4108', 'Yes', 'b8f2694903f882677cca3dbc512df0bb', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL),
(42, 'Elizabeth Rabanal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'lizizbeauty@icloud.com', '$2y$10$YXQpX0pBNboShBgp8uUZgeoZ7V8z1WTDzAqQBw30ysrxrGC3SUOky', NULL, '2020-12-18 03:19:29', '2020-12-18 03:19:29', 0, 0, 'd601e81daf18ffa52b70da4c25c4f1e1', 'Yes', '61add9b2c0d46095387824d4ae8f0e24', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '0', NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `order_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_subscriptions`
--

CREATE TABLE `user_subscriptions` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `subscription_id` int(191) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL,
  `allowed_products` int(11) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Free',
  `txnid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payment_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_orders`
--

CREATE TABLE `vendor_orders` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `order_id` int(191) NOT NULL,
  `qty` int(191) NOT NULL,
  `price` double NOT NULL,
  `order_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','processing','completed','declined','on delivery') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE `verifications` (
  `id` int(191) NOT NULL,
  `user_id` int(191) NOT NULL,
  `attachments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('Pending','Verified','Declined') DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `admin_warning` tinyint(1) NOT NULL DEFAULT '0',
  `warning_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(191) UNSIGNED NOT NULL,
  `user_id` int(191) UNSIGNED NOT NULL,
  `product_id` int(191) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

CREATE TABLE `withdraws` (
  `id` int(191) NOT NULL,
  `user_id` int(191) DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `swift` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` float DEFAULT NULL,
  `fee` float DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('pending','completed','rejected') NOT NULL DEFAULT 'pending',
  `type` enum('user','vendor') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessory_brands`
--
ALTER TABLE `accessory_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accessory_types`
--
ALTER TABLE `accessory_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_languages`
--
ALTER TABLE `admin_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user_conversations`
--
ALTER TABLE `admin_user_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user_messages`
--
ALTER TABLE `admin_user_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_types`
--
ALTER TABLE `cat_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_type_children`
--
ALTER TABLE `cat_type_children`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `childcategories`
--
ALTER TABLE `childcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counters`
--
ALTER TABLE `counters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite_sellers`
--
ALTER TABLE `favorite_sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generalsettings`
--
ALTER TABLE `generalsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_tracks`
--
ALTER TABLE `order_tracks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagesettings`
--
ALTER TABLE `pagesettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pickups`
--
ALTER TABLE `pickups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategory_id` (`subcategory_id`);
ALTER TABLE `products` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `products` ADD FULLTEXT KEY `attributes` (`attributes`);

--
-- Indexes for table `product_clicks`
--
ALTER TABLE `product_clicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `discounts_id` (`discount_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seotools`
--
ALTER TABLE `seotools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socialsettings`
--
ALTER TABLE `socialsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_providers`
--
ALTER TABLE `social_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stripe_keys`
--
ALTER TABLE `stripe_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_orders`
--
ALTER TABLE `vendor_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraws`
--
ALTER TABLE `withdraws`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessory_brands`
--
ALTER TABLE `accessory_brands`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `accessory_types`
--
ALTER TABLE `accessory_types`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_languages`
--
ALTER TABLE `admin_languages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_user_conversations`
--
ALTER TABLE `admin_user_conversations`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_user_messages`
--
ALTER TABLE `admin_user_messages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute_options`
--
ALTER TABLE `attribute_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `cat_types`
--
ALTER TABLE `cat_types`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cat_type_children`
--
ALTER TABLE `cat_type_children`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `childcategories`
--
ALTER TABLE `childcategories`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `counters`
--
ALTER TABLE `counters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorite_sellers`
--
ALTER TABLE `favorite_sellers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `generalsettings`
--
ALTER TABLE `generalsettings`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `order_tracks`
--
ALTER TABLE `order_tracks`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pagesettings`
--
ALTER TABLE `pagesettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_gateways`
--
ALTER TABLE `payment_gateways`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `pickups`
--
ALTER TABLE `pickups`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `product_clicks`
--
ALTER TABLE `product_clicks`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=516;

--
-- AUTO_INCREMENT for table `product_discounts`
--
ALTER TABLE `product_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seotools`
--
ALTER TABLE `seotools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `socialsettings`
--
ALTER TABLE `socialsettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_providers`
--
ALTER TABLE `social_providers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stripe_keys`
--
ALTER TABLE `stripe_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_orders`
--
ALTER TABLE `vendor_orders`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(191) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `withdraws`
--
ALTER TABLE `withdraws`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `subcategory_id_foreign_key` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`);

--
-- Constraints for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD CONSTRAINT `product_discounts_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_discounts_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
