

$('.owl-carousel.testimonials').owlCarousel({
    loop: true,
    margin: 0,
    // nav:true,
    dots: true,
    animateOut: 'fadeOut',
    autoplay: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})

$('.owl-carousel.blogs-slider').owlCarousel({
    loop: true,
    margin: 20,
    nav: true,
    items: 4,
    center: true,
    autoplay: true,
    dots: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 4
        },
        1000: {
            items: 4
        }
    }
});

$('.owl-carousel.working-slider').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    items: 3,
    center: true,
    autoplay: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});

$('.owl-carousel.review').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    autoplay: true,
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

$(document).ready(function () {
    $(".working-slider .item").mouseover(function () {
        $(".working-detail").removeClass("active");
        $(".working-detail", this).addClass("active");
    });
    $(".working-slider .item").mouseout(function () {
        $(".working-detail", this).removeClass("active");
        $(".working-detail.center").addClass("active");
    });
});

$('.js-preloader').preloadinator({
    scroll: false,
    minTime: 3000,
    animation: 'fadeOut',
    animationDuration: 400,
});