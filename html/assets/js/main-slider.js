
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 0,
    autoplayTimeout: 2000,
    autoplay: true,
    autoplayHoverPause: true,
    items: 1,
    center: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

$(document).ready(function () {
    var $slider = $('.your-class');
    $slider.slick({
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: true,
        infinite: true,
        prevArrow: '<i class="fa fa-angle-left prev1"></i>',
        nextArrow: '<i class="fa fa-angle-right next1"></i>',
    });
    $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var calc = ((nextSlide) / (slick.slideCount - 1)) * 100;
    });
});