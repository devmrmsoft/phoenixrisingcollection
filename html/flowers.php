<?php 
    include('includes/customize-header.php');
?>

<section class="product-creation">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Oil</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>
        <div class="row">
            <div class="col-md-12 product-creation-column">
                <div class="choose-flowers">
                    <div class="quiz-text text-center">
                        <h1>Choose Flowers</h1>
                        <h1>2 Max</h1>
                    </div>
                    <form class="flowers d-flex justify-content-center align-items-center" action="">
                        <nav class="nav flower-container text-center">
                            <li class="nav-item">
                                <input type="checkbox" name="Chamomile" id="Chamomile" class="input-hidden"/>
                                <label for="Chamomile">
                                    <img src="./assets/img/flower-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Chamomile</p>
                                </label>
                                <p>Ingredients: Lemon, Egg, Honey <a href="#" data-toggle="modal" data-target="#detail-popup">read more </a></p>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Rose" id="Rose" class="input-hidden"/>
                                <label for="Rose">
                                    <img src="./assets/img/flower-2.png" class="img-fluid custome-images">
                                    <p class="d-block">Rose</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Jasmin" id="Jasmin" class="input-hidden"/>
                                <label for="Jasmin">
                                    <img src="./assets/img/flower-3.png" class="img-fluid custome-images">
                                    <p class="d-block">Jasmin</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Chamomile" id="Chamomile" class="input-hidden"/>
                                <label for="Chamomile">
                                    <img src="./assets/img/flower-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Chamomile</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Rose" id="Rose" class="input-hidden"/>
                                <label for="Rose">
                                    <img src="./assets/img/flower-2.png" class="img-fluid custome-images">
                                    <p class="d-block">Rose</p>
                                </label>
                            </li>
                        </nav>
                        <nav class="nav flower-container text-center extra">
                            <li class="nav-item">
                                <input type="checkbox" name="Chamomile" id="Chamomile" class="input-hidden"/>
                                <label for="Chamomile">
                                    <img src="./assets/img/flower-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Chamomile</p>
                                </label>
                                <p>Ingredients: Lemon, Egg, Honey <a href="#" data-toggle="modal" data-target="#detail-popup">read more </a></p>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Rose" id="Rose" class="input-hidden"/>
                                <label for="Rose">
                                    <img src="./assets/img/flower-2.png" class="img-fluid custome-images">
                                    <p class="d-block">Rose</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Jasmin" id="Jasmin" class="input-hidden"/>
                                <label for="Jasmin">
                                    <img src="./assets/img/flower-3.png" class="img-fluid custome-images">
                                    <p class="d-block">Jasmin</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Chamomile" id="Chamomile" class="input-hidden"/>
                                <label for="Chamomile">
                                    <img src="./assets/img/flower-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Chamomile</p>
                                </label>
                            </li>
                        </nav>
                    </form>
                </div>
            </div>
            <div class="col-md-12 product-creation-column">
                <div class="next-page">
                    <a href="#" type="button">Next</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product Creation -->


<?php 
    include('includes/popup.php');
    include('includes/footer.php');
?>