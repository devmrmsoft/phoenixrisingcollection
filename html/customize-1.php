<?php 
    include('includes/customize-header.php');
?>

<section class="creation-1">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Oil</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>
        <div class="row">
            <div class="col-md-12 creation-1-column">
                <div class="choose-oil">
                    <div class="quiz-text text-center">
                        <h1>Choose Oil</h1>
                    </div>
                    <form action="" class="oil d-flex justify-content-center align-items-center">
                        <ul class="nav w-100">
                            <li class="nav-item text-center">
                                <input type="radio" name="oils" id="hair" class="input-hidden" />
                                <label for="hair">
                                    <img src="./assets/img/hair.png" class="img-fluid">
                                    <h5>HAIR</h5>
                                </label>
                            </li>
                            <li class="nav-item text-center">
                                <input type="radio" name="oils" id="body" class="input-hidden" />
                                <label for="body">
                                    <img src="./assets/img/body.png" class="img-fluid"/>
                                    <h5>BODY</h5>
                                </label>
                            </li>
                        </ul>
                    </form>
                    <div class="next-button text-center">
                        <a href="#" class="next-btn">next</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Creation Level 1 --> 

<?php 
    include('includes/footer.php');
?>