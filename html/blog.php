<?php 
    include('includes/inside-header.php');
?>

<section class="blog-banner">
    <div class="container">
        <div class="row h-100">
            <div class="col-md-5 m-auto blog-banner-column d-flex justify-content-center align-items-center">
                <div class="inner-column">
                    <div class="blog-heading">
                        <h1>blog</h1>
                        <img src="./assets/img/zigzag.png" class="img-fluid">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link">Home</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link active">Blog</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7 blog-banner-column">
                <div class="dropper-bg">

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog Banner -->

<section class="blog-post">
    <div class="container">
        <div class="row">
            <div class="col-md-4 blog-post-column">
                <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                    <div class="blog-upper-section text-center">
                        <img src="./assets/img/blog-post-1.png" class="img-fluid featured-image">
                        <div class="blog-title text-center">
                            <h5>Sed ut perspiciatis unde</h5>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you.</p>
                        </div>
                    </div>
                    <div class="blog-bottom-section d-flex">
                        <p>20Th October 2019</p>
                        <a href="#">Share</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 blog-post-column">
                <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                    <div class="blog-upper-section text-center">
                        <img src="./assets/img/blog-post-2.png" class="img-fluid featured-image">
                        <div class="blog-title text-center">
                            <h5>Et harum quidem rerum</h5>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat.</p>
                        </div>
                    </div>
                    <div class="blog-bottom-section d-flex">
                        <p>20Th October 2019</p>
                        <a href="#">Share</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 blog-post-column">
                <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                    <div class="blog-upper-section text-center">
                        <img src="./assets/img/blog-post-3.png" class="img-fluid featured-image">
                        <div class="blog-title text-center">
                            <h5>Temporibus autem quib</h5>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized.</p>
                        </div>
                    </div>
                    <div class="blog-bottom-section d-flex">
                        <p>20Th October 2019</p>
                        <a href="#">Share</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 blog-post-column pt-4">
                <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                    <div class="blog-upper-section text-center">
                        <img src="./assets/img/blog-post-4.png" class="img-fluid featured-image">
                        <div class="blog-title text-center">
                            <h5>There are many variations</h5>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden.</p>
                        </div>
                    </div>
                    <div class="blog-bottom-section d-flex">
                        <p>20Th October 2019</p>
                        <a href="#">Share</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 blog-post-column pt-4">
                <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                    <div class="blog-upper-section text-center">
                        <img src="./assets/img/blog-post-5.png" class="img-fluid featured-image">
                        <div class="blog-title text-center">
                            <h5>All the Lorem Ipsum</h5>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate.</p>
                        </div>
                    </div>
                    <div class="blog-bottom-section d-flex">
                        <p>20Th October 2019</p>
                        <a href="#">Share</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 blog-post-column pt-4">
                <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                    <div class="blog-upper-section text-center">
                        <img src="./assets/img/blog-post-6.png" class="img-fluid featured-image">
                        <div class="blog-title text-center">
                            <h5>Contrary to popular</h5>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin.</p>
                        </div>
                    </div>
                    <div class="blog-bottom-section d-flex">
                        <p>20Th October 2019</p>
                        <a href="#">Share</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 pagination d-flex justify-content-center align-items-center pt-5">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active">1</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">2</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-chevron-right"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>







<?php 
    include('includes/footer.php');
?>