<?php 
    include('includes/inside-header.php')
?>

<section class="About_Banner">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-1 d-flex flex-column justify-content-center align-items-start">
                <h1 class="heading">About Us</h1>
                <a class="text_about_us" href="#">Home <span>/</span> <span class="active">About</span></a>
            </div>
            <div class="col-md-6">
                <img src="./assets/img/Bottels.png" class="img-fluid img1">
            </div>
        </div>
    </div>
</section>
<!-- About Banner -->

<section class="about-tiles">
    <div class="container">
        <div class="row">
            <div class="col-md-6 section_col">
                <div class="col_pad d-flex">
                    <div class="tiles">
                        <h1 class=" tile_head">Lorem Ipsum</h1>
                        <img src="./assets/img/zigzag.png" alt="" class="img-fluid zigzag">
                        <p class="tile_para">Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
                            quam
                            nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla
                            pariatur
                        </p>
                    </div>
                    <div class="side-icon">
                        <img src="./assets/img/icon1.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-6  section_col">
                <div class="col_pad d-flex">
                    <div class="side-icon">
                        <img src="./assets/img/icon2.jpg" alt="">
                    </div>
                    <div class="tiles">
                        <h1 class=" tile_head1">Lorem Ipsum</h1>
                        <img src="./assets/img/zigzag.png" alt="" class="img-fluid zigzag1">
                        <p class="tile_para1">Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
                            quam
                            nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla
                            pariatur
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6  section_col">
                <div class="col_pad d-flex">
                    <div class="tiles">
                        <h1 class=" tile_head">Lorem Ipsum</h1>
                        <img src="./assets/img/zigzag.png" alt="" class="img-fluid zigzag">
                        <p class="tile_para">Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
                            quam
                            nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla
                            pariatur
                        </p>
                    </div>
                    <div class="side-icon">
                        <img src="./assets/img/icon3.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-6  section_col">
                <div class="col_pad d-flex">
                    <div class="side-icon">
                        <img src="./assets/img/icon4.jpg">
                    </div>
                    <div class="tiles">
                        <h1 class=" tile_head1">Lorem Ipsum</h1>
                        <img src="./assets/img/zigzag.png" class="img-fluid zigzag1">
                        <p class="tile_para1">Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
                            quam
                            nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla
                            pariatur
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Tiles -->

<section class="video_progress">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 pl-0">
                <img src="./assets/img/video_image.jpg" alt="" class=" img-fluid video_banner">
            </div>
            <div class="col-md-6 d-flex justify-content-start align-items-center">
                <div class="progress_div">
                    <h1 class="progress_head ">
                        What We Have
                    </h1>
                    <img src="./assets/img/zigzag_pro.jpg" alt="" class="img-fluid zigzag2">
                    <div class="prgress1 d-flex">
                        <h2 class="progress_text ">
                            Natural extracts
                        </h2 class="progress_text1 ">
                        100%
                        </h2>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width:100%; "></div>
                    </div>
                    <div class="prgress1 d-flex">
                        <h2 class="progress_text ">
                            Quality of Products
                        </h2 class="progress_text1 ">
                        100%
                        </h2>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width:100%; "></div>
                    </div>
                    <div class="prgress1 d-flex">
                        <h2 class="progress_text ">
                            Customer Satisfaction
                        </h2 class="progress_text1 ">
                        100%
                        </h2>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 100%; "></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Video Progess Bar -->

<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel testimonials owl-theme">
                    <div class="item">
                        <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                            <img src="./assets/img/testimonials.jpg" class="img-fluid clients">
                            <h3>Patricia Peterson</h3>
                            <div class="five-star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <p class="text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                            <img src="./assets/img/client.png" class="img-fluid clients">
                            <h3>Patricia Peterson</h3>
                            <div class="five-star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <p class="text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner-column d-flex flex-column justify-content-center align-items-center">
                            <img src="./assets/img/testimonials.jpg" class="img-fluid clients">
                            <h3>Patricia Peterson</h3>
                            <div class="five-star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <p class="text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Testimonials -->

<?php 
    include('includes/footer.php')
?>