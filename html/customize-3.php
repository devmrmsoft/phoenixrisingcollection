<?php 
    include('includes/customize-header.php');
?>

<section class="creation-3">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Oil</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>
        <div class="col-md-12 creation-3-column">
            <div class="last-stage">
                <div class="quiz-text text-center">
                    <h1>Size</h1>
                </div>
                <form action="" class="size d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <li class="nav-item size-item">
                            <input type="radio" name="emotion" id="sad" class="input-hidden" />
                            <label for="sad">
                                <img src="./assets/img/bottle-4oz.png" class="img-fluid">
                                <div class="inner-container">
                                    <h6>4.oz</h6>
                                    <p>$30.00</p>
                                </div>
                            </label>
                        </li>
                        <li class="nav-item size-item">
                            <input type="radio" name="emotion"id="happy" class="input-hidden" />
                            <label for="happy">
                                <img src="./assets/img/bottle-4oz.png" class="img-fluid">
                                <div class="inner-container">
                                    <h6>8.oz</h6>
                                    <p>$60.00</p>
                                </div>
                            </label>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="col-md-12 creation-3-column">
            <div class="last-stage">
                <div class="quiz-text text-center">
                    <h1>Frequency</h1>
                </div>
                <form action="" class="size d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" id="yearly" class="input-hidden" />
                            <label for="yearly">
                                <p>Every Months</p>
                            </label>
                        </li>
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" id="3-month" class="input-hidden" />
                            <label for="3-month">
                                <p>Every 3 Months</p>
                            </label>
                        </li>
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" id="6-month" class="input-hidden" />
                            <label for="6-month">
                                <p>Every 6 Months</p>
                            </label>
                        </li>
                        <li class="nav-item frequency-item">
                            <input type="radio" name="duration" id="once" class="input-hidden" />
                            <label for="once">
                                <p>Just Once</p>
                            </label>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="col-md-12 creation-3-column">
            <div class="last-stage">
                <div class="quiz-text text-center">
                    <h1>Boost Your Set?</h1>
                    <h3>Added Products</h3>
                </div>
                <div class="added-product d-flex justify-content-center align-items-start">
                    <div class="product">
                        <div class="image text-center">
                            <img src="./assets/img/product-1.png" class="img-fluid">
                            <h3>$25</h3>
                        </div>
                        <div class="detail">
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">ADD</a>
                        </div>
                    </div>
                    <div class="product">
                        <div class="image text-center">
                            <img src="./assets/img/product-2.png" class="img-fluid">
                            <h3>$20</h3>
                        </div>
                        <div class="detail">
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">ADD</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 creation-3-column"> 
            <div class="next-button text-center">
                <div class="checkbox">
                    <input type="checkbox" name="gift" id="gift">
                    <label for="gift">
                        <h3 class="d-inline">is this a gift?</h3>
                        <img src="./assets/img/gift.png" class="img-fluid">
                    </label>
                </div>
                <a href="#" type="button">NEXT</a>
            </div>
        </div>
    </div>
</section>
<!-- Product Creation Level 3 -->






<?php 
    include('includes/footer.php');
?>