<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="icon" href="./assets/img/16x16.png" type="image" sizes="16x16">
    <title>Signup</title>
</head>
<body>

    <section class="login-form signup-form">
        <div class="container">
            <div class="row">
                <div class="div col-md-5 offset-md-7 m-auto login-form-column">
                    <div class="inner-container-login">
                        <form action="" method="get">
                            <h1 class="text-center text-capitalize">sign up</h1>
                            <div class="md-form mb-0">
                                <input type="text" id="fullname" name="fullname" class="form-control" placeholder="Full Name" required>
                            </div>
                            <div class="md-form mb-0">
                                <input type="email" id="login-user" name="login" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="md-form mb-0">
                                <input type="password" id="pass-user" name="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="md-form mb-0">
                                <input type="password" id="pass-user" name="password" class="form-control" placeholder="Re-Password" required>
                            </div>
                            <a href="#" type="button" class="login-btn mt-4">
                                signup
                            </a>
                        </form>
                        <div class="signup-social-icons text-center">
                            <a href="#">OR</a>
                            <ul class="nav justify-content-center mt-3">
                                <li class="nav-item">
                                    <a href="#" class="">
                                        <img src="./assets/img/facebook.png">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="">
                                        <img src="./assets/img/gmail.png">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </section>




    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
</body>
</html>