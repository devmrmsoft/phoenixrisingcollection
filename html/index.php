
    <?php 
        include('includes/header.php');
    ?>

    <section class="slider">
        <div class="container-fluid no-gutters">
            <div class="row">
                <div class="col-12 pl-0 pr-0">
                    <div class="your-class">
                        <div class="image-wrapper">
                            <div class="caro-container">
                                <div class="text-cont">
                                    <div class="container">
                                        <div class="text-cont-heading animated fadeInRight">
                                            <h1>Hair care.</h1>
                                            <h1>Body care.</h1>
                                            <h1>Essential oil.</h1>
                                            <div class="text-btn animated fadeInRight">
                                                <a href="#" class="shop-btn">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="i-wrap animated fadeInRight">
                                    <img src="./assets/img/banner.jpg" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="image-wrapper">
                            <div class="caro-container">
                                <div class="text-cont">
                                    <div class="container">
                                        <div class="text-cont-heading animated fadeInRight">
                                            <h1>Hair care.</h1>
                                            <h1>Body care.</h1>
                                            <h1>Essential oil.</h1>
                                            <div class="text-btn animated fadeInRight">
                                                <a href="#" class="shop-btn">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="i-wrap animated fadeInRight">
                                    <img src="./assets/img/banner.jpg" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="image-wrapper">
                            <div class="caro-container">
                                <div class="text-cont">
                                    <div class="container">
                                        <div class="text-cont-heading animated fadeInRight">
                                            <h1>Hair care.</h1>
                                            <h1>Body care.</h1>
                                            <h1>Essential oil.</h1>
                                            <div class="text-btn animated fadeInRight">
                                                <a href="#" class="shop-btn">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="i-wrap animated fadeInRight">
                                    <img src="./assets/img/banner.jpg" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>            
                </div>
            </div>
        </div>
    </section>
    <!-- Slider Section -->

    <section class="customization">
        <div class="container">
            <div class="title text-center">
                <h1>Customize Your Oil</h1>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Body Oil</h1>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <h3>Organic oil</h3>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="#" class="product-btn" type="button">customize your product</a>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="#" class="hair-img d-block">
                        <img src="./assets/img/customize-2.png" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-center align-items-center padTop">
                    <a href="#" class="hair-img d-block">
                        <img src="./assets/img/customize-1.png" class="img-fluid">
                    </a>
                </div>
                <div class="col-md-5 offset-md-1 m-auto customization-column d-flex justify-content-end align-items-center padTop">
                    <div class="description">
                        <h1>Hair Oil</h1>
                        <p>A Daily Dose of Your Hair & Body Goals</p>
                        <div class="inner-description">
                            <h3>Organic oil</h3>
                            <img src="./assets/img/zigzag.png" class="img-fluid">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                        </div>
                        <a href="#" class="product-btn" type="button">customize your product</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Customize Your Hair Oil -->
 
    <section class="product-review">
        <div class="container-fluid no-gutters">
            <div class="row">
                <div class="col-md-4 offset-md-1 ml-auto mr-auto product-review-column">
                    <img src="./assets/img/flower-oil.png" class="img-fluid flower-img">
                </div>
                <div class="col-md-5 product-review-column d-flex justify-content-center align-items-center">
                    <div class="owl-carousel review owl-theme">
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>10,000 five-star reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            <img src="./assets/img/client.png" class="img-fluid">
                                            <h5>Emma Walker</h5>
                                            <p>Owner</p>
                                        </div>
                                        <div class="inner-2">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more-reviews">
                                    <a href="#" type="button" class="review-btn">read More reviews</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>10,000 five-star reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            <img src="./assets/img/client.png" class="img-fluid">
                                            <h5>Emma Walker</h5>
                                            <p>Owner</p>
                                        </div>
                                        <div class="inner-2">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more-reviews">
                                    <a href="#" type="button" class="review-btn">read More reviews</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="review-container">
                                <div class="five-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <h1>10,000 five-star reviews</h1>
                                <div class="accordion">
                                    <button class="btn" type="button" data-toggle="collapse" data-target="#happy-customer" aria-expanded="false" aria-controls="happy-customer">
                                        + happy customers
                                    </button>
                                    <div class="collapse show" id="happy-customer">
                                        <div class="inner-1">
                                            <img src="./assets/img/client.png" class="img-fluid">
                                            <h5>Emma Walker</h5>
                                            <p>Owner</p>
                                        </div>
                                        <div class="inner-2">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more-reviews">
                                    <a href="#" type="button" class="review-btn">read More reviews</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>
    <!-- Product Review -->

    <section class="how-it-works">
        <div class="container">
            <div class="row">
                <div class="col-md-12 work-column">
                    <div class="title d-flex justify-content-center align-items-center flex-column">
                        <h1>How It Works</h1>
                        <img src="./assets/img/big-zigzag.png" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-4 work-column working-slider review pt-5">
                    <div class="item">
                        <div class="inner-container d-flex justify-content-center align-items-center flex-column">
                            <div class="working-detail text-center">
                                <h5 class="d-inline-block">01</h5>
                                <h2 class="text-capitalize">Lorem ipsum</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo in</p>
                            </div>
                            <div class="working-btn">
                                <a href="#" type="button" class="read-more-btn">LOREM IPSUM</a>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="col-md-4 work-column working-slider review pt-5">
                    <div class="item">
                        <div class="inner-container d-flex justify-content-center align-items-center flex-column">
                            <div class="working-detail active center text-center">
                                <h5 class="d-inline-block">02</h5>
                                <h2 class="text-capitalize">Lorem ipsum</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo in</p>
                            </div>
                            <div class="working-btn">
                                <a href="#" type="button" class="read-more-btn">LOREM IPSUM</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 work-column working-slider review pt-5">
                    <div class="item">
                        <div class="inner-container d-flex justify-content-center align-items-center flex-column">
                            <div class="working-detail text-center">
                                <h5 class="d-inline-block">03</h5>
                                <h2 class="text-capitalize">Lorem ipsum</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo in</p>
                            </div>
                            <div class="working-btn">
                                <a href="#" type="button" class="read-more-btn">LOREM IPSUM</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- How it Works -->

    <section class="clear-ingredients">
        <div class="container">
            <div class="row">
                <div class="col-md-5 offset-md-1 m-auto clear-ingredients-column">
                    <div class="inner-description text-center">
                        <h1>Clean, Science-Backed Ingredients</h1>
                        <p>Sed ut perspiciatis unde omnis iste natus error  voluptatem accusantium </p>
                    </div>
                    <div class="brands text-center">
                        <img src="./assets/img/brands.png" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 clear-ingredients-column">
                    <div class="inner-image">
                        <img src="./assets/img/pink-bg.png" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Clear Ingredients -->

    <section class="our-blogs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 our-blogs-column">
                    <div class="title d-flex justify-content-center align-items-center flex-column">
                        <h1>Our Blogs</h1>
                        <img src="./assets/img/big-zigzag.png" class="img-fluid">
                    </div>
                    <div class="owl-carousel blogs-slider review owl-theme pt-5">
                        <div class="item">
                            <div class="blog-image">
                                <img src="./assets/img/blog-2.png" class="img-fluid">
                            </div>
                            <div class="blog-writing">
                                <p>Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium </p>
                                <a href="#" type="button">READ MORE</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-image">
                                <img src="./assets/img/blog-1.png" class="img-fluid">
                            </div>
                            <div class="blog-writing">
                                <p>Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium </p>
                                <a href="#" type="button">READ MORE</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-image">
                                <img src="./assets/img/blog-3.png" class="img-fluid">
                            </div>
                            <div class="blog-writing">
                                <p>Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium </p>
                                <a href="#" type="button">READ MORE</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-image">
                                <img src="./assets/img/blog-2.png" class="img-fluid">
                            </div>
                            <div class="blog-writing">
                                <p>Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium </p>
                                <a href="#" type="button">READ MORE</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-image">
                                <img src="./assets/img/blog-4.png" class="img-fluid">
                            </div>
                            <div class="blog-writing">
                                <p>Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium </p>
                                <a href="#" type="button">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Our Blogs -->

    <section class="discount-section">
        <div class="container">
            <div class="discount-offer">
                <div class="row h-100">
                    <div class="col-md-6 offset-md-6 discount-offer-column">
                        <div class="inner-column">
                            <div class="text">
                                <h3>One day</h3>
                                <h1 class="d-inline">10 <span>%</span> </h1>
                                <h2 class="d-inline">discount</h2>
                            </div>
                            <a href="#" type="button">05/9 - 20/10/2020</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Discount Banner  -->

    <section class="instagram-feed">

    </section>
    <!-- Instagram Feeds -->

    <?php 
        include('includes/footer.php');
    ?>
  