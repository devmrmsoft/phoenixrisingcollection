<?php 
    include('includes/customize-header.php');
?>

<section class="product-creation">
    <div class="container">
        <div class="title text-center">
            <h1>Customize Your Oil</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>
        <div class="row">
            <div class="col-md-12 product-creation-column">
                <div class="choose-flowers">
                    <div class="quiz-text text-center">
                        <h1>Choose Crystals</h1>
                        <h1>2 Max</h1>
                    </div>
                    <form class="flowers d-flex justify-content-center align-items-center" action="">
                        <nav class="nav flower-container text-center">
                            <li class="nav-item">
                                <input type="checkbox" name="Onyx" id="Onyx" class="input-hidden"/>
                                <label for="Onyx">
                                    <img src="./assets/img/crystal-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Onyx</p>
                                </label>
                                <p>Ingredients: Lemon, Egg, Honey <a href="#" data-toggle="modal" data-target="#detail-popup">read more </a></p>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Amethyst" id="Amethyst" class="input-hidden"/>
                                <label for="Amethyst">
                                    <img src="./assets/img/crystal-2.png" class="img-fluid custome-images">
                                    <p class="d-block">Amethyst</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Citrine" id="Citrine" class="input-hidden"/>
                                <label for="Citrine">
                                    <img src="./assets/img/crystal-3.png" class="img-fluid custome-images">
                                    <p class="d-block">Citrine</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Onyx" id="Onyx" class="input-hidden"/>
                                <label for="Onyx">
                                    <img src="./assets/img/crystal-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Onyx</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Amethyst" id="Amethyst" class="input-hidden"/>
                                <label for="Amethyst">
                                    <img src="./assets/img/crystal-2.png" class="img-fluid custome-images">
                                    <p class="d-block">Amethyst</p>
                                </label>
                            </li>
                        </nav>
                        <nav class="nav flower-container text-center extra">
                            <li class="nav-item">
                                <input type="checkbox" name="Onyx" id="Onyx" class="input-hidden"/>
                                <label for="Onyx">
                                    <img src="./assets/img/crystal-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Onyx</p>
                                </label>
                                <p>Ingredients: Lemon, Egg, Honey <a href="#" data-toggle="modal" data-target="#detail-popup">read more </a></p>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Amethyst" id="Amethyst" class="input-hidden"/>
                                <label for="Amethyst">
                                    <img src="./assets/img/crystal-2.png" class="img-fluid custome-images">
                                    <p class="d-block">Amethyst</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Citrine" id="Citrine" class="input-hidden"/>
                                <label for="Citrine">
                                    <img src="./assets/img/crystal-3.png" class="img-fluid custome-images">
                                    <p class="d-block">Citrine</p>
                                </label>
                            </li>
                            <li class="nav-item">
                                <input type="checkbox" name="Onyx" id="Onyx" class="input-hidden"/>
                                <label for="Onyx">
                                    <img src="./assets/img/crystal-1.png" class="img-fluid custome-images">
                                    <p class="d-block">Onyx</p>
                                </label>
                            </li>
                        </nav>
                    </form>
                </div>
            </div>
            <div class="col-md-12 product-creation-column">
                <div class="next-page">
                    <a href="#" type="button">Next</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product Creation -->
<?php 
    include('includes/popup.php');
    include('includes/footer.php');
?>