<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="icon" href="./assets/img/16x16.png" type="image" sizes="16x16">
    <title>Phoenix Rising Collection</title>
</head>
<body>
    <header class="cart-header">
        <div class="title text-center">
            <h2>Phoenix Rising Collection</h2>
        </div>
    </header>

    <section class="shopping-cart">
        <div class="container">
            <div class="title text-center">
                <h2>Shopping Cart</h2>
            </div>
            <div class="row">
                <div class="col-md-7 offset-md-1 ml-auto mr-auto shopping-cart-column">
                    <div class="account-login text-center">
                        <p class="d-inline">Have an account already?<a href="#"> Log In Here</a></p>
                    </div>
                    <div class="cart-details d-flex">
                        <div class="inner-col-1">
                            <div class="add-items">
                                <h3>Flowers:</h3>
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <img src="./assets/img/flower-1.png" class="img-fluid">
                                            <span>Chamomile</span>
                                            <span class="price">$5.00</span>
                                        </a>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <img src="./assets/img/flower-2.png" class="img-fluid">
                                            <span>Rose</span>
                                            <span class="price">$8.00</span>
                                        </a>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="add-items">
                                <h3>Crystals:</h3>
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <img src="./assets/img/crystal-1.png" class="img-fluid">
                                            <span>Onyx</span>
                                            <span class="price">$15.00</span>
                                        </a>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <img src="./assets/img/crystal-2.png" class="img-fluid">
                                            <span>Amethyst</span>
                                            <span class="price">$9.00</span>
                                        </a>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="add-items">
                                <h3>Essential Oil Blends:</h3>
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <img src="./assets/img/essential-1.png" class="img-fluid">
                                            <span>Love</span>
                                            <span class="price">$25.00</span>
                                        </a>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="inner-col-2">
                            <div class="text-center">
                                <img src="./assets/img/main-bottle.png" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 shopping-cart-column sticky-top">
                    <div class="order-summary">
                        <div class="inner-column-summary">
                            <h2>Order summary</h2>
                            <div class="item-oz d-flex">
                                <div class="images d-flex">
                                    <img src="./assets/img/shampoo-2.png">
                                    <img src="./assets/img/shampoo-1.png">
                                </div>
                                <div class="details">
                                    <p>16oz Hair Oil</p>
                                    <p>$62.00</p>
                                </div>
                            </div>
                        </div>
                        <div class="inner-column-total">
                            <div class="pricing d-flex">
                                <p>Subtotal</p>
                                <h6>$62.00</h6>
                            </div>
                            <div class="pricing d-flex">
                                <p>Shipping</p>
                                <h6>$0.00</h6>
                            </div>
                            <div class="pricing d-flex">
                                <p>Tax</p>
                                <h6>$0.00</h6>
                            </div>
                            <div class="pricing d-flex">
                                <p>Total</p>
                                <h6>$62.00</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="featured-products">
        <div class="container">
            <div class="row">
                <div class="col-md-12 creation-3-column">
                    <div class="last-stage">
                        <div class="quiz-text text-center">
                            <h1>Boost Your Set?</h1>
                            <h3>Added Products</h3>
                        </div>
                        <div class="added-product d-flex justify-content-center align-items-start">
                            <div class="product">
                                <div class="image text-center">
                                    <img src="./assets/img/product-1.png" class="img-fluid">
                                    <h3>$25</h3>
                                </div>
                                <div class="detail">
                                    <h3>Lorem Ipsum</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#">ADD</a>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image text-center">
                                    <img src="./assets/img/product-2.png" class="img-fluid">
                                    <h3>$20</h3>
                                </div>
                                <div class="detail">
                                    <h3>Lorem Ipsum</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#">ADD</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="cart-footer">
        <div class="title text-center">
            <h3>&copy; Phoenix Rising Collection 2020</h3>
        </div>
    </footer>

    


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
</body>
</html>