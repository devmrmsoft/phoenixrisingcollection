<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="icon" href="./assets/img/16x16.png" type="image" sizes="16x16">
    <title>Phoenix Rising Collection</title>
</head>
<body>
    <header class="cart-header">
        <div class="title text-center">
            <h2>Phoenix Rising Collection</h2>
        </div>
    </header>

    <section class="order-complete text-center">
        <h1>Thank you.</h1>
        <h3>Your order was completed successfully</h3>
    </section> 




    <footer class="cart-footer">
        <div class="title text-center">
            <h3>&copy; Phoenix Rising Collection 2020</h3>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
</body>
</html>