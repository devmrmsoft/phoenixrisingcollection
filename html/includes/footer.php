<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 site-footer-column">
                <div class="inner-footer-1">
                    <img src="./assets/img/footer-logo.png" class="img-fluid">
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</p>
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="" class="nav-link"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link"><i class="fab fa-instagram"></i></a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 site-footer-column d-flex justify-content-center">
                <div class="inner-footer-2">
                    <h3>Locate Us</h3>
                    <p>No 40 Baria Sreet 133/2</p>
                    <p>+ (156) 1800-366-6666</p>
                    <p>Liz-82@example.com</p>
                </div>
            </div>
            <div class="col-md-3 site-footer-column d-flex justify-content-center">
                <div class="inner-footer-3">
                    <h3>Profile</h3>
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a href="#" class="nav-link">My Account</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Checkout</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Order Tracking</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Help & Suppport</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 site-footer-column">
                <div class="inner-footer-4">
                    <h3>Newsletter</h3>
                    <p>Subscribe to our newsletter</p>
                    <input type="email" name="email" id="email" placeholder="Email">
                </div>
                <div class="copyrights">
                    <p>@ 2020 Phoenix Rising Collection</p>
                </div>
            </div>
        </div> 
    </div>
</footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="./assets/js/jquery.preloadinator.min.js"></script>
    <script src="https://kit.fontawesome.com/906be8c28d.js"></script>
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="./assets/js/main-slider.js"></script>
    <script>
        new WOW().init();
    </script>
</body>
</html>