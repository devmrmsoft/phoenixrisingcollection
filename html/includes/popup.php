<div class="modal fade" id="detail-popup" tabindex="-1" role="dialog" aria-labelledby="detail-popup-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    <h1>Benefits of roses for</h1>

                    <h3>Mind</h3>
                    <p> roses are known as a powerful mood enhancer, add rose petals to a warm bath will help you get rid of feelings of anxiety and, it will also help you sleep better</p>

                    <h3>Body</h3>
                    <p>roses detox the body Helps fight acne: The anti-bacterial properties found in rose makes it amazing for fighting acne and breakouts. Acts as sunscreen: A rich source of vitamin C, rose petals act as an excellent sunblock. Soothes skin: The natural oils found in roses help retain moisture in the skin. This results in your skin feeling smooth and soft. The sugars in rose petals especially benefit those with sensitive skin. Nourishes scalp:</p>

                    <h3>Spirit</h3>
                    <p> Pink and red roses are often a symbol of romance, love, gratitude, grace, joy, and admiration. It is believed that roses can promote love and stimulate one's sex drive. Furthermore, it is said that roses have the magical property to help in developing psychic knowledge and increase intuition. </p>
            </div>
        </div>
    </div>
</div>