<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="./assets/css/animate.min.css">
    <link rel="stylesheet" href="./assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="./assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="icon" href="./assets/img/16x16.png" type="image" sizes="16x16">
    <title>Phoenix Rising Collection</title>

</head>
<body>

    <div class="preloader js-preloader flex-center">
        <div class="spinner">
            <img src="./assets/img/preloader.gif" class="img-fluid">
        </div>
    </div>

    <header class="main-header bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 navigation-column d-flex justify-content-center align-items-center">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="#" class="nav-link">home</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">About</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Build Your own</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <img src="./assets/img/Phoenix-logo.png" alt="Website Logo" class="img-fluid">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">shop</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">blog</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section -->
