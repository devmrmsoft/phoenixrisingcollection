$(function ($) {
    "use strict";


    $(document).ready(function () {



// Cart Section

    $(document).on('click', '.add-to-cart', function(){



        $.get( $(this).data('href') , function( data ) {

            if(data == 'digital') {
              toastr.error(langg.already_cart);
             }
            else if(data == 0) {
              toastr.error(langg.out_stock);
              }
            else {
              $("#cart-count").html(data[0]);
              $("#cart-items").load(mainurl+'/carts/view');
              toastr.success(langg.add_cart);
              }
        });
                    return false;
    });


    $(document).on('click', '.cart-remove', function(){
      alert('test');
      var $selector = $(this).data('class');
      $('.'+$selector).hide();
        $.get( $(this).data('href') , function( data ) {
            if(data == 0) {
                $("#cart-count").html(data);
               $('.cart-table').html('<h3 class="mt-1 pl-3 text-left">Cart is empty.</h3>');
                $('#cart-items').html('<p class="mt-1 pl-3 text-left">Cart is empty.</p>');
                $('.cartpage .col-lg-4').html('');
              }
            else {
               $('.cart-quantity').html(data[1]);
               $('.cart-total').html(data[0]);
               $('.coupon-total').val(data[0]);
               $('.main-total').html(data[3]);
              }

        });
    });

// Adding Muliple Quantity Starts

    var sizes = "";
    var size_qty = "";
    var size_price = "";
    var size_key = "";
    var colors = "";
    var total = "";
    var stock = $("#stock").val();
    var keys = "";
    var values = "";
    var prices = "";



    /*-----------------------------
        Cart Page Quantity
    -----------------------------*/
    $(document).on('click', '.qtminus', function () {
        var el = $(this);
        var $tselector = el.parent().parent().find('.qttotal');
        total = $($tselector).text();
        if (total > 1) {
            total--;
        }
        $($tselector).text(total);
    });

    $(document).on('click', '.qtplus', function () {
        var el = $(this);
        var $tselector = el.parent().parent().find('.qttotal');
        total = $($tselector).text();
        if(stock != "")
        {
            var stk = parseInt(stock);
              if(total < stk)
              {
                 total++;
                 $($tselector).text(total);
              }
        }
        else {
        total++;
        }

        $($tselector).text(total);
    });




    $(document).on("click", "#addcrt" , function(){
     var qty = $('.qttotal').html();
      console.log(size_key);
      alert(qty);
     var pid = $(this).parent().parent().parent().parent().find("#product_id").val();

if($('.product-attr').length > 0)
{
  values = $(".product-attr:checked").map(function() {
   return $(this).val();
}).get();

keys = $(".product-attr:checked").map(function() {
   return $(this).data('key');
}).get();

prices = $(".product-attr:checked").map(function() {
   return $(this).data('price');
}).get();



}


        $.ajax({
          type: "GET",
          url:mainurl+"/addnumcart",
          data:{id:pid,qty:qty,size:sizes,color:colors,size_qty:size_qty,size_price:size_price,size_key:size_key,keys:keys,values:values,prices:prices},
          success:function(data){

            if(data == 'digital') {
                toastr.error(langg.already_cart);
             }
            else if(data == 0) {
                toastr.error(langg.out_stock);
              }
            else {
              $("#cart-count").html(data[0]);
              $("#cart-items").load(mainurl+'/carts/view');
                toastr.success(langg.add_cart);
              }
             }
          });

    });



// TRACK ORDER ENDS

});





});
