



function setCookie(cname, cvalue, exdays) {

  var d = new Date();

  d.setTime(d.getTime() + (exdays*24*60*60*1000));

  var expires = "expires="+ d.toUTCString();

  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

}



function getCookie(cname) {

  var name = cname + "=";

  var decodedCookie = decodeURIComponent(document.cookie);

  var ca = decodedCookie.split(';');

  for(var i = 0; i <ca.length; i++) {

    var c = ca[i];

    while (c.charAt(0) == ' ') {

      c = c.substring(1);

    }

    if (c.indexOf(name) == 0) {

      return c.substring(name.length, c.length);

    }

  }

  return "";

}





if (getCookie('loaded')) {



}

else

{

        $('.js-preloader').show()



   $('.js-preloader').preloadinator({

    scroll: false,

    minTime: 1500,

    animation: 'fadeOut',

    animationDuration: 400,

});

setCookie('loaded', 'true', 3);



}

$('.owl-carousel.testimonials').owlCarousel({
    loop: true,
    margin: 0,
    // nav:true,
    dots: true,
    animateOut: 'fadeOut',
    autoplay: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})

$('.owl-carousel.blogs-slider').owlCarousel({
    loop: true,
    margin: 20,
    nav: true,
    items: 4,
    center: true,
    autoplay: true,
    dots: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 4
        },
        1000: {
            items: 4
        }
    }
});

$('.owl-carousel.working-slider').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    items: 3,
    center: true,
    autoplay: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});

$('.owl-carousel.review').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    autoplay: true,
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

$(document).ready(function () {
    $(".working-slider .item").mouseover(function () {
        $(".working-detail").removeClass("active");
        $(".working-detail", this).addClass("active");
    });
    $(".working-slider .item").mouseout(function () {
        $(".working-detail", this).removeClass("active");
        $(".working-detail.center").addClass("active");
    });
});

// $(document).on('click', '.next-btn', function(event) {
//     event.preventDefault();
//     if ($('input[type="checkbox"]:checked').val() || $('input[name="bottle"]:checked').val() && $('input[name="duration"]:checked').val() != undefined) {
//         $('form').submit();
//     }
//     else{
//         alert('Please select atleast one item!');
//     }
// });
// $('.js-preloader').preloadinator({
//     scroll: false,
//     minTime: 1500,
//     animation: 'fadeOut',
//     animationDuration: 400,
// });


    $('#gift').change(function() {
    if ($('#chShipAdd').prop('checked')) {
        $('#shipadddiv').show();
    } else {
        $('#shipadddiv').hide();
    }
});

$('.ajaxCart').click(function(e) {
    // alert('test')
    e.preventDefault()
    $.ajax({
        url: $(this).attr('href'),
        type: 'GET',
        success: function(res) {
            alert('Successfully Added')
            // console.log(res);
            // alert(res);
        }
    });
})

$('#gift').change(function() {

    if ($(this).is(':checked')) {

        $('.gift-form').slideDown()

        var html = $('.gift-form').children('.form-group').children('input, textarea');

        html.attr('required',  true);

        }

    else{

        $('.gift-form').slideUp()

       var html = $('.gift-form').children('.form-group').children('input, textarea');

       html.attr('required',  false);



    }

});





$(document).on('click', '.next-btn', function(event) {

    event.preventDefault();

    if ($('input[name="oils"]:checked').val() 

        || $('input[name="flowers[]"]:checked').val() 

        || $('input[name="crystals[]"]:checked').val()        

        || $('input[name="blends[]"]:checked').val() 

        || $('input[name="bottle"]:checked').val() 

        && $('input[name="duration"]:checked').val() 

        != undefined) {

            var to = $('.gift-form').children('.form-group').children('#to');

            var from = $('.gift-form').children('.form-group').children('#from');

            var message = $('.gift-form').children('.form-group').children('#message');

            var html = $('.gift-form').children('.form-group').children('input, textarea');

            var attr = $(html).attr('required');            

            if (typeof attr !== typeof undefined && attr !== false) {

              if (to.val() == '' || from.val() == '' || message.val() == '') {

                alert('Please enter gift details!');

                }

                else{

                     $('form').submit();

                }

            }           

            else{ 

             $('form').submit();

            }

    }

    else{

        alert('Please select atleast one item!');

    }

});


function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}